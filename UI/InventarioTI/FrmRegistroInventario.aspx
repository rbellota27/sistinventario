﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="FrmRegistroInventario.aspx.cs" Inherits="UI.Reporte.FrmRegistroInventario" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary">
        <div class="container-fluid">
            <div class="load">
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h3>Inventario Sanicenter</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <br />
                    <button id="BtnNuevo" type="button" class="btn btn-primary"><b>Nuevo</b> </button>
                    <button id="btnExportar" type="button" class="btn btn-primary"><b>Exportar</b></button>
                </div>
                <div>
                    <iframe id="txtArea1" style="display: none"></iframe>
                </div>
            </div>
            <br />
        </div>
    </div>
    <br />
    <div class="table-responsive">
        <table class="table table-bordred table-striped" id="tabla_Documento_Inventario">
            <thead>
                <tr>
                    <th scope="col">EstadoAsignacion</th>
                    <th scope="col">Opciones</th>
                    <th scope="col">Id</th>
                    <th scope="col">CodInventario</th>
                    <th scope="col">AREA</th>
                    <th scope="col">SEDE</th>
                    <th scope="col">RESPONSABLE</th>
                    <th scope="col">CELULAR</th>
                    <th scope="col">DOMINIO</th>
                    <th scope="col">CARGO</th>
                    <th scope="col">USUARIO</th>
                    <th scope="col">CORREO</th>
                    <th scope="col">HOSTNAME</th>
                    <th scope="col">IP_</th>
                    <th scope="col">IPWIR</th>
                    <th scope="col">MAC_ETHERNET</th>
                    <th scope="col">MAC_WIRELESS</th>
                    <th scope="col">TIPO_CPU</th>
                    <th scope="col">SERIE_CPU</th>
                    <th scope="col">MARCA_CPU</th>
                    <th scope="col">MAINBOARD_CPU</th>
                    <th scope="col">PROCESADOR_CPU</th>
                    <th scope="col">MEMORIA_CPU</th>
                    <th scope="col">DISCO_DURO_CPU</th>
                    <th scope="col">COV_INV_MONIT</th>
                    <th scope="col">MARCA_MONIT</th>
                    <th scope="col">MODELO_MONIT</th>
                    <th scope="col">N_SERIE_MONIT</th>
                    <th scope="col">TECLADO</th>
                    <th scope="col">MOUSE</th>
                    <th scope="col">LICENCIA_WINDOWS</th>
                    <th scope="col">LICENCIA_OFFICE</th>
                    <th scope="col">Obs</th>
                    <th scope="col">FechaUltimaModificacion</th>
                    <th scope="col">TIPO_MEMORIA</th>
                    <th scope="col">TIPO_DISCO</th>
                    <th scope="col">CAPACIDAD</th>
                    <th scope="col">MARCA_DISCO</th>
                </tr>
            </thead>
            <tbody id="tabla_Documento_InventarioBody">
            </tbody>
        </table>
    </div>




    <table class="table table-bordred table-striped" id="tabla_exportar" style="visibility: hidden;">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">CodInventario</th>
                <th scope="col">AREA</th>
                <th scope="col">SEDE</th>
                <th scope="col">RESPONSABLE</th>
                <th scope="col">CELULAR</th>
                <th scope="col">DOMINIO</th>
                <th scope="col">CARGO</th>
                <th scope="col">USUARIO</th>
                <th scope="col">CORREO</th>
                <th scope="col">HOSTNAME</th>
                <th scope="col">IP_</th>
                <th scope="col">IPWIR</th>
                <th scope="col">MAC_ETHERNET</th>
                <th scope="col">MAC_WIRELESS</th>
                <th scope="col">TIPO_CPU</th>
                <th scope="col">SERIE_CPU</th>
                <th scope="col">MARCA_CPU</th>
                <th scope="col">MAINBOARD_CPU</th>
                <th scope="col">PROCESADOR_CPU</th>
                <th scope="col">MEMORIA_CPU</th>
                <th scope="col">DISCO_DURO_CPU</th>
                <th scope="col">COV_INV_MONIT</th>
                <th scope="col">MARCA_MONIT</th>
                <th scope="col">MODELO_MONIT</th>
                <th scope="col">N_SERIE_MONIT</th>
                <th scope="col">TECLADO</th>
                <th scope="col">MOUSE</th>
                <th scope="col">LICENCIA_WINDOWS</th>
                <th scope="col">LICENCIA_OFFICE</th>
                <th scope="col">Obs</th>
                <th scope="col">FechaUltimaModificacion</th>
                <th scope="col">TIPO_MEMORIA</th>
                <th scope="col">TIPO_DISCO</th>
                <th scope="col">CAPACIDAD</th>
                <th scope="col">MARCA_DISCO</th>
            </tr>
        </thead>
        <tbody id="tbody_exportar">
        </tbody>
    </table>


    <div class="modal fade bd-example-modal-xl" id="modal_Registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModal">Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="panel panel-primary">
                                <%--<div class="panel-heading" style="background-color: #3c8dbc">
                                    <b>Imagenes</b>
                                </div>--%>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Datos de Personal</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Codigo de Inventario</label>
                                                                <input class="form-control form-control-sm" id="txtcodinventario" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Sede</label>
                                                                <select class="form-control" id="cbosede"></select>
                                                                <%--<input class="form-control form-control-sm" id="txtsede" type="text" />--%>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Area</label>
                                                                <select class="form-control" id="cboarea"></select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Responsable</label>
                                                                <input class="form-control form-control-sm" id="txtresponsable" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Celular</label>
                                                                <input class="form-control form-control-sm" id="txtcelular" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Dominio</label>
                                                                <input class="form-control form-control-sm" id="txtdominio" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Cargo</label>
                                                                <input class="form-control form-control-sm" id="txtcargo" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Usuario</label>
                                                                <input class="form-control form-control-sm" id="txtusuario" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Correo</label>
                                                                <input class="form-control form-control-sm" id="txtcorreo" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Datos del Equipo</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">Hostname</label>
                                                                <input class="form-control form-control-sm" id="txthostname" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">IP</label>
                                                                <input class="form-control form-control-sm" id="txtip" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">IP WIR</label>
                                                                <input class="form-control form-control-sm" id="txtipwir" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MAC ethernet</label>
                                                                <input class="form-control form-control-sm" id="txtmac" type="text" onblur="fn_ValidarMacE(this);" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MAC WIRELESS</label>
                                                                <input class="form-control form-control-sm" id="txtmacwireless" type="text" onblur="fn_ValidarMacW(this);" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">TIPO</label>
                                                                <%--<input class="form-control form-control-sm" id="txtipo" type="text" />--%>
                                                                <select class="form-control" id="cbotipocpu"></select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">NRO COMPRA</label>
                                                                <input class="form-control form-control-sm" id="txtnrocompra" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">SERIE CPU</label>
                                                                <input class="form-control form-control-sm" id="txtseriecpu" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MARCA CPU</label>
                                                                <input class="form-control form-control-sm" id="txtmarcacpu" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MAINBOARD CPU</label>
                                                                <input class="form-control form-control-sm" id="txtmainboardcpu" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">PROCESADOR CPU</label>
                                                                <input class="form-control form-control-sm" id="txtprocesador" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Datos del Disco</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">DISCO DURO CPU</label>
                                                                <input class="form-control form-control-sm" id="txtdiscoduro" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">CAPACIDAD</label>
                                                                <select class="form-control" id="cbocapacidad">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">TIPO DISCO</label>
                                                                <select class="form-control" id="cbotipodisco">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MARCA DISCO</label>
                                                                <select class="form-control" id="cbomarcadisco">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Datos de Memoria</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label for="formGroupExampleInput">TIPO MEMORIA</label>
                                                                <select class="form-control" id="cbotipomemoria">
                                                                    <option value="0">SELECCIONAR</option>
                                                                    <option value="DDR2">DDR2</option>
                                                                    <option value="DDR3">DDR3</option>
                                                                    <option value="DDR4">DDR4</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label for="formGroupExampleInput">MEMORIA CPU</label>
                                                                <input class="form-control form-control-sm" id="txtmemoria" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Datos de Monitor</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">COV INT MONITOR</label>
                                                                <input class="form-control form-control-sm" id="txtcovint" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MARCA MONITOR</label>
                                                                <input class="form-control form-control-sm" id="txtmarcamonit" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">MODELO MONITOR</label>
                                                                <input class="form-control form-control-sm" id="txtmodelomonit" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">N SERIE MONITOR</label>
                                                                <input class="form-control form-control-sm" id="txtnseriemonit" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Teclado y Mouse</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">TECLADO</label>
                                                                <input class="form-control form-control-sm" id="txtteclado" type="text" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label for="formGroupExampleInput">MOUSE</label>
                                                                <input class="form-control form-control-sm" id="txtmouse" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Otros Datos</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">LICENCIA WINDOWS</label>
                                                                <input class="form-control form-control-sm" id="txtlicenciawin" type="text" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="formGroupExampleInput">LICENCIA OFFICE</label>
                                                                <input class="form-control form-control-sm" id="txtlicenciaoffice" type="text" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlTextarea1">Observaciones</label>
                                                                    <textarea class="form-control" id="txtobservaciones" rows="1"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnguardar">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal_Editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="width: 95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModal1">Editar
                        <label id="lblid" name="lblid"></label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Datos de Personal</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Codigo de Inventario</label>
                                                                    <input class="form-control form-control-sm" id="txtcodinventario1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Sede</label>
                                                                    <select class="form-control" id="cbosede1"></select>
                                                                    <%--<input class="form-control form-control-sm" id="txtsede" type="text" />--%>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Area</label>
                                                                    <select class="form-control" id="cboarea1"></select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Responsable</label>
                                                                    <input class="form-control form-control-sm" id="txtresponsable1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Celular</label>
                                                                    <input class="form-control form-control-sm" id="txtcelular1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Dominio</label>
                                                                    <input class="form-control form-control-sm" id="txtdominio1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Cargo</label>
                                                                    <input class="form-control form-control-sm" id="txtcargo1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Usuario</label>
                                                                    <input class="form-control form-control-sm" id="txtusuario1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Correo</label>
                                                                    <input class="form-control form-control-sm" id="txtcorreo1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Datos del Equipo</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">Hostname</label>
                                                                    <input class="form-control form-control-sm" id="txthostname1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">IP</label>
                                                                    <input class="form-control form-control-sm" id="txtip1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">IP WIR</label>
                                                                    <input class="form-control form-control-sm" id="txtipwir1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MAC ethernet</label>
                                                                    <input class="form-control form-control-sm" id="txtmac1" type="text" onblur="fn_ValidarMacE(this);" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MAC WIRELESS</label>
                                                                    <input class="form-control form-control-sm" id="txtmacwireless1" type="text" onblur="fn_ValidarMacW(this);" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">TIPO</label>
                                                                    <select class="form-control" id="cbotipocpu1"></select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">NRO COMPRA</label>
                                                                    <input class="form-control form-control-sm" id="txtnrocompra1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">SERIE CPU</label>
                                                                    <input class="form-control form-control-sm" id="txtseriecpu1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MARCA CPU</label>
                                                                    <input class="form-control form-control-sm" id="txtmarcacpu1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MAINBOARD CPU</label>
                                                                    <input class="form-control form-control-sm" id="txtmainboardcpu1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">PROCESADOR CPU</label>
                                                                    <input class="form-control form-control-sm" id="txtprocesador1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Datos del Disco</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">DISCO DURO CPU</label>
                                                                    <input class="form-control form-control-sm" id="txtdiscoduro1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">CAPACIDAD</label>
                                                                    <select class="form-control" id="cbocapacidad1">
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">TIPO DISCO</label>
                                                                    <select class="form-control" id="cbotipodisco1">
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MARCA DISCO</label>
                                                                    <select class="form-control" id="cbomarcadisco1">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Datos de Memoria</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <label for="formGroupExampleInput">TIPO MEMORIA</label>
                                                                    <select class="form-control" id="cbotipomemoria1">
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label for="formGroupExampleInput">MEMORIA CPU</label>
                                                                    <input class="form-control form-control-sm" id="txtmemoria1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Datos de Monitor</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">COV INT MONITOR</label>
                                                                    <input class="form-control form-control-sm" id="txtcovint1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MARCA MONITOR</label>
                                                                    <input class="form-control form-control-sm" id="txtmarcamonit1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">MODELO MONITOR</label>
                                                                    <input class="form-control form-control-sm" id="txtmodelomonit1" type="text" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="formGroupExampleInput">N SERIE MONITOR</label>
                                                                    <input class="form-control form-control-sm" id="txtnseriemonit1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Teclado y Mouse</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">TECLADO</label>
                                                                    <input class="form-control form-control-sm" id="txtteclado1" type="text" />
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="formGroupExampleInput">MOUSE</label>
                                                                    <input class="form-control form-control-sm" id="txtmouse1" type="text" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Otros Datos</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-2">
                                                                    <label for="formGroupExampleInput">LICENCIA WINDOWS</label>
                                                                    <input class="form-control form-control-sm" id="txtlicenciawin1" type="text" />
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label for="formGroupExampleInput">LICENCIA OFFICE</label>
                                                                    <input class="form-control form-control-sm" id="txtlicenciaoffice1" type="text" />
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label for="formGroupExampleInput">ESTADO ASIGNACION</label>
                                                                    <select class="form-control" id="cboestadoasignacion">
                                                                        <option value="0">SELECCIONAR</option>
                                                                        <option value="1">ASIGNADO</option>
                                                                        <option value="2">SIN ASIGNAR</option>
                                                                        <option value="3">ELIMINADO</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlTextarea1">Observaciones</label>
                                                                        <textarea class="form-control" id="txtobservaciones1" rows="1"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <h2 style="align-content: center; text-align: center;">HISTORIAL DE CAMBIOS</h2>
                                            </div>

                                            <div style="height: 550px; overflow: auto">
                                                <table class="table table-bordered table-striped mb-0 tableFixHead" id="tablalog">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">USUARIO</th>
                                                            <th scope="col">CAMPO</th>
                                                            <th scope="col">TEXTOANTERIOR</th>
                                                            <th scope="col">TEXTONUEVO</th>
                                                            <th scope="col">FECHA</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbody_tablalog">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--       <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">




                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Codigo de Inventario</label>
                                                <input class="form-control form-control-sm" id="txtcodinventario1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Sede</label>
                                                <select class="form-control" id="cbosede1"></select>                                                
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Area</label>
                                                <select class="form-control" id="cboarea1"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Responsable</label>
                                                <input class="form-control form-control-sm" id="txtresponsable1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Celular</label>
                                                <input class="form-control form-control-sm" id="txtcelular1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Dominio</label>
                                                <input class="form-control form-control-sm" id="txtdominio1" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Cargo</label>
                                                <input class="form-control form-control-sm" id="txtcargo1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Usuario</label>
                                                <input class="form-control form-control-sm" id="txtusuario1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Correo</label>
                                                <input class="form-control form-control-sm" id="txtcorreo1" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">Hostname</label>
                                                <input class="form-control form-control-sm" id="txthostname1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">IP</label>
                                                <input class="form-control form-control-sm" id="txtip1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">IP WIR</label>
                                                <input class="form-control form-control-sm" id="txtipwir1" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MAC ethernet</label>
                                                <input class="form-control form-control-sm" id="txtmac1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MAC WIRELESS</label>
                                                <input class="form-control form-control-sm" id="txtmacwireless1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">TIPO</label>
                                                <input class="form-control form-control-sm" id="txtipo1" type="text" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">SERIE CPU</label>
                                                <input class="form-control form-control-sm" id="txtseriecpu1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MARCA CPU</label>
                                                <input class="form-control form-control-sm" id="txtmarcacpu1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MAINBOARD CPU</label>
                                                <input class="form-control form-control-sm" id="txtmainboardcpu1" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">PROCESADOR CPU</label>
                                                <input class="form-control form-control-sm" id="txtprocesador1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MEMORIA CPU</label>
                                                <input class="form-control form-control-sm" id="txtmemoria1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">DISCO DURO CPU</label>
                                                <input class="form-control form-control-sm" id="txtdiscoduro1" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">COV INT MONITOR</label>
                                                <input class="form-control form-control-sm" id="txtcovint1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MARCA MONITOR</label>
                                                <input class="form-control form-control-sm" id="txtmarcamonit1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">MODELO MONITOR</label>
                                                <input class="form-control form-control-sm" id="txtmodelomonit1" type="text" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">N SERIE MONITOR</label>
                                                <input class="form-control form-control-sm" id="txtnseriemonit1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">TECLADO</label>
                                                <input class="form-control form-control-sm" id="txtteclado1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">txtmouse</label>
                                                <input class="form-control form-control-sm" id="txtmouse1" type="text" />
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">LICENCIA WINDOWS</label>
                                                <input class="form-control form-control-sm" id="txtlicenciawin1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="formGroupExampleInput">LICENCIA OFFICE</label>
                                                <input class="form-control form-control-sm" id="txtlicenciaoffice1" type="text" />
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Observaciones</label>
                                                    <textarea class="form-control" id="txtobservaciones1" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <label for="formGroupExampleInput">TIPO MEMORIA</label>
                                                <select class="form-control" id="cbotipomemoria1">
                                                    <option value="0">SELECCIONAR</option>
                                                    <option value="DDR2">DDR2</option>
                                                    <option value="DDR3">DDR3</option>
                                                    <option value="DDR4">DDR4</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formGroupExampleInput">TIPO DISCO</label>
                                                <select class="form-control" id="cbotipodisco1">
                                                    <option value="0">SELECCIONAR</option>
                                                    <option value="HDD">HDD</option>
                                                    <option value="SSD">SSD</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formGroupExampleInput">CAPACIDAD</label>
                                                <select class="form-control" id="cbocapacidad1">
                                                    <option value="0">SELECCIONAR</option>
                                                    <option value="GB">GB</option>
                                                    <option value="TB">TB</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formGroupExampleInput">MARCA DISCO</label>
                                                <select class="form-control" id="cbomarcadisco1">
                                                    <option value="0">SELECCIONAR</option>
                                                    <option value="WSTERNDIGITAL">WESTERNDIGITAL</option>
                                                    <option value="HP">HP</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formGroupExampleInput">ESTADO ASIGNACION</label>
                                                <select class="form-control" id="cboestadoasignacion">
                                                    <option value="0">SELECCIONAR</option>
                                                    <option value="1">ASIGNADO</option>
                                                    <option value="2">SIN ASIGNAR</option>
                                                    <option value="3">ELIMINADO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnActualizar">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        #tablalog {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #tablalog td, #tablalog th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #tablalog tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #tablalog tr:hover {
                background-color: #ddd;
            }

            #tablalog th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #337ab7;
                color: white;
            }

        .tableFixHead {
            overflow: auto;
            height: 100px;
        }

            .tableFixHead thead th {
                position: sticky;
                top: 0;
                z-index: 1;
            }

        /* Just common table stuff. Really. */
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px 16px;
        }

        th {
            background: #eee;
        }

        /*Lo mostramos 'hidden' por default*/
        .load {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .3 ) url(../img/cargando2.gif) 50% 50% no-repeat;
        }

        /* Cuando el body tiene la clase 'loading' ocultamos la barra de navegacion */
        body.loading {
            overflow: hidden;
        }
            /* Siempre que el body tenga la clase 'loading' mostramos el modal del loading */
            body.loading .load {
                display: block;
            }

        #tabla_ubicacion {
            font-size: 12px;
        }

        #btnnuevo {
            cursor: pointer;
        }

        .editar {
            cursor: pointer;
        }

        .eliminar {
            cursor: pointer;
        }

        .Limpiar {
            cursor: pointer;
        }

        .row {
            margin-top: 2px;
            padding: 0 1px;
        }

        .clickable {
            cursor: pointer;
        }

        .panel-heading span {
            margin-top: -5px;
            font-size: 15px;
        }



        #tabla_hcambios {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

            #tabla_hcambios td, #tabla_hcambios th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #tabla_hcambios tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #tabla_hcambios tr:hover {
                background-color: #337ab7;
            }

            #tabla_hcambios th {
                /*    padding-top: 12px;
                padding-bottom: 12px;*/
                text-align: left;
                background-color: #337ab7;
                color: white;
            }
    </style>




    -<%--<script src="../Archivos/SimpleAjaxUploader.js"></script>--%>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../plugins/JqueryEditTable/jquery.tabledit.js"></script>
    <script src="../plugins/JqueryEditTable/jquery.tabledit.min.js"></script>
    <script src="../Funciones_Js/Js_Reporteria1.js"></script>

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        //$(document).on('click', '.panel-heading span.clickable', function (e) {
        //    var $this = $(this);
        //    if (!$this.hasClass('panel-collapsed')) {
        //        $this.parents('.panel').find('.panel-body').slideUp();
        //        $this.addClass('panel-collapsed');
        //        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        //    } else {
        //        $this.parents('.panel').find('.panel-body').slideDown();
        //        $this.removeClass('panel-collapsed');
        //        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        //    }
        //});



         //<th scope="col">Id</th>
         //       <th scope="col">CodInventario</th>
         //       <th scope="col">AREA</th>
         //       <th scope="col">SEDE</th>
         //       <th scope="col">RESPONSABLE</th>
         //       <th scope="col">CELULAR</th>
         //       <th scope="col">DOMINIO</th>
         //       <th scope="col">CARGO</th>
         //       <th scope="col">USUARIO</th>
         //       <th scope="col">CORREO</th>
         //       <th scope="col">HOSTNAME</th>
         //       <th scope="col">IP_</th>
         //       <th scope="col">IPWIR</th>
         //       <th scope="col">MAC_ETHERNET</th>
         //       <th scope="col">MAC_WIRELESS</th>
         //       <th scope="col">TIPO_CPU</th>
         //       <th scope="col">SERIE_CPU</th>
         //       <th scope="col">MARCA_CPU</th>
         //       <th scope="col">MAINBOARD_CPU</th>
         //       <th scope="col">PROCESADOR_CPU</th>
         //       <th scope="col">MEMORIA_CPU</th>
         //       <th scope="col">DISCO_DURO_CPU</th>
         //       <th scope="col">COV_INV_MONIT</th>
         //       <th scope="col">MARCA_MONIT</th>
         //       <th scope="col">MODELO_MONIT</th>
         //       <th scope="col">N_SERIE_MONIT</th>
         //       <th scope="col">TECLADO</th>
         //       <th scope="col">MOUSE</th>
         //       <th scope="col">LICENCIA_WINDOWS</th>
         //       <th scope="col">LICENCIA_OFFICE</th>
         //       <th scope="col">Obs</th>

    </script>
</asp:Content>
