﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Web.Security;

namespace UI
{
    public partial class Login : System.Web.UI.Page
    {
        public static int IdUsuarioGeneral;
   
        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            Validar();  
          
        }

        private void Validar()
        {
            string Login, Clave;
            int IdPersona = 0;
            int Intentos = 0;
            int IdPerfil = 0;
            string IdTienda = string.Empty;
            Login = txtusuario.Text;
            Clave = txtclave.Text;

            IdPersona = (new BL.BL_Usuario()).ValidarIdentidad(Login, Clave);
            IdPerfil = (new BL.BL_Usuario()).ValidarIdentidadPerfil(Login, Clave);

            if (IdPersona != 0)
            {
                IdTienda = (new BL.BL_Usuario()).ValidarTiendaxUsuario(IdPersona);
                FormsAuthentication.RedirectFromLoginPage(Login, false);
                int IdCaja = (new BL.BL_Usuario()).SelectIdCajaxIdPersona(IdPersona);
                IdUsuarioGeneral = IdPersona;
                Session.Add("id", IdPersona);
                Session.Add("IdUsuario", IdPersona);
                Session.Add("IdCaja", IdCaja);
                Session.Add("IdPerfil", IdPerfil);
                Session.Add("idTiendaUsuario", IdTienda);
                decimal[] listaParametros = (new BL.BL_Usuario()).getValorParametroGeneral(new int[] { 17 });
                Session.Add("ConfigurarDatos", System.Convert.ToDecimal(listaParametros[0]));

                //List<BE.BE_Documento> listaUsuario = new List<BE.BE_Documento>();
                //listaUsuario = (new BL.BL_Documento().Obtener_Datos_Login(Login, Clave));                
                //Session.Add("nombre", listaUsuario[0].NOMBRE);
                //Session.Add("usuario", listaUsuario[0].USUARIO);
                //Session.Add("perfil", listaUsuario[0].PERFIL);
                //Session.Add("idtienda", listaUsuario[0].IDTIENDA);
                //Session.Add("tienda", listaUsuario[0].TIENDA);
                //Session.Add("idempresa", listaUsuario[0].IDEMPRESA);
                //Session.Add("idalmacenprincipal", listaUsuario[0].IDALMACENPRINCIPAL);

                Response.Redirect("~/Principal.aspx");
            }
            else
            {
                BL_Util Util = new BL_Util();
                //if (this.getIntentos() == 2)
                //{
                //    Util.Mensaje("Han terminado el Número de Intentos Permitidos", this);
                //    Util.Salir(this);
                //}
                //else
                //{

                //    Util.Mensaje("Usuario y/o Contraseña incorrectos.", this);
                //    Intentos = this.getIntentos();
                //    Intentos += 1;
                //    this.setIntentos(Intentos);

                //}

                Util.Mensaje("Hubo un Error al ingresar los datos", this);
                Util.Salir(this);
            }

        }

        private int getIntentos()
        {
            return System.Convert.ToInt32(ViewState["Intentos"]);
        }
        private void setIntentos(int Intentos)
        {
            ViewState.Add("Intentos", Intentos);
        }

    }
}