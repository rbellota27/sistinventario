﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="Principal.aspx.cs" Inherits="UI.Principal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .card-counter {
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

            .card-counter:hover {
                box-shadow: 4px 4px 20px #DADADA;
                transition: .3s linear all;
            }

            .card-counter.primary {
                background-color: #007bff;
                color: #FFF;
            }

            .card-counter.danger {
                background-color: #ef5350;
                color: #FFF;
            }

            .card-counter.success {
                background-color: #66bb6a;
                color: #FFF;
            }

            .card-counter.info {
                background-color: #26c6da;
                color: #FFF;
            }

            .card-counter i {
                font-size: 5em;
                opacity: 0.8;
            }

            .card-counter .count-numbers {
                position: absolute;
                right: 35px;
                top: 20px;
                font-size: 32px;
                display: block;
            }

            .card-counter .count-name {
                position: absolute;
                right: 35px;
                top: 65px;
                font-style: italic;
                text-transform: capitalize;                
                display: block;
                font-size: 18px;
            }
    </style>

    <div class="row">
        <div class="col-md-4">
            <div class="card-counter primary">
                <i class="fa fa-desktop"></i>
                <span class="count-numbers">
                    <label id="lblcantidad" name="lblcantidad"></label>
                </span>
                <span class="count-name">Estaciones</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card-counter success">
                <i class="fa fa-users"></i>
                <span class="count-numbers">
                    <label id="lblcantidad1" name="lblcantidad1"></label>
                </span>
                <span class="count-name">Asignadas</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card-counter danger">
                <i class="fa fa-users"></i>
                <span class="count-numbers">
                    <label id="lblcantidad2" name="lblcantidad2"></label>
                </span>
                <span class="count-name">Sin Asignar</span>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-6">
            <canvas id="myChart" style="width: 100%; max-width: 600px"></canvas>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />--%>
    <script>
        $(document).ready(function () {
            CargarCantidadEstaciones();
        });

        function CargarCantidadEstaciones() {

            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Reporteria.asmx/CountCantEstaciones',
                //data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (respuesta) {
                    var Filas = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    debugger;
                    document.getElementById("lblcantidad").innerHTML = Filas[0];
                    document.getElementById("lblcantidad1").innerHTML = Filas[1];
                    document.getElementById("lblcantidad2").innerHTML = Filas[2];
                }
            });
        }

        var xValues = ["Palao 9", "", "", "", ""];
        var yValues = [55, 49, 44, 24, 15];
        var barColors = ["red", "green", "blue", "orange", "brown"];

        new Chart("myChart", {
            type: "bar",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: "Estaciones por tienda"
                }
            }
        });

    </script>
</asp:Content>

