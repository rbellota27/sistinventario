﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="FrmSolicitudExtorno.aspx.cs" Inherits="UI.Ventas.FrmSolicitudExtorno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary">
        <div class="container-fluid">
            <div class="load">
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h3>Registro Solicitud de Extornos</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    Tienda
                     <select class="form-control" id="cboTiendaExtorno">
                         <%--<option value="0"><-Seleccionar-></option>--%>
                     </select>
                </div>
                <div class="col-md-2">
                    Filtro
                     <select class="form-control" id="cboFiltro">
                         <option value="X">Pendiente</option>
                         <option value="Y">Procesados</option>
                         <option value="Z">Todos</option>
                     </select>
                </div>

                <div class="col-md-4">
                    <br />
                    <button id="btnBuscarNuevo" type="button" class="btn btn-primary"><b>Buscar</b> </button>
                    <button id="btnAgregar" type="button" class="btn btn-primary"><b>Agregar</b> </button>
                    <button id="btnExportar" type="button" class="btn btn-primary" ><b>Imprimir</b></button>
                </div>
                <div><iframe id="txtArea1" style="display:none"></iframe></div>


            </div>
            <br />
            <hr />
            <div class="table-responsive">

                <table class="table table-hover  table-striped" id="tabla_Documentos_Ingresados">
                    <thead>
                        <tr style="font-size: 16px;">
                            <td>Id</td>
                            <td>Tipo Documento</td>
                            <td>Numero</td>
                            <td>Fecha Doc.</td>
                            <td>Monto Doc.</td>
                            <td>Tienda</td>
                            <td>Cliente</td>
                            <td>Monto Extorno</td>
                            <td>Fecha Extorno</td>
                            <td>Observacion</td>
                            <td>Usuario</td>
                            <td>Estado</td>
                            <td>Imagen</td>
                            <td>Estados</td>
                        </tr>
                    </thead>

                </table>

            </div>

        </div>
    </div>
    <!-- Modal nuevo -->
    <div class="modal fade" id="modal_nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLongTitle">Registrar Nueva Solicitud</h4>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="background-color: #3c8dbc">
                                    <b>Registro de Solicitud</b>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Tienda
                                            <select class="form-control" id="cboTiendaNuevo">
                                                <%--<option value="0"><-Seleccionar-></option>--%>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <b>Tipo Documento </b>
                                            <select class="form-control" id="idTipDocumento" disabled>
                                                <option value="1101353003">Nota de Credito</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <b>Serie</b>
                                            <select class="form-control" id="idSerieNuevo">
                                                <option value="0">---</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <b>Numero</b>
                                            <input type="text" class="form-control" id="txtNumeroDocNuevo">
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                            <b>Monto</b>
                                            <input type="text" class="form-control" id="txtMonto">
                                        </div>
                                        <div class="col-md-1">
                                        </div>

                                        <div class="col-md-7">
                                            <br />
                                            <div class="form-group">
                                                <input type="file" id="fuUpload1" class="File" accept="image/jpeg,image/png" />

                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <b>Observación</b>
                                            <input type="text" class="form-control" id="TxtObservacion">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <br />
                                            <button id="btnGuardar" type="button" class="btn btn-primary"><b>Guardar</b> </button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal"><b>Cerrar  </b></button>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_Imagen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModal">Imagen Guardada</h4>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="background-color: #3c8dbc">
                                    <b>Imagenes</b>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="form-group" style="text-align: center">

                                            <img id="ImagenMostrar" height='600' width='550'>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                </div>
            </div>
        </div>
    </div>

    <style>
        /*Lo mostramos 'hidden' por default*/
        .load {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .3 ) url(../img/cargando2.gif) 50% 50% no-repeat;
        }

        /* Cuando el body tiene la clase 'loading' ocultamos la barra de navegacion */
        body.loading {
            overflow: hidden;
        }
            /* Siempre que el body tenga la clase 'loading' mostramos el modal del loading */
            body.loading .load {
                display: block;
            }

        #tabla_ubicacion {
            font-size: 12px;
        }

        #btnnuevo {
            cursor: pointer;
        }

        .editar {
            cursor: pointer;
        }

        .eliminar {
            cursor: pointer;
        }

        .Limpiar {
            cursor: pointer;
        }
    </style>

    <%--<script src="../Archivos/SimpleAjaxUploader.js"></script>--%>
   
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../Funciones_Js/Js_Solicitud_Extorno.js"></script>

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>




</asp:Content>
