//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UI.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmpresaTienda
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EmpresaTienda()
        {
            this.EmpresaTiendaUsuario = new HashSet<EmpresaTiendaUsuario>();
        }
    
        public int IdTienda { get; set; }
        public int IdEmpresa { get; set; }
        public string et_CuentaContable { get; set; }
        public string et_CentroCosto { get; set; }
        public string et_Estado { get; set; }
        public string DB_NAME { get; set; }
        public System.Guid rowguid { get; set; }
    
        public virtual Tienda Tienda { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmpresaTiendaUsuario> EmpresaTiendaUsuario { get; set; }
    }
}
