﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using BE;
using BL;


namespace UI.ServicioWeb
{
    /// <summary>
    /// Descripción breve de WS_Contabilidad
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class WS_Contabilidad : System.Web.Services.WebService
    {
        BL_Documento obj_BL_Documento = new BL_Documento();
        BL_Tienda obj_BL_Tienda = new BL_Tienda();

        // Inicio de Sesion
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> Obtener_Datos_Login(string USUARIO, string CLAVE)
        {
            return obj_BL_Documento.Obtener_Datos_Login(USUARIO, CLAVE);
        }
        // Opcion de Menu por Usuario
        [WebMethod(EnableSession = true)]
        public List<BE_Opcion> Obtener_Datos_Menu(int id, string perfil)
        {
            return obj_BL_Documento.Obtener_Datos_Menu(id, perfil);
        }
        //Carga Todas las Tiendas
        [WebMethod(EnableSession = true)]
        public List<BE_Tienda> Obtener_Tienda()
        {
            return obj_BL_Tienda.Obtener_Tienda();
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Tienda> OBTENER_TIENDA_USUARIO(int IdUsuario)
        {
            return obj_BL_Tienda.OBTENER_TIENDA_USUARIO(IdUsuario);
        }        

        //Carga Todas los Tipos Documento
        [WebMethod(EnableSession = true)]
        public List<BE_Tienda> Obtener_TiposDocumento(int ID_TIENDA)
        {
            return obj_BL_Tienda.Obtener_TipoDocumento(ID_TIENDA);
        }
        //Carga serie documento
        [WebMethod(EnableSession = true)]
        public List<BE_Tienda> Obtener_Serie(int ID_TIPODOCUMENTO, int ID_TIENDA)
        {
            return obj_BL_Tienda.Obtener_Serie(ID_TIPODOCUMENTO, ID_TIENDA);
        }
        //
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA(string Doc_Serie, string Doc_Codigo, int TipoDocumento)
        {
            return obj_BL_Documento.LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA(Doc_Serie, Doc_Codigo, TipoDocumento);
        }
        //GUARDAR_REGISTROS_DOCUMENTOS
        [WebMethod(EnableSession = true)]
        public string GUARDAR_REGISTROS_DOCUMENTOS(string Tipo, int IdDocumento, decimal Monto, string Ruta, int IdUsuario)
        {
            return obj_BL_Documento.GUARDAR_REGISTROS_DOCUMENTOS(Tipo, IdDocumento, Monto, Ruta, IdUsuario);
        }
        [WebMethod(EnableSession = true)]
        public string GUARDAR_REGISTROS_SOLICITUDDOCUMENTOS(string Tipo, string doc_serie, string doc_codigo, decimal Monto, string Ruta, int IdUsuario, string Observacion)
        {
            return obj_BL_Documento.GUARDAR_REGISTROS_SOLICITUDDOCUMENTOS(Tipo, doc_serie, doc_codigo, Monto, Ruta, IdUsuario, Observacion);
        }
        [WebMethod(EnableSession = true)]
        public string APROBAR_REGISTROS_SOLICITUDDOCUMENTOS(string Tipo, int iddocumento, int IdUsuario,string Ruta)
        {
            return obj_BL_Documento.APROBAR_REGISTROS_SOLICITUDDOCUMENTOS(Tipo, iddocumento, IdUsuario,Ruta);
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> LISTAR_DOCUMENTOS_INGRESADOS(string Tipo)
        {
            return obj_BL_Documento.LISTAR_DOCUMENTOS_INGRESADOS(Tipo);
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> LISTAR_SOLICITUD_DOCUMENTOS(string Tipo,int Tienda,int IdUsuario)
        {
            return obj_BL_Documento.LISTAR_SOLICITUD_DOCUMENTOS(Tipo, Tienda, IdUsuario);
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> LISTAR_DOCUMENTOS_INGRESADOS_FECHAS(int Tienda, string fechainicial, string fechafinal)
        {
            return obj_BL_Documento.LISTAR_DOCUMENTOS_INGRESADOS_FECHAS(Tienda, fechainicial, fechafinal);
        }
        [WebMethod(EnableSession = true)]
        public string GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_IZIPAY(string Tipo, int IdDocumento, int IdDoc, string CODIGO, string TIPO_DE_MOVIMIENTO, string SERVICIO, string TRANSACCION, DateTime FECHA_DE_TRANSACCION,
          string HORA_DE_TRANSACCION, DateTime FECHA_DE_CIERRE_DE_LOTE, DateTime FECHA_DE_PROCESO, DateTime FECHA_DE_ABONO, string ESTADO, decimal IMPORTE, decimal COMISION, decimal IGV, decimal IMPORTE_NETO,
          decimal ABONO_DEL_LOTE, string NUM_DE_LOTE, string TERMINAL, string NUM_DE_REF_VOUCHER, string MARCA_DE_TARJETA, string NUM_DE_TARJETA, string CODIGO_DE_AUTORIZACION, int CUOTAS, string OBSERVACIONES,
          string MONEDA, string SERIE_TERMINAL, int USUARIO)
        {
            return obj_BL_Documento.GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_IZIPAY(Tipo, IdDocumento, IdDoc, CODIGO, TIPO_DE_MOVIMIENTO, SERVICIO, TRANSACCION, FECHA_DE_TRANSACCION,
            HORA_DE_TRANSACCION, FECHA_DE_CIERRE_DE_LOTE, FECHA_DE_PROCESO, FECHA_DE_ABONO, ESTADO, IMPORTE, COMISION, IGV, IMPORTE_NETO,
            ABONO_DEL_LOTE, NUM_DE_LOTE, TERMINAL, NUM_DE_REF_VOUCHER, MARCA_DE_TARJETA, NUM_DE_TARJETA, CODIGO_DE_AUTORIZACION, CUOTAS, OBSERVACIONES,
            MONEDA, SERIE_TERMINAL, USUARIO);
        }

        [WebMethod(EnableSession = true)]
        public string GUARDAR_REGISTROS_DOCUMENTOS_NIUBIZ(string Tipo, int IdDocumento, int IdDoc, string RUC, string RAZON_SOCIAL,string COD_COMERCIO,string NOMBRE_COMERCIAL,string FECHA_HORA_OPERACION,string FECHA_DEPOSITO,
         string PRODUCTO,string TIPO_OPERACION,string TARJETA,string ORIGEN_TARJETA,string TIPO_TARJETA,string MARCA_TARJETA,string MONEDA,decimal IMPORTE_OPERACION,string ES_DCC,decimal MONTO_DCC,decimal COMISION_TOTAL,decimal COMISION_NIUBIZ,decimal IGV,decimal SUMA_DEPOSITADA,string ESTADO,string ID_OPERACION,string CUENTA_BANCO_PAGADOR,string BANCO_PAGADOR,int USUARIO)
        {
            return obj_BL_Documento.GUARDAR_REGISTROS_DOCUMENTOS_NIUBIZ(Tipo, IdDocumento, IdDoc, RUC, RAZON_SOCIAL, COD_COMERCIO, NOMBRE_COMERCIAL, FECHA_HORA_OPERACION, FECHA_DEPOSITO,
                PRODUCTO, TIPO_OPERACION, TARJETA, ORIGEN_TARJETA, TIPO_TARJETA, MARCA_TARJETA, MONEDA, IMPORTE_OPERACION, ES_DCC, MONTO_DCC, COMISION_TOTAL, COMISION_NIUBIZ, IGV, SUMA_DEPOSITADA,
                ESTADO, ID_OPERACION, CUENTA_BANCO_PAGADOR, BANCO_PAGADOR, USUARIO);
        }

        [WebMethod(EnableSession = true)]
        public string GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_AMEX(string Tipo, int IdDocumento, int IdDoc, string Establecimiento, string Fecha, string Tarjeta, string CODIGO, string Moneda, Decimal Monto_Venta,
        Decimal Comision_Emisior, Decimal Comision_Mercant, Decimal IGV_Comision_Mercant, Decimal Monto_Depositar, string Fecha_Deposito, string Estado,int USUARIO)
        {
            return obj_BL_Documento.GUARDAR_REGISTROS_DOCUMENTOS_AMEX(Tipo, IdDocumento, IdDoc, Establecimiento, Fecha, Tarjeta, CODIGO, Moneda, Monto_Venta,
         Comision_Emisior, Comision_Mercant, IGV_Comision_Mercant, Monto_Depositar, Fecha_Deposito, Estado, USUARIO);
        }

        [WebMethod(EnableSession = true)]
        public string GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_DINERS(string Tipo, int IdDocumento, int IdDoc, string Red, string Codigo, string Nombre, DateTime Fecha, string Moneda, string Recap, string Ticket, string Tarjeta, DateTime Fecha_Consumo, decimal Consumo, decimal Consumo_Comision, decimal Manejo, decimal Comision_Manejo, decimal IGV, decimal Pago_Consumo, string Estado, string Documento_Autorizado
            , DateTime Fecha_Pago, string Banco, string Cuenta, string Numero_Autorizacion, int USUARIO)
        {
            return obj_BL_Documento.GUARDAR_REGISTROS_DOCUMENTOS_DINERS(Tipo, IdDocumento, IdDoc, Red, Codigo, Nombre, Fecha, Moneda, Recap, Ticket, Tarjeta, Fecha_Consumo, Consumo, Consumo_Comision, Manejo, Comision_Manejo, IGV, Pago_Consumo, Estado, Documento_Autorizado
            , Fecha_Pago, Banco, Cuenta, Numero_Autorizacion, USUARIO);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Guardar()
        {

            HttpContext Contexto = HttpContext.Current;
            HttpFileCollection ColeccionArchivos = Context.Request.Files; //'Context.Request.Files;
            String NombreArchivo = "";
            for (int ArchivoActual = 0; ArchivoActual < ColeccionArchivos.Count; ArchivoActual++)
            {
                NombreArchivo = ColeccionArchivos[ArchivoActual].FileName;
                String DatosArchivo = System.IO.Path.GetFileName(ColeccionArchivos[ArchivoActual].FileName);   

                String CarpetaParaGuardar = @"\\192.168.1.15\\Documentos\\" + DatosArchivo;

                ColeccionArchivos[ArchivoActual].SaveAs(CarpetaParaGuardar);



                Context.Response.ContentType = "application/json";
                Context.Response.Write("{\"success\":true,\"msg\":\"" + NombreArchivo + "\"}");
                Context.Response.End();
            }
        }


        [WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Guardar_Finanzas()
        {

            HttpContext Contexto = HttpContext.Current;
            HttpFileCollection ColeccionArchivos = Context.Request.Files; //'Context.Request.Files;
            String NombreArchivo = "";
            for (int ArchivoActual = 0; ArchivoActual < ColeccionArchivos.Count; ArchivoActual++)
            {
                NombreArchivo = ColeccionArchivos[ArchivoActual].FileName;
                String DatosArchivo = System.IO.Path.GetFileName(ColeccionArchivos[ArchivoActual].FileName);
                //String CarpetaParaGuardar = Server.MapPath("Archivos") + "\\" + DatosArchivo;

                String CarpetaParaGuardar = @"\\192.168.1.15\\Documentos_Finanzas\\" + DatosArchivo;
                //string targetPath = Context.Server.MapPath(@"~/images/") ;
                //ColeccionArchivos[ArchivoActual].SaveAs(Replace("~/", ServerHost))

                ColeccionArchivos[ArchivoActual].SaveAs(CarpetaParaGuardar);



                Context.Response.ContentType = "application/json";
                Context.Response.Write("{\"success\":true,\"msg\":\"" + NombreArchivo + "\"}");
                Context.Response.End();
            }
        }


        [WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string MuestraImagen(string Ruta_Archivo)
        {

            string resultado;
            //var Archivo = "21023120402712022.jpg";
            using (Image image = Image.FromFile(@"\\192.168.1.15\Documentos\" + Ruta_Archivo))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);

                    byte[] bytes = m.ToArray();
                    resultado = Convert.ToBase64String(bytes);

                }
            }

            return resultado;
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> OBTENER_TIPO_BANCO()
        {
            return obj_BL_Documento.OBTENER_TIPO_BANCO();

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> LISTAR_DOCUMENTOS_AMEX(int Tipo_Banco,int Tipo_Documento,  string fechainicial, string fechafinal,string serie,string Numero)
        {
            return obj_BL_Documento.LISTAR_DOCUMENTOS_AMEX(Tipo_Banco, Tipo_Documento, fechainicial, fechafinal, serie, Numero);

        }
        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS(string Tipo, string Estado)
        {
            return obj_BL_Documento.LISTAR_DOCUMENTOS_EXTORNOS(Tipo, Estado);
        }

    }
}
