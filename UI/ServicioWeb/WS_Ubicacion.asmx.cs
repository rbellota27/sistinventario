﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BE;
using BL;

namespace UI.ServicioWeb
{
    /// <summary>
    /// Descripción breve de WS_Ubicacion
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
     [System.Web.Script.Services.ScriptService]
    public class WS_Ubicacion : System.Web.Services.WebService
    {
        BL_Ubicacion obj_BL_Ubicacion = new BL_Ubicacion();

        
        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_Zona()
        {
            return obj_BL_Ubicacion.Obtener_Zona();

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_Usuarios()
        {
            return obj_BL_Ubicacion.Obtener_Usuarios();

        }
        

        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_Almacen()
        {
            return obj_BL_Ubicacion.Obtener_Almacen();

        }
        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_Linea()
        {
            return obj_BL_Ubicacion.Obtener_Linea();

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_SubLinea(int IDLINEA)
        {
            return obj_BL_Ubicacion.Obtener_SubLinea(IDLINEA);

        }
        
             [WebMethod(EnableSession = true)]
        public int Guardar_Ubicacion(int IDALMACEN,string NOMBREUBICACION,int IDLINEA,int IDSUBLINEA,int IDZONA)
        {
            return obj_BL_Ubicacion.Guardar_Ubicacion( IDALMACEN,  NOMBREUBICACION,  IDLINEA,  IDSUBLINEA,  IDZONA);

        }
        
                 [WebMethod(EnableSession = true)]
        public int Grabar_Zona_Usuario(int IDUSUARIO, int IDZONA)
        {
            return obj_BL_Ubicacion.Grabar_Zona_Usuario(IDUSUARIO, IDZONA);

        }

        
              [WebMethod(EnableSession = true)]
        public int Actualizar_Zona_Usuario(int ID_ZONA_USUARIO,int IDUSUARIO, int IDZONA,int IDESTADO)
        {
            return obj_BL_Ubicacion.Actualizar_Zona_Usuario(ID_ZONA_USUARIO,IDUSUARIO, IDZONA, IDESTADO);

        }




        [WebMethod(EnableSession = true)]
        public int Actualizar_Ubicacion(int IDUBICACION,int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA, int ESTADO)
        {
            return obj_BL_Ubicacion.Actualizar_Ubicacion(IDUBICACION,IDALMACEN, NOMBREUBICACION, IDLINEA, IDSUBLINEA, IDZONA, ESTADO);

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Listar_Ubicacion(int IDALMACEN,string NOMBREUBICACION,int IDLINEA,int IDSUBLINEA,int IDZONA ,int ESTADO)
        {
            return obj_BL_Ubicacion.Listar_Ubicacion(IDALMACEN, NOMBREUBICACION, IDLINEA, IDSUBLINEA, IDZONA, ESTADO);

        }

        
             [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Listar_Zona_Usuario(int IDUSUARIO, string IDZONA, int ESTADO)
        {
            return obj_BL_Ubicacion.Listar_Zona_Usuario(IDUSUARIO, IDZONA,ESTADO);

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_Ubicacion(int ID_UBICACION)
        {
            return obj_BL_Ubicacion.Obtener_Ubicacion(ID_UBICACION);

        }
        
               [WebMethod(EnableSession = true)]
        public List<BE_Ubicacion> Obtener_Zona_Usuario(int ID_ZONA_USUARIO)
        {
            return obj_BL_Ubicacion.Obtener_Zona_Usuario(ID_ZONA_USUARIO);

        }


        
              [WebMethod(EnableSession = true)]
        public int Eliminar_Zona_Usuario(int ID_ZONA_USUARIO)
        {
            return obj_BL_Ubicacion.Eliminar_Zona_Usuario(ID_ZONA_USUARIO);

        }


        
                  [WebMethod(EnableSession = true)]
        public int Eliminar_Ubicacion(int ID_UBICACION)
        {
            return obj_BL_Ubicacion.Eliminar_Ubicacion(ID_UBICACION);

        }
    }
}
