﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BE;
using BL;

namespace UI.ServicioWeb
{
    /// <summary>
    /// Descripción breve de WS_Despacho
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class WS_Despacho : System.Web.Services.WebService
    {
        BL_Despacho obj_BL_Despacho = new BL_Despacho();


        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Empresa_X_Usuario(int ID_USUARIO)
        {
            return obj_BL_Despacho.Obtener_Combo_Empresa_X_Usuario(ID_USUARIO);
        }
        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Tienda_x_Empresa_x_Usuario(int ID_USUARIO, int ID_EMPRESA)
        {
            return obj_BL_Despacho.Obtener_Combo_Tienda_x_Empresa_x_Usuario(ID_USUARIO, ID_EMPRESA);
        }


        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Serie_x_Empresa_Tiend_TipDoc(int ID_EMPRESA, int ID_TIENDA, int ID_TIPO_DOC)
        {
            return obj_BL_Despacho.Obtener_Combo_Serie_x_Empresa_Tiend_TipDoc(ID_EMPRESA, ID_TIENDA, ID_TIPO_DOC);
        }
        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Estado()
        {
            return obj_BL_Despacho.Obtener_Combo_Estado();
        }
        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_TipoOperacionxIdTpoDocumento(int ID_TIPO_DOCUMENTO)
        {
            return obj_BL_Despacho.Obtener_Combo_TipoOperacionxIdTpoDocumento(ID_TIPO_DOCUMENTO);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Motivo_Traslado(int ID_OPERACION)
        {
            return obj_BL_Despacho.Obtener_Combo_Motivo_Traslado(ID_OPERACION);
        }



        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_MotivoTrasladoxIdTipoOperacion(int ID_TIPO_OPERACION)
        {
            return obj_BL_Despacho.Obtener_Combo_MotivoTrasladoxIdTipoOperacion(ID_TIPO_OPERACION);
        }
        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_AlmacenxIdTienda(int ID_TIENDA)
        {
            return obj_BL_Despacho.Obtener_Combo_AlmacenxIdTienda(ID_TIENDA);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_TipoDocumentoRefxIdTipoDocumento(int ID_TIPO_DOC)
        {
            return obj_BL_Despacho.Obtener_Combo_TipoDocumentoRefxIdTipoDocumento(ID_TIPO_DOC);
        }

        [WebMethod]
        public List<BE_Despacho> listar_DocumentoReferencia(int ID_TIP_DOC_REF, int ID_EMPRESA, int ID_ALMACEN,
            string FEC_INICIO, string FEC_FIN, string SERIE, string CODIGO, int ID_PERSONA
            , int ID_TIPO_DOC, int ID_TIP_OPERACION)
        {
            return obj_BL_Despacho.listar_DocumentoReferencia(ID_TIP_DOC_REF, ID_EMPRESA, ID_ALMACEN,
             FEC_INICIO, FEC_FIN, SERIE, CODIGO, ID_PERSONA
            , ID_TIPO_DOC, ID_TIP_OPERACION);
        }

        [WebMethod]
        public List<BE_Despacho> LLenarCboSeriexIdsEmpTienTipoDoc(int COD_TIENDA, int COD_EMPRESA, int ID_TIPO_DOC)
        {
            return obj_BL_Despacho.LLenarCboSeriexIdsEmpTienTipoDoc(COD_TIENDA, COD_EMPRESA, ID_TIPO_DOC);
        }

        [WebMethod]
        public List<BE_Despacho> llenarCboAlmacenxIdTienda(int COD_TIENDA)
        {
            return obj_BL_Despacho.llenarCboAlmacenxIdTienda(COD_TIENDA);
        }





        [WebMethod]
        public List<BE_Despacho> listar_Destinatario(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            return obj_BL_Despacho.listar_Destinatario(TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL);
        }
        [WebMethod]
        public List<BE_Despacho> obtener_datos_remitente(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            return obj_BL_Despacho.obtener_datos_remitente(TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL);
        }

        [WebMethod]
        public List<BE_Despacho> obtener_datos_agentes(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            return obj_BL_Despacho.obtener_datos_agentes(TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL);
        }
        [WebMethod]
        public List<BE_Despacho> Obtener_datos_transportista_seleccionado(int ID_TRANSPORTISTA)
        {
            return obj_BL_Despacho.Obtener_datos_transportista_seleccionado(ID_TRANSPORTISTA);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_datos_remitente_seleccionado(int ID_REMITENTE)
        {
            return obj_BL_Despacho.Obtener_datos_remitente_seleccionado(ID_REMITENTE);
        }



        [WebMethod]
        public List<BE_Despacho> Obtener_datos_destinatario_seleccionado(int ID_DESTINATARIO)
        {
            return obj_BL_Despacho.Obtener_datos_destinatario_seleccionado(ID_DESTINATARIO);
        }


        [WebMethod]
        public List<BE_Despacho> Obtener_datos_agente_seleccionado(int ID_AGENTE)
        {
            return obj_BL_Despacho.Obtener_datos_agente_seleccionado(ID_AGENTE);
        }
        [WebMethod]
        public List<BE_Despacho> Obtener_datos_chofer_seleccionado(int ID_CHOFER)
        {
            return obj_BL_Despacho.Obtener_datos_chofer_seleccionado(ID_CHOFER);
        }


        [WebMethod]
        public List<BE_Despacho> obtener_vehiculos(string PLACA)
        {
            return obj_BL_Despacho.obtener_vehiculos(PLACA);
        }


        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Rol()
        {
            return obj_BL_Despacho.Obtener_Combo_Rol();
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_TipoAlmacen()
        {
            return obj_BL_Despacho.Obtener_Combo_TipoAlmacen();
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_AlmacenDestinatario_x_Empresa(string COD_ALM_DESTINATARIO, string COD_EMPRESA)
        {
            return obj_BL_Despacho.Obtener_Combo_AlmacenDestinatario_x_Empresa(COD_ALM_DESTINATARIO, COD_EMPRESA);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Departamento()
        {
            return obj_BL_Despacho.Obtener_Combo_Departamento();
        }


        [WebMethod]
        public List<BE_Documento> Obtener_Combo_Tipo_Existencia()
        {
            return obj_BL_Despacho.Obtener_Combo_Tipo_Existencia();
        }

        [WebMethod]
        public List<BE_Documento> Obtener_Combo_Linea_x_idexistencia(int ID_TIPO_EXISTENCIA)
        {
            return obj_BL_Despacho.Obtener_Combo_Linea_x_idexistencia(ID_TIPO_EXISTENCIA);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_sublinea_x_Linea_idexistencia(int ID_TIPO_EXISTENCIA, int ID_LINEA)
        {
            return obj_BL_Despacho.Obtener_Combo_sublinea_x_Linea_idexistencia(ID_TIPO_EXISTENCIA, ID_LINEA);
        }


        [WebMethod]
        public List<BE_Despacho> buscar_Productos(int ID_EXISTENCIA, int ID_LINEA, int ID_SUBLINEA, string CODIGO_SUBLINEA_BUSCAR_PRODUCTO,
            string COD_PRODUCTO, string DESCRIPCION_PRODUCTO, int ID_ALMACEN, int ID_EMPRESA, int PAGE_NUMBER, int PAGE_SIZE)
        {
            return obj_BL_Despacho.buscar_Productos(ID_EXISTENCIA, ID_LINEA, ID_SUBLINEA, CODIGO_SUBLINEA_BUSCAR_PRODUCTO,
                 COD_PRODUCTO, DESCRIPCION_PRODUCTO, ID_ALMACEN, ID_EMPRESA, PAGE_NUMBER, PAGE_SIZE);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Datos_Alm_General(string ID_ALMACEN_GENERAL)
        {
            return obj_BL_Despacho.Obtener_Datos_Alm_General(ID_ALMACEN_GENERAL);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Magnitud_Peso(string ID_MAG_PESO)
        {
            return obj_BL_Despacho.Obtener_Combo_Magnitud_Peso(ID_MAG_PESO);
        }

        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Provincia_x_iddepartamento(string ID_DEPARTAMENTO)
        {
            return obj_BL_Despacho.Obtener_Combo_Provincia_x_iddepartamento(ID_DEPARTAMENTO);
        }


        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Distrito_x_idprovincia(string ID_DEPARTAMENTO, string ID_PROVINCIA)
        {
            return obj_BL_Despacho.Obtener_Combo_Distrito_x_idprovincia(ID_DEPARTAMENTO, ID_PROVINCIA);
        }



        [WebMethod]
        public List<BE_Despacho> cargarPersona(int ID_PERSONA)
        {
            return obj_BL_Despacho.cargarPersona(ID_PERSONA);
        }


        [WebMethod]
        public List<BE_Despacho> Obtener_Datos_Doc_Ref(int ID_DOCUMENTO_REF)
        {
            return obj_BL_Despacho.Obtener_Datos_Doc_Ref(ID_DOCUMENTO_REF);
        }


        [WebMethod]
        public decimal Obtener_Equivalencia(int ID_PRODUCTO, int ID_UM, int VARIABLE, int ID_MAG_PESO, int ID_UM_PESO)
        {
            return obj_BL_Despacho.Obtener_Equivalencia(ID_PRODUCTO, ID_UM, VARIABLE, ID_MAG_PESO, ID_UM_PESO);
        }


        [WebMethod]
        public decimal Obtener_Equivalencia1(int ID_PRODUCTO, int ID_UM, int ID_UM_ST, int VARIABLE)
        {
            return obj_BL_Despacho.Obtener_Equivalencia1(ID_PRODUCTO, ID_UM, ID_UM_ST, VARIABLE);
        }



        [WebMethod]
        public decimal Obtener_Stock_Real(int ID_EMPRESA, int ID_ALMACEN, int ID_PRODUCTO)
        {
            return obj_BL_Despacho.Obtener_Stock_Real(ID_EMPRESA, ID_ALMACEN, ID_PRODUCTO);
        }



        [WebMethod]
        public decimal Obtener_Stock_Comprometido(int ID_EMPRESA, int ID_ALMACEN, int ID_PRODUCTO)
        {
            return obj_BL_Despacho.Obtener_Stock_Comprometido(ID_EMPRESA, ID_ALMACEN, ID_PRODUCTO);
        }




        [WebMethod]
        public List<BE_Despacho> Obtener_Productos_Kit(int ID_PRODUCTO)
        {
            return obj_BL_Despacho.Obtener_Productos_Kit(ID_PRODUCTO);
        }




        [WebMethod]
        public List<BE_Despacho> Obtener_datos_almacen_seleccionado(int ID_ALMACEN)
        {
            return obj_BL_Despacho.Obtener_datos_almacen_seleccionado(ID_ALMACEN);
        }



        [WebMethod]
        public List<BE_Despacho> Obtener_Combo_Almacen_destinatario(int TIPO_ALMACEN, int ID_DESTINATARIO)
        {
            return obj_BL_Despacho.Obtener_Combo_Almacen_destinatario(TIPO_ALMACEN, ID_DESTINATARIO);
        }


        [WebMethod]
        public List<BE_Despacho> listar_Guia_Remision(string FEC_INICIO, string FEC_FIN, string IDTIENDA, string SERIE, string CODIGO)
        {
            return obj_BL_Despacho.listar_Guia_Remision(FEC_INICIO, FEC_FIN, IDTIENDA, SERIE, CODIGO);
        }

        [WebMethod]
        public List<BE_Documento_Referencia> Listar_Documento_Referencia(int idDocumento)
        {
            return obj_BL_Despacho.listar_Documento_Referencia(idDocumento);
        }

        [WebMethod]
        public List<BE_Producto_Final> ListarProductoFinal(int idDocumento, int idUnidadPeso)
        {
            return obj_BL_Despacho.ListarProductoFinal(idDocumento, idUnidadPeso);
        }

        [WebMethod]
        public string INSERTAR_DOCUMENTO(string codigo, string serie, string FechaEmision, string FechaIniTraslado, string FechaAentregar,
          string FechaEntrega, string FechaVenc, decimal ImporteTotal, decimal Descuento, decimal SubTotal, decimal Igv, decimal Total, string TotalLetras,
          decimal TotalAPagar, decimal ValorReferencial, decimal Utilidad, int IdPersona, int IdUsuario, int IdTransportista, int IdRemitente, int IdDestinatario,
          int IdEstadoDoc, int IdCondicionPago, int IdMoneda, int Lugarentrega, int IdTipoOperacion, int IdTienda, int IdSerie, string ExportadoConta, string NroVoucherConta,
          int IdEmpresa, int IdChofer, int IdMotivoT, int IdTipoDoc, int IdVehiculo, int IdEstadoCan, int IdEstadoEnt, int IdAlmacen, int IdTipoPv, int IdCaja, int IdMedioCredito,
          int doc_CompPercepcion, string doc_FechaCancelacion, int IdUsuarioComision, string[][] VALORES, Boolean moverStock, Boolean ValidarDes, string ubigeoPP, string DireccionPP,
          string ubigepPL, string DireccionPL, string observaciones, string[][] DocRef, int almacenDest)
        {
            return obj_BL_Despacho.INSERTAR_DOCUMENTO(codigo, serie, FechaEmision, FechaIniTraslado, FechaAentregar,
             FechaEntrega, FechaVenc, ImporteTotal, Descuento, SubTotal, Igv, Total, TotalLetras,
             TotalAPagar, ValorReferencial, Utilidad, IdPersona, IdUsuario, IdTransportista, IdRemitente, IdDestinatario,
             IdEstadoDoc, IdCondicionPago, IdMoneda, Lugarentrega, IdTipoOperacion, IdTienda, IdSerie, ExportadoConta, NroVoucherConta,
             IdEmpresa, IdChofer, IdMotivoT, IdTipoDoc, IdVehiculo, IdEstadoCan, IdEstadoEnt, IdAlmacen, IdTipoPv, IdCaja, IdMedioCredito,
             doc_CompPercepcion, doc_FechaCancelacion, IdUsuarioComision, VALORES, moverStock, ValidarDes, ubigeoPP, DireccionPP, ubigepPL, DireccionPL,
             observaciones, DocRef, almacenDest);
        }
        [WebMethod]

        public List<BE.BE_Documento> SelectxIdSeriexCodigo(int IdSerie, string codigo)
        {
            return obj_BL_Despacho.SelectxIdSeriexCodigo(IdSerie, codigo);
        }

        [WebMethod]
        public List<BE.BE_Documento_Referencia> GuiaDocRef(int IdDocumento)
        {
            return obj_BL_Despacho.GuiaDocRef(IdDocumento);
        }
        [WebMethod]
        public List<BE.BE_Persona> SelectxIdPersona(int IdDocumento)
        {
            return obj_BL_Despacho.SelectxIdPersona(IdDocumento);
        }
        [WebMethod]
        public List<BE.BE_Despacho> SelectPuntoLlegada(int IdDocumento)
        {
            return obj_BL_Despacho.SelectPuntoLlegada(IdDocumento);
        }
        [WebMethod]
        public List<BE.BE_Documento> Detalle_Guia_Remision(int IdDocumento)
        {
            return obj_BL_Despacho.Detalle_Guia_Remision(IdDocumento);
        }

        [WebMethod]
        public List<BE.BE_Producto_Final> DETALLE_PRODUCTO_GUIA(int IdDocumento)
        {
            return obj_BL_Despacho.DETALLE_PRODUCTO_GUIA(IdDocumento);
        }
        [WebMethod]
        public List<BE.BE_Despacho> OBTENER_VEHICULOSXIDVEHICULO(int IdVehiculo)
        {
            return obj_BL_Despacho.OBTENER_VEHICULOSXIDVEHICULO(IdVehiculo);
        }

        [WebMethod]
        public List<BE.BE_Documento> OBTENER_OBSERVACIONES(int IdDocumento)
        {
            return obj_BL_Despacho.OBTENER_OBSERVACIONES(IdDocumento);
        }

    }
}
