﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using BE;
using BL;

namespace UI.ServicioWeb
{
    /// <summary>
    /// Descripción breve de WS_Reporteria
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class WS_Reporteria : System.Web.Services.WebService
    {
        BL_Documento obj_BL_Documento = new BL_Documento();


        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> OBTENER_ESTADO()
        {
            return obj_BL_Documento.OBTENER_ESTADO();
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS(string Tipo, string Estado)
        {
            return obj_BL_Documento.LISTAR_DOCUMENTOS_EXTORNOS( Tipo, Estado);
        }
        [WebMethod(EnableSession = true)] 
        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS_DETALLE(string Tipo, int IdDocum)
        {
            return obj_BL_Documento.LISTAR_DOCUMENTOS_EXTORNOS_DETALLE(Tipo, IdDocum);
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarComboArea()
        {
            return obj_BL_Documento.ListarComboArea();
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarComboSede()
        {
            return obj_BL_Documento.ListarComboSede();
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarTipoCpu()
        {
            return obj_BL_Documento.ListarTipoCpu();
        }


        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarCapacidad()
        {
            return obj_BL_Documento.ListarCapacidad();
        }


        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarTipoDisco()
        {
            return obj_BL_Documento.ListarTipoDisco();
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarMarcaDisco()
        {
            return obj_BL_Documento.ListarMarcaDisco();
        }


        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarTipoMemoria()
        {
            return obj_BL_Documento.ListarTipoMemoria();
        }
        


        [WebMethod(EnableSession = true)]
        public Boolean GuardarRegistro(string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
             string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
             string licenciaoffice, string obs, string cboarea, string cbosede, string cbotipomemoria, string cbotipodisco, string cbocapacidad, string marcadisco,string nrocompra,string idusuarioregistro)
        {
            return obj_BL_Documento.GuardarRegistro(cod, responsable, cel, dominio, cargo, usuario, correo, hostname, ip, ipwir, mac, macwireles, tipo,
             seriecpu, marcacpu, mainboardcpu, procesador, memoria, discoduro, covint, marcamonit, modelomonit, nseriemonit, teclado, mouse, licencia,
             licenciaoffice, obs, cboarea, cbosede, cbotipomemoria, cbotipodisco, cbocapacidad, marcadisco,nrocompra,idusuarioregistro);
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Reporte> ListarInventario()
        {

            List<BE_Reporte> Listado = new List<BE_Reporte>();
                Listado = obj_BL_Documento.ListarInventario();
            return Listado;
        }

        [WebMethod(EnableSession = true)]
       public BE_Reporte ListarInventarioxid(string iddocumento)
        {
            return obj_BL_Documento.ListarInventarioxid(iddocumento);
        }
        [WebMethod(EnableSession = true)]
        public Boolean ActualizarRegistro(string id, string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
    string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
    string licenciaoffice, string obs, string cboarea, string cbosede, string cbotipomemoria, string cbotipodisco, string cbocapacidad, string marcadisco, string estadoasoignacion,string nrocompra,string idusuarioregistro)
        {
            return obj_BL_Documento.ActualizarRegistro(id, cod, responsable, cel, dominio, cargo, usuario, correo, hostname, ip, ipwir, mac, macwireles, tipo,
             seriecpu, marcacpu, mainboardcpu, procesador, memoria, discoduro, covint, marcamonit, modelomonit, nseriemonit, teclado, mouse, licencia,
             licenciaoffice, obs, cboarea, cbosede, cbotipomemoria, cbotipodisco, cbocapacidad, marcadisco, estadoasoignacion,nrocompra,idusuarioregistro);
        }


        [WebMethod(EnableSession = true)]
        public List<int> CountCantEstaciones()
        {
            return obj_BL_Documento.CountCantEstaciones();
        }

        [WebMethod]
        public List<BE_Reporte> ListarAuditoriaxid(string iddocumento)
        {
            return obj_BL_Documento.ListarAuditoriaxid(iddocumento);
        }

        [WebMethod]
        public Boolean Eliminarxid(string iddocumento)
        {
            return obj_BL_Documento.Eliminarxid(iddocumento);
        }


        [WebMethod]
        public Boolean ValidarMacE(string macE)
        {
            return obj_BL_Documento.ValidarMacE(macE);
        }
        [WebMethod]
        public Boolean ValidarMacW(string macW)
        {
            return obj_BL_Documento.ValidarMacW(macW);
        }
        


        //LISTAR_DOCUMENTOS_EXTORNOS

        //OBTENER_ESTADO
    }
}
