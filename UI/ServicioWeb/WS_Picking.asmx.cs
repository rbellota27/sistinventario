﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BE;
using BL;

namespace UI.ServicioWeb
{
    /// <summary>
    /// Descripción breve de WS_Picking
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
     [System.Web.Script.Services.ScriptService]
    public class WS_Picking : System.Web.Services.WebService
    {

        BL_Documento obj_BL_Documento = new BL_Documento();

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Orden_Pedido_VP(string IdEmpresa,string IdAlmacen,string FecIni,
            string FecFin,string doc_Serie,string doc_Codigo,string IdPersona,string IdDocumento,
            string IdTipoOperacion,string IdTipoDocumento)
        {
            return obj_BL_Documento.listar_Orden_Pedido_VP( IdEmpresa,  IdAlmacen,  FecIni,
             FecFin,  doc_Serie,  doc_Codigo,  IdPersona,  IdDocumento,
             IdTipoOperacion,  IdTipoDocumento);
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Picking(string FecIni, string FecFin)
        {
            return obj_BL_Documento.listar_Picking(FecIni,FecFin);
        }
        
    [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_CheckList_Picking()
        {
            return obj_BL_Documento.listar_CheckList_Picking();
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> Listar_Productos_Asignados(int IDUSUARIO)
        {
            return obj_BL_Documento.Listar_Productos_Asignados(IDUSUARIO);
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Detalle_Orden_Pedido_VP(string ID_PEDIDO, string ID_TIPO_OPERACION)
        {
            return obj_BL_Documento.listar_Detalle_Orden_Pedido_VP(ID_PEDIDO, ID_TIPO_OPERACION);
        }


        
 [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Detalle_Tonos_Orden_Pedido_VP(string ID_PRODUCTO, string ID_ALMACEN,string ID_DOCUMENTO)
        {
            return obj_BL_Documento.listar_Detalle_Tonos_Orden_Pedido_VP( ID_PRODUCTO,  ID_ALMACEN,  ID_DOCUMENTO);
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Detalle_Picking(int ID_DOCUMENTO)
        {
            return obj_BL_Documento.listar_Detalle_Picking(ID_DOCUMENTO);
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Detalle_Picking_Tonos(int ID_DOCUMENTO, int ID_DETALLE_DOCUMENTO)
        {
            return obj_BL_Documento.listar_Detalle_Picking_Tonos(ID_DOCUMENTO, ID_DETALLE_DOCUMENTO);
        }       

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> listar_Detalle_Picking_EnProceso(int ID_DOCUMENTO)
        {
            return obj_BL_Documento.listar_Detalle_Picking_EnProceso(ID_DOCUMENTO);
        }
        [WebMethod(EnableSession = true)]
        public List<BE_Documento> ObtenerObservacion_Orden_Pedido_VP(string ID_PEDIDO)
        {
            return obj_BL_Documento.ObtenerObservacion_Orden_Pedido_VP(ID_PEDIDO);
        }
        


        [WebMethod(EnableSession = true)]
        public string Generar_Picking(string ID_PEDIDO,string ID_SERIE,string NRO_SERIE,string NRO_DOC, int USUARIO)
        {
            return obj_BL_Documento.Generar_Picking(ID_PEDIDO, ID_SERIE, NRO_SERIE, NRO_DOC,USUARIO);
        }

        [WebMethod(EnableSession = true)]
        public string Generar_Picking_Manual(string ID_PEDIDO, string ID_SERIE, string NRO_SERIE, string NRO_DOC, int USUARIO,string[][] VALORES)
        {
            return obj_BL_Documento.Generar_Picking_Manual(ID_PEDIDO, ID_SERIE, NRO_SERIE, NRO_DOC, USUARIO, VALORES);
        }

        


             [WebMethod(EnableSession = true)]
        public int Guardar_Verificacion_Picking(int ID_DOCUMENTO_DETALLE, decimal CANT_PICKEADA, decimal CANT_VERIFICADOR)
        {
            return obj_BL_Documento.Guardar_Verificacion_Picking(ID_DOCUMENTO_DETALLE, CANT_PICKEADA, CANT_VERIFICADOR);
        }


        [WebMethod(EnableSession = true)]
        public int Actualizar_Picking_Productos_Asignados(int ID_DETALLEDOCUMENTO, decimal CANTIDAD_ENCONTRADA,int IDUSUARIO)
        {
            return obj_BL_Documento.Actualizar_Picking_Productos_Asignados(ID_DETALLEDOCUMENTO, CANTIDAD_ENCONTRADA,IDUSUARIO);
        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> Obtener_Serie()
        {
            return obj_BL_Documento.Obtener_Serie();

        }

        
            [WebMethod(EnableSession = true)]
        public List<BE_Documento> Obtener_Consecutivo(string COD_SERIE)
        {
            return obj_BL_Documento.Obtener_Consecutivo(COD_SERIE);

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Documento> Obtener_Datos_Login(string USUARIO, string CLAVE)
        {

            return obj_BL_Documento.Obtener_Datos_Login(USUARIO, CLAVE);

        }

        [WebMethod(EnableSession = true)]
        public List<BE_Opcion> Obtener_Datos_Menu(int id,string perfil)
        {
            return obj_BL_Documento.Obtener_Datos_Menu(id,perfil);
        }

    }
}
