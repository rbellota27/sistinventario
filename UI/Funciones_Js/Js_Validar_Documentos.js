﻿$(document).ready(function () {

    var iddocum, iddoca;
    //var table = document.createElement("table");
    //table.border = "2";



    LISTAR_DOCUMENTOS_INGRESADOS()

    function eliminaFilas() {
        debugger;
        //OBTIENE EL NÚMERO DE FILAS DE LA TABLA
        var n = 0;
        $("#dvExcel tbody tr").each(function () {
            n++;
        });
        //BORRA LAS n-1 FILAS VISIBLES DE LA TABLA
        //LAS BORRA DE LA ULTIMA FILA HASTA LA SEGUNDA
        //DEJANDO LA PRIMERA FILA VISIBLE, MÁS LA FILA PLANTILLA OCULTA
        for (i = n - 1; i >= 0; i--) {
            //for (i = n - 1; i > 1; i--) {
            $("#dvExcel tbody tr:eq('" + i + "')").remove();
        };
    };

    //$('#Tbl_Documentos').dataTable({
    //    "bAutoWidth": false
    //});

    function LISTAR_DOCUMENTOS_INGRESADOS() {


        var tables = $('#Tbl_Documentos').DataTable(), Tienda = "0";
        tables.clear().draw();




        var Tipo = "2";

        var ajax_data = {
            "Tipo": Tipo,
            "Tienda": Tienda,
            "IdUsuario": $.session.get('id')

        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_SOLICITUD_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            //datatable.
            //    fnDestroy = true;


            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#Tbl_Documentos").show();
                for (var i = 0; i < opvp.length; i++) {
                    $('#Tbl_Documentos').dataTable().fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].TipoDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].FechaEmision,
                        opvp[i].doc_ImporteTotal,
                        opvp[i].Tienda,
                        opvp[i].Cliente,
                        opvp[i].doc_Total,
                        opvp[i].doc_FechaRegistro,
                        opvp[i].Observacion,
                        "<img src='../img/ver_doc.png'  class='verimg' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/editar_2.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >"
                        //"<img src='../img/" + opvp[i].Semaforo + "'  class='ver'  data-toggle='tooltip' title='" + opvp[i].EstadoDoc + "' style='height: 20px;width: 20px;text-align: center' >"



                    ]);
                }



                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    $(document).on('click', '.verimg', function (e) {


        var FechaImagen, rutaimg;

        //alert(str.substr(0, str.length - 3));

        FechaImagen = $(this).parents("tr").find("td").eq(8).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(0).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'




        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#imagenNuevo").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


        $('#modal_Imagen').modal();
        //$('#fileUpload').val('');

    });

    $(document).on('click', '.ver', function (e) {

        eliminaFilas();

        iddocum = $(this).parents("tr").find("td").eq(0).html();
        iddoca = "0";//$(this).parents("tr").find("td").eq(1).html()
        $('#modal_nuevo').modal();
        $('#fileUpload').val('');

    });

    $("#upload").click(function (e) {



        var Rut = document.getElementById('fileUpload').files[0].name;
        Rut = Rut.toUpperCase(); //toUpperCase();

        var fileUpload = $("#fileUpload")[0];

        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx|.xlt)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();

                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {


                        if (Rut == "IZIPAY.XLS" || Rut == "IZIPAY.XLSX" || Rut == "IZIPAY.XLT") {
                            ProcessExcel(e.target.result);

                        }
                        if (Rut == "NIUBIZ.XLS" || Rut == "NIUBIZ.XLSX" || Rut == "NIUBIZ.XLT") {
                            ProcessExcel_NIUBIZ(e.target.result);

                        }
                        if (Rut == "AMEX.XLS" || Rut == "AMEX.XLSX" || Rut == "AMEX.XLT") {
                            ProcessExcel_AMEX(e.target.result);

                        }

                        if (Rut == "DINERS.XLS" || Rut == "DINERS.XLSX" || Rut == "DINERS.XLT") {
                            ProcessExcel_DINERS(e.target.result);

                        }



                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {

            swal("Error!", "El archivo ingresado no pertenece al formato establecido", "error");
            return false
        }

    });

    function ProcessExcel(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Create a HTML Table element.
        var table = document.createElement("table");
        table.border = "2";


        //Add the header row.
        var row = table.insertRow(-1);

        //Add the header cells.
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "  CODIGO ";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "  TIPO DE MOVIMIENTO  ";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "  SERVICIO  ";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "TRANSACCION";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "FECHA DE TRANSACCION";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "HORA DE TRANSACCION";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "FECHA DE CIERRE DE LOTE";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "FECHA DE PROCESO";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "FECHA DE ABONO";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "ESTADO";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "IMPORTE";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "COMISION";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "IGV";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "IMPORTE NETO";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "ABONO DEL LOTE";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "NUM DE LOTE";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "TERMINAL";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "NUM DE REF(VOUCHER)";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "MARCA DE TARJETA";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "NUM DE TARJETA";
        row.appendChild(headerCell);


        headerCell = document.createElement("TH");
        headerCell.innerHTML = "CODIGO DE AUTORIZACION";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "  CUOTAS  ";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "OBSERVACIONES";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "MONEDA";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "SERIE TERMINAL";
        row.appendChild(headerCell);


        //Add the data rows from Excel file.
        for (var i = 0; i < excelRows.length; i++) {

            //Add the data row.
            var row = table.insertRow(-1);

            //Add the data cells.
            var cell = row.insertCell(-1);
            if (excelRows[i]["CODIGO"] == null || excelRows[i]["CODIGO"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["CODIGO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["TIPO DE MOVIMIENTO"] == null || excelRows[i]["TIPO DE MOVIMIENTO"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["TIPO DE MOVIMIENTO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["SERVICIO"] == null || excelRows[i]["SERVICIO"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["SERVICIO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["TRANSACCION"] == null || excelRows[i]["TRANSACCION"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["TRANSACCION"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["FECHA DE TRANSACCION"] == null || excelRows[i]["FECHA DE TRANSACCION"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["FECHA DE TRANSACCION"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["HORA DE TRANSACCION"] == null || excelRows[i]["HORA DE TRANSACCION"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["HORA DE TRANSACCION"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["FECHA DE CIERRE DE LOTE"] == null || excelRows[i]["FECHA DE CIERRE DE LOTE"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["FECHA DE CIERRE DE LOTE"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["FECHA DE PROCESO"] == null || excelRows[i]["FECHA DE PROCESO"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["FECHA DE PROCESO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["FECHA DE ABONO"] == null || excelRows[i]["FECHA DE ABONO"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["FECHA DE ABONO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["ESTADO"] == null || excelRows[i]["ESTADO"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["ESTADO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["IMPORTE"] == null || excelRows[i]["IMPORTE"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["IMPORTE"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["COMISION"] == null || excelRows[i]["COMISION"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["COMISION"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["IGV"] == null || excelRows[i]["IGV"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["IGV"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["IMPORTE NETO"] == null || excelRows[i]["IMPORTE NETO"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["IMPORTE NETO"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["ABONO DEL LOTE"] == null || excelRows[i]["ABONO DEL LOTE"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["ABONO DEL LOTE"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["NUM DE LOTE"] == null || excelRows[i]["NUM DE LOTE"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["NUM DE LOTE"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["TERMINAL"] == null || excelRows[i]["TERMINAL"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["TERMINAL"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["NUM DE REF (VOUCHER)"] == null || excelRows[i]["NUM DE REF (VOUCHER)"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["NUM DE REF (VOUCHER)"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["MARCA DE TARJETA"] == null || excelRows[i]["MARCA DE TARJETA"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["MARCA DE TARJETA"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["NUM DE TARJETA"] == null || excelRows[i]["NUM DE TARJETA"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["NUM DE TARJETA"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["CODIGO DE AUTORIZACION"] == null || excelRows[i]["CODIGO DE AUTORIZACION"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["CODIGO DE AUTORIZACION"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["CUOTAS"] == null || excelRows[i]["CUOTAS"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["CUOTAS"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["OBSERVACIONES"] == null || excelRows[i]["OBSERVACIONES"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["OBSERVACIONES"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["MONEDA"] == null || excelRows[i]["MONEDA"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["MONEDA"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["SERIE TERMINAL"] == null || excelRows[i]["SERIE TERMINAL"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["SERIE TERMINAL"];
            }


        }

        var dvExcel = $("#dvExcel");
        dvExcel.html("");
        dvExcel.append(table);

    };

    function ProcessExcel_NIUBIZ(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Create a HTML Table element.
        var table = document.createElement("table");
        table.border = "2";


        //Add the header row.
        var row = table.insertRow(-1);


        //Add the header cells.
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "RUC";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Razón Social";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Cód.Comercio";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Nombre Comercial";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha y Hora de Operación";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha de depósito";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Producto";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Tipo de Operación";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Tarjeta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Origen Tarjeta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Tipo de Tarjeta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Marca de Tarjeta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Moneda";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Importe de Operación";
        row.appendChild(headerCell);


        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Es DCC";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Monto DCC";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Comisión Total";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Comisión Niubiz";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "IGV";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Suma Depositada";
        row.appendChild(headerCell);


        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Estado";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "ID Operación";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Cuenta Banco Pagador";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Banco Pagador";
        row.appendChild(headerCell);

        //Add the data rows from Excel file.
        for (var i = 0; i < excelRows.length; i++) {

            //Add the data row.
            var row = table.insertRow(-1);

            //Add the data cells.
            var cell = row.insertCell(-1);
            if (excelRows[i]["RUC"] == null || excelRows[i]["RUC"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["RUC"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Razón Social"] == null || excelRows[i]["Razón Social"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Razón Social"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Cód. Comercio"] == null || excelRows[i]["Cód. Comercio"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Cód. Comercio"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Nombre Comercial"] == null || excelRows[i]["Nombre Comercial"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Nombre Comercial"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha y Hora de Operación"] == null || excelRows[i]["Fecha y Hora de Operación"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Fecha y Hora de Operación"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha de depósito"] == null || excelRows[i]["Fecha de depósito"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Fecha de depósito"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Producto"] == null || excelRows[i]["Producto"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Producto"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Tipo de Operación"] == null || excelRows[i]["Tipo de Operación"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Tipo de Operación"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Tarjeta"] == null || excelRows[i]["Tarjeta"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Tarjeta"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Origen Tarjeta"] == null || excelRows[i]["Origen Tarjeta"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Origen Tarjeta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Tipo de Tarjeta"] == null || excelRows[i]["Tipo de Tarjeta"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Tipo de Tarjeta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Marca de Tarjeta"] == null || excelRows[i]["Marca de Tarjeta"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Marca de Tarjeta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Moneda"] == null || excelRows[i]["Moneda"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Moneda"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Importe de Operación"] == null || excelRows[i]["Importe de Operación"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["Importe de Operación"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Es DCC"] == null || excelRows[i]["Es DCC"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Es DCC"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Monto DCC"] == null || excelRows[i]["Monto DCC"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["Monto DCC"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Comisión Total"] == null || excelRows[i]["Comisión Total"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["Comisión Total"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Comisión Niubiz"] == null || excelRows[i]["Comisión Niubiz"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["Comisión Niubiz"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["IGV"] == null || excelRows[i]["IGV"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["IGV"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Suma Depositada"] == null || excelRows[i]["Suma Depositada"] == "") {
                cell.innerHTML = 0.00;
            } else {
                cell.innerHTML = excelRows[i]["Suma Depositada"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Estado"] == null || excelRows[i]["Estado"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Estado"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["ID Operación"] == null || excelRows[i]["ID Operación"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["ID Operación"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Cuenta Banco Pagador"] == null || excelRows[i]["Cuenta Banco Pagador"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Cuenta Banco Pagador"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Banco Pagador"] == null || excelRows[i]["Banco Pagador"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Banco Pagador"];
            }




        }



        var dvExcel = $("#dvExcel");
        dvExcel.html("");
        dvExcel.append(table);


        //table.innerHTML = "";

    };

    function ProcessExcel_AMEX(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Create a HTML Table element.
        var table = document.createElement("table");
        table.border = "2";


        //Add the header row.
        var row = table.insertRow(-1);


        //Add the header cells.
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "Código de Establecimiento";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha y Hora de Venta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Tarjeta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Código de Autorización";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Moneda";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Monto de Venta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Comisión Emisor";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Comisión Merchant";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "IGV de Comisión Merchant";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Monto a depositar";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha de Depósito";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Estado";
        row.appendChild(headerCell);





        //Add the data rows from Excel file.
        for (var i = 0; i < excelRows.length; i++) {

            //Add the data row.
            var row = table.insertRow(-1);

            //Add the data cells.
            var cell = row.insertCell(-1);
            if (excelRows[i]["Código de Establecimiento"] == null || excelRows[i]["Código de Establecimiento"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Código de Establecimiento"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha y Hora de Venta"] == null || excelRows[i]["Fecha y Hora de Venta"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Fecha y Hora de Venta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Tarjeta"] == null || excelRows[i]["Tarjeta"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Tarjeta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Código de Autorización"] == null || excelRows[i]["Código de Autorización"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Código de Autorización"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Moneda"] == null || excelRows[i]["Moneda"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Moneda"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Monto de Venta"] == null || excelRows[i]["Monto de Venta"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Monto de Venta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Comisión Emisor"] == null || excelRows[i]["Comisión Emisor"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Comisión Emisor"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Comisión Merchant"] == null || excelRows[i]["Comisión Merchant"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Comisión Merchant"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["IGV de Comisión Merchant"] == null || excelRows[i]["IGV de Comisión Merchant"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["IGV de Comisión Merchant"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Monto a depositar"] == null || excelRows[i]["Monto a depositar"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Monto a depositar"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha de Depósito"] == null || excelRows[i]["Fecha de Depósito"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Fecha de Depósito"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Estado"] == null || excelRows[i]["Estado"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Estado"];
            }

        }



        var dvExcel = $("#dvExcel");
        dvExcel.html("");
        dvExcel.append(table);



    };

    function ProcessExcel_DINERS(data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Create a HTML Table element.
        var table = document.createElement("table");
        table.border = "2";


        //Add the header row.
        var row = table.insertRow(-1);



        //Add the header cells.
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "Red";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Código";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Nombre Comercial";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha Recepción";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Moneda";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Recap";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "#Ticket";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Tarjeta";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha de consumo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Consumo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Comisión del Consumo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Manejo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = " Comisión + Manejo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "IGV";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Pago del consumo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Estado del consumo";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Documento Autorizado";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Fecha de Pago";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Banco";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "Cuenta bancaria / N°Cheque";
        row.appendChild(headerCell);

        headerCell = document.createElement("TH");
        headerCell.innerHTML = "N° Autorización";
        row.appendChild(headerCell);

        //Add the data rows from Excel file.
        for (var i = 0; i < excelRows.length; i++) {

            //Add the data row.
            var row = table.insertRow(-1);

            //Add the data cells.
            var cell = row.insertCell(-1);
            if (excelRows[i]["Red"] == null || excelRows[i]["Red"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Red"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Código"] == null || excelRows[i]["Código"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Código"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Nombre Comercial"] == null || excelRows[i]["Nombre Comercial"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Nombre Comercial"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha Recepción"] == null || excelRows[i]["Fecha Recepción"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Fecha Recepción"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Moneda"] == null || excelRows[i]["Moneda"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Moneda"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Recap"] == null || excelRows[i]["Recap"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Recap"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["#Ticket"] == null || excelRows[i]["#Ticket"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["#Ticket"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Tarjeta"] == null || excelRows[i]["Tarjeta"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Tarjeta"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha de consumo"] == null || excelRows[i]["Fecha de consumo"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Fecha de consumo"];
            }



            cell = row.insertCell(-1);
            if (excelRows[i]["Consumo"] == null || excelRows[i]["Consumo"] == "") {
                cell.innerHTML = 0;
            } else {
                cell.innerHTML = excelRows[i]["Consumo"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Comisión del Consumo"] == null || excelRows[i]["Comisión del Consumo"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Comisión del Consumo"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Manejo"] == null || excelRows[i]["Manejo"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Manejo"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Comisión + Manejo"] == null || excelRows[i]["Comisión + Manejo"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Comisión + Manejo"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["IGV"] == null || excelRows[i]["IGV"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["IGV"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Pago del consumo"] == null || excelRows[i]["Pago del consumo"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Pago del consumo"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Estado del consumo"] == null || excelRows[i]["Estado del consumo"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Estado del consumo"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Documento Autorizado"] == null || excelRows[i]["Documento Autorizado"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Documento Autorizado"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Fecha de Pago"] == null || excelRows[i]["Fecha de Pago"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Fecha de Pago"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Banco"] == null || excelRows[i]["Banco"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Banco"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["Cuenta bancaria / N°Cheque"] == null || excelRows[i]["Cuenta bancaria / N°Cheque"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["Cuenta bancaria / N°Cheque"];
            }

            cell = row.insertCell(-1);
            if (excelRows[i]["N° Autorización"] == null || excelRows[i]["N° Autorización"] == "") {
                cell.innerHTML = "";
            } else {
                cell.innerHTML = excelRows[i]["N° Autorización"];
            }

        }

        var dvExcel = $("#dvExcel");
        dvExcel.html("");
        dvExcel.append(table);

    };


    $("#GuardarPlantilla").click(function (e) {



        var Nombre_Archivo = $('#fileUpload').val();
        var Rut = document.getElementById('fileUpload').files[0].name;
        Rut = Rut.toUpperCase(); //toUpperCase();

        if (Nombre_Archivo == "" || Nombre_Archivo == null || Nombre_Archivo == 0) {
            swal("Error!", "No ha seleccionado ningun archivo", "error");
            return false
        }


        if (Rut == "IZIPAY.XLS" || Rut == "IZIPAY.XLSX" || Rut == "IZIPAY.XLT") {
            Guardar_Documentos_Izipay();
        }
        if (Rut == "NIUBIZ.XLS" || Rut == "NIUBIZ.XLSX" || Rut == "NIUBIZ.XLT") {
            Guardar_DocumentoS_NIUBIZ();
        }
        if (Rut == "AMEX.XLS" || Rut == "AMEX.XLSX" || Rut == "AMEX.XLT") {
            Guardar_Documentos_AMEX();
        }

        if (Rut == "DINERS.XLS" || Rut == "DINERS.XLSX" || Rut == "DINERS.XLT") {
            Guardar_Documentos_DINERS();
        }



    });


    function Guardar_Documentos_Izipay() {
        var IdDocumento
            , IdDoc
            , Fecha_Registro
            , CODIGO
            , TIPO_DE_MOVIMIENTO
            , SERVICIO
            , TRANSACCION
            , FECHA_DE_TRANSACCION
            , HORA_DE_TRANSACCION
            , FECHA_DE_CIERRE_DE_LOTE
            , FECHA_DE_PROCESO
            , FECHA_DE_ABONO
            , ESTADO
            , IMPORTE
            , COMISION
            , IGV
            , IMPORTE_NETO
            , ABONO_DEL_LOTE
            , NUM_DE_LOTE
            , TERMINAL
            , NUM_DE_REF_VOUCHER
            , MARCA_DE_TARJETA
            , NUM_DE_TARJETA
            , CODIGO_DE_AUTORIZACION
            , CUOTAS
            , OBSERVACIONES
            , MONEDA
            , SERIE_TERMINAL
            , USUARIO = $.session.get('id')
            , Tipo = "I";



        var nFilas = $("#dvExcel tr").length;

        if (nFilas == 0) {
            swal("Error!", "No ha seleccionado ningun archivo", "error");
            return false
        }

        if (nFilas > 4) {
            swal("Error!", "La tabla solo debe tener 3 fila como maximo", "error");
            return false
        }



        var MyTabla = $('table#dvExcel').find('tbody').find('tr');
        for (var i = 0; i < MyTabla.length; i++) {
            CODIGO = $(MyTabla[i]).find('td:eq(0)').html();
            TIPO_DE_MOVIMIENTO = $(MyTabla[i]).find('td:eq(1)').html();
            SERVICIO = $(MyTabla[i]).find('td:eq(2)').html();
            TRANSACCION = $(MyTabla[i]).find('td:eq(3)').html();
            FECHA_DE_TRANSACCION = $(MyTabla[i]).find('td:eq(4)').html();
            HORA_DE_TRANSACCION = $(MyTabla[i]).find('td:eq(5)').html();
            FECHA_DE_CIERRE_DE_LOTE = $(MyTabla[i]).find('td:eq(6)').html();
            FECHA_DE_PROCESO = $(MyTabla[i]).find('td:eq(7)').html();
            FECHA_DE_ABONO = $(MyTabla[i]).find('td:eq(8)').html();
            ESTADO = $(MyTabla[i]).find('td:eq(9)').html();
            IMPORTE = $(MyTabla[i]).find('td:eq(10)').html();
            COMISION = $(MyTabla[i]).find('td:eq(11)').html();
            IGV = $(MyTabla[i]).find('td:eq(12)').html();
            IMPORTE_NETO = $(MyTabla[i]).find('td:eq(13)').html();
            ABONO_DEL_LOTE = $(MyTabla[i]).find('td:eq(14)').html();
            NUM_DE_LOTE = $(MyTabla[i]).find('td:eq(15)').html();
            TERMINAL = $(MyTabla[i]).find('td:eq(16)').html();
            NUM_DE_REF_VOUCHER = $(MyTabla[i]).find('td:eq(17)').html();
            MARCA_DE_TARJETA = $(MyTabla[i]).find('td:eq(18)').html();
            NUM_DE_TARJETA = $(MyTabla[i]).find('td:eq(19)').html();
            CODIGO_DE_AUTORIZACION = $(MyTabla[i]).find('td:eq(20)').html();
            CUOTAS = $(MyTabla[i]).find('td:eq(21)').html();
            OBSERVACIONES = $(MyTabla[i]).find('td:eq(22)').html();
            MONEDA = $(MyTabla[i]).find('td:eq(23)').html();
            SERIE_TERMINAL = $(MyTabla[i]).find('td:eq(24)').html();


            if (CODIGO !== undefined) {

                var ajax_data = {
                    "Tipo": Tipo,
                    "IdDocumento": iddocum,
                    "IdDoc": iddoca,
                    "CODIGO": CODIGO,
                    "TIPO_DE_MOVIMIENTO": TIPO_DE_MOVIMIENTO,
                    "SERVICIO": SERVICIO,
                    "TRANSACCION": TRANSACCION,
                    "FECHA_DE_TRANSACCION": FECHA_DE_TRANSACCION,
                    "HORA_DE_TRANSACCION": HORA_DE_TRANSACCION,
                    "FECHA_DE_CIERRE_DE_LOTE": FECHA_DE_CIERRE_DE_LOTE,
                    "FECHA_DE_PROCESO": FECHA_DE_PROCESO,
                    "FECHA_DE_ABONO": FECHA_DE_ABONO,
                    "ESTADO": ESTADO,
                    "IMPORTE": IMPORTE,
                    "COMISION": COMISION,
                    "IGV": IGV,
                    "IMPORTE_NETO": IMPORTE_NETO,
                    "ABONO_DEL_LOTE": ABONO_DEL_LOTE,
                    "NUM_DE_LOTE": NUM_DE_LOTE,
                    "TERMINAL": TERMINAL,
                    "NUM_DE_REF_VOUCHER": NUM_DE_REF_VOUCHER,
                    "MARCA_DE_TARJETA": MARCA_DE_TARJETA,
                    "NUM_DE_TARJETA": NUM_DE_TARJETA,
                    "CODIGO_DE_AUTORIZACION": CODIGO_DE_AUTORIZACION,
                    "CUOTAS": CUOTAS,
                    "OBSERVACIONES": OBSERVACIONES,
                    "MONEDA": MONEDA,
                    "SERIE_TERMINAL": SERIE_TERMINAL,
                    "USUARIO": USUARIO
                };



                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_IZIPAY',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('body').addClass('loading'); //Agregamos la clase loading al body
                    },
                    success: function (resultado) {
                        var num = resultado.d;


                        if (num == "EXITO") {

                            swal("Exito!", "Los datos se registraron ", "success");

                        } else {
                            //Indicador=1
                            swal("Error!", num, "error");
                            return false
                        }


                        $('body').removeClass('loading'); //Removemos la clase loading


                        $("#modal_nuevo .close").click()

                        LISTAR_DOCUMENTOS_INGRESADOS()

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });


            }


        }







    }


    function Guardar_DocumentoS_NIUBIZ() {

        var Tipo = "I", RUC, RAZON_SOCIAL, COD_COMERCIO, NOMBRE_COMERCIAL, FECHA_HORA_OPERACION, FECHA_DEPOSITO,
            PRODUCTO, TIPO_OPERACION, TARJETA, ORIGEN_TARJETA, TIPO_TARJETA, MARCA_TARJETA, MONEDA, IMPORTE_OPERACION, ES_DCC, MONTO_DCC,
            COMISION_TOTAL, COMISION_NIUBIZ, IGV, SUMA_DEPOSITADA,
            ESTADO, ID_OPERACION, CUENTA_BANCO_PAGADOR, BANCO_PAGADOR, USUARIO = $.session.get('id');


        var nFilas = $("#dvExcel tr").length;

        if (nFilas == 0) {
            swal("Error!", "No ha seleccionado ningun archivo", "error");
            return false
        }

        if (nFilas > 4) {
            swal("Error!", "La tabla solo debe tener 3 fila como maximo", "error");
            return false
        }

        //iddocum
        //iddoca


        var MyTabla = $('table#dvExcel').find('tbody').find('tr');
        for (var i = 0; i < MyTabla.length; i++) {
            RUC = $(MyTabla[i]).find('td:eq(0)').html();
            RAZON_SOCIAL = $(MyTabla[i]).find('td:eq(1)').html();
            COD_COMERCIO = $(MyTabla[i]).find('td:eq(2)').html();
            NOMBRE_COMERCIAL = $(MyTabla[i]).find('td:eq(3)').html();
            FECHA_HORA_OPERACION = $(MyTabla[i]).find('td:eq(4)').html();
            FECHA_DEPOSITO = $(MyTabla[i]).find('td:eq(5)').html();
            PRODUCTO = $(MyTabla[i]).find('td:eq(6)').html();
            TIPO_OPERACION = $(MyTabla[i]).find('td:eq(7)').html();
            TARJETA = $(MyTabla[i]).find('td:eq(8)').html();
            ORIGEN_TARJETA = $(MyTabla[i]).find('td:eq(9)').html();
            TIPO_TARJETA = $(MyTabla[i]).find('td:eq(10)').html();
            MARCA_TARJETA = $(MyTabla[i]).find('td:eq(11)').html();
            MONEDA = $(MyTabla[i]).find('td:eq(12)').html();
            IMPORTE_OPERACION = $(MyTabla[i]).find('td:eq(13)').html();
            ES_DCC = $(MyTabla[i]).find('td:eq(14)').html();
            MONTO_DCC = $(MyTabla[i]).find('td:eq(15)').html();
            COMISION_TOTAL = $(MyTabla[i]).find('td:eq(16)').html();
            COMISION_NIUBIZ = $(MyTabla[i]).find('td:eq(17)').html();
            IGV = $(MyTabla[i]).find('td:eq(18)').html();
            SUMA_DEPOSITADA = $(MyTabla[i]).find('td:eq(19)').html();
            ESTADO = $(MyTabla[i]).find('td:eq(20)').html();
            ID_OPERACION = $(MyTabla[i]).find('td:eq(21)').html();
            CUENTA_BANCO_PAGADOR = $(MyTabla[i]).find('td:eq(22)').html();
            BANCO_PAGADOR = $(MyTabla[i]).find('td:eq(23)').html();

            if (RUC !== undefined) {

                var ajax_data = {
                    "Tipo": Tipo,
                    "IdDocumento": iddocum,
                    "IdDoc": iddoca,
                    "RUC": RUC,
                    "RAZON_SOCIAL": RAZON_SOCIAL,
                    "COD_COMERCIO": COD_COMERCIO,
                    "NOMBRE_COMERCIAL": NOMBRE_COMERCIAL,
                    "FECHA_HORA_OPERACION": FECHA_HORA_OPERACION,
                    "FECHA_DEPOSITO": FECHA_DEPOSITO,
                    "PRODUCTO": PRODUCTO,
                    "TIPO_OPERACION": TIPO_OPERACION,
                    "TARJETA": TARJETA,
                    "ORIGEN_TARJETA": ORIGEN_TARJETA,
                    "TIPO_TARJETA": TIPO_TARJETA,
                    "MARCA_TARJETA": MARCA_TARJETA,
                    "MONEDA": MONEDA,
                    "IMPORTE_OPERACION": IMPORTE_OPERACION,
                    "ES_DCC": ES_DCC,
                    "MONTO_DCC": MONTO_DCC,
                    "COMISION_TOTAL": COMISION_TOTAL,
                    "COMISION_NIUBIZ": COMISION_NIUBIZ,
                    "IGV": IGV,
                    "SUMA_DEPOSITADA": SUMA_DEPOSITADA,
                    "ESTADO": ESTADO,
                    "ID_OPERACION": ID_OPERACION,
                    "CUENTA_BANCO_PAGADOR": CUENTA_BANCO_PAGADOR,
                    "BANCO_PAGADOR": BANCO_PAGADOR,
                    "USUARIO": USUARIO
                };



                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_DOCUMENTOS_NIUBIZ',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    //beforeSend: function () {
                    //    $('body').addClass('loading'); //Agregamos la clase loading al body
                    //},
                    success: function (resultado) {
                        var num = resultado.d;


                        if (num == "EXITO") {

                            swal("Exito!", "Los datos se registraron ", "success");

                        } else {
                            //Indicador=1
                            swal("Error!", num, "error");
                            return false
                        }


                        $('body').removeClass('loading'); //Removemos la clase loading


                        $("#modal_nuevo .close").click()

                        LISTAR_DOCUMENTOS_INGRESADOS()

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });


            }

        }

        //var fecha_horas = FECHA_HORA_OPERACION().getFullYear;

    }


    function Guardar_Documentos_AMEX() {

        var Establecimiento
            , Fecha
            , Tarjeta
            , CODIGO
            , Moneda
            , Monto_Venta
            , Comision_Emisior
            , Comision_Mercant
            , IGV_Comision_Mercant
            , Monto_Depositar
            , Fecha_Deposito
            , Estado
            , USUARIO = $.session.get('id')
            , Tipo = "I";



        var nFilas = $("#dvExcel tr").length;

        if (nFilas == 0) {
            swal("Error!", "No ha seleccionado ningun archivo", "error");
            return false
        }


        if (nFilas > 4) {
            swal("Error!", "La tabla solo debe tener 3 fila como maximo", "error");
            return false
        }


        var MyTabla = $('table#dvExcel').find('tbody').find('tr');
        for (var i = 0; i < MyTabla.length; i++) {
            Establecimiento = $(MyTabla[i]).find('td:eq(0)').html();
            Fecha = $(MyTabla[i]).find('td:eq(1)').html();
            Tarjeta = $(MyTabla[i]).find('td:eq(2)').html();
            CODIGO = $(MyTabla[i]).find('td:eq(3)').html();
            Moneda = $(MyTabla[i]).find('td:eq(4)').html();
            Monto_Venta = $(MyTabla[i]).find('td:eq(5)').html();
            Comision_Emisior = $(MyTabla[i]).find('td:eq(6)').html();
            Comision_Mercant = $(MyTabla[i]).find('td:eq(7)').html();
            IGV_Comision_Mercant = $(MyTabla[i]).find('td:eq(8)').html();
            Monto_Depositar = $(MyTabla[i]).find('td:eq(9)').html();
            Fecha_Deposito = $(MyTabla[i]).find('td:eq(10)').html();
            Estado = $(MyTabla[i]).find('td:eq(11)').html();

            if (Establecimiento !== undefined) {

                var ajax_data = {
                    "Tipo": Tipo,
                    "IdDocumento": iddocum,
                    "IdDoc": iddoca,
                    "Establecimiento": Establecimiento,
                    "Fecha": Fecha,
                    "Tarjeta": Tarjeta,
                    "CODIGO": CODIGO,
                    "Moneda": Moneda,
                    "Monto_Venta": Monto_Venta,
                    "Comision_Emisior": Comision_Emisior,
                    "Comision_Mercant": Comision_Mercant,
                    "IGV_Comision_Mercant": IGV_Comision_Mercant,
                    "Monto_Depositar": Monto_Depositar,
                    "Fecha_Deposito": Fecha_Deposito,
                    "Estado": Estado,
                    "USUARIO": USUARIO
                };



                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_AMEX',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    //beforeSend: function () {
                    //    $('body').addClass('loading'); //Agregamos la clase loading al body
                    //},
                    success: function (resultado) {
                        var num = resultado.d;


                        if (num == "EXITO") {

                            swal("Exito!", "Los datos se registraron ", "success");

                        } else {
                            //Indicador=1
                            swal("Error!", num, "error");
                            return false
                        }


                        $('body').removeClass('loading'); //Removemos la clase loading

                        $('#dvExcel > tbody').empty();
                        $("#modal_nuevo .close").click()

                        LISTAR_DOCUMENTOS_INGRESADOS()

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });

            }
            else {



            }
        }











    }


    function Guardar_Documentos_DINERS() {


        var Red
            , Codigo
            , Nombre
            , Fecha
            , Moneda
            , Recap
            , Ticket
            , Tarjeta
            , Fecha_Consumo
            , Consumo
            , Consumo_Comision
            , Manejo
            , Comision_Manejo
            , IGV
            , Pago_Consumo
            , Estado
            , Documento_Autorizado
            , Fecha_Pago
            , Banco
            , Cuenta
            , Numero_Autorizacion
            , USUARIO = $.session.get('id')
            , Tipo = "I";


        var nFilas = $("#dvExcel tr").length;

        if (nFilas == 0) {
            swal("Error!", "No ha seleccionado ningun archivo", "error");
            return false
        }

        if (nFilas > 4) {
            swal("Error!", "La tabla solo debe tener 3 fila como maximo", "error");
            return false
        }

        var MyTabla = $('table#dvExcel').find('tbody').find('tr');
        for (var i = 0; i < MyTabla.length; i++) {
            Red = $(MyTabla[i]).find('td:eq(0)').html();
            Codigo = $(MyTabla[i]).find('td:eq(1)').html();
            Nombre = $(MyTabla[i]).find('td:eq(2)').html();
            Fecha = $(MyTabla[i]).find('td:eq(3)').html();
            Moneda = $(MyTabla[i]).find('td:eq(4)').html();
            Recap = $(MyTabla[i]).find('td:eq(5)').html();
            Ticket = $(MyTabla[i]).find('td:eq(6)').html();
            Tarjeta = $(MyTabla[i]).find('td:eq(7)').html();
            Fecha_Consumo = $(MyTabla[i]).find('td:eq(8)').html();
            Consumo = $(MyTabla[i]).find('td:eq(9)').html();
            Consumo_Comision = $(MyTabla[i]).find('td:eq(10)').html();
            Manejo = $(MyTabla[i]).find('td:eq(11)').html();
            Comision_Manejo = $(MyTabla[i]).find('td:eq(12)').html();
            IGV = $(MyTabla[i]).find('td:eq(13)').html();
            Pago_Consumo = $(MyTabla[i]).find('td:eq(14)').html();
            Estado = $(MyTabla[i]).find('td:eq(15)').html();
            Documento_Autorizado = $(MyTabla[i]).find('td:eq(16)').html();
            Fecha_Pago = $(MyTabla[i]).find('td:eq(17)').html();
            Banco = $(MyTabla[i]).find('td:eq(18)').html();
            Cuenta = $(MyTabla[i]).find('td:eq(19)').html();
            Numero_Autorizacion = $(MyTabla[i]).find('td:eq(20)').html();


            if (Red !== undefined) {
                var ajax_data = {
                    "Tipo": Tipo,
                    "IdDocumento": iddocum,
                    "IdDoc": iddoca,
                    "Red": Red,
                    "Codigo": Codigo,
                    "Nombre": Nombre,
                    "Fecha": Fecha,
                    "Moneda": Moneda,
                    "Recap": Recap,
                    "Ticket": Ticket,
                    "Tarjeta": Tarjeta,
                    "Fecha_Consumo": Fecha_Consumo,
                    "Consumo": Consumo,
                    "Consumo_Comision": Consumo_Comision,
                    "Manejo": Manejo,
                    "Comision_Manejo": Comision_Manejo,
                    "IGV": IGV,
                    "Pago_Consumo": Pago_Consumo,
                    "Estado": Estado,
                    "Documento_Autorizado": Documento_Autorizado,
                    "Fecha_Pago": Fecha_Pago,
                    "Banco": Banco,
                    "Cuenta": Cuenta,
                    "Numero_Autorizacion": Numero_Autorizacion,
                    "USUARIO": USUARIO
                };


                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_DINERS',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    //beforeSend: function () {
                    //    $('body').addClass('loading'); //Agregamos la clase loading al body
                    //},
                    success: function (resultado) {
                        var num = resultado.d;


                        if (num == "EXITO") {

                            swal("Exito!", "Los datos se registraron ", "success");

                        } else {
                            //Indicador=1
                            swal("Error!", num, "error");
                            return false
                        }


                        $('body').removeClass('loading'); //Removemos la clase loading

                        $('#dvExcel > tbody').empty();
                        $("#modal_nuevo .close").click()

                        LISTAR_DOCUMENTOS_INGRESADOS()

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });

            }

        }


    }


});