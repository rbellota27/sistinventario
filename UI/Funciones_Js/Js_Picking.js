﻿$(document).ready(function () {
  



    $("#btnbuscar").click(function (e){

        listarPicking();

    });


    $(function fecha_actual() {
        $.datepicker.setDefaults($.datepicker.regional["es"]);
        var fecha = new Date();
      
        $("#txtfechaini").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date(fecha.getTime() - 24 * 60 * 60 * 1000));

        $("#txtfechafin").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());


    }); 

    function listarPicking() {

        var table = $('#tabla_picking').DataTable();
        table.clear().draw();
        var  FecIni, FecFin;


        FecIni = $('#txtfechaini').val();
        FecFin = $('#txtfechafin').val();

        var ajax_data = {
            "FecIni": FecIni,
            "FecFin": FecFin
          
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Picking',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_picking").show();
                for (var i = 0; i < opvp.length; i++) {


                    $('#tabla_picking').dataTable().fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].Tienda,
                        opvp[i].Almacen,
                        opvp[i].TipoOperacion,
                        opvp[i].EstadoEnt,
                        //opvp[i].EstadoDoc,
                        opvp[i].FechaEmision,
                        opvp[i].Tienda_Pedido,                       
                        "<img src='../img/ver_doc.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >"
                      //"<img src='../img/eliminarnew.png'  class='eliminar' style='height: 30px;width: 30px;text-align: center' >"
                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    $(document).on('click', '.ver', function (e) {
       
       
        var table = $('#tabla_picking_detalle').DataTable();
        table.clear().draw();
        table.destroy();
        var ID_DOCUMENTO;
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();
       
        var ajax_data = {
            "ID_DOCUMENTO": ID_DOCUMENTO
        };

        
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Detalle_Picking',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) == 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;


              

               for (var i = 0; i < detalle.length; i++) {
                   
                   $('#tabla_picking_detalle').dataTable().fnDestroy();
                   $('#tabla_picking_detalle').dataTable({
                       columnDefs: [
                           {
                               "targets": [0, 1],
                               "className": "hide_column"
                           }
                       ]
                   }).fnAddData( 
                    [  
                        detalle[i].IdDetalleDocumento,
                        detalle[i].IdDocumento,
                            detalle[i].CodigoProducto,
                            detalle[i].NroDocumento,
                            detalle[i].NombreProducto,
                            detalle[i].UM,
                            detalle[i].Cantidad,
                            detalle[i].StockReal,
                            detalle[i].StockComprometido,
                            detalle[i].peso_producto,
                            detalle[i].StockDisponible,

                          "<img src='../img/ver_doc.png'  class='VerTonos'  data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >"
                        ]);

                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_ver').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        

      
    });



    $(document).on('click', '.VerTonos', function (e) {

        var table = $('#tabla_tonos').DataTable();
        table.clear().draw();
        table.destroy();
        var ID_DOCUMENTO;
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(1).html();
        ID_DETALLE_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();

        var ajax_data = {
            "ID_DOCUMENTO": ID_DOCUMENTO,
            "ID_DETALLE_DOCUMENTO": ID_DETALLE_DOCUMENTO
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Detalle_Picking_Tonos',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) == 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < detalle.length; i++) {                
                    $('#tabla_tonos').dataTable().fnAddData
                        ([
                            detalle[i].idtono,
                            detalle[i].Tono,
                            detalle[i].cantidadtono
                        ]);

                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_ver_tonos').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

     });
    $(document).on('click', '.picking', function (e) {
        $('#modal_picking').modal(); 
        $('#txtnrodoc').val('');
        $('#ddlserie').val(0);

        var ID_PEDIDO;
        ID_PEDIDO = $(this).parents("tr").find("td").eq(0).html(); 
        $('#txtidnropedido').val(ID_PEDIDO);

    });


 


});