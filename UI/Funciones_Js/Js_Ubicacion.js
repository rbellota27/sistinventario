﻿$(document).ready(function () {
  
    OBTENER_ZONA();
    OBTENER_ALMACEN();
    OBTENER_LINEA();
    
    $("#btnnuevo").click(function (e) {



        $('#btnguardar').show();
        $('#btnactualizar').hide();
        $('#txtidubicacion').val('');
        $('#ddlalmacen').val(0);
        $('#txtubicacion').val('');
        $('#txtidsublinea').val('');
        $('#ddllinea').val(0);
        $('#ddlsublinea').empty().append('<option selected="selected" value="0"><-Seleccionar-></option>');  
        $('#ddlzona').val(0);
        $('#modal_nuevo').modal();




    });


    function OBTENER_ZONA() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Zona',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {


                    $('#ddlzona').append('<option value="' + serie[i].IdZona + '">' + serie[i].DescripcionZona + '</option>');
                    $('#ddlzonabuscar').append('<option value="' + serie[i].IdZona + '">' + serie[i].DescripcionZona + '</option>');

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    function OBTENER_ALMACEN() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Almacen',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {


                    $('#ddlalmacen').append('<option value="' + serie[i].IdAlmacen + '">' + serie[i].DescripcionAlmacen + '</option>');
                    $('#ddlalmacenbuscar').append('<option value="' + serie[i].IdAlmacen + '">' + serie[i].DescripcionAlmacen + '</option>');


                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    function OBTENER_LINEA() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Linea',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {


                    $('#ddllinea').append('<option value="' + serie[i].IdLinea + '">' + serie[i].DescripcionLinea + '</option>');
                    $('#ddllineabuscar').append('<option value="' + serie[i].IdLinea + '">' + serie[i].DescripcionLinea + '</option>');

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    $("#ddllinea").change(function () {
     

        $('#ddlsublinea').empty().append('<option selected="selected" value="0"><-Seleccionar-></option>');  
       

        var idlinea = $('select[id=ddllinea]').val();
        var ajax_data = {

            "IDLINEA": idlinea
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_SubLinea',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {


                    $('#ddlsublinea').append('<option value="' + terceros[i].IdSublinea + '">' + terceros[i].DescripcionSublinea + '</option>');



                }


                var idsublinea;
                idsublinea = $('#txtidsublinea').val();
                if (idsublinea !== '') {

                    $('#ddlsublinea').val(idsublinea);
                }
                else {  $('#ddlsublinea').val(0); }
               
              

                
                $('#txtidsublinea').val('');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });   
    $("#ddllineabuscar").change(function () {

        $('#ddlsublineabuscar').empty().append('<option selected="selected" value="0"><-Seleccionar-></option>');


        var idlinea = $('select[id=ddllineabuscar]').val();
        var ajax_data = {

            "IDLINEA": idlinea
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_SubLinea',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {


                    $('#ddlsublineabuscar').append('<option value="' + terceros[i].IdSublinea + '">' + terceros[i].DescripcionSublinea + '</option>');



                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });  

    $("#btnguardar").click(function (e) {

        var IDALMACEN,NOMBREUBICACION,IDLINEA,IDSUBLINEA,IDZONA ;
        IDALMACEN = $('#ddlalmacen').val();
        NOMBREUBICACION = $('#txtubicacion').val();
        IDLINEA = $('#ddllinea').val();
        IDSUBLINEA = $('#ddlsublinea').val();
        IDZONA = $('#ddlzona').val();

      

        var ajax_data = {
            "IDALMACEN": IDALMACEN,
            "NOMBREUBICACION": NOMBREUBICACION,
            "IDLINEA": IDLINEA,
            "IDSUBLINEA": IDSUBLINEA,
            "IDZONA": IDZONA

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Guardar_Ubicacion',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                if (num > 0) {

                    swal("Exito!", " La Ubicacion se Grabo Satisfactoriamente ", "success");
                    LISTAR_UBICACION();
                }
                else {

                    swal("Error!", " La Ubicacion no se Grabo  ", "error");

                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });





    });
    $("#btnactualizar").click(function (e) {




        var IDUBICACION, IDALMACEN, NOMBREUBICACION, IDLINEA, IDSUBLINEA, IDZONA, ESTADO;
        IDUBICACION = $('#txtidubicacion').val();
        IDALMACEN = $('#ddlalmacen').val();
        NOMBREUBICACION = $('#txtubicacion').val();
        IDLINEA = $('#ddllinea').val();
        IDSUBLINEA = $('#ddlsublinea').val();
        IDZONA = $('#ddlzona').val();
        ESTADO = $('#ddlestadoeditar').val();

        var ajax_data = {
            "IDUBICACION": IDUBICACION,
            "IDALMACEN": IDALMACEN,
            "NOMBREUBICACION": NOMBREUBICACION,
            "IDLINEA": IDLINEA,
            "IDSUBLINEA": IDSUBLINEA,
            "IDZONA": IDZONA,
            "ESTADO": ESTADO

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Actualizar_Ubicacion',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                if (num > 0) {

                    swal("Exito!", " La Ubicacion se Actualizo Satisfactoriamente ", "success");
                    LISTAR_UBICACION();
                }
                else {

                    swal("Error!", " La Ubicacion no se Actualizo  ", "error");

                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });





    });
    function LISTAR_UBICACION() {

        var table = $('#tabla_ubicacion').DataTable();

        table.clear().draw();

        var IDALMACEN, NOMBREUBICACION, IDLINEA, IDSUBLINEA, IDZONA;
        IDALMACEN = $('#ddlalmacenbuscar').val();
        NOMBREUBICACION = $('#txtubicacionbuscar').val();
        IDLINEA = $('#ddllineabuscar').val();
        IDSUBLINEA = $('#ddlsublineabuscar').val();
        IDZONA = $('#ddlzonabuscar').val();
        ESTADO = $('#ddlestadobuscar').val();
        var ajax_data = {
            "IDALMACEN": IDALMACEN,
            "NOMBREUBICACION": NOMBREUBICACION,
            "IDLINEA": IDLINEA,
            "IDSUBLINEA": IDSUBLINEA,
            "IDZONA": IDZONA,
            "ESTADO": ESTADO

        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Listar_Ubicacion',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_ubicacion").show();
                for (var i = 0; i < opvp.length; i++) {


                    $('#tabla_ubicacion').dataTable().fnAddData([
                        opvp[i].IdUbicacion,
                        opvp[i].DescripcionUbicacion,
                        opvp[i].DescripcionAlmacen,
                        opvp[i].DescripcionLinea,
                        opvp[i].DescripcionSublinea,
                        opvp[i].DescripcionZona,
                        opvp[i].DescripcionEstado,

                        "<img src='../img/editar_2.png'  class='editar'  data-toggle='tooltip' title='Editar..!' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/eliminarnew.png'  class='eliminar'  data-toggle='tooltip' title='Eliminar..!' style='height: 20px;width: 20px;text-align: center' >"
                      


                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    $("#btnbuscar").click(function (e) {
        LISTAR_UBICACION();

    });

    $(document).on('click', '.editar', function (e) {

        $('#btnguardar').hide();
        $('#btnactualizar').show();
        
        var ID_UBICACION;
        ID_UBICACION = $(this).parents("tr").find("td").eq(0).html();

        var ajax_data = {
            "ID_UBICACION": ID_UBICACION
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Ubicacion',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < detalle.length; i++) {


                    $('#txtidubicacion').val(detalle[i].IdUbicacion),
                        $('#ddlalmacen').val(detalle[i].IdAlmacen),
                        $('#txtubicacion').val(detalle[i].DescripcionUbicacion),
                        $('#ddllinea').val(detalle[i].IdLinea),

                    $('#ddlzona').val(detalle[i].IdZona),
                        $('#txtidsublinea').val(detalle[i].IdSublinea)
                    $('#ddlestadoeditar').val(detalle[i].Estado)
                }
                $("#ddllinea").change();
               

                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_nuevo').modal();
            },
          
            
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
            

        }) ;

 

        

    });

    $(document).on('click', '.eliminar', function (e) {
        var ID_UBICACION;
        ID_UBICACION = $(this).parents("tr").find("td").eq(0).html();

        swal({
            title: "¿Estás seguro?",
            text: "Se anulara el Registro ",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, anularlo!",
            cancelButtonText: "No, cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {

                    var ajax_data = {
                        "ID_UBICACION": ID_UBICACION
                    };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Ubicacion.asmx/Eliminar_Ubicacion',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {
                            var num = respuesta.d;

                            swal("Anulado!", "El Registro se Anuló.", "success");
                            LISTAR_UBICACION();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });
                } else {
                    swal("Cancelado", "El Registro no se Anuló.", "error");
                }
            });


    });
});