﻿$(document).ready(function () {
    var iddocum;

  
    OBTENER_TIENDA_EXTORNO();
    Muestra_Datos_Ingresado();
 
    //$(function fecha_actual() {
    //    $.datepicker.setDefaults($.datepicker.regional["es"]);


    //    $("#txtFechaInicial").datepicker({
    //        dateFormat: 'dd/mm/yy',
    //        firstDay: 1
    //    }).datepicker("setDate", new Date());

    //    $("#txtFechaFinal").datepicker({
    //        dateFormat: 'dd/mm/yy',
    //        firstDay: 1
    //    }).datepicker("setDate", new Date());

    //});

    function OBTENER_TIENDA_EXTORNO() {


        var ajax_data = {
            "IdUsuario": $.session.get('id')
        };


        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/OBTENER_TIENDA_USUARIO',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {
                    //debugger;

                    $('#cboTiendaExtorno').append('<option value="' + serie[i].IdTienda + '">' + serie[i].Descripcion + '</option>');
                    $('#cboTiendaNuevo').append('<option value="' + serie[i].IdTienda + '">' + serie[i].Descripcion + '</option>');
                    
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    $("#cboTiendaNuevo").change(function () {

        $('#idSerieNuevo').empty().append('<option selected="selected" value="0">---</option>');

        var ID_TIPODOCUMENTO = $('select[id=idTipDocumento]').val();
        var ID_TIENDA = $('select[id=cboTiendaNuevo]').val();

        var ajax_data = {
            "ID_TIPODOCUMENTO": ID_TIPODOCUMENTO,
            "ID_TIENDA": ID_TIENDA
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_Serie',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {
                    $('#idSerieNuevo').append('<option value="' + terceros[i].IdSerie + '">' + terceros[i].NombreSerie + '</option>');
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });

    $("#btnAgregar").click(function (e) {



        $('#txtNumeroDocNuevo').val('');
        $('#cboTiendaNuevo').val(0);
        $('#txtMonto').val('');
        $('#TxtObservacion').val('');
        $('#fuUpload1').val('');
        $('#idSerieNuevo').empty().append('<option selected="selected" value="0">---</option>');

      
        $('#modal_nuevo').modal();

    });


    $("#txtNumeroDocNuevo").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    function forceNumber(element) {
        element
            .data("oldValue", '')
            .bind("paste", function (e) {
                var validNumber = /^[-]?\d+(\.\d{1,3})?$/;
                element.data('oldValue', element.val())
                setTimeout(function () {
                    if (!validNumber.test(element.val()))
                        element.val(element.data('oldValue'));
                }, 0);
            });
        element
            .keypress(function (event) {
                var text = $(this).val();
                if ((event.which != 46 || text.indexOf('.') != -1) && //if the keypress is not a . or there is already a decimal point
                    ((event.which < 48 || event.which > 57) && //and you try to enter something that isn't a number
                        (event.which != 45 || (element[0].selectionStart != 0 || text.indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
                        (event.which != 0 && event.which != 8))) { //and the keypress is not a backspace or arrow key (in FF)
                    event.preventDefault(); //cancel the keypress
                }

                if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 3) && //if there is a decimal point, and there are more than two digits after the decimal point
                    ((element[0].selectionStart - element[0].selectionEnd) == 0) && //and no part of the input is selected
                    (element[0].selectionStart >= element.val().length - 3) && //and the cursor is to the right of the decimal point
                    (event.which != 45 || (element[0].selectionStart != 0 || text.indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
                    (event.which != 0 && event.which != 8)) { //and the keypress is not a backspace or arrow key (in FF)
                    event.preventDefault(); //cancel the keypress
                }
            });
    }

    forceNumber($("#txtMonto"));

    $("#btnGuardar").click(function (e) {


        var Tipo, IDUSUARIO, Monto, ID_DOCUMENTO, RUTA, Doc_Serie, Doc_Codigo, IdTipoDocumento, idTienda,Observacion;


        IdTipoDocumento = $('select[id=idTipDocumento]').val();
        Doc_Serie = document.getElementById('idSerieNuevo').value;//$('#idSerieNuevo').val();
        Doc_Codigo = $('#txtNumeroDocNuevo').val();
        idTienda = $('select[id=cboTiendaNuevo]').val();
        Monto = $('#txtMonto').val();
        IDUSUARIO = $.session.get('id');
        Tipo = "I";
        RUTA = $('#fuUpload1').val();
        Observacion = $('#TxtObservacion').val();
        

        if (idTienda == "" || idTienda == 0 || idTienda == null) {
            swal("Error", "No selecciono la tienda", "error");
            return false
        }
        if (IdTipoDocumento == "" || IdTipoDocumento == 0 || IdTipoDocumento == null) {
            swal("Error", "No selecciono el Tipo de documento", "error");
            return false
        }

        if (Doc_Serie == "" || Doc_Serie == 0 || Doc_Serie == null) {
            swal("Error", "No selecciono la serie del documento", "error");
            return false
        }

        if (Doc_Codigo == "" || Doc_Codigo == 0 || Doc_Codigo == null) {
            swal("Error", "No ingreso el numero del documento", "error");
            return false
        }


        if (Monto == "" || Monto == 0 || Monto == null) {
            swal("Error", "Debe ingresar el monto", "error");
            return false
        }

        if (RUTA == "" || RUTA == 0 || RUTA == null) {
            swal("Error", "Debe ingresar la ruta", "error");
            return false
        }

    
        var ajax_data = {
            "Tipo": Tipo,
            "doc_serie": Doc_Serie,
            "doc_codigo": Doc_Codigo,
            "Monto": Monto,
            "Ruta": RUTA,
            "IdUsuario": IDUSUARIO,
            "Observacion": Observacion
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_SOLICITUDDOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                iddocum = resultado.d;

                if (num.length == 10) {
                    Guardar_Archivo();

                    var tables = $('#tabla_Documentos_Ingresados').DataTable();
                    tables.clear().draw();

                    Muestra_Datos_Ingresado();
                    swal("Exito!", "Los datos se registraron ", "success");
                    //return false
                } else {
                    //Indicador=1
                    swal("Error!", num, "error");
                    return false
                }


                $('body').removeClass('loading'); //Removemos la clase loading
                $("#modal_nuevo .close").click()
             



            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

        function Guardar_Archivo() {

            var f = new Date();
            var dia = f.getDate();
            var mes = (f.getMonth() + 1);
            var Ano = f.getFullYear()

            var Fech = dia + "" + mes + "" + Ano

            var formData = new FormData();
            for (var i = 0; i < $(".File").length; i++) {
                formData.append($(".File")[i].files[0].name, $(".File")[i].files[0], iddocum + Fech + ".jpg");
            }
            $.ajax({
                url: '../ServicioWeb/WS_Contabilidad.asmx/Guardar',
                type: 'POST',
                data: formData,
                //data: JSON.stringify(),
                //data: JSON.stringify(ajax_data),
                cache: false,
                contentType: false,
                processData: false,
                success: function () {
                    //Cargar_Archivo();
                    //swal("Exito!", "Los datos se registraron", "success");
                }, error: function () {
                    //alert(r.d);
                }
            });
        }

    
    });

    function Muestra_Datos_Ingresado() {


        var Tipo = $('select[id=cboFiltro]').val();
        var Tienda = $('select[id=cboTiendaExtorno]').val();

        if (Tienda === null || Tienda === undefined) {
            Tienda = "9";
        }

        var ajax_data = {
            "Tipo": Tipo,
            "Tienda": Tienda,
            "IdUsuario": $.session.get('id')
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_SOLICITUD_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos_Ingresados").show();
                for (var i = 0; i < Filas.length; i++) {
                    $('#tabla_Documentos_Ingresados').dataTable().fnAddData([
                        Filas[i].IdDocumento,
                        Filas[i].TipoDocumento,
                        Filas[i].NroDocumento,
                        Filas[i].FechaEmision,
                        Filas[i].doc_ImporteTotal,
                        Filas[i].Tienda,
                        Filas[i].Cliente,
                        Filas[i].doc_Total,
                        Filas[i].doc_FechaRegistro,
                        Filas[i].Observacion,
                        Filas[i].NOMBRE,
                        "<img src='../img/" + Filas[i].Semaforo + "'  class='VerTonos'  data-toggle='tooltip' title='" + Filas[i].EstadoDoc + "' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >",
                        Filas[i].EstadoDoc
                    ]);
                }

                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    }

    $(document).on('click', '.ver', function (e) {

        //$("#rfcestatus").empty();


        $("#ImagenMostrar").attr('src', '');

        //$("#ImagenMostrar").empty();

        var FechaImagen="", rutaimg="";

        FechaImagen = $(this).parents("tr").find("td").eq(8).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(0).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'

        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#ImagenMostrar").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                return false;
                //alert(error.Message);
            }
        });


        $('#modal_Imagen').modal();

    });

    $("#btnBuscarNuevo").click(function (e) {

        var table = $('#tabla_Documentos_Ingresados').DataTable();
        table.clear().draw();

        Muestra_Datos_Ingresado();

    });

    $("#btnExportar").click(function (e) {
        fnExcelReport();
    });

    $('#tabla_Documentos_Ingresados').dataTable({
        "bAutoWidth": false
    });

    function fnExcelReport() {
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('tabla_Documentos_Ingresados'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

});