﻿$(document).ready(function () {


    ListarInventario();
    CargaComboEditar();
    // Obtener_Estados();
    // Muestra_Datos_Ingresado();

});


function CargaComboEditar() {


    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarComboArea',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].idarea + "'>" + obj[i].ar_Nombrelargo + "</option>";
            }
            $("#cboarea1").append(htmlTags);
        }
    });


    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarComboSede',
        //   data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].idTienda + "'>" + obj[i].tie_nombre + "</option>";
            }
            $("#cbosede1").append(htmlTags);

        }
    });




    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarTipoCpu',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_tipocpu + "'>" + obj[i].vc_tipocpu + "</option>";
            }
            $("#cbotipocpu1").append(htmlTags);
        }
    });


    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarCapacidad',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";

            htmlTags += "<option value = '0'>SELECCIONAR</option>";

            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_capacidad_disco + "'>" + obj[i].vc_capacidad_disco + "</option>";
            }
            $("#cbocapacidad1").append(htmlTags);
        }
    });




    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarTipoDisco',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_tipo_disco + "'>" + obj[i].vc_tipo_disco + "</option>";
            }
            $("#cbotipodisco1").append(htmlTags);
        }
    });





    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarMarcaDisco',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_marcadisco + "'>" + obj[i].vc_marcadisco + "</option>";
            }
            $("#cbomarcadisco1").append(htmlTags);
        }
    });



    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarTipoMemoria',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_tipomemoria + "'>" + obj[i].vc_tipomemoria + "</option>";
            }
            $("#cbotipomemoria1").append(htmlTags);
        }
    });



};


function Obtener_Estados() {

    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/OBTENER_ESTADO',
        data: JSON.stringify(),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,

        success: function (respuesta) {
            var serie = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;

            for (var i = 0; i < serie.length; i++) {

                $('#idEstado').append('<option value="' + serie[i].Campo1 + '">' + serie[i].Campo2 + '</option>');

            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var error = eval("(" + XMLHttpRequest.responseText + ")");
            alert(error.Message);
        }
    });
}


function Muestra_Datos_Ingresado() {

    var table = $('#tabla_Documentos_Aprobar').DataTable({ retrieve: true, paging: false });
    table.clear().draw();
    table.destroy();


    var Tipo = "3";
    var Estado = "X";

    var ajax_data = {
        "Tipo": Tipo,
        "Estado": Estado
    };
    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/LISTAR_DOCUMENTOS_EXTORNOS',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,


        success: function (respuesta) {
            var Filas = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;
            $("#tabla_Documentos_Aprobar").show();
            for (var i = 0; i < Filas.length; i++) {
                $('#tabla_Documentos_Aprobar').dataTable().fnDestroy();
                $('#tabla_Documentos_Aprobar').dataTable().fnAddData([
                    Filas[i].Campo1,
                    Filas[i].Campo2,
                    Filas[i].Campo3,
                    Filas[i].Campo4,
                    Filas[i].Campo5,
                    Filas[i].Campo6,
                    Filas[i].Campo7,
                    Filas[i].Campo8,
                    Filas[i].Campo9,
                    Filas[i].Campo10,
                    Filas[i].Campo11,
                    Filas[i].Campo12,
                    Filas[i].Campo13,
                    Filas[i].Campo14,
                    Filas[i].Campo15,
                    Filas[i].Campo16,
                    Filas[i].Campo17,
                    Filas[i].Campo18,
                    Filas[i].Campo19,
                    Filas[i].Campo20,
                    "<img src='../img/report.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >",
                    "<img src='../img/ver_doc.png'  class='VerDetalle' style='height: 20px;width: 20px;text-align: center' >",
                    Filas[i].Campo21,
                    Filas[i].Campo22

                ]);
            }

            $('body').removeClass('loading'); //Removemos la clase loading

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var error = eval("(" + XMLHttpRequest.responseText + ")");
            alert(error.Message);
        }
    });


}

$("#btnExportar").click(function (e) {
    fnExcelReport();
});
$("#btnguardar").click(function (e) {
    GuardarRegistro();
});

$("#btnActualizar").click(function (e) {
    ActualizarRsegistro();
});

$("#BtnNuevo").click(function (e) {
    //  $('#modal_Registro').show(); 
    LimpiarRegistro();
    $('#modal_Registro').modal();
    CargarCombo();
});


function LimpiarRegistro() {
    document.getElementById("txtcodinventario").value = "";
    document.getElementById("txtresponsable").value = "";
    document.getElementById("txtcelular").value = "";
    document.getElementById("txtdominio").value = "";
    document.getElementById("txtcargo").value = "";
    document.getElementById("txtusuario").value = "";
    document.getElementById("txtcorreo").value = "";
    document.getElementById("txthostname").value = "";
    document.getElementById("txtip").value = "";
    document.getElementById("txtipwir").value = "";
    document.getElementById("txtmac").value = "";
    document.getElementById("txtmacwireless").value = "";
    //document.getElementById("txtipo").value = "";
    document.getElementById("txtseriecpu").value = "";
    document.getElementById("txtmarcacpu").value = "";
    document.getElementById("txtmainboardcpu").value = "";
    document.getElementById("txtprocesador").value = "";
    document.getElementById("txtmemoria").value = "";
    document.getElementById("txtdiscoduro").value = "";
    document.getElementById("txtcovint").value = "";
    document.getElementById("txtmarcamonit").value = "";
    document.getElementById("txtmodelomonit").value = "";
    document.getElementById("txtnseriemonit").value = "";
    document.getElementById("txtteclado").value = "";
    document.getElementById("txtmouse").value = "";
    document.getElementById("txtlicenciawin").value = "";
    document.getElementById("txtlicenciaoffice").value = "";
    document.getElementById("txtobservaciones").value = "";
    document.getElementById("cboarea").value = "1";
    document.getElementById("cbosede").value = "1";


    document.getElementById("cbotipomemoria").value = "0";
    document.getElementById("cbotipodisco").value = "0";
    document.getElementById("cbocapacidad").value = "0";

    document.getElementById("cbomarcadisco").value = "0";
}


function CargarCombo() {
    $("#cboarea").empty();
    $("#cbosede").empty();

    $("#cbotipocpu").empty();
    $("#cbocapacidad").empty();
    $("#cbotipodisco").empty();
    $("#cbomarcadisco").empty();
    $("#cbotipomemoria").empty();


    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarTipoCpu',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_tipocpu + "'>" + obj[i].vc_tipocpu + "</option>";
            }
            $("#cbotipocpu").append(htmlTags);
        }
    });


    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarCapacidad',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";

            htmlTags += "<option value = '0'>SELECCIONAR</option>";

            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_capacidad_disco + "'>" + obj[i].vc_capacidad_disco + "</option>";
            }
            $("#cbocapacidad").append(htmlTags);
        }
    });




    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarTipoDisco',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_tipo_disco + "'>" + obj[i].vc_tipo_disco + "</option>";
            }
            $("#cbotipodisco").append(htmlTags);
        }
    });





    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarMarcaDisco',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_marcadisco + "'>" + obj[i].vc_marcadisco + "</option>";
            }
            $("#cbomarcadisco").append(htmlTags);
        }
    });



    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarTipoMemoria',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            htmlTags += "<option value = '0'>SELECCIONAR</option>";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].vc_tipomemoria + "'>" + obj[i].vc_tipomemoria + "</option>";
            }
            $("#cbotipomemoria").append(htmlTags);
        }
    });
    




    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarComboArea',
        //  data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].idarea + "'>" + obj[i].ar_Nombrelargo + "</option>";
            }
            $("#cboarea").append(htmlTags);
        }
    });


    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarComboSede',
        //   data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var obj = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') : respuesta.d;
            var htmlTags = "";
            for (var i = 0; i < obj.length; i++) {
                htmlTags += "<option value='" + obj[i].idTienda + "'>" + obj[i].tie_nombre + "</option>";
            }
            $("#cbosede").append(htmlTags);

        }
    });

}





function ActualizarRsegistro() {
    var id = document.getElementById("lblid").value;
    var cod = document.getElementById("txtcodinventario1").value;
    var responsable = document.getElementById("txtresponsable1").value;
    var cel = document.getElementById("txtcelular1").value;
    var dominio = document.getElementById("txtdominio1").value;
    var cargo = document.getElementById("txtcargo1").value;
    var usuario = document.getElementById("txtusuario1").value;
    var correo = document.getElementById("txtcorreo1").value;
    var hostname = document.getElementById("txthostname1").value;
    var ip = document.getElementById("txtip1").value;
    var ipwir = document.getElementById("txtipwir1").value;
    var mac = document.getElementById("txtmac1").value;
    var macwireles = document.getElementById("txtmacwireless1").value;
    var tipo = document.getElementById("cbotipocpu").value;
    var seriecpu = document.getElementById("txtseriecpu1").value;
    var marcacpu = document.getElementById("txtmarcacpu1").value;
    var mainboardcpu = document.getElementById("txtmainboardcpu1").value;
    var procesador = document.getElementById("txtprocesador1").value;
    var memoria = document.getElementById("txtmemoria1").value;
    var discoduro = document.getElementById("txtdiscoduro1").value;
    var covint = document.getElementById("txtcovint1").value;
    var marcamonit = document.getElementById("txtmarcamonit1").value;
    var modelomonit = document.getElementById("txtmodelomonit1").value;
    var nseriemonit = document.getElementById("txtnseriemonit1").value;
    var teclado = document.getElementById("txtteclado1").value;
    var mouse = document.getElementById("txtmouse1").value;
    var licencia = document.getElementById("txtlicenciawin1").value;
    var licenciaoffice = document.getElementById("txtlicenciaoffice1").value;
    var obs = document.getElementById("txtobservaciones1").value;

    var cboarea = document.getElementById("cboarea1").value;
    var cbosede = document.getElementById("cbosede1").value;


    var cbotipomemoria = document.getElementById("cbotipomemoria1").value;
    var cbotipodisco = document.getElementById("cbotipodisco1").value;
    var cbocapacidad = document.getElementById("cbocapacidad1").value;
    var cbomarcadisco = document.getElementById("cbomarcadisco1").value;

    var estadoasoignacion = document.getElementById("cboestadoasignacion").value;

    var nrocompra = document.getElementById("txtnrocompra").value;



    var idusuarioregistro = $.session.get('id');

    var ajax_data = {
        "id": id,
        "cod": cod,
        "responsable": responsable,
        "cel": cel,
        "dominio": dominio,
        "cargo": cargo,
        "usuario": usuario,
        "correo": correo,
        "hostname": hostname,
        "ip": ip,
        "ipwir": ipwir,
        "mac": mac,
        "macwireles": macwireles,
        "tipo": tipo,
        "seriecpu": seriecpu,
        "marcacpu": marcacpu,
        "mainboardcpu": mainboardcpu,
        "procesador": procesador,
        "memoria": memoria,
        "discoduro": discoduro,
        "covint": covint,
        "marcamonit": marcamonit,
        "modelomonit": modelomonit,
        "nseriemonit": nseriemonit,
        "teclado": teclado,
        "mouse": mouse,
        "licencia": licencia,
        "licenciaoffice": licenciaoffice,
        "obs": obs,
        "cboarea": cboarea,
        "cbosede": cbosede,
        "cbotipomemoria": cbotipomemoria,
        "cbotipodisco": cbotipodisco,
        "cbocapacidad": cbocapacidad,
        "marcadisco": cbomarcadisco,
        "estadoasoignacion": estadoasoignacion,
        "nrocompra": nrocompra,
        "idusuarioregistro":idusuarioregistro
    };

    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ActualizarRegistro',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            alert("Actualizado Correctamente");
            $('#modal_Editar').modal('hide');
            ListarInventario();
        }
    });
}

function GuardarRegistro() {
    var cod = document.getElementById("txtcodinventario").value;
    var responsable = document.getElementById("txtresponsable").value;
    var cel = document.getElementById("txtcelular").value;
    var dominio = document.getElementById("txtdominio").value;
    var cargo = document.getElementById("txtcargo").value;
    var usuario = document.getElementById("txtusuario").value;
    var correo = document.getElementById("txtcorreo").value;
    var hostname = document.getElementById("txthostname").value;
    var ip = document.getElementById("txtip").value;
    var ipwir = document.getElementById("txtipwir").value;
    var mac = document.getElementById("txtmac").value;
    var macwireles = document.getElementById("txtmacwireless").value;
    var tipo = document.getElementById("cbotipocpu").value;
    var seriecpu = document.getElementById("txtseriecpu").value;
    var marcacpu = document.getElementById("txtmarcacpu").value;
    var mainboardcpu = document.getElementById("txtmainboardcpu").value;
    var procesador = document.getElementById("txtprocesador").value;
    var memoria = document.getElementById("txtmemoria").value;
    var discoduro = document.getElementById("txtdiscoduro").value;
    var covint = document.getElementById("txtcovint").value;
    var marcamonit = document.getElementById("txtmarcamonit").value;
    var modelomonit = document.getElementById("txtmodelomonit").value;
    var nseriemonit = document.getElementById("txtnseriemonit").value;
    var teclado = document.getElementById("txtteclado").value;
    var mouse = document.getElementById("txtmouse").value;
    var licencia = document.getElementById("txtlicenciawin").value;
    var licenciaoffice = document.getElementById("txtlicenciaoffice").value;
    var obs = document.getElementById("txtobservaciones").value;

    var cboarea = document.getElementById("cboarea").value;
    var cbosede = document.getElementById("cbosede").value;

    var cbotipomemoria = document.getElementById("cbotipomemoria").value;
    var cbotipodisco = document.getElementById("cbotipodisco").value;
    var cbocapacidad = document.getElementById("cbocapacidad").value;
    var cbomarcadisco = document.getElementById("cbomarcadisco").value;

    var nrocompra = document.getElementById("txtnrocompra").value;


    var idusuarioregistro = $.session.get('id');

    var ajax_data = {
        "cod": cod,
        "responsable": responsable,
        "cel": cel,
        "dominio": dominio,
        "cargo": cargo,
        "usuario": usuario,
        "correo": correo,
        "hostname": hostname,
        "ip": ip,
        "ipwir": ipwir,
        "mac": mac,
        "macwireles": macwireles,
        "tipo": tipo,
        "seriecpu": seriecpu,
        "marcacpu": marcacpu,
        "mainboardcpu": mainboardcpu,
        "procesador": procesador,
        "memoria": memoria,
        "discoduro": discoduro,
        "covint": covint,
        "marcamonit": marcamonit,
        "modelomonit": modelomonit,
        "nseriemonit": nseriemonit,
        "teclado": teclado,
        "mouse": mouse,
        "licencia": licencia,
        "licenciaoffice": licenciaoffice,
        "obs": obs,
        "cboarea": cboarea,
        "cbosede": cbosede,
        "cbotipomemoria": cbotipomemoria,
        "cbotipodisco": cbotipodisco,
        "cbocapacidad": cbocapacidad,
        "marcadisco": cbomarcadisco,
        "nrocompra": nrocompra,
        "idusuarioregistro" : idusuarioregistro
    };

    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/GuardarRegistro',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            alert("Registrado Correctamente");
            $('#modal_Registro').modal('hide');
            ListarInventario();
        }
    });
}


function ListarInventario() {
    $('#tabla_Documento_Inventario').dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarInventario',
        //   data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var Filas = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;
            var contenido = "";
            //    $("#tabla_Documentos_Aprobar").show();
            for (var i = 0; i < Filas.length; i++) {
                contenido +=
                    "<tr style='text-align:center;'><td>"
                if (Filas[i].Campo32 == 1) {
                    contenido += "<img src='../img/Verde.png' style='width: 20px;'/>"
                } else {
                    contenido += "<img src='../img/Rojo.png' style='width: 20px;''/>"
                }
                contenido += "</td><td><img src='../img/editar_2.png'  class='editar_2' style='height: 20px;width: 20px;text-align: center' /><img src='../img/eliminarnew.png' class='eliminar_2' style='height: 20px;width: 20px;text-align: center' /></td>" +
                    "<td>" + Filas[i].Campo1 + "</td>" +
                    "<td>" + Filas[i].Campo2 + "</td>" +
                    "<td>" + Filas[i].Campo3 + "</td>" +
                    "<td>" + Filas[i].Campo4 + "</td>" +
                    "<td>" + Filas[i].Campo5 + "</td>" +
                    "<td>" + Filas[i].Campo6 + "</td>" +
                    "<td>" + Filas[i].Campo7 + "</td>" +
                    "<td>" + Filas[i].Campo8 + "</td>" +
                    "<td>" + Filas[i].Campo9 + "</td>" +
                    "<td>" + Filas[i].Campo10 + "</td>" +
                    "<td>" + Filas[i].Campo11 + "</td>" +
                    "<td>" + Filas[i].Campo12 + "</td>" +
                    "<td>" + Filas[i].Campo13 + "</td>" +
                    "<td>" + Filas[i].Campo14 + "</td>" +
                    "<td>" + Filas[i].Campo15 + "</td>" +
                    "<td>" + Filas[i].Campo16 + "</td>" +
                    "<td>" + Filas[i].Campo17 + "</td>" +
                    "<td>" + Filas[i].Campo18 + "</td>" +
                    "<td>" + Filas[i].Campo19 + "</td>" +
                    "<td>" + Filas[i].Campo20 + "</td>" +
                    "<td>" + Filas[i].Campo21 + "</td>" +
                    "<td>" + Filas[i].Campo22 + "</td>" +
                    "<td>" + Filas[i].Campo23 + "</td>" +
                    "<td>" + Filas[i].Campo24 + "</td>" +
                    "<td>" + Filas[i].Campo25 + "</td>" +
                    "<td>" + Filas[i].Campo26 + "</td>" +
                    "<td>" + Filas[i].Campo27 + "</td>" +
                    "<td>" + Filas[i].Campo28 + "</td>" +
                    "<td>" + Filas[i].Campo29 + "</td>" +
                    "<td>" + Filas[i].Campo30 + "</td>" +
                    "<td>" + Filas[i].Campo31 + "</td>" +
                    "<td>" + Filas[i].Campo33 + "</td>" +
                    "<td>" + Filas[i].Campo34 + "</td>" +
                    "<td>" + Filas[i].Campo35 + "</td>" +
                    "<td>" + Filas[i].Campo36 + "</td>" +
                    "<td>" + Filas[i].Campo37 + "</td>"
                    + "</tr>";
            }
            $('#tabla_Documento_InventarioBody').empty().html(contenido);
            $('body').removeClass('loading'); //Removemos la clase loading
            $('#tabla_Documento_Inventario').dataTable();
        },
        error: function (response) {
            if (response.length != 0)
                alert(response);
        }


    });
}

$(document).on('click', '.eliminar_2', function (e) {
    var iddocumento = $(this).parents("tr").find("td").eq(2).html();

    var ajax_data = {
        "iddocumento": iddocumento
    };

    if (confirm("¿Estas seguro de eliminar el registro?")) {
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/Eliminarxid',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                alert('Registro eliminado');

            }
        });
        ListarInventario();
    }
});

$(document).on('click', '.editar_2', function (e) {

    var iddocumento = $(this).parents("tr").find("td").eq(2).html();

    var ajax_data = {
        "iddocumento": iddocumento
    };


    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarInventarioxid',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var Filas = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;
            //    $("#tabla_Documentos_Aprobar").show();
            $('#modal_Editar').modal();


            document.getElementById("lblid").value = Filas.Campo1;
            document.getElementById("txtcodinventario1").value = Filas.Campo2;
            //var e = document.getElementById("cbosede1");
            //var e1 = document.getElementById("cboarea1");

            //var strUser = e.options[e.selectedIndex].text;
            document.getElementById("cbosede1").value = Filas.Campo4;
            document.getElementById("cboarea1").value = Filas.Campo3;
            document.getElementById("txtresponsable1").value = Filas.Campo5;
            document.getElementById("txtcelular1").value = Filas.Campo6;
            document.getElementById("txtdominio1").value = Filas.Campo7;
            document.getElementById("txtcargo1").value = Filas.Campo8;
            document.getElementById("txtusuario1").value = Filas.Campo9;
            document.getElementById("txtcorreo1").value = Filas.Campo10;
            document.getElementById("txthostname1").value = Filas.Campo11;
            document.getElementById("txtip1").value = Filas.Campo12;
            document.getElementById("txtipwir1").value = Filas.Campo13;
            document.getElementById("txtmac1").value = Filas.Campo14;
            document.getElementById("txtmacwireless1").value = Filas.Campo15;
            document.getElementById("cbotipocpu").value = Filas.Campo16;
            document.getElementById("txtseriecpu1").value = Filas.Campo17;
            document.getElementById("txtmarcacpu1").value = Filas.Campo18;
            document.getElementById("txtmainboardcpu1").value = Filas.Campo19;
            document.getElementById("txtprocesador1").value = Filas.Campo20;
            document.getElementById("txtmemoria1").value = Filas.Campo21;
            document.getElementById("txtdiscoduro1").value = Filas.Campo22;
            document.getElementById("txtcovint1").value = Filas.Campo23;
            document.getElementById("txtmarcamonit1").value = Filas.Campo24;
            document.getElementById("txtmodelomonit1").value = Filas.Campo25;
            document.getElementById("txtnseriemonit1").value = Filas.Campo26;
            document.getElementById("txtteclado1").value = Filas.Campo27;
            document.getElementById("txtmouse1").value = Filas.Campo28;
            document.getElementById("txtlicenciawin1").value = Filas.Campo29;
            document.getElementById("txtlicenciaoffice1").value = Filas.Campo30;
            document.getElementById("txtobservaciones1").value = Filas.Campo31;
            document.getElementById("cbotipomemoria1").value = Filas.Campo32;
            document.getElementById("cbotipodisco").value = Filas.Campo33;
            document.getElementById("cbocapacidad").value = Filas.Campo34;
            document.getElementById("cbomarcadisco").value = Filas.Campo35;
            document.getElementById("cboestadoasignacion").value = Filas.Campo36;

            document.getElementById("txtnrocompra").value = Filas.Campo37;

        }
    });

    //CAARGAR HISTORIAL DE CAMBIOS

    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarAuditoriaxid',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var contenido = '';
            var Filas = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;
            debugger;
            for (var i = 0; i < Filas.length; i++) {
                contenido += "<tr><td>" + Filas[i].vc_usuario + "</td>" +
                    "<td>" + Filas[i].vc_campo_cambio + "</td>" +
                    "<td>" + Filas[i].vc_textoanterior + "</td>" +
                    "<td>" + Filas[i].vc_textonuevo + "</td>" +
                    "<td>" + Filas[i].fecha + "</td>"
                    + "</tr>";
            }
            $('#tbody_tablalog').empty().html(contenido);
            $('body').removeClass('loading'); //Removemos la clase loading
            //$('#tabla_hcambios').dataTable(
            //    "scrollX" = true
            //);
        },
        error: function (response) {
            if (response.length != 0)
                alert(response);
        }

    });


});


$("#BtnBuscar").click(function (e) {
    Muestra_Datos_Condicion();
});



function Muestra_Datos_Condicion() {
    //$('#tabla_Documentos_Aprobar').DataTable().fnDestroy();
    //var table = $('#tabla_Documentos_Aprobar').DataTable();
    //table.clear().draw();

    var table = $('#tabla_Documentos_Aprobar').DataTable({ retrieve: true, paging: false });
    table.clear().draw();
    table.destroy();


    var Estado = $('select[id=idEstado]').val();

    var Tipo = "3";
    //var Estado = "X";

    var ajax_data = {
        "Tipo": Tipo,
        "Estado": Estado
    };
    $.ajax({

        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/LISTAR_DOCUMENTOS_EXTORNOS',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,


        success: function (respuesta) {
            var Filas = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;

            $("#tabla_Documentos_Aprobar").show();
            for (var i = 0; i < Filas.length; i++) {
                $('#tabla_Documentos_Aprobar').dataTable().fnDestroy();
                $('#tabla_Documentos_Aprobar').dataTable(


                ).fnAddData([
                    Filas[i].Campo1,
                    Filas[i].Campo2,
                    Filas[i].Campo3,
                    Filas[i].Campo4,
                    Filas[i].Campo5,
                    Filas[i].Campo6,
                    Filas[i].Campo7,
                    Filas[i].Campo8,
                    Filas[i].Campo9,
                    Filas[i].Campo10,
                    Filas[i].Campo11,
                    Filas[i].Campo12,
                    Filas[i].Campo13,
                    Filas[i].Campo14,
                    Filas[i].Campo15,
                    Filas[i].Campo16,
                    Filas[i].Campo17,
                    Filas[i].Campo18,
                    Filas[i].Campo19,
                    Filas[i].Campo20,
                    "<img src='../img/report.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >",
                    "<img src='../img/ver_doc.png'  class='VerDetalle' style='height: 20px;width: 20px;text-align: center' >",
                    Filas[i].Campo21,
                    Filas[i].Campo22

                ]);
            }

            $('body').removeClass('loading'); //Removemos la clase loading

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var error = eval("(" + XMLHttpRequest.responseText + ")");
            alert(error.Message);
        }
    });


}

$(document).on('click', '.VerImagen', function (e) {



    $("#ImagenMostrar").attr('src', '');

    var FechaImagen = "", rutaimg = "";

    //alert(str.substr(0, str.length - 3));

    FechaImagen = $(this).parents("tr").find("td").eq(5).html()
    var dia = FechaImagen.substr(0, 2);
    var mes = FechaImagen.substr(3, 2);
    var yyy = FechaImagen.substr(6, 4);

    rutaimg = $(this).parents("tr").find("td").eq(22).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'




    var ajax_data = {
        "Ruta_Archivo": rutaimg
    };


    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
        data: JSON.stringify(ajax_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        beforeSend: function () {
            //$('body').addClass('loading'); //Agregamos la clase loading al body
        },
        success: function (resultado) {
            var num = resultado.d;

            $("#ImagenMostrar").attr('src', 'data:image/jpg;base64,' + num);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var error = eval("(" + XMLHttpRequest.responseText + ")");
            alert(error.Message);
        }
    });




    $('#modal_Imagen').modal();

});



function fnExcelReport() {
    ListarDatosExportar();
}


function ListarDatosExportar() {
    $.ajax({
        type: "POST",
        url: '../ServicioWeb/WS_Reporteria.asmx/ListarInventario',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (respuesta) {
            var Filas = (typeof respuesta.d) === 'string' ?
                eval('(' + respuesta.d + ')') :
                respuesta.d;
            var contenido = "";
            for (var i = 0; i < Filas.length; i++) {
                contenido +=
                    "<tr>" +
                    "<td>" + Filas[i].Campo1 + "</td>" +
                    "<td>" + Filas[i].Campo2 + "</td>" +
                    "<td>" + Filas[i].Campo3 + "</td>" +
                    "<td>" + Filas[i].Campo4 + "</td>" +
                    "<td>" + Filas[i].Campo5 + "</td>" +
                    "<td>" + Filas[i].Campo6 + "</td>" +
                    "<td>" + Filas[i].Campo7 + "</td>" +
                    "<td>" + Filas[i].Campo8 + "</td>" +
                    "<td>" + Filas[i].Campo9 + "</td>" +
                    "<td>" + Filas[i].Campo10 + "</td>" +
                    "<td>" + Filas[i].Campo11 + "</td>" +
                    "<td>" + Filas[i].Campo12 + "</td>" +
                    "<td>" + Filas[i].Campo13 + "</td>" +
                    "<td>" + Filas[i].Campo14 + "</td>" +
                    "<td>" + Filas[i].Campo15 + "</td>" +
                    "<td>" + Filas[i].Campo16 + "</td>" +
                    "<td>" + Filas[i].Campo17 + "</td>" +
                    "<td>" + Filas[i].Campo18 + "</td>" +
                    "<td>" + Filas[i].Campo19 + "</td>" +
                    "<td>" + Filas[i].Campo20 + "</td>" +
                    "<td>" + Filas[i].Campo21 + "</td>" +
                    "<td>" + Filas[i].Campo22 + "</td>" +
                    "<td>" + Filas[i].Campo23 + "</td>" +
                    "<td>" + Filas[i].Campo24 + "</td>" +
                    "<td>" + Filas[i].Campo25 + "</td>" +
                    "<td>" + Filas[i].Campo26 + "</td>" +
                    "<td>" + Filas[i].Campo27 + "</td>" +
                    "<td>" + Filas[i].Campo28 + "</td>" +
                    "<td>" + Filas[i].Campo29 + "</td>" +
                    "<td>" + Filas[i].Campo30 + "</td>" +
                    "<td>" + Filas[i].Campo31 + "</td>" +
                    "<td>" + Filas[i].Campo33 + "</td>" +
                    "<td>" + Filas[i].Campo34 + "</td>" +
                    "<td>" + Filas[i].Campo35 + "</td>" +
                    "<td>" + Filas[i].Campo36 + "</td>" +
                    "<td>" + Filas[i].Campo37 + "</td>"

                    + "</tr>";
            }
            $('#tbody_exportar').empty().html(contenido);
            ImpresionExcel();
        },
        error: function (response) {
            if (response.length != 0)
                alert(response);
        }
    });
}


function ImpresionExcel() {
    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j = 0;
    tab = document.getElementById('tabla_exportar'); // id of table

    for (j = 0; j < tab.rows.length; j++) {
        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
    }

    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
    }
    else
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

    return (sa);
}


function fn_ValidarMacE(obj) {
    var macE = obj.value;


    var ajax_data = {
        "macE": macE
    };

    if (macE.length == "17") {
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/ValidarMacE',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {                
                if (respuesta.d == true) {
                    //SIGNIFICA QUE YA EXISTE
                    alert("MAC ETHERNET YA EXISTE");
                    document.getElementById("txtmac").value = "";
                }
            }
        });
    }
}


function fn_ValidarMacW(obj) {
    var macE = obj.value;


    var ajax_data = {
        "macW": macW
    };

    if (macW.length == "17") {
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/ValidarMacW',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {
                if (respuesta.d == true) {
                    //SIGNIFICA QUE YA EXISTE
                    alert("MAC WIRELESS YA EXISTE");
                    document.getElementById("txtmacwireless").value = "";
                }
            }
        });
    }
}



