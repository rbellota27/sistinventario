﻿$(document).ready(function () {
    var iddocum, ID_DOCUMENTO;



    Muestra_Datos_Ingresado();



    function Muestra_Datos_Ingresado() {

        $('#fuUpload1').val('');
        var tab = $('#tabla_Documentos_AprobarFinanzas').DataTable();
        tab.clear().draw();

        var Tipo = "3";
        var Tienda = "0";

        var ajax_data = {
            "Tipo": Tipo,
            "Tienda": Tienda,
            "IdUsuario": $.session.get('id')
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_SOLICITUD_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos_AprobarFinanzas").show();
                for (var i = 0; i < Filas.length; i++) {
                    //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                    $('#tabla_Documentos_AprobarFinanzas').dataTable(
                        //{
                        //    columnDefs: [
                        //        {
                        //            "targets": [11],
                        //            "className": "hide_column"

                        //        },
                        //    ]
                        //}

                    ).fnAddData([
                        Filas[i].IdDocumento,
                        Filas[i].TipoDocumento,
                        Filas[i].NroDocumento,
                        Filas[i].FechaEmision,
                        Filas[i].doc_ImporteTotal,
                        Filas[i].Tienda,
                        Filas[i].Cliente,
                        Filas[i].doc_Total,
                        Filas[i].doc_FechaRegistro,
                        Filas[i].Observacion,
                        "<img src='../img/report.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='VerDetalle' style='height: 20px;width: 20px;text-align: center' >",
                        Filas[i].EstadoDoc,
                        "<img src='../img/check_2.png'  class='Aprobar' style='height: 20px;width: 20px;text-align: center' >",
                        Filas[i].TipoOperacion



                    ]);
                }

                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    }


    $(document).on('click', '.VerImagen', function (e) {

        $("#ImagenMostrar").attr('src', '');

        var FechaImagen, rutaimg;

        //alert(str.substr(0, str.length - 3));

        FechaImagen = $(this).parents("tr").find("td").eq(8).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(0).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'




        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#ImagenMostrar").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

        $('#modal_Imagen').modal();

    });


    $(document).on('click', '.Aprobar', function (e) {

       

        var TipoDocum, NumNC,Ruta="";
        var Tipo = "B", IDUSUARIO;

        Ruta = $('#fuUpload1').val();

        IDUSUARIO = $.session.get('id');


        TipoDocum = $(this).parents("tr").find("td").eq(1).html();
        NumNC = $(this).parents("tr").find("td").eq(2).html();
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();


        if (Ruta == "" || Ruta == 0 || Ruta == null) {
            swal("Error", "Debe ingresar la imagen adjunta", "error");
            return false
        }


        swal({
            title: "¿Esta seguro de aprobar el extorno en la NC" + " N° " + NumNC + "  ?",
            //text: "No podrás deshacer este paso...",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "No",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: false
        },

            function (isConfirm) {
                if (isConfirm == true) {
                    var ajax_data = {
                        "Tipo": Tipo,
                        "iddocumento": ID_DOCUMENTO,
                        "IdUsuario": IDUSUARIO,
                        "Ruta": Ruta
                    };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Contabilidad.asmx/APROBAR_REGISTROS_SOLICITUDDOCUMENTOS',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                        },
                        success: function (resultado) {
                            var num = resultado.d;

                            if (num == "EXITO") {

                                Guardar_Archivo();
                                var tables = $('#tabla_Documentos_AprobarFinanzas').DataTable();
                                tables.clear().draw();

                                Muestra_Datos_Ingresado()
                                swal("Exito!", "Los datos se registraron ", "success");
                            } else {
                                //Indicador=1
                                swal("Error!", num, "error");
                                return false
                            }


                            $('body').removeClass('loading'); //Removemos la clase loading

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });
                }
                ;
            }


        );


    });

    $(document).on('click', '.VerDetalle', function (e) {


        var amex = $('#tabla_Documentos_Amex').DataTable();
        amex.clear().draw();

        var dinners = $('#tabla_Documentos_Dinners').DataTable();
        dinners.clear().draw();

        var IzipayTab = $('#tabla_Documentos_Izipay').DataTable();
        IzipayTab.clear().draw();

        var NIUBIZ = $('#tabla_Documentos_NIUBIZ').DataTable();
        NIUBIZ.clear().draw();


        var iddocumento = $(this).parents("tr").find("td").eq(0).html()
        var Tipo = $(this).parents("tr").find("td").eq(14).html()


        if (Tipo == "0") {
            swal("Error!", "No existe registros para mosrar", "error");
            return false
        }


        if (Tipo == "1") {
            $('#Amex').show();
            $('#Dinners').hide();
            $('#Izipay').hide();
            $('#NIUBIZ').hide();
        }
        if (Tipo == "2") {
            $('#Amex').hide();
            $('#Dinners').show();
            $('#Izipay').hide();
            $('#NIUBIZ').hide();
        }
        if (Tipo == "3") {
            $('#Amex').hide();
            $('#Dinners').hide();
            $('#NIUBIZ').hide();
            $('#Izipay').show();
        }
        if (Tipo == "4") {
            $('#Amex').hide();
            $('#Dinners').hide();
            $('#NIUBIZ').show();
            $('#Izipay').hide();
        }

        //var Estado = "X";

        var ajax_data = {
            "Tipo": Tipo,
            "IdDocum": iddocumento
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/LISTAR_DOCUMENTOS_EXTORNOS_DETALLE',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;


                if (Tipo == "1") {
                    $("#tabla_Documentos_Amex").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_Amex').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18

                        ]);
                    }
                }
                if (Tipo == "2") {
                    $("#tabla_Documentos_Dinners").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_Dinners').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18,
                            Filas[i].Campo19,
                            Filas[i].Campo20,
                            Filas[i].Campo21,
                            Filas[i].Campo22,
                            Filas[i].Campo23,
                            Filas[i].Campo24,
                            Filas[i].Campo25,
                            Filas[i].Campo26,
                            Filas[i].Campo27

                        ]);
                    }
                }
                if (Tipo == "3") {
                    $("#tabla_Documentos_Izipay").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_Izipay').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18,
                            Filas[i].Campo19,
                            Filas[i].Campo20,
                            Filas[i].Campo21,
                            Filas[i].Campo22,
                            Filas[i].Campo23,
                            Filas[i].Campo24,
                            Filas[i].Campo25,
                            Filas[i].Campo26,
                            Filas[i].Campo27,
                            Filas[i].Campo28,
                            Filas[i].Campo29,
                            Filas[i].Campo30

                        ]);
                    }
                }
                if (Tipo == "4") {
                    $("#tabla_Documentos_NIUBIZ").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_NIUBIZ').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18,
                            Filas[i].Campo19,
                            Filas[i].Campo20,
                            Filas[i].Campo21,
                            Filas[i].Campo22,
                            Filas[i].Campo23,
                            Filas[i].Campo24,
                            Filas[i].Campo25,
                            Filas[i].Campo26,
                            Filas[i].Campo27,
                            Filas[i].Campo28,
                            Filas[i].Campo29,
                            Filas[i].Campo30

                        ]);
                    }
                }




                $('body').removeClass('loading'); //Removemos la clase loading
                Muestra_Datos_Ingresado();
                $('#modal_Detalle').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    });


    function Guardar_Archivo() {

        var f = new Date();
        var dia = f.getDate();
        var mes = (f.getMonth() + 1);
        var Ano = f.getFullYear()

        var Fech = dia + "" + mes + "" + Ano

        var formData = new FormData();
        for (var i = 0; i < $(".File").length; i++) {
            formData.append($(".File")[i].files[0].name, $(".File")[i].files[0], ID_DOCUMENTO + Fech + ".jpg");
        }
        $.ajax({
            url: '../ServicioWeb/WS_Contabilidad.asmx/Guardar_Finanzas',
            type: 'POST',
            data: formData,
            //data: JSON.stringify(ajax_data),
            cache: false,
            contentType: false,
            processData: false,
            success: function () {
                //Cargar_Archivo();
                //swal("Exito!", "Los datos se registraron", "success");
            }, error: function () {
                //alert(r.d);
            }
        });
    }


});