﻿$(document).ready(function () {
    var almacen_principal;
    var ubigeo;
    var guiaRemision;
    LLenarCboDepartamento();
    LLenarComboEmpresaXUsuario();
    LLenarCboEstadoDocumento();
    llenarCboTipoOperacionxIdTpoDocumento();
    LlenarCboMotivoTrasladoxIdTipoOperacion();
    llenarCboAlmacenxIdTienda();
    LLenarCboTipoDocumentoRefxIdTipoDocumento();
    LlenarCboRol();
    LlenarCboTipoAlmacen();    
    llenarCboTipoExistencia();
    Obtener_UM_X_Magnitud();

    //$('. modal_nuevo').on('hidden', function () { $(this).removeData(); })

    //$('#tabla_picking').dataTable({
    //    aLengthMenu: [
    //        [25, 50, 100, 200, -1],
    //        [25, 50, 100, 200, "All"]
    //    ],
    //    iDisplayLength: -1
    //});

    function LLenarComboEmpresaXUsuario()
    {
      
        var ID_USUARIO;
        ID_USUARIO = $.session.get('id');               

       var ajax_data = {
            "ID_USUARIO": ID_USUARIO
        };

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Empresa_X_Usuario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlempresanuevo').append('<option value="' + data[i].idempresa + '">' + data[i].nomempresa + '</option>');
                }
                LlenarComboTiendaxIdEmpresaxIdUsuario();
                cargarPersona();
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
      
    }




    function LlenarComboTiendaxIdEmpresaxIdUsuario() {

        var ID_USUARIO,ID_EMPRESA,ID_TIENDA;
        ID_USUARIO = $.session.get('id');
        ID_EMPRESA = $('#ddlempresanuevo').val();
        ID_TIENDA = $.session.get('idtienda');

        var ajax_data = {
            "ID_USUARIO": ID_USUARIO,
            "ID_EMPRESA": ID_EMPRESA
        };

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Tienda_x_Empresa_x_Usuario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddltiendanuevo').append('<option value="' + data[i].idtienda + '">' + data[i].nomtienda + '</option>');
                    $('#ddlTiend').append('<option value="' + data[i].idtienda + '">' + data[i].nomtienda + '</option>');
                }

                $('#ddltiendanuevo').val(ID_TIENDA);
                $('#ddlTiend').val(0);
                LLenarCboSeriexIdsEmpTienTipoDoc();
             
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }
    function LLenarCboSeriexIdsEmpTienTipoDoc() {

        var ID_USUARIO, ID_EMPRESA, ID_TIENDA,ID_TIPO_DOC; 
        ID_USUARIO = $.session.get('id');
        ID_EMPRESA = $.session.get('idempresa');
        ID_TIENDA = $.session.get('idtienda');
        ID_TIPO_DOC = 6;

        var ajax_data = {
            "ID_EMPRESA": ID_EMPRESA,
            "ID_TIENDA": ID_TIENDA,
            "ID_TIPO_DOC": ID_TIPO_DOC
        };

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Serie_x_Empresa_Tiend_TipDoc',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlserienuevo').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                    $('#ddlSerie').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                }

                $("#ddlserienuevo").change();
                $("#ddlSerie").change();
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }
    function LLenarCboEstadoDocumento() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Estado',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlestadonuevo').append('<option value="' + data[i].idestado + '">' + data[i].nomestado + '</option>');
                }

                $('#ddlestadonuevo').val(1);
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }    
    function LlenarCboRol() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Rol',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlrol_destinatario').append('<option value="' + data[i].idrol + '">' + data[i].nombre_rol + '</option>');
                    $('#ddlrol_transportista_buscar').append('<option value="' + data[i].idrol + '">' + data[i].nombre_rol + '</option>');
                    $('#ddlrol_chofer_buscar').append('<option value="' + data[i].idrol + '">' + data[i].nombre_rol + '</option>');
                    $('#ddlrol_agente_buscar').append('<option value="' + data[i].idrol + '">' + data[i].nombre_rol + '</option>');
                    $('#ddlrol_remitente_buscar').append('<option value="' + data[i].idrol + '">' + data[i].nombre_rol + '</option>');
                }

                //$('#ddlrol_destinatario').val(1);
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }     
    function LlenarCboTipoAlmacen() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_TipoAlmacen',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddltipoalmacen_destinatario2').append('<option value="' + data[i].idtipoalmacen + '">' + data[i].nomtipoalmacen + '</option>');
                }

                //$('#ddlrol_destinatario').val(1);
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }  
    function llenarCboTipoOperacionxIdTpoDocumento() {
     
       var ID_TIPO_DOCUMENTO = 6;
      

        var ajax_data = {
            "ID_TIPO_DOCUMENTO": ID_TIPO_DOCUMENTO
         
        };

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_TipoOperacionxIdTpoDocumento',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?  
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddloperacionnuevo').append('<option value="' + data[i].idtipooperacion + '">' + data[i].nomtipooperacion + '</option>');
                }

               
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }    
    function LlenarCboMotivoTrasladoxIdTipoOperacion() {

        var ID_TIPO_OPERACION = $('#ddloperacionnuevo').val();

        var ajax_data = {
            "ID_TIPO_OPERACION": ID_TIPO_OPERACION

        };

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_MotivoTrasladoxIdTipoOperacion',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddloperacionnuevo').append('<option value="' + data[i].idtipooperacion + '">' + data[i].nomtipooperacion + '</option>');
                }


                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }   
    function llenarCboAlmacenxIdTienda() {

      var  ID_TIENDA = $.session.get('idtienda');
        
        var ajax_data = {
            "ID_TIENDA": ID_TIENDA

        };

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenxIdTienda',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                  
                    if (data[i].flag_almacen_principal == 'True')
                    {                        
                        almacen_principal=  data[i].idalmacen;                           
                    }
                    $('#ddlalmacennuevo').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                }
              
                $('#ddlalmacennuevo').val(almacen_principal);

                // alert($('#ddlalmacennuevo').text());
              Obtener_Datos_Alm_General();
                $('body').removeClass('loading'); //Removemos la clase loading
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }
    function LLenarCboTipoDocumentoRefxIdTipoDocumento() {
        var  ID_TIPO_DOC;       
        ID_TIPO_DOC = 6;
        var ajax_data = {         
            "ID_TIPO_DOC": ID_TIPO_DOC};
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_TipoDocumentoRefxIdTipoDocumento',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                for (var i = 0; i < data.length; i++) {
                    $('#ddltipodocumento').append('<option value="' + data[i].idtipodocumento + '">' + data[i].nomtipodocumento + '</option>');
                    $('#ddltipodocumento').val(1101353004);
                }                
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }
    $(function fecha_actual() {
        $.datepicker.setDefaults($.datepicker.regional["es"]);

        $("#txtfecemisionnuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtiniciotrasladonuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfecinicio").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfecfin").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());
       
        $("#txtfechaini1").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1            
        }).datepicker("setDate", new Date());

        $("#txtfechafin1").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

    });   
    function LLenarCboDepartamento() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Departamento',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddldepartamento_punto_llegada').append('<option value="' + data[i].iddepartamento + '">' + data[i].nomdepartamento + '</option>');
                    $('#ddldepartamento_punto_partida').append('<option value="' + data[i].iddepartamento + '">' + data[i].nomdepartamento + '</option>');
                }

               

                //$('#ddlrol_destinatario').val(1);
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }     
    function Obtener_Datos_Alm_General() {
    //    $("#ddldepartamento_punto_partida").change(function ()
        var  direccion;

      //  alert('almacen principal: ' + $('#ddlalmacennuevo').val());
        var idalmacen_general = $('#ddlalmacennuevo').val();

        
       // alert(idalmacen_general);
        var ajax_data = {"ID_ALMACEN_GENERAL": idalmacen_general};
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Datos_Alm_General',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                for (var i = 0; i < data.length; i++) {
                    ubigeo = data[i].ubigeo;
                    $('#txtdireccion_punto_partida').val(data[i].direccion);
                }
                //SELECCIONA DEPARTAMENTO SEGUN UBIGEO
                var departamento = ubigeo.substring(0, 2);
                         
                $('#ddldepartamento_punto_partida').val(departamento);   
                $('#ddldepartamento_punto_partida').change();
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
             
                swal("Error! Obtener_Datos_Alm_General",error.Message,"error");
            }
        });
       
    }
    function cargarPersona() {

        var ID_PERSONA = $('#ddlempresanuevo').val(); 
       
        var ajax_data = {
            "ID_PERSONA": ID_PERSONA

        };


        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/cargarPersona',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++)
                {
                  
                    $('#txtrazon_social_remitente').val(data[i].razonsocial);
                    $('#txtcodigo_remitente').val(data[i].idpersona);
                    $('#txtdni_remitente').val(data[i].dni);
                    $('#txtruc_remitente').val(data[i].ruc);
                   

                }

                //$('#ddlrol_destinatario').val(1);
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    } 
    function Obtener_UM_X_Magnitud() {
        var IdMagnitudPeso = 1;
        var ajax_data = {
            "ID_MAG_PESO": IdMagnitudPeso
        };
      

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Magnitud_Peso',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlumpesototal').append('<option value="' + data[i].id_um + '">' + data[i].um_producto + '</option>');
                  
                }

                //$('#ddlrol_destinatario').val(1);
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    }     

    $("#btnnuevo").click(function (e) {
        
        $('#btnguardar').show();
        $('#btnactualizar').hide();
       
        limpiarDiv();
        $('#modal_nuevo').modal();
        $('#Observaciones').val('');
       
    }); 

    $("#btnCerrar").click(function (e) {
        limpiarDiv();
        //location.reload();
    }); 

    $("#btnCerrar2").click(function (e) {
        limpiarDiv();
        //location.reload();
    }); 



    function limpiarDiv() {
    
        var table1 = $('#tabla_doc_referencia_final').DataTable();       
        table1.clear().draw();
        $('#txtpesototal').val('');
        $("#ddltiendanuevo").val(4);
        $("#ddltiendanuevo").change();
        $("#ddloperacionnuevo").val(0);
        LlenarCboMotivoTrasladoxIdTipoOperacion();
        $("#ddlmot_trasladonuevo").val(0);
        $("#ddloperacionnuevo").change();
        $('#txtrazon_social_destinatario2').val('');
        $('#txtcodigo_destinatario2').val('');
        $('#txtruc_destinatario2').val('');
        $('#txtdni_destinatario2').val('');
        $('#txtdireccion_punto_llegada').val('');
        $("#ddltipoalmacen_destinatario2").val(0);
        $("#ddldepartamento_punto_llegada").val(0);
        $("#ddlprovincia_punto_llegada").val(00);
        $("#ddldistrito_punto_llegada").val(00);

        $("#txtfecemisionnuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtiniciotrasladonuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        var table = $('#tabla_producto_final').DataTable();
       // table.fnDestroy();
        table.clear().draw();
        $('#txtpesototal').val('');
        $('#txtnombretransportisa').val('');
        $('#txtcodigo_transportista').val('');
        $('#txtdni_transportista').val('');
        $('#txtruc_transportista').val('');
        $('#txtdireccion_transportista').val('');
        $('#txtnombre_chofer').val('');
        $('#txtcodigo_chofer').val('');
        $('#txtdni_chofer').val('');
        $('#txtruc_chofer').val('');
        $('#txtlicencia_Chofer').val('');
        $('#txtmodelo_vehiculo').val('');
        $('#txtplaca_vehiculo').val('');
        $('#txtcertificado_vehiculo').val('');
        $('#txtnombre_agente').val('');
        $('#txtcodigo_agente').val('');
        $('#txtdni_agente').val('');
        $('#txtruc_agente').val('');
        $('#txtdireccion_agente').val('');
        $('#txtobservaciones').val('');        
        table1.destroy();
        table.destroy();
        eliminaFilas();


    }

    function eliminaFilas() {
        //OBTIENE EL NÚMERO DE FILAS DE LA TABLA
        var n = 0;
        $("#tabla_doc_referencia_final tbody tr").each(function () {
            n++;
        });
        //BORRA LAS n-1 FILAS VISIBLES DE LA TABLA
        //LAS BORRA DE LA ULTIMA FILA HASTA LA SEGUNDA
        //DEJANDO LA PRIMERA FILA VISIBLE, MÁS LA FILA PLANTILLA OCULTA
        for (i = n - 1; i > 1; i--) {
            $("#tabla_doc_referencia_final tbody tr:eq('" + i + "')").remove();
        };
    };
 

    $("#volver_atras_destinatario").click(function (e) {
        $('#divbuscardestinatario').hide();
        $('#divseleccionardestinatario').show("slow");
        
    });    
    $("#btncerrarmodal").click(function (e) {

        $('#modal_buscar_remitente').modal().hide();
        $('#modal_buscar_remitente').removeClass('modal-open');
        $('#modal_buscar_remitente').remove();
    });
    $("#btnbuscar_documento_referencia").click(function (e) {
        $('#modal_buscar_doc_referencia').modal();
    }); 
    $("#btnbuscardocumento_referencia").click(function (e) {


        var table = $('#tabla_doc_referencia').DataTable();

        table.clear().draw();

        var ID_TIP_DOC_REF, ID_EMPRESA, ID_ALMACEN, FEC_INICIO, FEC_FIN, SERIE, CODIGO, ID_PERSONA, ID_TIPO_DOC, ID_TIP_OPERACION;

        ID_TIP_DOC_REF = $('#ddltipodocumento').val();
        ID_EMPRESA = $('#ddlempresanuevo').val();
        ID_ALMACEN = $('#ddlalmacennuevo').val();
        FEC_INICIO = $('#txtfecinicio').val();
        FEC_FIN = $('#txtfecfin').val();
        SERIE = $('#txtserie').val();
        CODIGO = $('#txtcodigo').val();
        ID_PERSONA = 0;
        ID_TIPO_DOC = 6;
        ID_TIP_OPERACION = $('#ddloperacionnuevo').val();

        var ajax_data = {
            "ID_TIP_DOC_REF": ID_TIP_DOC_REF,
            "ID_EMPRESA": ID_EMPRESA,
            "ID_ALMACEN": ID_ALMACEN,
            "FEC_INICIO": FEC_INICIO,
            "FEC_FIN": FEC_FIN,
            "SERIE": SERIE,
            "CODIGO": CODIGO,
            "ID_PERSONA": ID_PERSONA,
            "ID_TIPO_DOC": ID_TIPO_DOC,
            "ID_TIP_OPERACION": ID_TIP_OPERACION

        };

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/listar_DocumentoReferencia',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_doc_referencia").show();
                for (var i = 0; i < opvp.length; i++) {
                    $('#tabla_doc_referencia').dataTable().fnDestroy();

                    $('#tabla_doc_referencia').dataTable({columnDefs: [
                        {
                            "targets": [0,13,14,15],
                            "className": "hide_column"

                        },

                       
                    ]
                    }).fnAddData([

                        opvp[i].iddocumento,
                        opvp[i].nomtipodocumento,
                        opvp[i].numdoc,
                        opvp[i].fechaemision,                      
                        "<label style='font-weight:bold;'><span>" + opvp[i].serieOP + "</span></label>",                                               
                        "<label style='font-weight:bold;'><span>" + opvp[i].codigoOP + "</span></label>",
                        opvp[i].ruc,
                        opvp[i].dni,
                        opvp[i].almacen,
                        opvp[i].motivo_traslado,
                        opvp[i].tipo_operacion,
                        opvp[i].fecha_registro,
                        "<img src='../img/check1.png'  class='seleccionar_doc_ref' style='height: 20px;width: 20px;text-align: center' >",
                        opvp[i].idalmacen,
                        opvp[i].idtipooperacion,
                        opvp[i].idpersona

                       
                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });



       
    }); 


    $("#ddlTiend").change(function () {

        var codTienda = $('select[id=ddlTiend]').val();
        var codempresa = 1;
        var ID_TIPO_DOC = 6;


        //OBTENER DDLSERIE
        var ajax_data = {
            "COD_TIENDA": codTienda,
            "COD_EMPRESA": codempresa,
            "ID_TIPO_DOC": ID_TIPO_DOC
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/LLenarCboSeriexIdsEmpTienTipoDoc',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {
                $('#ddlSerie').empty();
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlSerie').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                }

                var codSerie = $('#ddlSerie').val();
                var ajax_data = {
                    "COD_SERIE": codSerie
                };
                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Picking.asmx/Obtener_Consecutivo',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    success: function (respuesta) {
                        var nrodoc = (typeof respuesta.d) === 'string' ?
                            eval('(' + respuesta.d + ')') :
                            respuesta.d;
                        for (var i = 0; i < nrodoc.length; i++) {
                            $('#txtCodigo').val(nrodoc[0].doc_Codigo);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        }); 


    });


    $("#ddlSerie").change(function () {
        $('#txtCodigo').val('');

        var codSerie = $('select[id=ddlSerie]').val();
        var ajax_data = {

            "COD_SERIE": codSerie
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Obtener_Consecutivo',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var nrodoc = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < nrodoc.length; i++) {
                    $('#txtCodigo').val(nrodoc[0].doc_Codigo);
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });





    $("#ddltiendanuevo").change(function () {

        var codTienda = $('select[id=ddltiendanuevo]').val();
        var codempresa = $('#ddlempresanuevo').val();
        var  ID_TIPO_DOC = 6;


        //OBTENER DDLSERIE
        var ajax_data = {
            "COD_TIENDA": codTienda,
            "COD_EMPRESA": codempresa,
            "ID_TIPO_DOC": ID_TIPO_DOC
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/LLenarCboSeriexIdsEmpTienTipoDoc',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {
                $('#ddlserienuevo').empty();
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlserienuevo').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                }
               
                var codSerie = $('#ddlserienuevo').val();
                var ajax_data = {
                    "COD_SERIE": codSerie
                };
                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Picking.asmx/Obtener_Consecutivo',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    success: function (respuesta) {
                        var nrodoc = (typeof respuesta.d) === 'string' ?
                            eval('(' + respuesta.d + ')') :
                            respuesta.d;
                        for (var i = 0; i < nrodoc.length; i++) {
                            $('#txtcorrelativo').val(nrodoc[0].doc_Codigo);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        //FIN......

        //OBTENER DDLALMACEN
        var ajax_data2 = {
            "COD_TIENDA": codTienda
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/llenarCboAlmacenxIdTienda',
            data: JSON.stringify(ajax_data2),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {
                $('#ddlalmacennuevo').empty();
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlalmacennuevo').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');
                }


                $("#ddlalmacennuevo").change();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
         //FIN......

        var table = $('#tabla_producto_final').DataTable();
        table.clear().draw();
        $('#txtpesototal').val('');
    });


    $("#ddlserienuevo").change(function () {
        $('#txtcorrelativo').val('');


        var codSerie = $('select[id=ddlserienuevo]').val();
        var ajax_data = {

            "COD_SERIE": codSerie
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Obtener_Consecutivo',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var nrodoc = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < nrodoc.length; i++) {
                    $('#txtcorrelativo').val(nrodoc[0].doc_Codigo);
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });  


   

    $("#check_fecha_emision").on('change', function () {
        if ($(this).is(':checked')) {
          
            $("#check_nro_documento").prop('checked', false);
            $("#xdocumentoserie").hide();
            $("#xdocumentocodigo").hide();
            $("#xfechaini").show();
            $("#xfechafin").show();
        }
        else {
           
            $("#check_nro_documento").prop('checked', true);  
            $("#xdocumentoserie").show();
            $("#xdocumentocodigo").show();
            $("#xfechaini").hide();
            $("#xfechafin").hide();
            
            
        }
    });  
    $("#check_nro_documento").on('change', function () {
        if ($(this).is(':checked')) {

          
            $("#check_fecha_emision").prop('checked', false);
            $("#xdocumentoserie").show();
            $("#xdocumentocodigo").show();
            $("#xfechaini").hide();
            $("#xfechafin").hide();
        }
        else {
           
            $("#check_fecha_emision").prop('checked', true);
            $("#xdocumentoserie").hide();
            $("#xdocumentocodigo").hide();
            $("#xfechaini").show();
            $("#xfechafin").show();
        }
    });  
    $("#ddltipoalmacen_destinatario2").change(function () {

        var codAlmacen_destinatario = $('select[id=ddltipoalmacen_destinatario2]').val();
        var codempresa = $('#ddlempresanuevo').val();
   
        var ajax_data = {
            "COD_ALM_DESTINATARIO": codAlmacen_destinatario,
            "COD_EMPRESA": codempresa
         
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenDestinatario_x_Empresa',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (respuesta) {
                $('#ddlalmacen_destinatario2').empty(); 
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddlalmacen_destinatario2').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');
                }


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        //FIN......

       
     
    });  
    $("#ddloperacionnuevo").change(function () {
        var ID_OPERACION = $('select[id=ddloperacionnuevo]').val();
        var ajax_data = { "ID_OPERACION": ID_OPERACION };
        
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Motivo_Traslado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $('#ddlmot_trasladonuevo').empty();
                for (var i = 0; i < data.length; i++) {
                    $('#ddlmot_trasladonuevo').append('<option value="' + data[i].IdMotivoT + '">' + data[i].motivo_traslado + '</option>');
                }
            
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                // alert(error.Message);
                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
            }
        });
       
    })
    $("#ddlalmacennuevo").change(function () {

        var DIRECCION
        var ID_ALMACEN = $('#ddlalmacennuevo').val()
        var ajax_data = {
            "ID_ALMACEN": ID_ALMACEN
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_almacen_seleccionado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');//Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < opvp.length; i++) {


                    ubigeo = opvp[i].ubigeo;
                    DIRECCION = opvp[i].direccion;
                    //$('#txtdireccion_punto_partida').val(opvp[i].direccion);


                }


                var departamento = ubigeo.substring(0, 2);
                var provincia = ubigeo.substring(2, 4);
                var distrito = ubigeo.substring(4, 6);

                $('#ddldepartamento_punto_partida').val(departamento);
                $('#ddldepartamento_punto_partida').change();
                $('#ddlprovincia_punto_partida').val(provincia);
                $('#ddlprovincia_punto_partida').change();
                $('#ddldistrito_punto_partida').val(distrito);
                $('#txtdireccion_punto_partida').val(DIRECCION);

                $('body').removeClass('loading'); //Removemos la clase loading



            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });



    })
    $("#btnbuscar_destinatario").click(function (e) {
        $('#divseleccionardestinatario').hide();
        $('#divbuscardestinatario').show("slow");
        
    });
    $("#ir_buscar_destinatario").click(function (e) {
        $('#divseleccionardestinatario').hide();
        $('#divbuscardestinatario').show("slow");

    });   
    $("#btnbuscar_destinatario2").click(function (e) {
        var table = $('#tabla_destinatario').DataTable();
        table.clear().draw();
        var TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL;
        TIPO_PERSONA = $('#ddltipo_persona_destinatario').val();
        DNI = $('#txtdni_destinatario').val();
        RUC = $('#txtruc_destinatario').val();
        ROL = $('#ddlrol_destinatario').val();
        RAZON_SOCIAL = $('#txtrazon_social_destinatario').val();           
        var ajax_data = {
            "TIPO_PERSONA": TIPO_PERSONA,
            "DNI": DNI,
            "RUC": RUC,
            "ROL": ROL,
            "RAZON_SOCIAL": RAZON_SOCIAL
           

        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/listar_Destinatario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_destinatario").show();
                for (var i = 0; i < opvp.length; i++) {
                  
                    $('#tabla_destinatario').dataTable().fnAddData([
                        opvp[i].idpersona,
                        opvp[i].nombre_persona,
                        opvp[i].ruc,
                        opvp[i].dni,
                        opvp[i].nacionalidad,
                        opvp[i].estado,
                        "<img src='../img/check1.png'  class='seleccionar_destinatario' style='height: 20px;width: 20px;text-align: center' >"

                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });
    $(document).on('click', '.seleccionar_destinatario', function (e) {


        var RAZON_SOCIAL, DNI, RUC, ID_DESTINATARIO,TIPO_ALMACEN,PROPIETARIO;
        ID_DESTINATARIO = $(this).parents("tr").find("td").eq(0).html();
        RAZON_SOCIAL = $(this).parents("tr").find("td").eq(1).html();
        DNI = $(this).parents("tr").find("td").eq(3).html();
        RUC = $(this).parents("tr").find("td").eq(2).html();
                  

        var table = $('#tabla_destinatario').DataTable();
        table.clear().draw();

        var ajax_data = {
            "ID_DESTINATARIO": ID_DESTINATARIO
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_destinatario_seleccionado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');//Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < opvp.length; i++) {


                    $('#txtcodigo_destinatario2').val(opvp[i].idpersona);
                    $('#txtrazon_social_destinatario2').val(opvp[i].descripcion);
                    $('#txtdni_destinatario2').val(opvp[i].dni);
                    $('#txtruc_destinatario2').val(opvp[i].ruc);
                   

                    ubigeo = opvp[i].ubigeo;
                    DIRECCION = opvp[i].direccion;
                    PROPIETARIO = opvp[i].propietario;
                }
                $('body').removeClass('loading'); //Removemos la clase loading
                     $('#divbuscardestinatario').hide();
                $('#divseleccionardestinatario').show("slow");

                //llenar combo destiantario_almacen

                TIPO_ALMACEN=$('#ddltipoalmacen_destinatario2').val();
              var ajax_data = {
                  "TIPO_ALMACEN": TIPO_ALMACEN,
                  "ID_DESTINATARIO": ID_DESTINATARIO
                       };
               $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Almacen_destinatario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {

                   
                    $('#ddlalmacen_destinatario2').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                }

                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

               // alert(PROPIETARIO);
                if (PROPIETARIO == 1)
                {
                    var ID_ALMACEN = $('#ddlalmacennuevo').val()
                    var ajax_data = {
                        "ID_ALMACEN": ID_ALMACEN
                    };
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_almacen_seleccionado',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading');//Agregamos la clase loading al body
                        },
                        success: function (respuesta) {
                            var opvp = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;

                            for (var i = 0; i < opvp.length; i++) {

                                ubigeo = opvp[i].ubigeo;
                                $('#txtdireccion_punto_llegada').val(opvp[i].direccion);

                            }

                            var departamento = ubigeo.substring(0, 2);
                            var provincia = ubigeo.substring(2, 4);
                            var distrito = ubigeo.substring(4, 6);

                            $('#ddldepartamento_punto_llegada').val(departamento);
                            $('#ddldepartamento_punto_llegada').change();
                            $('#ddlprovincia_punto_llegada').val(provincia);
                            $('#ddlprovincia_punto_llegada').change();
                            $('#ddldistrito_punto_llegada').val(distrito);
                            $('#txtdireccion_punto_llegada').val(DIRECCION);

                            $('body').removeClass('loading'); //Removemos la clase loading



                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });



                }
                else
                {

                    var departamento = ubigeo.substring(0, 2);
                    var provincia = ubigeo.substring(2, 4);
                    var distrito = ubigeo.substring(4, 6);

                    $('#ddldepartamento_punto_llegada').val(departamento);
                    $('#ddldepartamento_punto_llegada').change();
                    $('#ddlprovincia_punto_llegada').val(provincia);
                    $('#ddlprovincia_punto_llegada').change();
                    $('#ddldistrito_punto_llegada').val(distrito);
                    $('#txtdireccion_punto_llegada').val(DIRECCION);

                }




                $('body').removeClass('loading');
                $("#modal_buscar_remitente .close").click()
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


        

    });   
    $("#btnbuscar_remitente_principal").click(function (e) {

        $('#ddltipo_persona_remitente_buscar').val(0);
        $('#txtdni_remitente_buscar').val('');
        $('#txtruc_remitente_buscar').val('');
        $('#ddlrol_remitente_buscar').val(0);
        $('#txtrazon_social_remitente_buscar').val('');

        $('#modal_buscar_remitente').modal();
    });
    $("#btnbuscar_remitente_detalle").click(function (e) {

        var TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL;

        TIPO_PERSONA = $('#ddltipo_persona_remitente_buscar').val();

        DNI = $('#txtdni_remitente_buscar').val();
        RUC = $('#txtruc_remitente_buscar').val();
        ROL = $('#ddlrol_remitente_buscar').val();
        RAZON_SOCIAL = $('#txtrazon_social_remitente_buscar').val();

        if (TIPO_PERSONA == 0) {
            swal("¡Seleccione un Tipo de Persona..!", "", "warning");
        }
        else {

            var table = $('#tabla_remitente').DataTable();
            table.clear().draw();
            var ajax_data = {
                "TIPO_PERSONA": TIPO_PERSONA,
                "DNI": DNI,
                "RUC": RUC,
                "ROL": ROL,
                "RAZON_SOCIAL": RAZON_SOCIAL


            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/obtener_datos_remitente',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');//Agregamos la clase loading al body
                },
                success: function (respuesta) {
                    var opvp = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    $("#tabla_remitente").show();
                    for (var i = 0; i < opvp.length; i++) {

                        $('#tabla_remitente').dataTable().fnAddData([
                            opvp[i].idpersona,
                            opvp[i].nombre_persona,
                            opvp[i].ruc,
                            opvp[i].dni,
                            opvp[i].nacionalidad,
                            opvp[i].estado,
                            "<img src='../img/check1.png'  class='seleccionar_remitente' style='height: 20px;width: 20px;text-align: center' >"

                        ]);


                    }

                    $('#tabla_remitente').dataTable().fnDestroy();
                    $('#tabla_remitente').dataTable({
                        "searching": false,
                        "paging": false,
                        "info": false
                    });
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }







    });
    $(document).on('click', '.seleccionar_remitente', function (e) {


        var RAZON_SOCIAL, DNI, RUC, ID_REMITENTE, EMPRESA,UBIGEO,DIRECCION;
        ID_REMITENTE = $(this).parents("tr").find("td").eq(0).html();
        RAZON_SOCIAL = $(this).parents("tr").find("td").eq(1).html();
        DNI = $(this).parents("tr").find("td").eq(3).html();
        RUC = $(this).parents("tr").find("td").eq(2).html();
        EMPRESA = $('#ddlempresanuevo').val();
        var table = $('#tabla_remitente').DataTable();
        table.clear().draw();

        var ajax_data = {
            "ID_REMITENTE": ID_REMITENTE
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_remitente_seleccionado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');//Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < opvp.length; i++) {


                    $('#txtcodigo_remitente').val(opvp[i].idpersona);
                    $('#txtrazon_social_remitente').val(opvp[i].descripcion);
                    $('#txtdni_remitente').val(opvp[i].dni);
                    $('#txtruc_remitente').val(opvp[i].ruc);


                    ubigeo = opvp[i].ubigeo;
                    DIRECCION = opvp[i].direccion;

                }
                $('body').removeClass('loading'); //Removemos la clase loading

                if (EMPRESA == ID_REMITENTE) {
                    var ID_ALMACEN = $('#ddlalmacennuevo').val()
                    var ajax_data = {
                        "ID_ALMACEN": ID_ALMACEN
                    };
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_almacen_seleccionado',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading');//Agregamos la clase loading al body
                        },
                        success: function (respuesta) {
                            var opvp = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;

                            for (var i = 0; i < opvp.length; i++) {


                                ubigeo=opvp[i].ubigeo;
                                $('#txtdireccion_punto_partida').val(opvp[i].direccion);


                            }


                            var departamento = ubigeo.substring(0, 2);
                            var provincia = ubigeo.substring(2, 4);
                            var distrito = ubigeo.substring(4, 6);
                           
                            $('#ddldepartamento_punto_partida').val(departamento);
                            $('#ddldepartamento_punto_partida').change();
                            $('#ddlprovincia_punto_partida').val(provincia);
                            $('#ddlprovincia_punto_partida').change();
                            $('#ddldistrito_punto_partida').val(distrito);

                            $('body').removeClass('loading'); //Removemos la clase loading



                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });



                }
                else {
                  
                    var departamento = ubigeo.substring(0, 2);
                    var provincia = ubigeo.substring(2, 4);
                    var distrito = ubigeo.substring(4, 6);
                    $('#txtdireccion_punto_partida').val(DIRECCION)
                    $('#ddldepartamento_punto_partida').val(departamento);
                    $('#ddldepartamento_punto_partida').change();
                    $('#ddlprovincia_punto_partida').val(provincia);
                    $('#ddlprovincia_punto_partida').change();
                    $('#ddldistrito_punto_partida').val(distrito);

                }



                $("#modal_buscar_remitente .close").click()
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });







    });   
    $("#ddldepartamento_punto_partida").change(function ()
    {
       
        $("#ddlprovincia_punto_partida").empty();
       
        var codDepartamento = $('select[id=ddldepartamento_punto_partida]').val();      
        var ajax_data = { "ID_DEPARTAMENTO": codDepartamento };
       // alert( 'combo_departamento----'+ $('#ddldepartamento_punto_partida').text());
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Provincia_x_iddepartamento',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++)
                {
                 $('#ddlprovincia_punto_partida').append('<option value="' + data[i].idprovincia + '">' + data[i].nomprovincia + '</option>');
                }
                var provincia = ubigeo.substring(2,4);
                $('#ddlprovincia_punto_partida').val(provincia);

                $("#ddlprovincia_punto_partida").change();

                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
               // alert(error.Message);
                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
            }
        }); 
    });   
    $("#ddlprovincia_punto_partida").change(function () {
       
        $("#ddldistrito_punto_partida").empty();
        var coddepartamento = $('select[id=ddldepartamento_punto_partida]').val();
        var codprovincia = $('select[id=ddlprovincia_punto_partida]').val();
        var ajax_data = {
            "ID_DEPARTAMENTO": coddepartamento,
           "ID_PROVINCIA": codprovincia
        };      
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Distrito_x_idprovincia',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddldistrito_punto_partida').append('<option value="' + data[i].iddistrito + '">' + data[i].nomdistrito + '</option>');
                }
                var distrito = ubigeo.substring(4, 6);
                
                $('#ddldistrito_punto_partida').val(distrito);

                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
          
                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
            }
        });
    }); 
    $("#ddldepartamento_punto_llegada").change(function () {
        var codDepartamento = $('select[id=ddldepartamento_punto_llegada]').val();
        var ajax_data = { "ID_DEPARTAMENTO": codDepartamento };
        // alert( 'combo_departamento----'+ $('#ddldepartamento_punto_partida').text());
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Provincia_x_iddepartamento',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $('#ddlprovincia_punto_llegada').empty();
                for (var i = 0; i < data.length; i++) {
                    $('#ddlprovincia_punto_llegada').append('<option value="' + data[i].idprovincia + '">' + data[i].nomprovincia + '</option>');
                }
                var provincia = ubigeo.substring(2, 4);
              

                $('#ddlprovincia_punto_llegada').val(provincia);
               
                $("#ddlprovincia_punto_llegada").change();

                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                // alert(error.Message);
                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
            }
        });
    })
    $("#ddlprovincia_punto_llegada").change(function () {
        var coddepartamento = $('select[id=ddldepartamento_punto_llegada]').val();
        var codprovincia = $('select[id=ddlprovincia_punto_llegada]').val();
        var ajax_data = {
            "ID_DEPARTAMENTO": coddepartamento,
            "ID_PROVINCIA": codprovincia
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Distrito_x_idprovincia',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $('#ddldistrito_punto_llegada').empty();
               
                for (var i = 0; i < data.length; i++) {
                    $('#ddldistrito_punto_llegada').append('<option value="' + data[i].iddistrito + '">' + data[i].nomdistrito + '</option>');
                }
                var distrito = ubigeo.substring(4, 6);
                $('#ddldistrito_punto_llegada').val(distrito);

                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");

                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
            }
        });
    }); 


    
    $(document).on('click', '.seleccionar_doc_ref', function (e) {
       
        var rowCount = $('#tabla_doc_referencia_final tr').length;   
       
        if (rowCount > 1) {
            var table = document.getElementById('tabla_doc_referencia_final'),
                rows = table.getElementsByTagName('tr'),
                cells, alm, tipoO, Dest,Num;

            cells = rows[1].getElementsByTagName('td');
            Num = cells[6].innerHTML;
            alm = cells[7].innerHTML;
            tipoO = cells[8].innerHTML;
            Dest = cells[9].innerHTML;
           
        
            var table = $('#tabla_doc_referencia').DataTable();
            table.clear().draw();
            var alm2, tipoO2, Dest2, Num2;
            Num2 = $(this).parents("tr").find("td").eq(0).html();
            alm2 = $(this).parents("tr").find("td").eq(13).html();
            tipoO2 = $(this).parents("tr").find("td").eq(14).html();
            Dest2 = $(this).parents("tr").find("td").eq(15).html();

            if (Num == Num2) {
                alert('No se permite la operación! El Documento ya se encuentra agregado');
            } else {

            if (tipoO == 1) {
                if (tipoO == tipoO2 && alm == alm2 && Dest == Dest2) {
                    $('#tabla_doc_referencia_final').dataTable().fnDestroy();
                    var ajax_data = {
                        "idDocumento": ID_DOCUMENTO_REF
                    };
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/Listar_Documento_Referencia',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading');
                        },
                        success: function (respuesta) {
                            $('#ddlalmacennuevo').empty();
                            var detalle = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                           
                            for (var i = 0; i < detalle.length; i++) {
                                $('#tabla_doc_referencia_final').dataTable(
                                    {
                                        "searching": false,
                                        "paging": false,
                                        "info": false,
                                        columnDefs: [
                                            {
                                                "targets": [6, 7, 8, 9],
                                                "className": "hide_column"
                                            }
                                        ]
                                    }
                                ).fnAddData(
                                    [detalle[i].documento,
                                    detalle[i].doc_Serie + '-' + detalle[i].doc_Codigo,
                                    detalle[i].empresa,
                                    detalle[i].tienda,
                                    detalle[i].almacen,
                                    detalle[i].estado,
                                    detalle[i].IdDocumento,
                                    detalle[i].Idalmacen,
                                    detalle[i].tipoOperacion,
                                    detalle[i].dcodigo
                                    ]);

                                //Llenar combotienda
                                $("#ddltiendanuevo").val(detalle[i].idTienda);

                                //Llenar combo de Serie por tienda
                                var codTienda = detalle[i].idTienda;
                                var codempresa = $('#ddlempresanuevo').val();
                                var ID_TIPO_DOC = 6;
                                var ajax_data = {
                                    "COD_TIENDA": codTienda,
                                    "COD_EMPRESA": codempresa,
                                    "ID_TIPO_DOC": ID_TIPO_DOC
                                };
                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/LLenarCboSeriexIdsEmpTienTipoDoc',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    success: function (respuesta) {
                                        $('#ddlserienuevo').empty();
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddlserienuevo').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                                        }
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        alert(error.Message);
                                    }
                                });

                                //Llenar Fechas y Estado 
                             
                                $('#ddlestadonuevo').val(detalle[i].IdEstado);

                                //LLenarAlmacen                 
                                var ajax_data = {
                                    "ID_TIENDA": detalle[i].idTienda,
                                };
                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenxIdTienda',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        for (var i = 0; i < data.length; i++) {
                                            if (data[i].flag_almacen_principal == 'True') {
                                                almacen_principal = data[i].idalmacen;
                                            }
                                            $('#ddlalmacennuevo').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                                        }
                                        //Obtener Punto de Partida
                                        Obtener_Datos_Alm_General();
                                        $('body').removeClass('loading'); //Removemos la clase loading

                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        alert(error.Message);
                                    }
                                });
                                $('#ddlalmacennuevo').val(detalle[i].Idalmacen);


                                //Obtener TipoOperacion
                                $('#ddloperacionnuevo').val(detalle[i].tipoOperacion);


                                //Obtener motivo de Translado                    
                                var ID_OPERACION = detalle[i].tipoOperacion;
                                var ajax_data = { "ID_OPERACION": ID_OPERACION };

                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Motivo_Traslado',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        $('#ddlmot_trasladonuevo').empty();
                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddlmot_trasladonuevo').append('<option value="' + data[i].IdMotivoT + '">' + data[i].motivo_traslado + '</option>');
                                        }
                                        $('body').removeClass('loading');
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                    }
                                });
                                $('#ddlmot_trasladonuevo').val(detalle[i].motivoTrans);


                                //Obtener Remitente
                                $('#txtrazon_social_remitente').val(detalle[i].rsocial);
                                $('#txtcodigo_remitente').val(detalle[i].IdCliente);
                                $('#txtdni_remitente').val(detalle[i].dni);
                                $('#txtruc_remitente').val(detalle[i].ruc);


                                //Obtener Destinatario
                                $('#txtrazon_social_destinatario2').val(detalle[i].dnombres);
                                $('#txtcodigo_destinatario2').val(detalle[i].dcodigo);
                                $('#ddltipoalmacen_destinatario2').val(detalle[i].idTipoAlmacen);
                                var almacen = detalle[i].idAlm;

                                var ajax_data = {
                                    "COD_ALM_DESTINATARIO": detalle[i].idTipoAlmacen,
                                    "COD_EMPRESA": 1

                                };
                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenDestinatario_x_Empresa',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    success: function (respuesta) {

                                        if (almacen == 0) {
                                            //alert('if');
                                            $('#ddlalmacen_destinatario2').val(0);

                                        }
                                        else {
                                            //alert('else');
                                            $('#ddlalmacen_destinatario2').empty();

                                        }
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddlalmacen_destinatario2').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                                        }
                                        $('#ddlalmacen_destinatario2').val(almacen);

                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        alert(error.Message);
                                    }
                                });

                                $('#ddlalmacen_destinatario2').val(detalle[i].idAlm);
                                $('#txtdni_destinatario2').val(detalle[i].ddni);
                                $('#txtruc_destinatario2').val(detalle[i].druc);

                                //Obtener Punto de LLegada
                                $('#txtdireccion_punto_llegada').val(detalle[i].lldireccion);
                                $('#ddldepartamento_punto_llegada').val(detalle[i].idDepartamentoL);

                                var provincia = detalle[i].idProvinciaL;
                                var codDepartamento = $('#ddldepartamento_punto_llegada').val();
                                var ajax_data = { "ID_DEPARTAMENTO": codDepartamento };
                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Provincia_x_iddepartamento',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        $('#ddlprovincia_punto_llegada').empty();
                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddlprovincia_punto_llegada').append('<option value="' + data[i].idprovincia + '">' + data[i].nomprovincia + '</option>');

                                        }
                                        $('#ddlprovincia_punto_llegada').val(provincia);
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        // alert(error.Message);
                                        swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                    }
                                });



                                var distrito = detalle[i].idDistritoL;
                                var coddepartamento = $('select[id=ddldepartamento_punto_llegada]').val();
                                var codprovincia = detalle[i].idProvinciaL;
                                var ajax_data = {
                                    "ID_DEPARTAMENTO": coddepartamento,
                                    "ID_PROVINCIA": codprovincia
                                };
                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Distrito_x_idprovincia',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    beforeSend: function () {
                                        $('body').addClass('loading'); //Agregamos la clase loading al body
                                    },
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        $('#ddldistrito_punto_llegada').empty();

                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddldistrito_punto_llegada').append('<option value="' + data[i].iddistrito + '">' + data[i].nomdistrito + '</option>');
                                        }
                                        $('#ddldistrito_punto_llegada').val(distrito);

                                        $('body').removeClass('loading'); //Removemos la clase loading
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");

                                        swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                    }
                                });




                            }
                            $('body').removeClass('loading'); //Removemos la clase loading
                            $('#modal_ver').modal();
                        },


                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }

                    });


                    var ajax_data = {
                        "idDocumento": ID_DOCUMENTO_REF,
                        "idUnidadPeso": ID_UM_PESO
                    };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/ListarProductoFinal',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading');
                        },
                        success: function (respuesta) {

                            var detalle = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < detalle.length; i++) {
                                var cantidad;
                                var peso, almacen;
                                cantidad = detalle[i].cantidad;
                                peso = detalle[i].peso;
                                almacen = detalle[i].idAlmacen;

                                if (almacen == 200000002 || almacen == 20000016 || almacen == 200000003 || almacen == 20000011 || almacen == 20000012 || almacen == 20000013 || almacen == 20000014 || almacen == 20090017 || almacen == 300346608 || almacen == 1500000002 || almacen == 20000009 || almacen == 20000010 || almacen == 7) {

                                    $('#tabla_producto_final').dataTable().fnDestroy();
                                    $('#tabla_producto_final').dataTable(
                                        {
                                            "searching": false,
                                            "paging": false,
                                            "info": false,

                                            columnDefs: [
                                                {
                                                    "targets": [12, 13, 14, 15, 16],
                                                    "className": "hide_column"
                                                }
                                            ]
                                        }

                                    ).fnAddData(
                                        [detalle[i].codigo,
                                        detalle[i].nroDocumento,
                                        detalle[i].descripcion,
                                        detalle[i].UM,
                                        "<input type='text' class='cantidad2 form-control' value='" + cantidad + "'>",
                                        detalle[i].pendiente,
                                        "<input type='text' class='peso form-control'  style='width :80px; heigth: 1px'  disabled>",
                                        detalle[i].stockReal,
                                        detalle[i].stockComprometido,
                                        detalle[i].stockdisponible,
                                        "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                        "<input type='text' class='form-control'   id='" + detalle[i].idDetalleDoc + "'>",
                                        detalle[i].equivalencia,
                                        detalle[i].equivalencia1,
                                        detalle[i].idProducto,
                                        detalle[i].idDocumento,
                                        detalle[i].idDetalleDoc,
                                        "<img src='../img/eliminarnew.png' class='borrar' data-toggle='tooltip' style='cursor: pointer;height: 20px;width: 20px;text-align: center' >"

                                        ]);
                                } else {

                                    $('#tabla_producto_final').dataTable().fnDestroy();
                                    $('#tabla_producto_final').dataTable(
                                        {
                                            "searching": false,
                                            "paging": false,
                                            "info": false,

                                            columnDefs: [
                                                {
                                                    "targets": [10, 11, 12, 13, 14, 15, 16],
                                                    "className": "hide_column"
                                                }
                                            ]
                                        }

                                    ).fnAddData(
                                        [detalle[i].codigo,
                                        detalle[i].nroDocumento,
                                        detalle[i].descripcion,
                                        detalle[i].UM,
                                        "<input type='text' class='cantidad2 form-control' value='" + cantidad + "'>",
                                        detalle[i].pendiente,
                                        "<input type='text' class='peso form-control'  style='width :80px; heigth: 1px'  disabled>",
                                        detalle[i].stockReal,
                                        detalle[i].stockComprometido,
                                        detalle[i].stockdisponible,
                                        "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                        "<input type='text' class='form-control'  id='" + detalle[i].idDetalleDoc + "'>",
                                        detalle[i].equivalencia,
                                        detalle[i].equivalencia1,
                                        detalle[i].idProducto,
                                        detalle[i].idDocumento,
                                        detalle[i].idDetalleDoc,
                                        "<img src='../img/eliminarnew.png' class='borrar' data-toggle='tooltip' style='cursor: pointer;height: 20px;width: 20px;text-align: center' >"

                                        ]);


                                }


                                $(".cantidad2").on('keyup', function () {

                                    $('#txtpesototal').val('');
                                    var EQUIVALENCIA, EQUIVALENCIA1, CANTIDAD, RESULTADO;
                                    EQUIVALENCIA = $(this).parents("tr").find("td").eq(12).html();
                                    EQUIVALENCIA1 = $(this).parents("tr").find("td").eq(13).html();
                                    CANTIDAD = $(this).val();
                                    RESULTADO = CANTIDAD * EQUIVALENCIA * EQUIVALENCIA1
                                    $(this).parents("tr").find("td").eq(6).find(":input").val(RESULTADO.toFixed(3));
                                    var total = 0;
                                    $('#tabla_producto_final tbody tr').each(function () {
                                        var peso = $(this).find("td").eq(6).find(":input").val();
                                        var cantidad = $(this).find("td").eq(4).find(":input").val();
                                        if (cantidad > 0) {
                                            total = parseFloat(total) + parseFloat(peso);
                                            $('#txtpesototal').val(total.toFixed(3));
                                        }



                                    })

                                }).keyup();



                            }
                            $('body').removeClass('loading'); //Removemos la clase loading
                            $('#modal_ver').modal();

                        },


                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }

                    });

                } else {

                    alert('No se Puede Proceder con la Operacion...Verificar Almacen y/o Tipo Operación');

                }
            }

            else {
                if (tipoO == tipoO2 && alm == alm2) {

                    var ID_DOCUMENTO_REF;
                    ID_DOCUMENTO_REF = $(this).parents("tr").find("td").eq(0).html();

                $('#tabla_doc_referencia_final').dataTable().fnDestroy();

                    var ajax_data = {
                    "idDocumento": ID_DOCUMENTO_REF
                    };

                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Despacho.asmx/Listar_Documento_Referencia',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('body').addClass('loading');
                    },
                    
                    success: function (respuesta) {
                        $('#ddlalmacennuevo').empty();
                        var detalle = (typeof respuesta.d) === 'string' ?
                            eval('(' + respuesta.d + ')') :
                            respuesta.d;
                     
                        for (var i = 0; i < detalle.length; i++) {
                            $('#tabla_doc_referencia_final').dataTable(
                                {
                                    "searching": false,
                                    "paging": false,
                                    "info": false,
                                    columnDefs: [
                                        {
                                            "targets": [6, 7, 8, 9],
                                            "className": "hide_column"
                                        }
                                    ]
                                }
                            ).fnAddData(
                                [detalle[i].documento,
                                detalle[i].doc_Serie + '-' + detalle[i].doc_Codigo,
                                detalle[i].empresa,
                                detalle[i].tienda,
                                detalle[i].almacen,
                                detalle[i].estado,
                                detalle[i].IdDocumento ,
                                detalle[i].Idalmacen,
                                detalle[i].tipoOperacion,
                                detalle[i].dcodigo
                                ]);

                            //Llenar combotienda
                            $("#ddltiendanuevo").val(detalle[i].idTienda);

                            //Llenar combo de Serie por tienda
                            var codTienda = detalle[i].idTienda;
                            var codempresa = $('#ddlempresanuevo').val();
                            var ID_TIPO_DOC = 6;
                            var ajax_data = {
                                "COD_TIENDA": codTienda,
                                "COD_EMPRESA": codempresa,
                                "ID_TIPO_DOC": ID_TIPO_DOC
                            };
                            $.ajax({
                                type: "POST",
                                url: '../ServicioWeb/WS_Despacho.asmx/LLenarCboSeriexIdsEmpTienTipoDoc',
                                data: JSON.stringify(ajax_data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: true,
                                success: function (respuesta) {
                                    $('#ddlserienuevo').empty();
                                    var data = (typeof respuesta.d) === 'string' ?
                                        eval('(' + respuesta.d + ')') :
                                        respuesta.d;
                                    for (var i = 0; i < data.length; i++) {
                                        $('#ddlserienuevo').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                                    }
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                                    alert(error.Message);
                                }
                            });

                            //Llenar Fechas y Estado 
                         
                            $('#ddlestadonuevo').val(detalle[i].IdEstado);

                            //LLenarAlmacen                 
                            var ajax_data = {
                                "ID_TIENDA": detalle[i].idTienda,
                            };
                            $.ajax({
                                type: "POST",
                                url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenxIdTienda',
                                data: JSON.stringify(ajax_data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: true,
                                success: function (respuesta) {
                                    var data = (typeof respuesta.d) === 'string' ?
                                        eval('(' + respuesta.d + ')') :
                                        respuesta.d;
                                    for (var i = 0; i < data.length; i++) {
                                        if (data[i].flag_almacen_principal == 'True') {
                                            almacen_principal = data[i].idalmacen;
                                        }
                                        $('#ddlalmacennuevo').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                                    }
                                    //Obtener Punto de Partida
                                    Obtener_Datos_Alm_General();
                                    $('body').removeClass('loading'); //Removemos la clase loading

                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                                    alert(error.Message);
                                }
                            });
                            $('#ddlalmacennuevo').val(detalle[i].Idalmacen);


                            //Obtener TipoOperacion
                            $('#ddloperacionnuevo').val(detalle[i].tipoOperacion);


                            //Obtener motivo de Translado                    
                            var ID_OPERACION = detalle[i].tipoOperacion;
                            var ajax_data = { "ID_OPERACION": ID_OPERACION };

                            $.ajax({
                                type: "POST",
                                url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Motivo_Traslado',
                                data: JSON.stringify(ajax_data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: true,
                                success: function (respuesta) {
                                    var data = (typeof respuesta.d) === 'string' ?
                                        eval('(' + respuesta.d + ')') :
                                        respuesta.d;
                                    $('#ddlmot_trasladonuevo').empty();
                                    for (var i = 0; i < data.length; i++) {
                                        $('#ddlmot_trasladonuevo').append('<option value="' + data[i].IdMotivoT + '">' + data[i].motivo_traslado + '</option>');
                                    }
                                    $('body').removeClass('loading');
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                                    swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                }
                            });
                            $('#ddlmot_trasladonuevo').val(detalle[i].motivoTrans);


                            //Obtener Remitente
                            $('#txtrazon_social_remitente').val(detalle[i].rsocial);
                            $('#txtcodigo_remitente').val(detalle[i].IdCliente);
                            $('#txtdni_remitente').val(detalle[i].dni);
                            $('#txtruc_remitente').val(detalle[i].ruc);


                            //Obtener Destinatario
                            $('#txtrazon_social_destinatario2').val(detalle[i].dnombres);
                            $('#txtcodigo_destinatario2').val(detalle[i].dcodigo);
                            $('#ddltipoalmacen_destinatario2').val(detalle[i].idTipoAlmacen);
                            var almacen = detalle[i].idAlm;

                            var ajax_data = {
                                "COD_ALM_DESTINATARIO": detalle[i].idTipoAlmacen,
                                "COD_EMPRESA": 1

                            };
                            $.ajax({
                                type: "POST",
                                url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenDestinatario_x_Empresa',
                                data: JSON.stringify(ajax_data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: true,
                                success: function (respuesta) {

                                    if (almacen == 0) {
                                        //alert('if');
                                        $('#ddlalmacen_destinatario2').val(0);

                                    }
                                    else {
                                        //alert('else');
                                        $('#ddlalmacen_destinatario2').empty();

                                    }
                                    var data = (typeof respuesta.d) === 'string' ?
                                        eval('(' + respuesta.d + ')') :
                                        respuesta.d;
                                    for (var i = 0; i < data.length; i++) {
                                        $('#ddlalmacen_destinatario2').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                                    }
                                    $('#ddlalmacen_destinatario2').val(almacen);

                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                                    alert(error.Message);
                                }
                            });

                            $('#ddlalmacen_destinatario2').val(detalle[i].idAlm);
                            $('#txtdni_destinatario2').val(detalle[i].ddni);
                            $('#txtruc_destinatario2').val(detalle[i].druc);

                            //Obtener Punto de LLegada
                            $('#txtdireccion_punto_llegada').val(detalle[i].lldireccion);
                            $('#ddldepartamento_punto_llegada').val(detalle[i].idDepartamentoL);

                            var provincia = detalle[i].idProvinciaL;
                            var codDepartamento = $('#ddldepartamento_punto_llegada').val();
                            var ajax_data = { "ID_DEPARTAMENTO": codDepartamento };
                            $.ajax({
                                type: "POST",
                                url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Provincia_x_iddepartamento',
                                data: JSON.stringify(ajax_data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: true,
                                success: function (respuesta) {
                                    var data = (typeof respuesta.d) === 'string' ?
                                        eval('(' + respuesta.d + ')') :
                                        respuesta.d;
                                    $('#ddlprovincia_punto_llegada').empty();
                                    for (var i = 0; i < data.length; i++) {
                                        $('#ddlprovincia_punto_llegada').append('<option value="' + data[i].idprovincia + '">' + data[i].nomprovincia + '</option>');

                                    }
                                    $('#ddlprovincia_punto_llegada').val(provincia);
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                                    // alert(error.Message);
                                    swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                }
                            });



                            var distrito = detalle[i].idDistritoL;
                            var coddepartamento = $('select[id=ddldepartamento_punto_llegada]').val();
                            var codprovincia = detalle[i].idProvinciaL;
                            var ajax_data = {
                                "ID_DEPARTAMENTO": coddepartamento,
                                "ID_PROVINCIA": codprovincia
                            };
                            $.ajax({
                                type: "POST",
                                url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Distrito_x_idprovincia',
                                data: JSON.stringify(ajax_data),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: true,
                                beforeSend: function () {
                                    $('body').addClass('loading'); //Agregamos la clase loading al body
                                },
                                success: function (respuesta) {
                                    var data = (typeof respuesta.d) === 'string' ?
                                        eval('(' + respuesta.d + ')') :
                                        respuesta.d;
                                    $('#ddldistrito_punto_llegada').empty();

                                    for (var i = 0; i < data.length; i++) {
                                        $('#ddldistrito_punto_llegada').append('<option value="' + data[i].iddistrito + '">' + data[i].nomdistrito + '</option>');
                                    }
                                    $('#ddldistrito_punto_llegada').val(distrito);

                                    $('body').removeClass('loading'); //Removemos la clase loading
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var error = eval("(" + XMLHttpRequest.responseText + ")");

                                    swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                }
                            });




                        }
                        $('body').removeClass('loading'); //Removemos la clase loading
                        $('#modal_ver').modal();
                    },


                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }

                });

                var ajax_data = {
                    "idDocumento": ID_DOCUMENTO_REF,
                    "idUnidadPeso": ID_UM_PESO
                };

                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Despacho.asmx/ListarProductoFinal',
                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('body').addClass('loading');
                    },
                    success: function (respuesta) {

                        var detalle = (typeof respuesta.d) === 'string' ?
                            eval('(' + respuesta.d + ')') :
                            respuesta.d;
                        for (var i = 0; i < detalle.length; i++) {
                            var cantidad;
                            var peso, almacen;
                            cantidad = detalle[i].cantidad;
                            peso = detalle[i].peso;
                            almacen = detalle[i].idAlmacen;

                            if (almacen == 200000002 || almacen == 20000016 || almacen == 200000003 || almacen == 20000011 || almacen == 20000012 || almacen == 20000013 || almacen == 20000014 || almacen == 20090017 || almacen == 300346608 || almacen == 1500000002 || almacen == 20000009 || almacen == 20000010 || almacen == 7) {

                                $('#tabla_producto_final').dataTable().fnDestroy();
                                $('#tabla_producto_final').dataTable(
                                    {
                                        "searching": false,
                                        "paging": false,
                                        "info": false,

                                        columnDefs: [
                                            {
                                                "targets": [12, 13, 14, 15, 16],
                                                "className": "hide_column"
                                            }
                                        ]
                                    }

                                ).fnAddData(
                                    [detalle[i].codigo,
                                    detalle[i].nroDocumento,
                                    detalle[i].descripcion,
                                    detalle[i].UM,
                                    "<input type='text' class='cantidad2 form-control' value='" + cantidad + "'>",
                                    detalle[i].pendiente,
                                        "<input type='text' class='peso form-control'  style='width :80px; heigth: 1px'  disabled>",
                                    detalle[i].stockReal,
                                    detalle[i].stockComprometido,
                                    detalle[i].stockdisponible,
                                        "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                    "<input type='text' class='form-control'  id='" + detalle[i].idDetalleDoc + "'>",
                                    detalle[i].equivalencia,
                                    detalle[i].equivalencia1,
                                    detalle[i].idProducto,
                                    detalle[i].idDocumento,
                                    detalle[i].idDetalleDoc,
                                        "<img src='../img/eliminarnew.png' class='borrar' data-toggle='tooltip' style='cursor: pointer;height: 20px;width: 20px;text-align: center' >"

                                    ]);
                            } else {

                                $('#tabla_producto_final').dataTable().fnDestroy();
                                $('#tabla_producto_final').dataTable(
                                    {
                                        "searching": false,
                                        "paging": false,
                                        "info": false,

                                        columnDefs: [
                                            {
                                                "targets": [10, 11, 12, 13, 14, 15, 16],
                                                "className": "hide_column"
                                            }

                                        ]
                                    }

                                ).fnAddData(
                                    [detalle[i].codigo,
                                    detalle[i].nroDocumento,
                                    detalle[i].descripcion,
                                    detalle[i].UM,
                                    "<input type='text' class='cantidad2 form-control' value='" + cantidad + "'>",
                                    detalle[i].pendiente,
                                        "<input type='text' class='peso form-control'  style='width :80px; heigth: 1px'  disabled>",
                                    detalle[i].stockReal,
                                    detalle[i].stockComprometido,
                                    detalle[i].stockdisponible,
                                        "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                    "<input type='text' class='form-control'  id='" + detalle[i].idDetalleDoc + "'>",
                                    detalle[i].equivalencia,
                                    detalle[i].equivalencia1,
                                    detalle[i].idProducto,
                                    detalle[i].idDocumento,
                                    detalle[i].idDetalleDoc,
                                        "<img src='../img/eliminarnew.png' class='borrar' data-toggle='tooltip' style='cursor: pointer;height: 20px;width: 20px;text-align: center' >"

                                    ]);


                            }


                            $(".cantidad2").on('keyup', function () {

                                $('#txtpesototal').val('');
                                var EQUIVALENCIA, EQUIVALENCIA1, CANTIDAD, RESULTADO;
                                EQUIVALENCIA = $(this).parents("tr").find("td").eq(12).html();
                                EQUIVALENCIA1 = $(this).parents("tr").find("td").eq(13).html();
                                CANTIDAD = $(this).val();
                                RESULTADO = CANTIDAD * EQUIVALENCIA * EQUIVALENCIA1
                                $(this).parents("tr").find("td").eq(6).find(":input").val(RESULTADO.toFixed(3));
                                var total = 0;
                                $('#tabla_producto_final tbody tr').each(function () {
                                    var peso = $(this).find("td").eq(6).find(":input").val();
                                    var cantidad = $(this).find("td").eq(4).find(":input").val();
                                    if (cantidad > 0) {
                                        total = parseFloat(total) + parseFloat(peso);
                                        $('#txtpesototal').val(total.toFixed(3));
                                    }



                                })

                            }).keyup();



                        }
                        $('body').removeClass('loading'); //Removemos la clase loading
                        $('#modal_ver').modal();

                    },


                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }

                });
                }
                else {
               
                alert('No se Puede Proceder con la Operacion...Verificar Almacen, Tipo Operación y/o Cliente');

                }
            }
            }


        } else {
            var table = $('#tabla_doc_referencia').DataTable();
            table.clear().draw();
            var ID_PEDIDO;
            ID_DOCUMENTO_REF = $(this).parents("tr").find("td").eq(0).html();
            ID_UM_PESO = 20;
            //ID_UM_PESO = $('#ddlumpesototal').val();

            var ajax_data = {
                "idDocumento": ID_DOCUMENTO_REF
            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/Listar_Documento_Referencia',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (respuesta) {
                    $('#ddlalmacennuevo').empty();
                    var detalle = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    for (var i = 0; i < detalle.length; i++) {
                        $('#tabla_doc_referencia_final').dataTable(
                            {
                                "searching": false,
                                "paging": false,
                                "info": false,
                                columnDefs: [
                                    {
                                        "targets": [6, 7, 8, 9],
                                        "className": "hide_column"
                                    }
                                ]
                            }
                        ).fnAddData(
                            [detalle[i].documento,
                            detalle[i].doc_Serie + '-' + detalle[i].doc_Codigo,
                            detalle[i].empresa,
                            detalle[i].tienda,
                            detalle[i].almacen,
                            detalle[i].estado,
                            detalle[i].IdDocumento,
                            detalle[i].Idalmacen,
                            detalle[i].tipoOperacion,
                            detalle[i].dcodigo
                            ]);

                        //Llenar combotienda
                        $("#ddltiendanuevo").val(detalle[i].idTienda);

                        //Llenar combo de Serie por tienda
                        var codTienda = detalle[i].idTienda;
                        var codempresa = $('#ddlempresanuevo').val();
                        var ID_TIPO_DOC = 6;
                        var ajax_data = {
                            "COD_TIENDA": codTienda,
                            "COD_EMPRESA": codempresa,
                            "ID_TIPO_DOC": ID_TIPO_DOC
                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/LLenarCboSeriexIdsEmpTienTipoDoc',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                $('#ddlserienuevo').empty();
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                for (var i = 0; i < data.length; i++) {
                                    $('#ddlserienuevo').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>');
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });

                        //Llenar Fechas y Estado 
                        
                        $('#ddlestadonuevo').val(detalle[i].IdEstado);

                        //LLenarAlmacen                 
                        var ajax_data = {
                            "ID_TIENDA": detalle[i].idTienda,
                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenxIdTienda',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].flag_almacen_principal == 'True') {
                                        almacen_principal = data[i].idalmacen;
                                    }
                                    $('#ddlalmacennuevo').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                                }
                                //Obtener Punto de Partida
                                Obtener_Datos_Alm_General();
                                $('body').removeClass('loading'); //Removemos la clase loading

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });
                        $('#ddlalmacennuevo').val(detalle[i].Idalmacen);


                        //Obtener TipoOperacion
                        $('#ddloperacionnuevo').val(detalle[i].tipoOperacion);


                        //Obtener motivo de Translado                    
                        var ID_OPERACION = detalle[i].tipoOperacion;
                        var ajax_data = { "ID_OPERACION": ID_OPERACION };

                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Motivo_Traslado',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                $('#ddlmot_trasladonuevo').empty();
                                for (var i = 0; i < data.length; i++) {
                                    $('#ddlmot_trasladonuevo').append('<option value="' + data[i].IdMotivoT + '">' + data[i].motivo_traslado + '</option>');
                                }
                                $('body').removeClass('loading');
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                            }
                        });
                        $('#ddlmot_trasladonuevo').val(detalle[i].motivoTrans);


                        //Obtener Remitente
                        $('#txtrazon_social_remitente').val(detalle[i].rsocial);
                        $('#txtcodigo_remitente').val(detalle[i].IdCliente);
                        $('#txtdni_remitente').val(detalle[i].dni);
                        $('#txtruc_remitente').val(detalle[i].ruc);


                        //Obtener Destinatario
                        $('#txtrazon_social_destinatario2').val(detalle[i].dnombres);
                        $('#txtcodigo_destinatario2').val(detalle[i].dcodigo);
                        $('#ddltipoalmacen_destinatario2').val(detalle[i].idTipoAlmacen);
                        var almacen = detalle[i].idAlm;

                        var ajax_data = {
                            "COD_ALM_DESTINATARIO": detalle[i].idTipoAlmacen,
                            "COD_EMPRESA": 1

                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenDestinatario_x_Empresa',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {

                                if (almacen == 0) {
                                    //alert('if');
                                    $('#ddlalmacen_destinatario2').val(0);

                                }
                                else {
                                    //alert('else');
                                    $('#ddlalmacen_destinatario2').empty();

                                }
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                for (var i = 0; i < data.length; i++) {
                                    $('#ddlalmacen_destinatario2').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');

                                }
                                $('#ddlalmacen_destinatario2').val(almacen);

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });

                        $('#ddlalmacen_destinatario2').val(detalle[i].idAlm);
                        $('#txtdni_destinatario2').val(detalle[i].ddni);
                        $('#txtruc_destinatario2').val(detalle[i].druc);

                        //Obtener Punto de LLegada
                        $('#txtdireccion_punto_llegada').val(detalle[i].lldireccion);
                        $('#ddldepartamento_punto_llegada').val(detalle[i].idDepartamentoL);

                        var provincia = detalle[i].idProvinciaL;
                        var codDepartamento = $('#ddldepartamento_punto_llegada').val();
                        var ajax_data = { "ID_DEPARTAMENTO": codDepartamento };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Provincia_x_iddepartamento',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                $('#ddlprovincia_punto_llegada').empty();
                                for (var i = 0; i < data.length; i++) {
                                    $('#ddlprovincia_punto_llegada').append('<option value="' + data[i].idprovincia + '">' + data[i].nomprovincia + '</option>');

                                }
                                $('#ddlprovincia_punto_llegada').val(provincia);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                // alert(error.Message);
                                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                            }
                        });



                        var distrito = detalle[i].idDistritoL;
                        var coddepartamento = $('select[id=ddldepartamento_punto_llegada]').val();
                        var codprovincia = detalle[i].idProvinciaL;
                        var ajax_data = {
                            "ID_DEPARTAMENTO": coddepartamento,
                            "ID_PROVINCIA": codprovincia
                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Distrito_x_idprovincia',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            beforeSend: function () {
                                $('body').addClass('loading'); //Agregamos la clase loading al body
                            },
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                $('#ddldistrito_punto_llegada').empty();

                                for (var i = 0; i < data.length; i++) {
                                    $('#ddldistrito_punto_llegada').append('<option value="' + data[i].iddistrito + '">' + data[i].nomdistrito + '</option>');
                                }
                                $('#ddldistrito_punto_llegada').val(distrito);

                                $('body').removeClass('loading'); //Removemos la clase loading
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");

                                swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                            }
                        });




                    }
                    $('body').removeClass('loading'); //Removemos la clase loading
                    $('#modal_ver').modal();
                },


                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }

            });
                                  

            var ajax_data = {
                "idDocumento": ID_DOCUMENTO_REF,
                "idUnidadPeso": ID_UM_PESO
            };

            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/ListarProductoFinal',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (respuesta) {

                    var detalle = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    for (var i = 0; i < detalle.length; i++) {
                        var cantidad;
                        var peso,almacen;
                        cantidad = detalle[i].cantidad;
                        peso = detalle[i].peso;
                        almacen = detalle[i].idAlmacen;

                        if (almacen == 200000002 || almacen == 20000016 || almacen == 200000003 || almacen == 20000011 || almacen == 20000012 || almacen == 20000013 || almacen == 20000014 || almacen == 20090017 || almacen == 300346608 || almacen == 1500000002 || almacen == 20000009 || almacen == 20000010 || almacen ==7) {

                            $('#tabla_producto_final').dataTable().fnDestroy();
                            $('#tabla_producto_final').dataTable(
                                {
                                    "searching": false,
                                    "paging": false,
                                    "info": false,

                                    columnDefs: [
                                        {
                                            "targets": [12, 13, 14, 15, 16],
                                            "className": "hide_column"
                                        }
                                    ]
                                }

                            ).fnAddData(
                                [detalle[i].codigo,
                                detalle[i].nroDocumento,
                                detalle[i].descripcion,
                                detalle[i].UM,
                                "<input type='text' class='cantidad2 form-control' value='" + cantidad + "'>",
                                detalle[i].pendiente,
                                    "<input type='text' class='peso form-control'  style='width :80px; heigth: 1px'  disabled>",
                                detalle[i].stockReal,
                                detalle[i].stockComprometido,
                                detalle[i].stockdisponible,
                                    "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                "<input type='text' class='form-control'  id='" + detalle[i].idDetalleDoc + "'>",
                                detalle[i].equivalencia,
                                detalle[i].equivalencia1,
                                detalle[i].idProducto,
                                detalle[i].idDocumento,
                                detalle[i].idDetalleDoc,
                                    "<img src='../img/eliminarnew.png' class='borrar' data-toggle='tooltip' style='cursor: pointer;height: 20px;width: 20px;text-align: center' >"

                                ]);
                        } else {

                            $('#tabla_producto_final').dataTable().fnDestroy();
                            $('#tabla_producto_final').dataTable(
                                {
                                    "searching": false,
                                    "paging": false,
                                    "info": false,

                                    columnDefs: [
                                        {
                                            "targets": [10,11,12, 13, 14, 15, 16],
                                            "className": "hide_column"
                                        }
                                    ]
                                }

                            ).fnAddData(
                                [detalle[i].codigo,
                                detalle[i].nroDocumento,
                                detalle[i].descripcion,
                                detalle[i].UM,
                                "<input type='text' class='cantidad2 form-control' value='" + cantidad + "'>",
                                detalle[i].pendiente,
                                "<input type='text' class='peso form-control'  style='width :80px; heigth: 1px'  disabled>",
                                detalle[i].stockReal,
                                detalle[i].stockComprometido,
                                detalle[i].stockdisponible,
                                "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                "<input type='text' class='form-control'  id='" + detalle[i].idDetalleDoc + "'>",
                                detalle[i].equivalencia,
                                detalle[i].equivalencia1,
                                detalle[i].idProducto,
                                detalle[i].idDocumento,
                                detalle[i].idDetalleDoc,
                                "<img src='../img/eliminarnew.png' class='borrar' data-toggle='tooltip' style='cursor: pointer;height: 20px;width: 20px;text-align: center' >"

                                ]);


                        }

                      
                        $(".cantidad2").on('keyup', function () {

                            $('#txtpesototal').val('');
                            var EQUIVALENCIA, EQUIVALENCIA1, CANTIDAD, RESULTADO;
                            EQUIVALENCIA = $(this).parents("tr").find("td").eq(12).html();
                            EQUIVALENCIA1 = $(this).parents("tr").find("td").eq(13).html();
                            CANTIDAD = $(this).val();
                            RESULTADO = CANTIDAD * EQUIVALENCIA * EQUIVALENCIA1
                            $(this).parents("tr").find("td").eq(6).find(":input").val(RESULTADO.toFixed(3));
                            var total = 0;
                            $('#tabla_producto_final tbody tr').each(function () {
                                var peso = $(this).find("td").eq(6).find(":input").val();
                                var cantidad = $(this).find("td").eq(4).find(":input").val();
                                if (cantidad > 0) {
                                    total = parseFloat(total) + parseFloat(peso);
                                    $('#txtpesototal').val(total.toFixed(3));
                                }



                            })

                        }).keyup();



                    }
                    $('body').removeClass('loading'); //Removemos la clase loading
                        $('#modal_ver').modal();
                   
                },


                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }

            });

        }
        var table = $('#tabla_producto').DataTable();
        table.clear().draw();
       
    
    });


    //-----------------

    
    $(document).on('click', '.borrar', function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });

   
    $("#btnbuscarproductos_modal").click(function (e) {

        $('#modal_productos').modal();

    });
    $("#btnguardar_tono_seleccionado").click(function (e) {
    
        var id_producto_seleccionado, idtono_seleccionado, cant_tono_seleccionado,detalle_doc;
        var lista_tonos_id = '';         
        var stock;       
     
                 
        $('#tabla_tono_detalle input[type=checkbox]').each(function () {

            if (this.checked) {

                id_producto_seleccionado = $(this).parents("tr").find("td").eq(5).html();
                idtono_seleccionado = $(this).parents("tr").find("td").eq(0).html();
                stock = $(this).parents("tr").find("td").eq(6).html();     
                detalle_doc = $(this).parents("tr").find("td").eq(7).html();  
                cant_tono_seleccionado = $(this).parents("tr").find("td").eq(3).find(":input").val();               
                
                //Primera Validacion AQUI!!!

                if (parseFloat(cant_tono_seleccionado) > stock) {
                    alert('La cantidad que intenta despachar en uno de los productos es mayor al stock disponible.');
                } else {
                    var a = ('id-producto :' + id_producto_seleccionado + '-id_tono :' + idtono_seleccionado + '- cantidad :' + cant_tono_seleccionado);
                    lista_tonos_id = lista_tonos_id + "/" + idtono_seleccionado + ":" + cant_tono_seleccionado;

                    if (detalle_doc !== '') {                    
                      
                        $('#' + detalle_doc).val(lista_tonos_id);

                    } else {                                         
                     
                        $('#' + id_producto_seleccionado).val(lista_tonos_id);
                    }                               
                }
            }
            else { }
        });
        

    });
    function llenarCboTipoExistencia() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Tipo_Existencia',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < data.length; i++) {
                    $('#ddltipoexistencia').append('<option value="' + data[i].idtipo_existencia + '">' + data[i].existencia_nombre + '</option>');
                }
              
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });}
    $("#ddltipoexistencia").change(function () {
        var codtipoexistencia = $('select[id=ddltipoexistencia]').val();
     
        var ajax_data = {
            "ID_TIPO_EXISTENCIA": codtipoexistencia
            
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Linea_x_idexistencia',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $('#ddllinea').empty().append('<option value=0><<-Seleccionar->></option>');

                for (var i = 0; i < data.length; i++) {
                    $('#ddllinea').append('<option value="' + data[i].idlinea + '">' + data[i].Linea + '</option>');
                }
               
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");

                swal("Error! ", error.Message, "error");
            }
        });
    }); 
    $("#ddllinea").change(function () {
        var codtipoexistencia = $('select[id=ddltipoexistencia]').val();
        var codlinea = $('select[id=ddllinea]').val();
        var ajax_data = {
            "ID_TIPO_EXISTENCIA": codtipoexistencia,
            "ID_LINEA": codlinea
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_sublinea_x_Linea_idexistencia',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var data = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $('#ddlsublinea').empty().append('<option value=0><<-Seleccionar->></option>');

                for (var i = 0; i < data.length; i++) {
                    $('#ddlsublinea').append('<option value="' + data[i].idsublinea + '">' + data[i].sublinea + '</option>');
                }

                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");

                swal("Error! ", error.Message, "error");
            }
        });
    }); 
    $("#btnbuscar_producto").click(function (e) {
        var table = $('#tabla_producto').DataTable();
        table.clear().draw();
        var id_existencia, id_linea, id_sublinea, codigo_producto_descripcion;
        id_existencia = $("#ddltipoexistencia").val();
        id_linea = $("#ddllinea").val();
        id_sublinea = $("#ddlsublinea").val();
        codigo_producto = $("#txtcodigo_producto").val();
        descripcion = $("#txtdescripcion_producto").val();
        ID_EMPRESA = $('#ddlempresanuevo').val();
        ID_ALMACEN = $('#ddlalmacennuevo').val();
        PAGE_NUMBER = 0;
        PAGE_SIZE = 20;
        var ajax_data = {
            "ID_EXISTENCIA": id_existencia,
            "ID_LINEA": id_linea,
            "ID_SUBLINEA": id_sublinea,
            "CODIGO_SUBLINEA_BUSCAR_PRODUCTO": "",
            "DESCRIPCION_PRODUCTO": descripcion,
            "ID_ALMACEN": ID_ALMACEN,
            "ID_EMPRESA": ID_EMPRESA,
            "PAGE_NUMBER": PAGE_NUMBER,
            "PAGE_SIZE": PAGE_SIZE,
            "COD_PRODUCTO": codigo_producto
        };

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/buscar_Productos',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_producto").show();
               
                for (var i = 0; i < opvp.length; i++) {
                 
                    $('#tabla_producto').css("width", "400px");
                    $('#tabla_producto').dataTable().fnAddData([
                        opvp[i].codigo_producto,
                        opvp[i].descripcion_producto,
                        opvp[i].um_producto,
                        opvp[i].stock_disponible,
                        "<img src='../img/check1.png'  class='seleccionar_producto' style='height: 20px;width: 20px;text-align: center' >",
                        opvp[i].id_producto,
                        opvp[i].id_um,
                        opvp[i].kit

                    ]);
                }
                $('#tabla_producto').dataTable().fnDestroy();
                $('#tabla_producto').dataTable({
                    columnDefs: [
                        {
                            "targets": [5, 6, 7],
                            "className": "hide_column"
                        }
                    ]
                })
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });
    $("#btneliminarproductos").click(function (e) {
        var table = $('#tabla_producto_final').DataTable();
        table.clear().draw();
        $('#txtpesototal').val('');
    });


    $("#btneliminar_doc_referencia").click(function (e) {
        var table = $('#tabla_doc_referencia_final').DataTable();
        table.clear().draw();
        $('#txtpesototal').val(''); 
        $("#ddltiendanuevo").val(2);
        $("#ddltiendanuevo").change();
        $("#ddloperacionnuevo").val(0);
        LlenarCboMotivoTrasladoxIdTipoOperacion();
        $("#ddlmot_trasladonuevo").val(0);
        $("#ddloperacionnuevo").change();
        $('#txtrazon_social_destinatario2').val('');
        $('#txtcodigo_destinatario2').val('');        
        $('#txtruc_destinatario2').val('');
        $('#txtdireccion_punto_llegada').val('');
        $("#ddltipoalmacen_destinatario2").val(0);
        $("#ddldepartamento_punto_llegada").val(0);
        $("#ddlprovincia_punto_llegada").val(00);
        $("#ddldistrito_punto_llegada").val(00);
        
        $("#tabla_doc_referencia_final tbody").remove();       
        $('#tabla_doc_referencia_final').dataTable().fnDestroy();
        $("#txtfecemisionnuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtiniciotrasladonuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());
      
    });


    $(document).on('click', '.seleccionar_producto', function (e) {     


        //VARIABLES **********************************************************************
        var ID_PRODUCTO, ID_UM, KIT, COD_PRODUCTO, DESCRIPCION_PRODUCTO,
            UM, NRO_DOCUMENTO, CANTIDAD, PENDIENTE, PESO, STOCK_REAL=0, STOCK_COMPROMETIDO=0, STOCK_DISPONIBLE=0, ID_UM_PESO,
            EQUIVALENCIA,EQUIVALENCIA1,ID_EMPRESA,ID_ALMACEN;
        //********************************************************************************


        ID_EMPRESA = $('#ddlempresanuevo').val();
        ID_ALMACEN = $('#ddlalmacennuevo').val();
        ID_PRODUCTO = $(this).parents("tr").find("td").eq(5).html();
        ID_UM = $(this).parents("tr").find("td").eq(6).html();
        KIT = $(this).parents("tr").find("td").eq(7).html();
        COD_PRODUCTO = $(this).parents("tr").find("td").eq(0).html();
        NRO_DOCUMENTO = '';
        DESCRIPCION_PRODUCTO = $(this).parents("tr").find("td").eq(1).html();
        UM = $(this).parents("tr").find("td").eq(2).html();
        CANTIDAD = '';
        PENDIENTE = '';
        ID_UM_PESO = $('#ddlumpesototal').val();



        //OBTENER EQUIVALENCIA********************************************************************************
        var ajax_data1 = {
            "ID_PRODUCTO": ID_PRODUCTO,
            "ID_UM": ID_UM,
            "VARIABLE": 1,
            "ID_MAG_PESO": 1,
            "ID_UM_PESO": ID_UM_PESO
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Equivalencia',
            data: JSON.stringify(ajax_data1),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            beforeSend: function () {
                $('body').addClass('loading'); 
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                EQUIVALENCIA=detalle;
                $('body').removeClass('loading'); //Removemos la clase loading
             
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        //FIN EQUIVALENCIA*************************************************************************************


        //OBTENER EQUIVALENCIA 1 ********************************************************************************
        var ajax_data2 = {
            "ID_PRODUCTO": ID_PRODUCTO,
            "ID_UM": ID_UM,
            "ID_UM_ST": ID_UM,
            "VARIABLE": 1,
           
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Equivalencia1',
            data: JSON.stringify(ajax_data2),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                EQUIVALENCIA1 = detalle;
                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        //FIN EQUIVALENCIA 1      ********************************************************************************


        //STOCK REAL****************************************************************************************************
        var ajax_data3 = {
            "ID_EMPRESA": ID_EMPRESA,
            "ID_ALMACEN": ID_ALMACEN,
            "ID_PRODUCTO": ID_PRODUCTO

        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Stock_Real',
            data: JSON.stringify(ajax_data3),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                STOCK_REAL = detalle;
               
             
                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        //FIN STOCK REAL****************************************************************************************************


        //STOCK COMPROMETIDO****************************************************************************************************
        var ajax_data4 = {
            "ID_EMPRESA": ID_EMPRESA,
            "ID_ALMACEN": ID_ALMACEN,
            "ID_PRODUCTO": ID_PRODUCTO

        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Stock_Comprometido',
            data: JSON.stringify(ajax_data4),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                STOCK_COMPROMETIDO = detalle;
                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
        //FIN STOCK COMPROMETIDO****************************************************************************************************

        //STOCK DISPONIBLE****************************************************************************************************

        STOCK_DISPONIBLE = STOCK_REAL - STOCK_COMPROMETIDO;
        //FIN STOCK DISPONIBLE****************************************************************************************************


        if (KIT == 'True')
        {
            //$('#tabla_producto_final').dataTable().fnAddData([

            //    "<span style='color:red;' >" +COD_PRODUCTO+"</span>",
            //    0,
            //    "<span style='color:red;' >" + DESCRIPCION_PRODUCTO + "</span>",
            //    UM,
            //    "<input type='text' class='cantidad form-control'  >",
            //    0,
            //    "<input type='text' class='peso form-control' style='width :80px; heigth: 1px'  disabled>",
            //    STOCK_REAL,
            //    STOCK_COMPROMETIDO,
            //    STOCK_DISPONIBLE,
            //    "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
            //    "<input type='text' class='form-control'  id='" + ID_PRODUCTO + "'>",
            //    EQUIVALENCIA,
            //    EQUIVALENCIA1,
            //    ID_PRODUCTO,
            //    0,
            //    0,
            //    0
            //]);
        }
        else {


            if (ID_ALMACEN == 200000002 || ID_ALMACEN == 20000016 || ID_ALMACEN == 200000003 || ID_ALMACEN == 20000011 || ID_ALMACEN == 20000012 || ID_ALMACEN == 20000013 || ID_ALMACEN == 20000014 || ID_ALMACEN == 20090017 || ID_ALMACEN == 300346608 || ID_ALMACEN == 1500000002 || ID_ALMACEN == 20000009 || ID_ALMACEN == 20000010 || ID_ALMACEN == 7) {

                $('#tabla_producto_final').dataTable().fnAddData([

                    COD_PRODUCTO,
                    0,
                    DESCRIPCION_PRODUCTO,
                    UM,
                    "<input type='text' class='cantidad form-control'  >",
                    0,
                    "<input type='text' class='peso form-control' style='width :80px; heigth: 1px'  disabled>",
                    STOCK_REAL,
                    STOCK_COMPROMETIDO,
                    STOCK_DISPONIBLE,
                    "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                    "<input type='text' class='form-control'  id='" + ID_PRODUCTO + "'>",
                    EQUIVALENCIA,
                    EQUIVALENCIA1,
                    ID_PRODUCTO,
                    "",
                    "",
                    0
                ]);
                $('#tabla_producto_final').dataTable().fnDestroy();
                $('#tabla_producto_final').dataTable({
                    "searching": false,
                    "paging": false,
                    "info": false,
                    columnDefs: [
                        {
                            "targets": [12, 13, 14, 15, 16, 17],
                            "className": "hide_column"
                        }
                    ]
                })
            }

            else {

                $('#tabla_producto_final').dataTable().fnAddData([

                    COD_PRODUCTO,
                    0,
                    DESCRIPCION_PRODUCTO,
                    UM,
                    "<input type='text' class='cantidad form-control'  >",
                    0,
                    "<input type='text' class='peso form-control' style='width :80px; heigth: 1px'  disabled>",
                    STOCK_REAL,
                    STOCK_COMPROMETIDO,
                    STOCK_DISPONIBLE,
                    "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                    "<input type='text' class='form-control'  id='" + ID_PRODUCTO +"'>",
                    EQUIVALENCIA,
                    EQUIVALENCIA1,
                    ID_PRODUCTO,
                    "",
                    "",
                    0
                ]);
                $('#tabla_producto_final').dataTable().fnDestroy();
                $('#tabla_producto_final').dataTable({
                    "searching": false,
                    "paging": false,
                    "info": false,
                    columnDefs: [
                        {
                            "targets": [10, 11, 12, 13, 14, 15, 16, 17],
                            "className": "hide_column"
                        }
                    ]
                })
            }


        }
        

        //$('#tabla_producto_final').dataTable().fnDestroy();
        //$('#tabla_producto_final').dataTable({
        //    "searching": false,
        //    "paging": false,
        //    "info": false,
        //    columnDefs: [
        //        {
        //            "targets": [12,13,14,15,16,17],
        //            "className": "hide_column"
        //        }
        //    ]
        //})





        if (KIT == 'True')
        {

            var ID_KIT, ID_COMPONENTE, CANT_COMPONENTE, ID_UM_COMPONENTE, COMPONENTE, COD_PROD_COMPONENTE, UM_COMPONENTE,
                ID_MONEDA_COMPONENTE, PRECIO_COMPONENTE, MONEDA_COMPONENTE, PORCENTAJE_KIT;


            var ajax_data5 = {
               
                "ID_PRODUCTO": ID_PRODUCTO

            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Productos_Kit',
                data: JSON.stringify(ajax_data5),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (respuesta) {
                    var data = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    for (var i = 0; i < data.length; i++)
                    {
                        
                        ID_KIT = data[i].idkit;
                        ID_COMPONENTE = data[i].idcomponente;
                        CANT_COMPONENTE = data[i].cantidad_componente;
                        ID_UM_COMPONENTE = data[i].idumcomponente;
                        COMPONENTE = data[i].componente;
                        COD_PROD_COMPONENTE = data[i].codprodcomponente;
                        UM_COMPONENTE = data[i].umcomponente;
                        ID_MONEDA_COMPONENTE = data[i].idmonedacomponente;
                        PRECIO_COMPONENTE = data[i].preciocomponente;
                        MONEDA_COMPONENTE = data[i].monedacomponente;
                        PORCENTAJE_KIT = data[i].porcentajekit;



                        ID_PRODUCTO = ID_COMPONENTE;
                        ID_UM = ID_UM_COMPONENTE;
                        COD_PRODUCTO = COD_PROD_COMPONENTE;
                        NRO_DOCUMENTO = '';
                        DESCRIPCION_PRODUCTO = COMPONENTE;
                        UM = UM_COMPONENTE;
                        CANTIDAD = '';
                        PENDIENTE = '';
                        ID_UM_PESO = $('#ddlumpesototal').val();
                        0;



                        //OBTENER EQUIVALENCIA********************************************************************************
                        var ajax_data1 = {
                            "ID_PRODUCTO": ID_PRODUCTO,
                            "ID_UM": ID_UM,
                            "VARIABLE": 1,
                            "ID_MAG_PESO": 1,
                            "ID_UM_PESO": ID_UM_PESO
                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Equivalencia',
                            data: JSON.stringify(ajax_data1),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: false,
                            beforeSend: function () {
                                $('body').addClass('loading');
                            },
                            success: function (respuesta) {
                                var detalle = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;

                                EQUIVALENCIA = detalle;
                                $('body').removeClass('loading'); //Removemos la clase loading

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });
                        //FIN EQUIVALENCIA*************************************************************************************


                        //OBTENER EQUIVALENCIA 1 ********************************************************************************
                        var ajax_data2 = {
                            "ID_PRODUCTO": ID_PRODUCTO,
                            "ID_UM": ID_UM,
                            "ID_UM_ST": ID_UM,
                            "VARIABLE": 1,

                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Equivalencia1',
                            data: JSON.stringify(ajax_data2),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: false,
                            beforeSend: function () {
                                $('body').addClass('loading');
                            },
                            success: function (respuesta) {
                                var detalle = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;

                                EQUIVALENCIA1 = detalle;
                                $('body').removeClass('loading'); //Removemos la clase loading

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });
                        //FIN EQUIVALENCIA 1      ********************************************************************************


                        //STOCK REAL****************************************************************************************************
                        var ajax_data3 = {
                            "ID_EMPRESA": ID_EMPRESA,
                            "ID_ALMACEN": ID_ALMACEN,
                            "ID_PRODUCTO": ID_PRODUCTO

                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Stock_Real',
                            data: JSON.stringify(ajax_data3),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: false,
                            beforeSend: function () {
                                $('body').addClass('loading');
                            },
                            success: function (respuesta) {
                                var detalle = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;

                                STOCK_REAL = detalle;


                                $('body').removeClass('loading'); //Removemos la clase loading

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });
                        //FIN STOCK REAL****************************************************************************************************


                        //STOCK COMPROMETIDO****************************************************************************************************
                        var ajax_data4 = {
                            "ID_EMPRESA": ID_EMPRESA,
                            "ID_ALMACEN": ID_ALMACEN,
                            "ID_PRODUCTO": ID_PRODUCTO

                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Stock_Comprometido',
                            data: JSON.stringify(ajax_data4),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: false,
                            beforeSend: function () {
                                $('body').addClass('loading');
                            },
                            success: function (respuesta) {
                                var detalle = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;

                                STOCK_COMPROMETIDO = detalle;
                                $('body').removeClass('loading'); //Removemos la clase loading

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });
                        //FIN STOCK COMPROMETIDO****************************************************************************************************

                        //STOCK DISPONIBLE****************************************************************************************************

                        STOCK_DISPONIBLE = STOCK_REAL - STOCK_COMPROMETIDO;
                        //FIN STOCK DISPONIBLE****************************************************************************************************

                        if (ID_ALMACEN == 200000002 || ID_ALMACEN == 20000016 || ID_ALMACEN == 200000003 || ID_ALMACEN == 20000011 || ID_ALMACEN == 20000012 || ID_ALMACEN == 20000013 || ID_ALMACEN == 20000014 || ID_ALMACEN == 20090017 || ID_ALMACEN == 300346608 || ID_ALMACEN == 1500000002 || ID_ALMACEN == 20000009 || ID_ALMACEN == 20000010 || ID_ALMACEN == 7) {

                            $('#tabla_producto_final').dataTable().fnAddData([

                                COD_PRODUCTO,
                                0,
                                DESCRIPCION_PRODUCTO,
                                UM,
                                "<input type='text' class='cantidad form-control'  >",
                                0,
                                "<input type='text' class='peso form-control' style='width :80px; heigth: 1px'  disabled>",
                                STOCK_REAL,
                                STOCK_COMPROMETIDO,
                                STOCK_DISPONIBLE,
                                "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                "<input type='text' class='form-control'  id='" + ID_PRODUCTO + "'>",
                                EQUIVALENCIA,
                                EQUIVALENCIA1,
                                ID_PRODUCTO,
                                0,
                                0,
                                0
                            ]);
                            $('#tabla_producto_final').dataTable().fnDestroy();
                            $('#tabla_producto_final').dataTable({
                                "searching": false,
                                "paging": false,
                                "info": false,
                                columnDefs: [
                                    {
                                        "targets": [12, 13, 14, 15, 16, 17],
                                        "className": "hide_column"
                                    }
                                ]
                            })
                        }

                        else {

                            $('#tabla_producto_final').dataTable().fnAddData([

                                COD_PRODUCTO,
                                0,
                                DESCRIPCION_PRODUCTO,
                                UM,
                                "<input type='text' class='cantidad form-control'  >",
                                0,
                                "<input type='text' class='peso form-control' style='width :80px; heigth: 1px'  disabled>",
                                STOCK_REAL,
                                STOCK_COMPROMETIDO,
                                STOCK_DISPONIBLE,
                                "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                "<input type='text' class='form-control'  id='" + ID_PRODUCTO +  "'>",
                                EQUIVALENCIA,
                                EQUIVALENCIA1,
                                ID_PRODUCTO,
                                0,
                                0,
                                0
                            ]);
                            $('#tabla_producto_final').dataTable().fnDestroy();
                            $('#tabla_producto_final').dataTable({
                                "searching": false,
                                "paging": false,
                                "info": false,
                                columnDefs: [
                                    {
                                        "targets": [10,11,12, 13, 14, 15, 16, 17],
                                        "className": "hide_column"
                                    }
                                ]
                            })
                        }
                       
                       

                    }


                    $('body').removeClass('loading'); //Removemos la clase loading

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });



        }                     
        var table = $('#tabla_producto').DataTable();
        table.clear().draw();

      
          $(".cantidad").on('keyup', function () {
      
            $('#txtpesototal').val('');
            var EQUIVALENCIA, EQUIVALENCIA1,CANTIDAD,RESULTADO;
            EQUIVALENCIA= $(this).parents("tr").find("td").eq(12).html();
            EQUIVALENCIA1 = $(this).parents("tr").find("td").eq(13).html();
            CANTIDAD = $(this).val();
            RESULTADO = CANTIDAD * EQUIVALENCIA * EQUIVALENCIA1
            $(this).parents("tr").find("td").eq(6).find(":input").val(RESULTADO.toFixed(3));          
            var total=0 ;
            $('#tabla_producto_final tbody tr').each(function () {
                var peso = $(this).find("td").eq(6).find(":input").val();
                var cantidad = $(this).find("td").eq(4).find(":input").val();
                if (cantidad > 0)
                {
                    total = parseFloat(total) + parseFloat(peso);               
                    $('#txtpesototal').val(total.toFixed(3));
                }

                

            })
           
    }).keyup();
    
    });   



    //Aqui
    $(document).on('click', '.VerTonos', function (e) {
            CANT_SOLICITADA = $(this).parents("tr").find("td").eq(4).find(":input").val();

            if (CANT_SOLICITADA <= 0 || CANT_SOLICITADA =='')
            {
                swal("¡Ingrese cantidad..!", "", "warning");
            }
            else {

             var table = $('#tabla_tono_detalle').DataTable();
            table.clear().draw();
            var ID_PRODUCTO, ID_ALMACEN, ID_DOCUMENTO, CANT_SOLICITADA, STOCKDISPONIBLE;

            STOCKDISPONIBLE = $(this).parents("tr").find("td").eq(9).html();
            ID_PRODUCTO = $(this).parents("tr").find("td").eq(14).html();
            ID_DETALLE = $(this).parents("tr").find("td").eq(16).html();
            ID_ALMACEN = $('#ddlalmacennuevo').val();
            ID_DOCUMENTO = $(this).parents("tr").find("td").eq(15).html();
            $('#' + ID_PRODUCTO).val('');
            var ajax_data = {
                "ID_PRODUCTO": ID_PRODUCTO,
                "ID_ALMACEN": ID_ALMACEN,
                "ID_DOCUMENTO": ID_DOCUMENTO

            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Picking.asmx/listar_Detalle_Tonos_Orden_Pedido_VP',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading'); //Agregamos la clase loading al body
                },
                success: function (respuesta) {
                    var detalle = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;

                    for (var i = 0; i < detalle.length; i++) {

                        $('#tabla_tono_detalle').dataTable().fnDestroy();
                        $('#tabla_tono_detalle').css("width", "600px");
                        $('#tabla_tono_detalle').dataTable({
                            columnDefs: [
                                {
                                    "targets": [5,6,7],
                                    "className": "hide_column"
                                }
                            ],

                            "paging": false
                        }).fnAddData(
                            [

                                detalle[i].idtono,
                                detalle[i].nomtono,
                                detalle[i].cantidadtono,
                                "<input type='text' class='form-control'  value='" + CANT_SOLICITADA + "'>",
                                "<input type='checkbox' class='minimal' >",
                                ID_PRODUCTO,
                                STOCKDISPONIBLE,
                                ID_DETALLE
                            ]);

                    }

                    $('body').removeClass('loading'); //Removemos la clase loading
                    $('#modal_ver_tono').modal();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });}
            
    }); 

    $("#btn_buscar_transportista").click(function (e) {
        
        $('#ddltipo_persona_transportista_buscar').val(0);
        $('#txtdni_transportista_buscar').val('');
        $('#txtruc_transportista_buscar').val('');
        $('#ddlrol_transportista_buscar').val(4);
        $('#txtrazon_social_transportista_buscar').val('');


        $('#modal_buscar_personal').modal();
    });
    $("#btnbuscar_transportista_detalle").click(function (e) {
    
        var TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL;

        TIPO_PERSONA = $('#ddltipo_persona_transportista_buscar').val();
    
            DNI = $('#txtdni_transportista_buscar').val();
            RUC = $('#txtruc_transportista_buscar').val();
            ROL = $('#ddlrol_transportista_buscar').val();
        RAZON_SOCIAL = $('#txtrazon_social_transportista_buscar').val();

        if (TIPO_PERSONA == 0) {
            swal("¡Seleccione un Tipo de Persona..!", "", "warning");
        }
        else
        {
           
            var table = $('#tabla_transportista').DataTable();
            table.clear().draw();
            var ajax_data = {
                "TIPO_PERSONA": TIPO_PERSONA,
                "DNI": DNI,
                "RUC": RUC,
                "ROL": ROL,
                "RAZON_SOCIAL": RAZON_SOCIAL


            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/listar_Destinatario',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');//Agregamos la clase loading al body
                },
                success: function (respuesta) {
                    var opvp = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    $("#tabla_transportista").show();
                    for (var i = 0; i < opvp.length; i++) {

                        $('#tabla_transportista').dataTable().fnAddData([
                            opvp[i].idpersona,
                            opvp[i].nombre_persona,
                            opvp[i].ruc,
                            opvp[i].dni,
                            opvp[i].nacionalidad,
                            opvp[i].estado,
                            "<img src='../img/check1.png'  class='seleccionar_transportista' style='height: 20px;width: 20px;text-align: center' >"

                        ]);


                    }

                    $('#tabla_transportista').dataTable().fnDestroy();
                    $('#tabla_transportista').dataTable({
                        "searching": false,
                        "paging": false,
                        "info": false
                    });
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }


       




    });   
    $(document).on('click', '.seleccionar_transportista', function (e) {
        var RAZON_SOCIAL, DNI, RUC, ID_TRANSPORTISTA;
        ID_TRANSPORTISTA = $(this).parents("tr").find("td").eq(0).html();
        RAZON_SOCIAL = $(this).parents("tr").find("td").eq(1).html();
        DNI = $(this).parents("tr").find("td").eq(3).html();
        RUC = $(this).parents("tr").find("td").eq(2).html();
        var table = $('#tabla_transportista').DataTable();
        table.clear().draw();
        var ajax_data = {
            "ID_TRANSPORTISTA": ID_TRANSPORTISTA
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_transportista_seleccionado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');//Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
               
                for (var i = 0; i < opvp.length; i++) {

                  
                    $('#txtcodigo_transportista').val(opvp[i].idpersona);
                    $('#txtnombretransportisa').val(opvp[i].descripcion);
                    $('#txtdni_transportista').val(opvp[i].dni);
                    $('#txtruc_transportista').val(opvp[i].ruc);
                    $('#txtdireccion_transportista').val(opvp[i].direccion);
              

                }
                $('body').removeClass('loading'); //Removemos la clase loading
              
                $("#modal_buscar_personal .close").click()
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });   
    $("#btn_buscar_chofer").click(function (e) {
       
        $('#ddltipo_persona_chofer_buscar').val(0);
        $('#txtdni_chofer_buscar').val('');
        $('#txtruc_chofer_buscar').val('');
        $('#ddlrol_chofer_buscar').val(0);
        $('#txtrazon_social_chofer_buscar').val('');

        $('#modal_buscar_chofer').modal();

    });
    $("#btnbuscar_chofer_detalle").click(function (e) {

        var TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL;

        TIPO_PERSONA = $('#ddltipo_persona_chofer_buscar').val();

        DNI = $('#txtdni_chofer_buscar').val();
        RUC = $('#txtruc_chofer_buscar').val();
        ROL = $('#ddlrol_chofer_buscar').val();
        RAZON_SOCIAL = $('#txtrazon_social_chofer_buscar').val();

        if (TIPO_PERSONA == 0) {
            swal("¡Seleccione un Tipo de Persona..!", "", "warning");
        }
        else {

            var table = $('#tabla_chofer').DataTable();
            table.clear().draw();
            var ajax_data = {
                "TIPO_PERSONA": TIPO_PERSONA,
                "DNI": DNI,
                "RUC": RUC,
                "ROL": ROL,
                "RAZON_SOCIAL": RAZON_SOCIAL


            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/listar_Destinatario',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');//Agregamos la clase loading al body
                },
                success: function (respuesta) {
                    var opvp = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    $("#tabla_chofer").show();
                    for (var i = 0; i < opvp.length; i++) {

                        $('#tabla_chofer').dataTable().fnAddData([
                            opvp[i].idpersona,
                            opvp[i].nombre_persona,
                            opvp[i].ruc,
                            opvp[i].dni,
                            opvp[i].nacionalidad,
                            opvp[i].estado,
                            "<img src='../img/check1.png'  class='seleccionar_chofer' style='height: 20px;width: 20px;text-align: center' >"

                        ]);


                    }

                    $('#tabla_chofer').dataTable().fnDestroy();
                    $('#tabla_chofer').dataTable({
                        "searching": false,
                        "paging": false,
                        "info": false
                    });
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }







    });   
    $(document).on('click', '.seleccionar_chofer', function (e) {


        var ID_CHOFER;
        ID_CHOFER = $(this).parents("tr").find("td").eq(0).html();

        var table = $('#tabla_chofer').DataTable();
        table.clear().draw();

        var ajax_data = {
            "ID_CHOFER": ID_CHOFER
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_chofer_seleccionado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');//Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < opvp.length; i++) {


                    $('#txtcodigo_chofer').val(opvp[i].idpersona);
                    $('#txtnombre_chofer').val(opvp[i].descripcion);
                    $('#txtdni_chofer').val(opvp[i].dni);
                    $('#txtruc_chofer').val(opvp[i].ruc);
                    $('#txtlicencia_Chofer').val(opvp[i].nrolicencia);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $("#modal_buscar_chofer .close").click()
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    });   
    $("#btn_buscar_vehiculo").click(function (e) {

        $('#txtplaca_buscar').val('');
       
        $('#modal_buscar_vehiculo').modal();
    });
    $("#btnbuscar_vehiculo_detalle").click(function (e) {

        var PLACA;

        PLACA = $('#txtplaca_buscar').val();

       
            var table = $('#tabla_vehiculo').DataTable();
        table.clear().draw();


            var ajax_data = {
                "PLACA": PLACA
            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/obtener_vehiculos',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');//Agregamos la clase loading al body
                },
                success: function (respuesta) {
                    var opvp = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    $("#tabla_vehiculo").show();
                    for (var i = 0; i < opvp.length; i++) {

                        $('#tabla_vehiculo').dataTable().fnAddData([
                            opvp[i].veh_Placa,
                            opvp[i].veh_Modelo,
                            opvp[i].veh_NConstanciaIns,                           
                            "<img src='../img/check1.png'  class='seleccionar_vehiculo' style='height: 20px;width: 20px;text-align: center' >"

                        ]);


                    }

                    $('#tabla_vehiculo').dataTable().fnDestroy();
                    $('#tabla_vehiculo').dataTable({
                        "searching": false,
                        "paging": false,
                        "info": false
                    });
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        







    });   
    $(document).on('click', '.seleccionar_vehiculo', function (e) {


        var MODELO, CERTIFICADO, PLACA, CODIGO;
      
        MODELO = $(this).parents("tr").find("td").eq(1).html();
        PLACA = $(this).parents("tr").find("td").eq(0).html();
        CERTIFICADO = $(this).parents("tr").find("td").eq(2).html();

       
        $('#txtmodelo_vehiculo').val(MODELO);
        $('#txtplaca_vehiculo').val(PLACA);
        $('#txtcertificado_vehiculo').val(CERTIFICADO);


        var table = $('#tabla_vehiculo').DataTable();
        table.clear().draw();

        $("#modal_buscar_vehiculo .close").click()

    });   
    $("#btn_buscar_agente").click(function (e) {

        $('#ddltipo_persona_agente_buscar').val(0);
        $('#txtdni_agente_buscar').val('');
        $('#txtruc_agente_buscar').val('');
        $('#ddlrol_agente_buscar').val(0);
        $('#txtrazon_social_agente_buscar').val('');

        $('#modal_buscar_agente').modal();
    });
    $("#btnbuscar_agente_detalle").click(function (e) {

        var TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL;

        TIPO_PERSONA = $('#ddltipo_persona_agente_buscar').val();

        DNI = $('#txtdni_agente_buscar').val();
        RUC = $('#txtruc_agente_buscar').val();
        ROL = $('#ddlrol_agente_buscar').val();
        RAZON_SOCIAL = $('#txtrazon_social_agente_buscar').val();

        if (TIPO_PERSONA == 0) {
            swal("¡Seleccione un Tipo de Persona..!", "", "warning");
        }
        else {

            var table = $('#tabla_agente').DataTable();
            table.clear().draw();
            var ajax_data = {
                "TIPO_PERSONA": TIPO_PERSONA,
                "DNI": DNI,
                "RUC": RUC,
                "ROL": ROL,
                "RAZON_SOCIAL": RAZON_SOCIAL


            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/obtener_datos_agentes',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading');//Agregamos la clase loading al body
                },
                success: function (respuesta) {
                    var opvp = (typeof respuesta.d) === 'string' ?
                        eval('(' + respuesta.d + ')') :
                        respuesta.d;
                    $("#tabla_agente").show();
                    for (var i = 0; i < opvp.length; i++) {

                        $('#tabla_agente').dataTable().fnAddData([
                            opvp[i].idpersona,
                            opvp[i].nombre_persona,
                            opvp[i].ruc,
                            opvp[i].dni,
                            opvp[i].nacionalidad,
                            opvp[i].estado,
                            "<img src='../img/check1.png'  class='seleccionar_agente' style='height: 20px;width: 20px;text-align: center' >"

                        ]);


                    }

                    $('#tabla_agente').dataTable().fnDestroy();
                    $('#tabla_agente').dataTable({
                        "searching": false,
                        "paging": false,
                        "info": false
                    });
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }

  


    });   
    $(document).on('click', '.seleccionar_agente', function (e) {


        var RAZON_SOCIAL, DNI, RUC, ID_AGENTE;
        ID_AGENTE = $(this).parents("tr").find("td").eq(0).html();
        RAZON_SOCIAL = $(this).parents("tr").find("td").eq(1).html();
        DNI = $(this).parents("tr").find("td").eq(3).html();
        RUC = $(this).parents("tr").find("td").eq(2).html();







        var table = $('#tabla_agente').DataTable();
        table.clear().draw();

        var ajax_data = {
            "ID_AGENTE": ID_AGENTE
        };
        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_agente_seleccionado',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');//Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < opvp.length; i++) {
                    $('#txtcodigo_agente').val(opvp[i].idpersona);
                    $('#txtnombre_agente').val(opvp[i].descripcion);
                    $('#txtdni_agente').val(opvp[i].dni);
                    $('#txtruc_agente').val(opvp[i].ruc);
                    $('#txtdireccion_agente').val(opvp[i].direccion);
                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $("#modal_buscar_agente .close").click();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    });   

 

    //Prueba corregir 
   
    $("#btnbuscar").click(function (e) {
        var table = $('#tabla_picking').DataTable();
        table.clear().draw();
       //$("#tabla_picking").DataTable().fnDestroy();
        $('#tabla_picking tbody').empty();
        //var table = $('#tabla_picking').DataTable(
        //    {
        //        columnDefs: [
        //            {
        //                "targets": [7, 8],
        //                "className": "hide_column"
        //            }
        //        ]
        //    }
        //);
        //table.clear().draw();
      
       

        var FEC_INICIO, FEC_FIN, IDTIENDA, SERIE, CODIGO;
      
        FEC_INICIO = $('#txtfechaini1').val();
        FEC_FIN = $('#txtfechafin1').val();
        IDTIENDA = $('#ddlTiend').val();
        SERIE = $('#ddlSerie').val();
        CODIGO = $('#txtCodigo').val();
       

        var ajax_data = {    
            "FEC_INICIO": FEC_INICIO,
            "FEC_FIN": FEC_FIN,  
            "IDTIENDA": IDTIENDA,
            "SERIE": SERIE,
            "CODIGO": CODIGO

        };

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/listar_Guia_Remision',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                
                $("#tabla_picking").show();               
                for (var i = 0; i < opvp.length; i++) {                   
                 
                    //$('#tabla_picking').dataTable();
                    $('#tabla_picking').dataTable(
                        //{
                          
                        //    columnDefs: [
                        //        {
                        //            "targets": [7,8],
                        //            "className": "hide_column"
                        //        }
                        //    ]
                        //}
                    ).fnAddData([
                       
                        opvp[i].iddocumento,                        
                        opvp[i].nomtipodocumento,
                        opvp[i].numdoc,
                        opvp[i].fechaemision,                 
                        opvp[i].almacen,                       
                        opvp[i].tipo_operacion,
                        opvp[i].estado,    
                        opvp[i].idserie,
                        opvp[i].Codigo,
                        "<img src='../img/imprimir.png'  class='VerImpresion' style='cursor: pointer;height: 20px;width: 20px;text-align: center'  >",
                        "<img src='../img/binoculars_256.png'  class='VerGuia' style='cursor: pointer;height: 20px;width: 20px;text-align: center'  >"
                       
                    ]);
                  
                   
                }
               $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });




    }); 

    //function SumarColumna(grilla, columna) {

    //    var resultVal = 0.0;

    //    $("#" + grilla + " tbody tr").not(':first').not(':last').each(
    //        function () {

    //            var celdaValor = $(this).find('td:eq(' + columna + ')');

    //            if (celdaValor.val() != null)
    //                resultVal += parseFloat(celdaValor.html().replace(',', '.'));

    //        } //function

    //    ) //each

    //    $("#" + grilla + " tbody tr:last td:eq(" + columna + ")").html(resultVal.toFixed(2).toString().replace('.', ','));
    //    console.log(resultVal);
    //    $('#txtpesototal').val(resultVal);
    //}   




    $(document).on('click', '.VerImpresion', function (e) {
    
        var IDGUIA;
        IDGUIA = $(this).parents("tr").find("td").eq(0).html(); 
        guiaRemision = IDGUIA;

        $('#modal_impresion').modal();
    });


    $(document).on('click', '.VerGuia', function (e) {
        var idserie,codigo,serie;
        idserie = $(this).parents("tr").find("td").eq(7).html(); 
        codigo = $(this).parents("tr").find("td").eq(8).html();
       


        var ajax_data = {

            "IdSerie": idserie,
            "codigo": codigo
        };

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Despacho.asmx/SelectxIdSeriexCodigo',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (respuesta) {
                $('#modal_nuevo').modal();
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;              

                for (var i = 0; i < opvp.length; i++) {
                    
                    $('#ddltiendanuevo').val(opvp[i].IdTienda);           
                    $('#txtfecemisionnuevo').val(opvp[i].doc_FechaEmision);
                    $('#txtiniciotrasladonuevo').val(opvp[i].doc_FechaIniTraslado); 
                    serie = opvp[i].IdSerie;

                    var codTienda = opvp[i].IdTienda;
                    var codempresa = $('#ddlempresanuevo').val();
                    var ID_TIPO_DOC = 6;
                    var ajax_data = {
                        "COD_TIENDA": codTienda,
                        "COD_EMPRESA": codempresa,
                        "ID_TIPO_DOC": ID_TIPO_DOC
                    };
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/LLenarCboSeriexIdsEmpTienTipoDoc',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {
                            $('#ddlprueba').empty();
                            var data = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < data.length; i++) {
                                $('#ddlserienuevo').append('<option value="' + data[i].idserie + '">' + data[i].numserie + '</option>'); 
                                $('#ddlserienuevo').val(serie);
                            }
                          
                        },
                       
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                                                
                    });                                    
                    $('#txtcorrelativo').val(opvp[i].doc_Codigo);
                    $('#ddlestadonuevo').val(opvp[i].IdEstadoDoc);
                    var IdUsuarioComision = opvp[i].IdUsuarioComision;

                    var ID_TIENDA = opvp[i].IdTienda;
                    var idalmacen = opvp[i].IdAlmacen;
                    var motivot;
                    var ajax_data = {
                        "ID_TIENDA": ID_TIENDA
                    };

                    $.ajax({

                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_AlmacenxIdTienda',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading'); //Agregamos la clase loading al body
                        },
                        success: function (respuesta) {
                            $('#ddlalmacennuevo').empty();
                            var data = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;

                            for (var i = 0; i < data.length; i++) {

                                if (data[i].flag_almacen_principal == 'True') {
                                    almacen_principal = data[i].idalmacen;
                                }
                                $('#ddlalmacennuevo').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');                               
                            }
                            $('#ddlalmacennuevo').val(idalmacen);                            
                            $('body').removeClass('loading'); //Removemos la clase loading
                            Obtener_Datos_Alm_General();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });  

                     motivot = opvp[i].IdMotivoT;             

                    $('#ddloperacionnuevo').val(opvp[i].IdTipoOperacion); 

                    var ID_OPERACION = opvp[i].IdTipoOperacion;
                    var ajax_data = { "ID_OPERACION": ID_OPERACION };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Motivo_Traslado',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading'); //Agregamos la clase loading al body
                        },
                        success: function (respuesta) {
                            var data = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            $('#ddlmot_trasladonuevo').empty();
                            for (var i = 0; i < data.length; i++) {
                                $('#ddlmot_trasladonuevo').append('<option value="' + data[i].IdMotivoT + '">' + data[i].motivo_traslado + '</option>');
                                $('#ddlmot_trasladonuevo').val(motivot);
                            }

                            $('body').removeClass('loading'); //Removemos la clase loading
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            // alert(error.Message);
                            swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                        }
                    });

                    var IDOCUMENTO = opvp[i].IdDocumento;
                    var ajax_data = { "IdDocumento": IDOCUMENTO };
                    
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/GuiaDocRef',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {
                           // $('#ddlalmacennuevo').empty();
                        
                            var detalle = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < detalle.length; i++) {
                                $("#tabla_doc_referencia_final").dataTable().fnDestroy();
                                $('#tabla_doc_referencia_final').dataTable(
                                    {
                                        "searching": false,
                                        "paging": false,
                                        "info": false,
                                        columnDefs: [
                                            {
                                                "targets": [6, 7, 8, 9],
                                                "className": "hide_column"
                                            }
                                        ]
                                    }
                                ).fnAddData(
                                    [detalle[i].documento,
                                    detalle[i].doc_Serie + '-' + detalle[i].doc_Codigo,
                                    detalle[i].empresa,
                                    detalle[i].tienda,
                                    detalle[i].almacen,
                                    detalle[i].estado,
                                    0,
                                    0,
                                    0,
                                    0
                                    ]);
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            // alert(error.Message);
                            swal("Error! Obtener la Lista de Documento de Referencia", error.Message, "error");
                        }
                    });   

                    var alm, tipoAlm;
                    var ajax_data = { "IdDocumento": IDOCUMENTO };                  

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/SelectxIdPersona',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {
                            var detalle = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < detalle.length; i++) {
                                if (detalle[i].jur_Rsocial.length > 0) {
                                    $('#txtrazon_social_destinatario2').val(detalle[i].jur_Rsocial)
                                } else {
                                    $('#txtrazon_social_destinatario2').val(detalle[i].Nombre)
                                }                             
                                $('#txtcodigo_destinatario2').val(detalle[i].IdPersona),
                                
                               
                                tipoAlm = detalle[i].IdTipoAlmacen;
                                if (tipoAlm == 0) {
                                    tipoAlm = 1;
                                } else {
                                    tipoAlm = tipoAlm;
                                }

                                $('#ddltipoalmacen_destinatario2').val(tipoAlm),
                                //$('#ddlalmacen_destinatario2').val(detalle[i].IdAlmacen)
                                $('#txtdni_destinatario2').val(detalle[i].Dni)
                                $('#txtruc_destinatario2').val(detalle[i].Ruc)
                              
                                alm = detalle[i].IdAlmacen;                             

                             
                                var ajax_data = {
                                    "TIPO_ALMACEN": tipoAlm,
                                    "ID_DESTINATARIO": 1
                                };
                                $.ajax({

                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Almacen_destinatario',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    beforeSend: function () {
                                        $('body').addClass('loading'); //Agregamos la clase loading al body
                                    },
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;

                                        for (var i = 0; i < data.length; i++) {

                                            $('#ddlalmacen_destinatario2').append('<option value="' + data[i].idalmacen + '">' + data[i].nomalmacen + '</option>');                                        
                                        }
                                        $('#ddlalmacen_destinatario2').val(alm);

                                        $('body').removeClass('loading'); //Removemos la clase loading

                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        alert(error.Message);
                                    }
                                });
                            }
                        }
                    });

                    var ajax_data = { "IdDocumento": IDOCUMENTO };
                    var dist, prov;
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/SelectPuntoLlegada',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                            $('body').addClass('loading'); //Agregamos la clase loading al body
                        },
                        success: function (respuesta) {
                            var data = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;                           
                            for (var i = 0; i < data.length; i++) {
                                $('#txtdireccion_punto_llegada').val(data[i].direccion);
                                $('#ddldepartamento_punto_llegada').val(data[i].iddepartamento);
                                prov = data[i].idprovincia;
                                dist = data[i].iddistrito;


                                var codDepartamento = data[i].iddepartamento;
                                
                                var ajax_data = { "ID_DEPARTAMENTO": codDepartamento };

                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Provincia_x_iddepartamento',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    beforeSend: function () {
                                        $('body').addClass('loading'); //Agregamos la clase loading al body
                                    },
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        $('#ddlprovincia_punto_llegada').empty();
                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddlprovincia_punto_llegada').append('<option value="' + data[i].idprovincia + '">' + data[i].nomprovincia + '</option>');
                                        }
                                      
                                        $('#ddlprovincia_punto_llegada').val(prov);


                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                                        // alert(error.Message);
                                        swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                    }
                                });

                                var ajax_data = {
                                    "ID_DEPARTAMENTO": data[i].iddepartamento,
                                    "ID_PROVINCIA": prov
                                };
                                $.ajax({
                                    type: "POST",
                                    url: '../ServicioWeb/WS_Despacho.asmx/Obtener_Combo_Distrito_x_idprovincia',
                                    data: JSON.stringify(ajax_data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true,
                                    beforeSend: function () {
                                        $('body').addClass('loading'); //Agregamos la clase loading al body
                                    },
                                    success: function (respuesta) {
                                        var data = (typeof respuesta.d) === 'string' ?
                                            eval('(' + respuesta.d + ')') :
                                            respuesta.d;
                                        $('#ddldistrito_punto_llegada').empty();

                                        for (var i = 0; i < data.length; i++) {
                                            $('#ddldistrito_punto_llegada').append('<option value="' + data[i].iddistrito + '">' + data[i].nomdistrito + '</option>');
                                        }
                                      
                                        $('#ddldistrito_punto_llegada').val(dist);

                                        $('body').removeClass('loading'); //Removemos la clase loading
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        var error = eval("(" + XMLHttpRequest.responseText + ")");

                                        swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                                    }
                                });
                                
                            }

                            

                            $('body').removeClass('loading'); //Removemos la clase loading
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");                         
                            swal("Error! Obtener_Combo_Provincia_x_iddepartamento", error.Message, "error");
                        }
                    });
                                       

            // Traer Productos
                   
                    var ajax_data = { "IdDocumento": IDOCUMENTO };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/DETALLE_PRODUCTO_GUIA',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {
                            // $('#ddlalmacennuevo').empty();
                            var peso,total=0.00;
                            var detalle = (typeof respuesta.d) === 'string' ?                              
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < detalle.length; i++) {
                                $("#tabla_producto_final").dataTable().fnDestroy();
                                $('#tabla_producto_final').dataTable(
                                    {
                                        "searching": false,
                                        "paging": false,
                                        "info": false,
                                        columnDefs: [
                                            {
                                                "targets": [12,13,14,15,16,17],
                                                "className": "hide_column"
                                            }
                                        ]
                                    }
                                ).fnAddData(
                                    [detalle[i].codigo,
                                    detalle[i].nroDocumento,
                                    detalle[i].descripcion,
                                    detalle[i].UM,
                                    "<input type='text' class='cantidad form-control' value='" + detalle[i].cantidad+"' >",
                                    detalle[i].Pend,
                                    "<input type='text' class='form-control' value='" + detalle[i].peso + "' >",                                    
                                    detalle[i].stockReal,
                                    detalle[i].stockComprometido,
                                    detalle[i].stockdisponible,
                                    "<img src='../img/ver_doc.png' class='VerTonos' data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                                    "<input type='text' class='form-control'  id='" + detalle[i].idDetalleDoc + "' value='" + detalle[i].Tonos+"'>",                                    
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                
                                    ]);                               
                                peso = detalle[i].peso;                                
                                total = total + peso;
                               
                            }
                            $('#txtpesototal').val(total);                            
                          
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            // alert(error.Message);
                            swal("Error! Obtener la Lista de Documento de Referencia", error.Message, "error");
                        }
                    }); 

                                      
                   

                  
             // Traer Transportista
                    var IdTransportista = opvp[i].IdTransportista;
                    if (IdTransportista !== 0) {
                    var ajax_data = { "ID_TRANSPORTISTA": IdTransportista };
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_transportista_seleccionado',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {                           
                            var data = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < data.length; i++) {
                                $('#txtnombretransportisa').val(data[i].nombre_persona);
                                $('#txtcodigo_transportista').val(data[i].idpersona);
                                $('#txtdni_transportista').val(data[i].dni);
                                $('#txtruc_transportista').val(data[i].ruc);
                                $('#txtdireccion_transportista').val(data[i].direccion);
                            }

                        },

                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }

                    });    
                    }

                   // Obtener Chofer
                    var IdChofer = opvp[i].IdChofer;
                    if (IdChofer !== 0) {
                        var ajax_data = { "ID_CHOFER": IdChofer };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_chofer_seleccionado',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                for (var i = 0; i < data.length; i++) {
                                    $('#txtnombre_chofer').val(data[i].nombre_persona);
                                    $('#txtcodigo_chofer').val(data[i].idpersona);
                                    $('#txtdni_chofer').val(data[i].dni);
                                    $('#txtruc_chofer').val(data[i].ruc);
                                    $('#txtlicencia_Chofer').val(data[i].direccion);
                                }

                            },

                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }

                        });
                    }


                   // Obtener Vehiculo
                    var IdVehiculo = opvp[i].IdVehiculo;
                    if (IdVehiculo !== 0) {
                        var ajax_data = { "IdVehiculo": IdVehiculo };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/OBTENER_VEHICULOSXIDVEHICULO',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                for (var i = 0; i < data.length; i++) {
                                    $('#txtmodelo_vehiculo').val(data[i].veh_Modelo);
                                    $('#txtplaca_vehiculo').val(data[i].veh_Placa);
                                    $('#txtcertificado_vehiculo').val(data[i].veh_NConstanciaIns);

                                }

                            },

                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }

                        });
                    }


                    // Obtener agencia                    
                    var ajax_data = { "ID_AGENTE": IdUsuarioComision };
                    if (IdUsuarioComision !== 0) {                        
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Despacho.asmx/Obtener_datos_agente_seleccionado',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var data = (typeof respuesta.d) === 'string' ?
                                    eval('(' + respuesta.d + ')') :
                                    respuesta.d;
                                for (var i = 0; i < data.length; i++) {

                                    if (data[i].nomcomercial !== '') {
                                        $('#txtnombre_agente').val(data[i].nomcomercial);
                                    } else {
                                        $('#txtnombre_agente').val(data[i].nombre_persona);
                                    }
                                    $('#txtcodigo_agente').val(data[i].idpersona);
                                    $('#txtdni_agente').val(data[i].dni);
                                    $('#txtruc_agente').val(data[i].ruc);
                                    $('#txtdireccion_agente').val(data[i].direccion);


                                }

                            },

                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }

                        });
                    }

                    //Obtener Observaciones
                    var ajax_data = { "IdDocumento": IDOCUMENTO };
                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Despacho.asmx/OBTENER_OBSERVACIONES',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (respuesta) {
                            var data = (typeof respuesta.d) === 'string' ?
                                eval('(' + respuesta.d + ')') :
                                respuesta.d;
                            for (var i = 0; i < data.length; i++) {
                               
                                $('#txtobservaciones').val(data[i].Observacion);                     


                            }

                        },

                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }

                    });
                  
               }               
               
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
                       
    
        });
        $('#btnguardar').attr('disabled', 'disabled');

    });




    $("#btn_impresion1").click(function (e) {    
         window.open('../../Informes/VisorDocumento.aspx?iReporte=6&IdDocumento=' + guiaRemision + '&tipoImpresion=' + 1, 'Remision', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
       });

    $("#btn_impresion2").click(function (e) {

        window.open('../../Informes/VisorDocumento.aspx?iReporte=6&IdDocumento=' + guiaRemision + '&tipoImpresion=' + 4, 'Remision', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
    });

    $("#btn_impresion3").click(function (e) {
        window.open('../../Informes/VisorDocumento.aspx?iReporte=6&IdDocumento=' + guiaRemision + '&tipoImpresion=' + 3, 'Remision', 'resizable=yes,width=1000,height=800,scrollbars=1', null);

    });

    




    $("#btnguardar").click(function (e) {
       
       // $("#modal-content .close").click();
        var row = $('#tabla_doc_referencia_final >tbody >tr').length;
        var rowCount = $('#tabla_producto_final >tbody >tr').length;
        var moverstock = false;
        var validarDespacho = false;
        var usuariocomi = $('#txtcodigo_agente').val() == '' ? '0' : $('#txtcodigo_agente').val();


        var codigo,  serie,  FechaEmision,  FechaIniTraslado,  FechaAentregar, FechaEntrega,  FechaVenc,  
            ImporteTotal,  Descuento,  SubTotal,  Igv,  Total,  TotalLetras,  TotalAPagar, ValorReferencial,  
            Utilidad,  IdPersona,  IdUsuario,  IdTransportista,  IdRemitente,  IdDestinatario, IdEstadoDoc,  
            IdCondicionPago, IdMoneda,  Lugarentrega,  IdTipoOperacion,  IdTienda,  IdSerie,  ExportadoConta, 
            NroVoucherConta, IdEmpresa,  IdChofer,  IdMotivoT,  IdTipoDoc,  IdVehiculo,  IdEstadoCan,  IdEstadoEnt,  
            IdAlmacen, IdTipoPv, IdCaja, IdMedioCredito, doc_CompPercepcion, doc_FechaCancelacion, IdUsuarioComision,
            departamentopp, provinciapp, distritopp, ubigeopp, departamentoPL, provinciaPl, distritoPl, ubigeoPL, direccionpp,
            direccionpL, observaciones, almacenDes;
       


        
            codigo = $('#txtcorrelativo').val();
            serie =  $("#ddlserienuevo option:selected").html();
            FechaEmision = $("#txtfecemisionnuevo").val();
            FechaIniTraslado = $('#txtiniciotrasladonuevo').val();
            FechaAentregar = '';
            FechaEntrega = '';
            FechaVenc = '';
            ImporteTotal = 0;
            Descuento = 0;
            SubTotal = 0;             
            Igv = 0;
            Total = 0;
            TotalLetras = '';
            TotalAPagar = 0;
            ValorReferencial = 0;
            Utilidad = 0;
            IdPersona = $('#txtcodigo_destinatario2').val();
            IdUsuario = 6;
            if ($('#txtcodigo_transportista').val() == '') {
                IdTransportista = 0 ;
             } else {
            IdTransportista = $('#txtcodigo_transportista').val();
            }
             IdRemitente = $('#txtcodigo_remitente').val();
             IdDestinatario = $('#txtcodigo_destinatario2').val();
             IdEstadoDoc = $('#ddlestadonuevo').val();
             IdCondicionPago =0;
             IdMoneda = 0 ;
             Lugarentrega = 0;
             IdTipoOperacion = $('#ddloperacionnuevo').val();
             IdTienda = $('#ddltiendanuevo').val();
             IdSerie = $('#ddlserienuevo').val();
             ExportadoConta = '';
             NroVoucherConta = '';
             IdEmpresa = $('#ddlempresanuevo').val();
             IdChofer = 0;
             IdMotivoT = $('#ddlmot_trasladonuevo').val();
             IdTipoDoc = 6;
             IdVehiculo = 0;
             IdEstadoCan = 0;
             IdEstadoEnt = 2;
             IdAlmacen = $('#ddlalmacennuevo').val();
             IdTipoPv = 0;
             IdCaja = 0;
             IdMedioCredito = 0;
             doc_CompPercepcion = 0;
             doc_FechaCancelacion = '';
             IdUsuarioComision = usuariocomi;
             departamentopp = $('#ddldepartamento_punto_partida').val();
             provinciapp = $('#ddldepartamento_punto_partida').val();
             distritopp = $('#ddldepartamento_punto_partida').val();
             ubigeopp = departamentopp.concat(provinciapp, distritopp);
             departamentoPL = $('#ddldepartamento_punto_llegada').val();
             provinciaPl = $('#ddlprovincia_punto_llegada').val();
             distritoPl=$('#ddldistrito_punto_llegada').val();
             ubigeoPL = departamentoPL.concat(provinciaPl, distritoPl);
             direccionpp = $('#txtdireccion_punto_partida').val();
             direccionpL = $('#txtdireccion_punto_llegada').val();
             almacenDes = $('#ddlalmacen_destinatario2').val();

        if (IdTipoOperacion != 2 && IdTipoOperacion != 5) {
            if (row > 0) {
                moverstock = true;
                validarDespacho = true;
                if (IdTipoOperacion == 4) {
                    validarDespacho = false;
                }
            }
                else {
                    moverstock == true;
                    validarDespacho == false;
                }
            
        }
            else {

                moverstock == true;
                validarDespacho == false;
            }
       

        valores_tonos = new Array();
        $('#tabla_producto_final tbody tr').each(function () {
           
            
            var ID_PICKING = $(this).find("td").eq(15).html().trim();
            var ID_PRODUCTO = $(this).find("td").eq(14).html().trim();
            var U_M = $(this).find("td").eq(3).html().trim();
            var TONO_SELECCIONADO = $(this).find("td").eq(11).find(":input").val();           
            var ID_DETALLE_DOCUMENTO = $(this).find("td").eq(16).html().trim();
            var CANTIDAD = $(this).find("td").eq(4).find(":input").val();
            valor = new Array(
                ID_PICKING, ID_PRODUCTO, U_M, TONO_SELECCIONADO, ID_DETALLE_DOCUMENTO,CANTIDAD
            );
            valores_tonos.push(valor);
        });

        docRef = new Array();
        $('#tabla_doc_referencia_final tbody tr').each(function () {
            var documento = $(this).find("td").eq(6).html().trim();
            doc = new Array(documento);
            docRef.push(doc);
        });


        observaciones = $('#txtobservaciones').val().trim();

        if (row <= 0 && IdTipoOperacion == 1) {

            swal("DEBE INGRESAR DOCUMENTOS DE REFERENCIA", "NO SE PERMITE LA OPERACIÓN.","error");
        }
        else {
           
        if (rowCount <= 30) {

            var ajax_data = {
                "codigo": codigo,
                "serie": serie,
                "FechaEmision": FechaEmision,
                "FechaIniTraslado": FechaIniTraslado,
                "FechaAentregar": FechaAentregar,
                "FechaEntrega": FechaEntrega,
                "FechaVenc": FechaVenc,
                "ImporteTotal": ImporteTotal,
                "Descuento": Descuento,
                "SubTotal": SubTotal,
                "Igv": Igv,
                "Total": Total,
                "TotalLetras": TotalLetras,
                "TotalAPagar": TotalAPagar,
                "ValorReferencial": ValorReferencial,
                "Utilidad": Utilidad,
                "IdPersona": IdPersona,
                "IdUsuario": IdUsuario,
                "IdTransportista": IdTransportista,
                "IdRemitente": IdRemitente,
                "IdDestinatario": IdDestinatario,
                "IdEstadoDoc": IdEstadoDoc,
                "IdCondicionPago": IdCondicionPago,
                
                "IdMoneda": IdMoneda,
                "Lugarentrega": Lugarentrega,
                "IdTipoOperacion": IdTipoOperacion,
                "IdTienda": IdTienda,
                "IdSerie": IdSerie,
                "ExportadoConta": ExportadoConta,
                "NroVoucherConta": NroVoucherConta,
                "IdEmpresa": IdEmpresa,
                "IdChofer": IdChofer,
                "IdMotivoT": IdMotivoT,
                "IdTipoDoc": IdTipoDoc,
                "IdVehiculo": IdVehiculo,
                "IdEstadoCan": IdEstadoCan,
                "IdEstadoEnt": IdEstadoEnt,
                "IdAlmacen": IdAlmacen,
                "IdTipoPv": IdTipoPv,
                "IdCaja": IdCaja,
                "IdMedioCredito": IdMedioCredito,
                "doc_CompPercepcion": doc_CompPercepcion,
                "doc_FechaCancelacion": doc_FechaCancelacion,
                "IdUsuarioComision": IdUsuarioComision,


                "VALORES": valores_tonos,
                "moverStock": moverstock,
                "ValidarDes": validarDespacho,

                "ubigeoPP": ubigeopp,
                "DireccionPP": direccionpp,
                "ubigepPL": ubigeoPL,
                "DireccionPL": direccionpL,
                "observaciones": observaciones,
                "DocRef": docRef,
                "almacenDest": almacenDes



            };
            

            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Despacho.asmx/INSERTAR_DOCUMENTO',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading'); //Agregamos la clase loading al body
                },
                success: function (resultado) {
                  
                    var num = resultado.d;
                    if (num > 0) {
                        $('body').removeClass('loading'); //Removemos la clase loading
                        swal("Exito!", " Guia de Remision Registrada ", "success");

                    }
                    else {
                        swal("Error!", num, "error");
                    }
                    console.log($('#txtcodigo_agente').val());
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });

        }
        else {
            swal('No se Permiten más de 30 productos');
        }

            

        }

        //setTimeout("location.href='GuiaRemision.aspx'", 5000);

        });


   


});