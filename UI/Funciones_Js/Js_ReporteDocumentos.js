﻿$(document).ready(function () {

    var Ruta_Archivo;

    OBTENER_TIENDA();
    OBTENER_TIPO_BANCO();

    $(function fecha_actual() {
        $.datepicker.setDefaults($.datepicker.regional["es"]);


        $("#txtfechaini1").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfechafin1").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

    });

    function OBTENER_TIENDA() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_Tienda',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {

                    $('#BuscaTienda').append('<option value="' + serie[i].IdTienda + '">' + serie[i].Descripcion + '</option>');

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    function OBTENER_TIPO_BANCO() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/OBTENER_TIPO_BANCO',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {

                    $('#BuscaTipoBanco').append('<option value="' + serie[i].IdSerie + '">' + serie[i].doc_Serie + '</option>');

                }
                 
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }



    $("#btnbuscar").click(function (e) {


        var tables = $('#Tabla_Reporte_Documentos').DataTable();
        tables.clear().draw();


        var Tienda = $('select[id=BuscaTienda]').val();
        var fechainicial = $('#txtfechaini1').val();
        var fechafinal = $('#txtfechafin1').val();


        var ajax_data = {
            "Tienda": Tienda,
            "fechainicial": fechainicial,
            "fechafinal": fechafinal
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_DOCUMENTOS_INGRESADOS_FECHAS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#Tabla_Reporte_Documentos").show();
                for (var i = 0; i < opvp.length; i++) {
                    $('#Tabla_Reporte_Documentos').dataTable().fnAddData([
                        opvp[i].TipoDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].doc_ImporteTotal,
                        opvp[i].FechaEmision,
                        opvp[i].Tienda,
                        opvp[i].doc_Total,
                        opvp[i].doc_FechaRegistro,
                        opvp[i].NOMBRE,
                        "<img src='../img/" + opvp[i].EstadoDoc + "'  class='VerTonos'  data-toggle='tooltip' title='" + opvp[i].Semaforo + "' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >",
                       opvp[i].Almacen 
                    ]);
                }

               



                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    });

    //$("#VerTonos").click(function (e) {

    $(document).on('click', '.ver', function (e) {

        Ruta_Archivo = $(this).parents("tr").find("td").eq(10).html()

        var ajax_data = {
            "Ruta_Archivo": Ruta_Archivo
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#imagen").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


        //debugger;



        //$.ajax({
        //    url: '../ServicioWeb/WS_Contabilidad.asmx/ObtenerArchivoGuardado',
        //    type: 'POST',
        //    //data: param,
        //    //data: JSON.stringify(ajax_data),
        //    cache: false,
        //    contentType: false,
        //    processData: false,

        //    success: function (result) {
        //        debugger;
        //        console.log(result);
        //        $("#imagen").attr("src", "data:image/jpg;base64, " + result);
        //    }, error: function (r) {
        //        alert(r.d);
        //    }
        //});

        $('#modal_nuevo').modal();

    });


    //function Cargar_Archivo() {
    //    //var param = new FormData();
    //    //param.append("Archivo", "21023304972412022.jpg");


    //}

    $("#btnbuscarReporte").click(function (e) {

        //<table class="table table-hover  table-striped" id="Tabla_IZIPAY">
        //</table>

        //    <table class="table table-hover  table-striped" id="Tabla_AMEX">
        //    </table>
        //    <table class="table table-hover  table-striped" id="Tabla_DINERS">
        //    </table>

        //    <table class="table table-hover  table-striped" id="Tabla_NIUBIZ">
        //    </table>


        var IZIPAY = $('#Tabla_IZIPAY').DataTable();
        IZIPAY.clear().draw();
        var AMEX = $('#Tabla_AMEX').DataTable();
        AMEX.clear().draw();
        var DINERS = $('#Tabla_DINERS').DataTable();
        DINERS.clear().draw();
        var NIUBIZ = $('#Tabla_NIUBIZ').DataTable();
        NIUBIZ.clear().draw();


        var Tienda = $('select[id=BuscaTienda]').val();
        var fechainicial = $('#txtfechaini1').val();
        var fechafinal = $('#txtfechafin1').val();


        var ajax_data = {
            "Tienda": Tienda,
            "fechainicial": fechainicial,
            "fechafinal": fechafinal
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_DOCUMENTOS_AMEX',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#Tabla_AMEX").show();
                for (var i = 0; i < opvp.length; i++) {
                    $('#Tabla_AMEX').dataTable().fnAddData([
                        opvp[i].TipoDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].doc_ImporteTotal,
                        opvp[i].FechaEmision,
                        opvp[i].Tienda,
                        opvp[i].doc_Total,
                        opvp[i].doc_FechaRegistro,
                        opvp[i].NOMBRE,
                        "<img src='../img/" + opvp[i].EstadoDoc + "'  class='VerTonos'  data-toggle='tooltip' title='" + opvp[i].Semaforo + "' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >",
                        opvp[i].Almacen
                    ]);
                }





                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    });

    
});