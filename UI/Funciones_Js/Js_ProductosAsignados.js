﻿$(document).ready(function () {

    LISTAR_PRODUCTOS_ASIGNADOS();

  function LISTAR_PRODUCTOS_ASIGNADOS() {

      var table = $('#tabla_productos_asignados').DataTable();

        table.clear().draw();
   
      var IDUSUARIO;
      IDUSUARIO = $.session.get('id');

        var ajax_data = {
            "IDUSUARIO": IDUSUARIO

        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Listar_Productos_Asignados',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_productos_asignados").show();
              
                for (var i = 0; i < opvp.length; i++) {
                  
                    $('#tabla_productos_asignados').dataTable().fnDestroy();
                    $('#tabla_productos_asignados').css("width","800px");
                    $('#tabla_productos_asignados').dataTable({
                        columnDefs: [
                            {                                    
                                "targets": [0,1,2,3,7,8,9,10,11],
                                "className": "hide_column",                               
                                "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                                "iDisplayLength": 25

                            },
                             {
                                
                                "targets": [4,15],                                
                                 "width": "3px"
                           }
                        ]
                    }).fnAddData([

                        opvp[i].NroDocumento,
                        opvp[i].IdDocumento,
                        opvp[i].IdDetalleDocumento,
                        opvp[i].IdProducto,
                        opvp[i].prod_Codigo,
                        opvp[i].prod_Nombre,
                        opvp[i].Cantidad,
                        opvp[i].UM,
                        opvp[i].Zona,
                        opvp[i].Linea,
                        opvp[i].SubLinea,
                        opvp[i].cantidad_final,
                        opvp[i].doc_referencia,
                        opvp[i].Tie_Ref,
                        opvp[i].obsRef,
                        "<img src='../img/ver_doc.png'  class='ver'  data-toggle='tooltip' title='Editar..!' style='height: 20px;width: 20px;text-align: center' >"
                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    } 

    $(document).on('click', '.ver', function (e) {
        $('#modal_ver').modal();

        var table = $('#tabla_picking_detalle').DataTable();
        table.clear().draw();
        var ID_DOCUMENTO, ID_DETALLEDOCUMENTO, ID_PRODUCTO, CODIGOPRODUCTO, NOMPRODUCTO, NRODOC, ZONA, LINEA, SUBLINEA, CANTIDAD,
            UM,CANTIDAD_FINAL;
        NRODOC = $(this).parents("tr").find("td").eq(0).html();
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(1).html();
        ID_DETALLEDOCUMENTO = $(this).parents("tr").find("td").eq(2).html();
        ID_PRODUCTO = $(this).parents("tr").find("td").eq(3).html();
        CODIGOPRODUCTO = $(this).parents("tr").find("td").eq(4).html();
        NOMPRODUCTO = $(this).parents("tr").find("td").eq(5).html();

        ZONA = $(this).parents("tr").find("td").eq(8).html();
        LINEA = $(this).parents("tr").find("td").eq(9).html();
        SUBLINEA = $(this).parents("tr").find("td").eq(10).html();
        CANTIDAD = $(this).parents("tr").find("td").eq(6).html();
        UM = $(this).parents("tr").find("td").eq(7).html();
        CANTIDAD_FINAL = $(this).parents("tr").find("td").eq(11).html();

        $('#txtid_detalle_documento').val(ID_DETALLEDOCUMENTO);
        $('#txtnropicking').val(NRODOC);
        $('#txtzona').val(ZONA);
        $('#txtlinea').val(LINEA);
        $('#txtsublinea').val(SUBLINEA);
        $('#txtcantidad').val(CANTIDAD);
        $('#txtum').val(UM);
        $('#txtcantidadpicking').val(CANTIDAD_FINAL);
        $('#txtcantidadpendiente').val(CANTIDAD_FINAL);  

    });


    $("#btnguardar").click(function (e) {

        var ID_DETALLEDOCUMENTO, CANTIDAD_ENCONTRADA, ID;
        ID_DETALLEDOCUMENTO = $('#txtid_detalle_documento').val();
        CANTIDAD_ENCONTRADA = $('#txtcantidadpicking').val();
        ID = $.session.get('id');
      

        var ajax_data = {
            "ID_DETALLEDOCUMENTO": ID_DETALLEDOCUMENTO,
            "CANTIDAD_ENCONTRADA": CANTIDAD_ENCONTRADA,
            "IDUSUARIO": ID
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Actualizar_Picking_Productos_Asignados',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                if (num > 0) {

                    swal("Exito!", " La Ubicacion se Grabo Satisfactoriamente ", "success");
                    LISTAR_PRODUCTOS_ASIGNADOS();
                }
                else {

                    swal("Error!", " La Ubicacion no se Grabo  ", "error");

                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $("#modal_ver .close").click()
             

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });





    });



  });