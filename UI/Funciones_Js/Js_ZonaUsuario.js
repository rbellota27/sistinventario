﻿$(document).ready(function () {
  
    OBTENER_ZONA();
    OBTENER_USUARIOS();
    $("#btnnuevo").click(function (e) {
      
        $('#modal_nuevo').modal();
        $('#DivEstado').hide();

        $('#txtidzona_usuario').val('');
        $('#ddlzonanuevo').val(0);
        $('#ddlusuarionuevo').val(0);
        $('#ddlestadonuevo').val(0);
        $('#btnguardar').show();
        $('#btnactualizar').hide();
        $('.selectpicker').selectpicker('refresh');       


        
    });
    function OBTENER_ZONA() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Zona',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {


                    //$('#ddlzona').append('<option value="' + serie[i].IdZona + '">' + serie[i].DescripcionZona + '</option>');
                    $('#ddlzonanuevo').append('<option value="' + serie[i].IdZona + '">' + serie[i].DescripcionZona + '</option>');
                    $('#ddlzonabuscar').append('<option value="' + serie[i].IdZona + '">' + serie[i].DescripcionZona + '</option>');
                }
                $('.selectpicker').selectpicker('refresh');
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }
    function OBTENER_USUARIOS() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Usuarios',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {


                    //$('#ddlzona').append('<option value="' + serie[i].IdZona + '">' + serie[i].DescripcionZona + '</option>');
                    $('#ddlusuarionuevo').append('<option value="' + serie[i].IdUsuario + '">' + serie[i].DescripcionUsuario + '</option>');
                    $('#ddlusuariobuscar').append('<option value="' + serie[i].IdUsuario + '">' + serie[i].DescripcionUsuario + '</option>');
                }
                $('.selectpicker').selectpicker('refresh');
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    } 
    function LISTAR_ZONA_USUARIO() {

        var table = $('#tabla_zona_usuario').DataTable();

        table.clear().draw();

        var IDUSUARIO,  IDZONA,ESTADO;
        IDUSUARIO = $('#ddlusuariobuscar').val();        
        IDZONA = $('#ddlzonabuscar').val();
        ESTADO = $('#ddlestadobuscar').val();
        var ajax_data = {
            "IDUSUARIO": IDUSUARIO,
            "IDZONA": IDZONA,
            "ESTADO": ESTADO

        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Listar_Zona_Usuario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_zona_usuario").show();
                for (var i = 0; i < opvp.length; i++) {


                    $('#tabla_zona_usuario').dataTable().fnAddData([
                        opvp[i].IdZona_Usuario,
                        opvp[i].DescripcionZona,
                        opvp[i].DescripcionUsuario,
                        opvp[i].DescripcionEstado,
                      

                        "<img src='../img/editar_2.png'  class='editar'  data-toggle='tooltip' title='Editar..!' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/eliminarnew.png'  class='eliminar'  data-toggle='tooltip' title='Eliminar..!' style='height: 20px;width: 20px;text-align: center' >"



                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    } 
    $("#btnbuscar").click(function (e) {
        LISTAR_ZONA_USUARIO();

    }); 
    $("#btnactualizar").click(function (e) {

        var ID_ZONA_USUARIO, IDUSUARIO, IDZONA, IDESTADO;
        ID_ZONA_USUARIO = $('#txtidzona_usuario').val();       
        IDUSUARIO = $('#ddlusuarionuevo').val();       
        IDZONA = $('#ddlzonanuevo').val();
        IDESTADO = $('#ddlestadonuevo').val();
        var ajax_data = {
            "ID_ZONA_USUARIO": ID_ZONA_USUARIO,
            "IDUSUARIO": IDUSUARIO,
            "IDZONA": IDZONA,
            "IDESTADO": IDESTADO

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Actualizar_Zona_Usuario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                if (num > 0) {

                    swal("Exito!", " El Registro se Actualizo Satisfactoriamente ", "success");
                    LISTAR_ZONA_USUARIO();
                }
                else {

                    swal("Error!", " El Registro no se Actualizo  ", "error");

                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });





    });
    $("#btnguardar").click(function (e) {

        var IDUSUARIO, IDZONA;
        IDUSUARIO = $('#ddlusuarionuevo').val();
        IDZONA = $('#ddlzonanuevo').val();

        var ajax_data = {
            "IDUSUARIO": IDUSUARIO,
            "IDZONA": IDZONA

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Grabar_Zona_Usuario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                if (num > 0) {

                    swal("Exito!", " El Registro se Grabo Satisfactoriamente ", "success");
                     LISTAR_ZONA_USUARIO();
                }
                else {

                    swal("Error!", " El Registro no se Grabo  ", "error");

                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });





    });
    $(document).on('click', '.editar', function (e) {

        $('#btnguardar').hide();
        $('#btnactualizar').show();
        $('#DivEstado').show();

        var ID_ZONA_USUARIO;
        ID_ZONA_USUARIO = $(this).parents("tr").find("td").eq(0).html();

        var ajax_data = {
            "ID_ZONA_USUARIO": ID_ZONA_USUARIO
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Ubicacion.asmx/Obtener_Zona_Usuario',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < detalle.length; i++) {


                        $('#txtidzona_usuario').val(detalle[i].IdZona_Usuario),
                        $('#ddlzonanuevo').val(detalle[i].IdZona),
                        $('#ddlusuarionuevo').val(detalle[i].IdUsuario),
                        $('#ddlestadonuevo').val(detalle[i].Estado)
                }
              
                $('.selectpicker').selectpicker('refresh');

                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_nuevo').modal();
            },


            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }


        });





    });
    $(document).on('click', '.eliminar', function (e) {
        var ID_ZONA_USUARIO;
        ID_ZONA_USUARIO = $(this).parents("tr").find("td").eq(0).html();

            swal({
                title: "¿Estás seguro?",
                text: "Se anulara el Registro ",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, anularlo!",
                cancelButtonText: "No, cancelar!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {

                        var ajax_data = {
                            "ID_ZONA_USUARIO": ID_ZONA_USUARIO
                        };

                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Ubicacion.asmx/Eliminar_Zona_Usuario',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            success: function (respuesta) {
                                var num = respuesta.d;

                                swal("Anulado!", "El Registro se Anuló.", "success");
                                LISTAR_ZONA_USUARIO();
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });





                    } else {
                        swal("Cancelado", "El Registro no se Anuló.", "error");
                    }
                });

        
    });





});