﻿$(document).ready(function () {

    LISTAR_DOCUMENTOS();


    $("#btnBuscarNuevo").click(function (e) {

        LISTAR_DOCUMENTOS();

    });

    function LISTAR_DOCUMENTOS() {

        var table = $('#tabla_Documentos').DataTable();
        table.clear().draw();

        var Doc_Serie, Doc_Codigo, IdTipoDocumento;

        IdTipoDocumento = $('select[id=IdTipoDocumentoNuevo]').val();
        Doc_Serie = document.getElementById('idSerieNuevo').value;//$('#idSerieNuevo').val();
        Doc_Codigo = $('#txtNumeroDocNuevo').val();

        var ajax_data = {
            "Doc_Serie": Doc_Serie,
            "Doc_Codigo": Doc_Codigo,
            "TipoDocumento": IdTipoDocumento

        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos").show();
                for (var i = 0; i < opvp.length; i++) {


                    $('#tabla_Documentos').dataTable().fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].TipoDocumento,
                        opvp[i].doc_Codigo,
                        opvp[i].FechaEmision,
                        opvp[i].doc_ImporteTotal,
                        opvp[i].Tienda,
                        opvp[i].Cliente,
                        opvp[i].TipoOperacion


                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }

    $("#btnguardar").click(function (e) {

        var Tipo, IdDocumento, Monto, Ruta, IdUsuario;

        Tipo = "I";
        IdDocumento = $(this).parents("tr").find("td").eq(0).html();

        IDALMACEN = $('#ddlalmacen').val();
        NOMBREUBICACION = $('#txtubicacion').val();
        IDLINEA = $('#ddllinea').val();
        IDSUBLINEA = $('#ddlsublinea').val();
        IDZONA = $('#ddlzona').val();



        var ajax_data = {
            "IDALMACEN": IDALMACEN,
            "NOMBREUBICACION": NOMBREUBICACION,
            "IDLINEA": IDLINEA,
            "IDSUBLINEA": IDSUBLINEA,
            "IDZONA": IDZONA

        };
        //obj_BL_Documento.GUARDAR_REGISTROS_DOCUMENTOS(Tipo, IdDocumento, Monto, Ruta, IdUsuario)

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                if (num > 0) {

                    swal("Exito!", " La Ubicacion se Grabo Satisfactoriamente ", "success");
                    LISTAR_UBICACION();
                }
                else {

                    swal("Error!", " La Ubicacion no se Grabo  ", "error");

                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

    });


});