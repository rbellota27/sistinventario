﻿$(document).ready(function () {

  

    Obtener_Estados();
    Muestra_Datos_Ingresado();

    function Obtener_Estados() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/OBTENER_ESTADO',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {

                    $('#idEstado').append('<option value="' + serie[i].Campo1 + '">' + serie[i].Campo2 + '</option>');

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }


    function Muestra_Datos_Ingresado() {

        var table = $('#tabla_Documentos_Aprobar').DataTable({ retrieve: true, paging: false });
        table.clear().draw();
        table.destroy();


        var Tipo = "3";
        var Estado = "X";

        var ajax_data = {
            "Tipo": Tipo,
            "Estado": Estado
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/LISTAR_DOCUMENTOS_EXTORNOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos_Aprobar").show();
                for (var i = 0; i < Filas.length; i++) {
                    $('#tabla_Documentos_Aprobar').dataTable().fnDestroy();
                    $('#tabla_Documentos_Aprobar').dataTable().fnAddData([
                        Filas[i].Campo1,
                        Filas[i].Campo2,
                        Filas[i].Campo3,
                        Filas[i].Campo4,
                        Filas[i].Campo5,
                        Filas[i].Campo6,
                        Filas[i].Campo7,
                        Filas[i].Campo8,
                        Filas[i].Campo9,
                        Filas[i].Campo10,
                        Filas[i].Campo11,
                        Filas[i].Campo12,
                        Filas[i].Campo13,
                        Filas[i].Campo14,
                        Filas[i].Campo15,
                        Filas[i].Campo16,
                        Filas[i].Campo17,
                        Filas[i].Campo18,
                        Filas[i].Campo19,
                        Filas[i].Campo20,
                        "<img src='../img/report.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='VerDetalle' style='height: 20px;width: 20px;text-align: center' >",
                        Filas[i].Campo21,
                        Filas[i].Campo22

                    ]);
                }

                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    }

    $("#btnExportar").click(function (e) {
        fnExcelReport();
    });


    $("#BtnBuscar").click(function (e) {
        Muestra_Datos_Condicion();
    });

    function Muestra_Datos_Condicion() {
        //$('#tabla_Documentos_Aprobar').DataTable().fnDestroy();
        //var table = $('#tabla_Documentos_Aprobar').DataTable();
        //table.clear().draw();

        var table = $('#tabla_Documentos_Aprobar').DataTable({ retrieve: true, paging: false });
        table.clear().draw();
        table.destroy();


        var Estado = $('select[id=idEstado]').val();

        var Tipo = "3";
        //var Estado = "X";

        var ajax_data = {
            "Tipo": Tipo,
            "Estado": Estado
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/LISTAR_DOCUMENTOS_EXTORNOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                $("#tabla_Documentos_Aprobar").show();
                for (var i = 0; i < Filas.length; i++) {
                    $('#tabla_Documentos_Aprobar').dataTable().fnDestroy();
                    $('#tabla_Documentos_Aprobar').dataTable(


                    ).fnAddData([
                        Filas[i].Campo1,
                        Filas[i].Campo2,
                        Filas[i].Campo3,
                        Filas[i].Campo4,
                        Filas[i].Campo5,
                        Filas[i].Campo6,
                        Filas[i].Campo7,
                        Filas[i].Campo8,
                        Filas[i].Campo9,
                        Filas[i].Campo10,
                        Filas[i].Campo11,
                        Filas[i].Campo12,
                        Filas[i].Campo13,
                        Filas[i].Campo14,
                        Filas[i].Campo15,
                        Filas[i].Campo16,
                        Filas[i].Campo17,
                        Filas[i].Campo18,
                        Filas[i].Campo19,
                        Filas[i].Campo20,
                        "<img src='../img/report.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='VerDetalle' style='height: 20px;width: 20px;text-align: center' >",
                        Filas[i].Campo21,
                        Filas[i].Campo22

                    ]);
                }

                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    }

    $(document).on('click', '.VerImagen', function (e) {

      

        $("#ImagenMostrar").attr('src', '');

        var FechaImagen = "", rutaimg = "";

        //alert(str.substr(0, str.length - 3));

        FechaImagen = $(this).parents("tr").find("td").eq(5).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(22).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'




        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#ImagenMostrar").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });




        $('#modal_Imagen').modal();

    });

    $(document).on('click', '.VerDetalle', function (e) {
        //debugger;

        var amex = $('#tabla_Documentos_Amex').DataTable();
        amex.clear().draw();

        var dinners = $('#tabla_Documentos_Dinners').DataTable();
        dinners.clear().draw();

        var IzipayTab = $('#tabla_Documentos_Izipay').DataTable();
        IzipayTab.clear().draw();

        var NIUBIZ = $('#tabla_Documentos_NIUBIZ').DataTable();
        NIUBIZ.clear().draw();
        

        var iddocumento = $(this).parents("tr").find("td").eq(22).html()
        var Tipo = $(this).parents("tr").find("td").eq(23).html()


        if (Tipo == "0") {
            swal("Error!", "No existe registros para mosrar", "error");
            return false
        }

        
        if (Tipo == "1") {
            $('#Amex').show();
            $('#Dinners').hide();
            $('#Izipay').hide();
            $('#NIUBIZ').hide();
        }
        if (Tipo == "2") {
            $('#Amex').hide();
            $('#Dinners').show();
            $('#Izipay').hide();
            $('#NIUBIZ').hide();
        }
        if (Tipo == "3") {
            $('#Amex').hide();
            $('#Dinners').hide();
            $('#NIUBIZ').hide();
            $('#Izipay').show();
        }
        if (Tipo == "4") {
            $('#Amex').hide();
            $('#Dinners').hide();
            $('#NIUBIZ').show();
            $('#Izipay').hide();
        }

        //var Estado = "X";
        
        var ajax_data = {
            "Tipo": Tipo,
            "IdDocum": iddocumento
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Reporteria.asmx/LISTAR_DOCUMENTOS_EXTORNOS_DETALLE',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;


                if (Tipo == "1") {
                    $("#tabla_Documentos_Amex").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_Amex').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18

                        ]);
                    }
                }
                if (Tipo == "2") {
                    $("#tabla_Documentos_Dinners").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_Dinners').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18,
                            Filas[i].Campo19,
                            Filas[i].Campo20,
                            Filas[i].Campo21,
                            Filas[i].Campo22,
                            Filas[i].Campo23,
                            Filas[i].Campo24,
                            Filas[i].Campo25,
                            Filas[i].Campo26,
                            Filas[i].Campo27

                        ]);
                    }
                }
                if (Tipo == "3") {
                    $("#tabla_Documentos_Izipay").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_Izipay').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18,
                            Filas[i].Campo19,
                            Filas[i].Campo20,
                            Filas[i].Campo21,
                            Filas[i].Campo22,
                            Filas[i].Campo23,
                            Filas[i].Campo24,
                            Filas[i].Campo25,
                            Filas[i].Campo26,
                            Filas[i].Campo27,
                            Filas[i].Campo28,
                            Filas[i].Campo29,
                            Filas[i].Campo30

                        ]);
                    }
                }
                if (Tipo == "4") {
                    $("#tabla_Documentos_NIUBIZ").show();
                    for (var i = 0; i < Filas.length; i++) {
                        //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                        $('#tabla_Documentos_NIUBIZ').dataTable().fnAddData([
                            Filas[i].Campo1,
                            Filas[i].Campo2,
                            Filas[i].Campo3,
                            Filas[i].Campo4,
                            Filas[i].Campo5,
                            Filas[i].Campo6,
                            Filas[i].Campo7,
                            Filas[i].Campo8,
                            Filas[i].Campo9,
                            Filas[i].Campo10,
                            Filas[i].Campo11,
                            Filas[i].Campo12,
                            Filas[i].Campo13,
                            Filas[i].Campo14,
                            Filas[i].Campo15,
                            Filas[i].Campo16,
                            Filas[i].Campo17,
                            Filas[i].Campo18,
                            Filas[i].Campo19,
                            Filas[i].Campo20,
                            Filas[i].Campo21,
                            Filas[i].Campo22,
                            Filas[i].Campo23,
                            Filas[i].Campo24,
                            Filas[i].Campo25,
                            Filas[i].Campo26,
                            Filas[i].Campo27,
                            Filas[i].Campo28,
                            Filas[i].Campo29,
                            Filas[i].Campo30

                        ]);
                    }
                }
                



                $('body').removeClass('loading'); //Removemos la clase loading
                Muestra_Datos_Ingresado();
                $('#modal_Detalle').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

       
    });

    $('#tabla_Documentos_Aprobar').dataTable({
        "bAutoWidth": false
    });


    function fnExcelReport() {
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('tabla_Documentos_Aprobar'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

});