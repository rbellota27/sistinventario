﻿$(document).ready(function () {
    var iddocum, ID_DOCUMENTO;

    Muestra_Datos_Ingresado();



    function Muestra_Datos_Ingresado() {

        var table = $('#tabla_Documentos_Aprobar').DataTable();
        table.clear().draw();


        var Tipo = "0";
        var Tienda = "0";

        var ajax_data = {
            "Tipo": Tipo,
            "Tienda": Tienda,
            "IdUsuario": $.session.get('id')
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_SOLICITUD_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos_Aprobar").show();
                for (var i = 0; i < Filas.length; i++) {
                    $('#tabla_Documentos_Aprobar').dataTable().fnDestroy();
                    $('#tabla_Documentos_Aprobar').dataTable(
                        {
                            "bAutoWidth": false
                        }

                        //{
                        //    columnDefs: [
                        //        {
                        //            "targets": [11],
                        //            "className": "hide_column"

                        //        },
                        //    ]
                        //}


                    ).fnAddData([
                        Filas[i].IdDocumento,
                        Filas[i].TipoDocumento,
                        Filas[i].NroDocumento,
                        Filas[i].FechaEmision,
                        Filas[i].doc_ImporteTotal,
                        Filas[i].Tienda,
                        Filas[i].Cliente,
                        Filas[i].doc_Total,
                        Filas[i].doc_FechaRegistro,
                        Filas[i].Observacion,
                        Filas[i].NOMBRE,
                        //"<img src='../img/" + Filas[i].Semaforo + "'  class='VerTonos'  data-toggle='tooltip' title='" + Filas[i].EstadoDoc + "' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/check_2.png'  class='Aprobar' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/eliminarnew.png'  class='Rechazar' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >"
                    ]);
                }

                $('body').removeClass('loading'); //Removemos la clase loading



            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    }

    $(document).on('click', '.VerImagen', function (e) {

        $("#ImagenMostrar").attr('src', '');

        var FechaImagen="", rutaimg="";

        //alert(str.substr(0, str.length - 3));

        FechaImagen = $(this).parents("tr").find("td").eq(8).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(0).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'

        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#ImagenMostrar").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

        $('#modal_Imagen').modal();

    });


    $(document).on('click', '.Aprobar', function (e) {


        var TipoDocum, NumNC;
        var Tipo = "I", IDUSUARIO;
        var Ruta = "Vacio";


        IDUSUARIO = $.session.get('id');


        TipoDocum = $(this).parents("tr").find("td").eq(1).html();
        NumNC = $(this).parents("tr").find("td").eq(2).html();
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();

        swal({
            title: "¿Esta seguro de aprobar la NC" + " N° " + NumNC + "  ?",
            //text: "No podrás deshacer este paso...",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "No",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: false
        },

            function (isConfirm) {
                if (isConfirm == true) {
                    var ajax_data = {
                        "Tipo": Tipo,
                        "iddocumento": ID_DOCUMENTO,
                        "IdUsuario": IDUSUARIO,
                        "Ruta": Ruta
                    };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Contabilidad.asmx/APROBAR_REGISTROS_SOLICITUDDOCUMENTOS',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                        },
                        success: function (resultado) {
                            var num = resultado.d;
                          
                            if (num == "EXITO") {
                                swal("Exito!", "Los datos se registraron ", "success");
                            } else {
                                //Indicador=1
                                swal("Error!", num, "error");
                                return false
                            }


                            $('body').removeClass('loading'); //Removemos la clase loading
                            //$("#modal_nuevo .close").click()
                            Muestra_Datos_Ingresado()


                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });
                }
                //swal("¡Hecho!",
                //    "Acabas de proceder con la aprobacion",
                //    "success")
            ;}


        );
   

    });



    $(document).on('click', '.Rechazar', function (e) {

        var TipoDocum, NumNC;
        var Tipo = "A", IDUSUARIO;


        IDUSUARIO = $.session.get('id');


        TipoDocum = $(this).parents("tr").find("td").eq(1).html();
        NumNC = $(this).parents("tr").find("td").eq(2).html();
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();

        swal({
            title: "¿Esta seguro de Anular la solicitud de NC" + " N° " + NumNC + "  ?",
            //text: "No podrás deshacer este paso...",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "No",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: false
        },

            function (isConfirm) {
                if (isConfirm == true) {
                    var ajax_data = {
                        "Tipo": Tipo,
                        "iddocumento": ID_DOCUMENTO,
                        "IdUsuario": IDUSUARIO
                    };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Contabilidad.asmx/APROBAR_REGISTROS_SOLICITUDDOCUMENTOS',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                        },
                        success: function (resultado) {
                            var num = resultado.d;

                            if (num == "EXITO") {
                                swal("Exito!", "Se procedio con la Anulación ", "success");
                            } else {
                                //Indicador=1
                                swal("Error!", num, "error");
                                return false
                            }


                            $('body').removeClass('loading'); //Removemos la clase loading
                            //$("#modal_nuevo .close").click()
                            Muestra_Datos_Ingresado()


                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });
                }
                //swal("¡Hecho!",
                //    "Acabas de proceder con la aprobacion",
                //    "success")
                ;
            }


        );


    });


});