﻿



$(document).ready(function () {

    OBTENER_TIENDA();
    LISTAR_DOCUMENTOS();
    LISTAR_DOCUMENTOS_INGRESADOS();

    //listarOP();
    //LLenarCboSeriexIdsEmpTienTipoDoc();
    //OBTENER_TIPOSDOCUMENTO();
    //OBTENER_SERIE_DOCUMENTO();

    $("#btnnuevo").click(function (e) {
        //Muestra el formulario oculto
        //Limpiar();
        $('#fuUpload1').val('');
        $('#modal_nuevo').modal();
        $('#txtMonto').val('');
        $('#txtNumeroDocNuevo').val('');
        $('#idTiendaNuevo').val(0);
        $('#IdTipoDocumentoNuevo').empty().append('<option selected="selected" value="0"><-Seleccionar-></option>');
        $('#idSerieNuevo').empty().append('<option selected="selected" value="0">----</option>');
        var table = $('#tabla_Documentos').DataTable();
        table.clear().draw();
        $('#IdImagen').val('');
        $('myImg').val('');


    });



    $("#txtNumeroDoc").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });


    $("#txtNumeroDocNuevo").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    function forceNumber(element) {
        element
            .data("oldValue", '')
            .bind("paste", function (e) {
                var validNumber = /^[-]?\d+(\.\d{1,3})?$/;
                element.data('oldValue', element.val())
                setTimeout(function () {
                    if (!validNumber.test(element.val()))
                        element.val(element.data('oldValue'));
                }, 0);
            });
        element
            .keypress(function (event) {
                var text = $(this).val();
                if ((event.which != 46 || text.indexOf('.') != -1) && //if the keypress is not a . or there is already a decimal point
                    ((event.which < 48 || event.which > 57) && //and you try to enter something that isn't a number
                        (event.which != 45 || (element[0].selectionStart != 0 || text.indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
                        (event.which != 0 && event.which != 8))) { //and the keypress is not a backspace or arrow key (in FF)
                    event.preventDefault(); //cancel the keypress
                }

                if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 3) && //if there is a decimal point, and there are more than two digits after the decimal point
                    ((element[0].selectionStart - element[0].selectionEnd) == 0) && //and no part of the input is selected
                    (element[0].selectionStart >= element.val().length - 3) && //and the cursor is to the right of the decimal point
                    (event.which != 45 || (element[0].selectionStart != 0 || text.indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
                    (event.which != 0 && event.which != 8)) { //and the keypress is not a backspace or arrow key (in FF)
                    event.preventDefault(); //cancel the keypress
                }
            });
    }

    forceNumber($("#txtMonto"));

    function OBTENER_TIENDA() {

        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_Tienda',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {


                    $('#ddlalmacen').append('<option value="' + serie[i].IdTienda + '">' + serie[i].Descripcion + '</option>');
                    $('#ddlalmacenbuscar').append('<option value="' + serie[i].IdTienda + '">' + serie[i].Descripcion + '</option>');
                    $('#idTiendaNuevo').append('<option value="' + serie[i].IdTienda + '">' + serie[i].Descripcion + '</option>');

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }
    //Obtener_TiposDocumento
    $("#ddlalmacenbuscar").change(function () {

        $('#ddTipoDocumento').empty().append('<option selected="selected" value="0"><-Seleccionar-></option>');
        $('#idSerie').empty().append('<option selected="selected" value="0">----</option>');

        var ID_TIENDA = $('select[id=ddlalmacenbuscar]').val();
        var ajax_data = {

            "ID_TIENDA": ID_TIENDA
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_TiposDocumento',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {
                    $('#ddTipoDocumento').append('<option value="' + terceros[i].IdTipoDocumento + '">' + terceros[i].NombreTipoDocumento + '</option>');
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });


    $("#ddTipoDocumento").change(function () {

        $('#idSerie').empty().append('<option selected="selected" value="0">----</option>');

        var ID_TIPODOCUMENTO = $('select[id=ddTipoDocumento]').val();
        var ID_TIENDA = $('select[id=ddlalmacenbuscar]').val();

        var ajax_data = {
            "ID_TIPODOCUMENTO": ID_TIPODOCUMENTO,
            "ID_TIENDA": ID_TIENDA
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_Serie',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {
                    $('#idSerie').append('<option value="' + terceros[i].IdSerie + '">' + terceros[i].NombreSerie + '</option>');
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });


    $(function fecha_actual() {
        $.datepicker.setDefaults($.datepicker.regional["es"]);

        $("#txtfecemisionnuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtiniciotrasladonuevo").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfecinicio").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfecfin").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfechaini1").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

        $("#txtfechafin1").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());

    });

    //Obtener_TiposDocumento Nuevo
    $("#idTiendaNuevo").change(function () {

        $('#IdTipoDocumentoNuevo').empty().append('<option selected="selected" value="0"><-Seleccionar-></option>');
        $('#idSerieNuevo').empty().append('<option selected="selected" value="0">----</option>');

        var ID_TIENDA = $('select[id=idTiendaNuevo]').val();
        var ajax_data = {

            "ID_TIENDA": ID_TIENDA
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_TiposDocumento',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {
                    $('#IdTipoDocumentoNuevo').append('<option value="' + terceros[i].IdTipoDocumento + '">' + terceros[i].NombreTipoDocumento + '</option>');
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });


    $("#IdTipoDocumentoNuevo").change(function () {

        $('#idSerieNuevo').empty().append('<option selected="selected" value="0">---</option>');
        $('#txtMonto').val('');
        $('#txtNumeroDocNuevo').val('');

        var ID_TIPODOCUMENTO = $('select[id=IdTipoDocumentoNuevo]').val();
        var ID_TIENDA = $('select[id=idTiendaNuevo]').val();

        var ajax_data = {
            "ID_TIPODOCUMENTO": ID_TIPODOCUMENTO,
            "ID_TIENDA": ID_TIENDA
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_Serie',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var terceros = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < terceros.length; i++) {
                    $('#idSerieNuevo').append('<option value="' + terceros[i].IdSerie + '">' + terceros[i].NombreSerie + '</option>');
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });


    $("#btnBuscarNuevo").click(function (e) {

        var Doc_Serie, Doc_Codigo, IdTipoDocumento, idTienda;

        IdTipoDocumento = $('select[id=IdTipoDocumentoNuevo]').val();
        Doc_Serie = document.getElementById('idSerieNuevo').value;//$('#idSerieNuevo').val();
        Doc_Codigo = $('#txtNumeroDocNuevo').val();
        idTienda = $('select[id=idTiendaNuevo]').val();

        var table = $('#tabla_Documentos').DataTable();
        table.clear().draw();

        if (idTienda == "" || idTienda == 0 || idTienda == null) {
            swal("Error", "No selecciono la tienda", "error");
            return false
        }

        if (IdTipoDocumento == "" || IdTipoDocumento == 0 || IdTipoDocumento == null) {
            swal("Error", "No selecciono el Tipo de documento", "error");
            return false
        }

        if (Doc_Serie == "" || Doc_Serie == 0 || Doc_Serie == null) {
            swal("Error", "No selecciono la serie del documento", "error");
            return false
        }

        if (Doc_Codigo == "" || Doc_Codigo == 0 || Doc_Codigo == null) {
            swal("Error", "No ingreso el numero del documento", "error");
            return false
        }




        LISTAR_DOCUMENTOS();

    });

    function LISTAR_DOCUMENTOS() {

        var table = $('#tabla_Documentos').DataTable();
        table.clear().draw();

        var mensaje = ''

        var Doc_Serie, Doc_Codigo, IdTipoDocumento;

        IdTipoDocumento = $('select[id=IdTipoDocumentoNuevo]').val();
        Doc_Serie = document.getElementById('idSerieNuevo').value;//$('#idSerieNuevo').val();
        Doc_Codigo = $('#txtNumeroDocNuevo').val();


        var ajax_data = {
            "Doc_Serie": Doc_Serie,
            "Doc_Codigo": Doc_Codigo,
            "TipoDocumento": IdTipoDocumento

        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos").show();
                for (var i = 0; i < opvp.length; i++) {


                    $('#tabla_Documentos').dataTable().fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].TipoDocumento,
                        opvp[i].doc_Codigo,
                        opvp[i].FechaEmision,
                        opvp[i].doc_ImporteTotal,
                        opvp[i].Tienda,
                        opvp[i].Cliente,
                        opvp[i].TipoOperacion,
                        "<img src='../img/" + opvp[i].EstadoDoc + "'  class='VerTonos'  data-toggle='tooltip' title='Tonos..!' style='height: 40px;width: 40px;text-align: center' >"



                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }




    function Cargar_Archivo() {
        var param = new FormData();
        param.append("Archivo", "21023304972412022.jpg");

        var cadenas = "data:image/png;base64,";



        $.ajax({
            url: '../ServicioWeb/WS_Contabilidad.asmx/ObtenerArchivo',
            type: 'POST',
            data: param,
            //data: JSON.stringify(param),
            cache: false,
            contentType: false,
            processData: false,
            //data: { base64Str: $("#img").attr("src").split(",")[1] },

            success: function (result) {
                //debugger;
                console.log(result);

                cadenas = cadenas + result;

                //swal("Exito!", "Los datos se registraron", "success");
                $("#imagen").attr("src", "data:image/jpg;base64, " + result);
                //$("#img").attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==');
                //$("#imagen").attr("src",  result);
                //$("#imagen").attr("src",  result);
            }, error: function (r) {
                alert(r.d);
            }
        });
    }



    $("#btnguardar").click(function (e) {

        var Tipo, IDUSUARIO, Monto, ID_DOCUMENTO, RUTA, Doc_Serie, Doc_Codigo, IdTipoDocumento, idTienda, Indicador = 0;

        var MyTabla = $('table#tabla_Documentos').find('tbody').find('tr');
        for (var i = 0; i < MyTabla.length; i++) {
            ID_DOCUMENTO = $(MyTabla[i]).find('td:eq(0)').html();
        }


        IdTipoDocumento = $('select[id=IdTipoDocumentoNuevo]').val();
        Doc_Serie = document.getElementById('idSerieNuevo').value;//$('#idSerieNuevo').val();
        Doc_Codigo = $('#txtNumeroDocNuevo').val();
        idTienda = $('select[id=idTiendaNuevo]').val();
        Monto = $('#txtMonto').val();
        IDUSUARIO = $.session.get('id');
        Tipo = "I";
        //ID_DOCUMENTO = $('#tabla_Documentos').parents("tr").find("td").eq(0).html();
        RUTA = $('#fuUpload1').val();

        if (idTienda == "" || idTienda == 0 || idTienda == null) {
            swal("Error", "No selecciono la tienda", "error");
            return false
        }

        if (IdTipoDocumento == "" || IdTipoDocumento == 0 || IdTipoDocumento == null) {
            swal("Error", "No selecciono el Tipo de documento", "error");
            return false
        }

        if (Doc_Serie == "" || Doc_Serie == 0 || Doc_Serie == null) {
            swal("Error", "No selecciono la serie del documento", "error");
            return false
        }

        if (Doc_Codigo == "" || Doc_Codigo == 0 || Doc_Codigo == null) {
            swal("Error", "No ingreso el numero del documento", "error");
            return false
        }


        if (Monto == "" || Monto == 0 || Monto == null) {
            swal("Error", "Debe ingresar el monto", "error");
            return false
        }

        if (RUTA == "" || RUTA == 0 || RUTA == null) {
            swal("Error", "Debe ingresar la ruta", "error");
            return false
        }


        //UARDAR_REGISTROS_DOCUMENTOS(int Tipo, int IdDocumento, decimal Monto, string Ruta, int IdUsuario)

        var ajax_data = {
            "Tipo": Tipo,
            "IdDocumento": ID_DOCUMENTO,
            "Monto": Monto,
            "Ruta": RUTA,
            "IdUsuario": IDUSUARIO
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/GUARDAR_REGISTROS_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;
                debugger;

                if (num == "EXITO") {
                    Guardar_Archivo();
                    //Cargar_Archivo();
                    //Cargar_Archivo();
                    swal("Exito!", "Los datos se registraron ", "success");
                    //return false
                } else {
                    //Indicador=1
                    swal("Error!", num, "error");
                    return false
                }


                $('body').removeClass('loading'); //Removemos la clase loading
                $("#modal_nuevo .close").click()
                LISTAR_DOCUMENTOS_INGRESADOS()


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

        function Guardar_Archivo() {

            debugger;
            var f = new Date();
            var dia = f.getDate();
            var mes = (f.getMonth() + 1);
            var Ano = f.getFullYear()

            var Fech = dia + "" + mes + "" + Ano

            var formData = new FormData();
            for (var i = 0; i < $(".File").length; i++) {
                formData.append($(".File")[i].files[0].name, $(".File")[i].files[0], ID_DOCUMENTO + Fech + ".jpg");
            }
            $.ajax({
                url: '../ServicioWeb/WS_Contabilidad.asmx/Guardar',
                type: 'POST',
                data: formData,
                //data: JSON.stringify(ajax_data),
                cache: false,
                contentType: false,
                processData: false,
                success: function () {
                    //Cargar_Archivo();
                    //swal("Exito!", "Los datos se registraron", "success");
                }, error: function () {
                    //alert(r.d);
                }
            });
        }



    });




    function LISTAR_DOCUMENTOS_INGRESADOS() {

        var Tienda = "0";

        var table = $('#tabla_Documentos_Ingresados').DataTable();
        table.clear().draw();


        var Tipo = "2";

        var ajax_data = {
            "Tipo": Tipo,
            "Tienda": Tienda

        };
        $.ajax({
            
            type: "POST",
            //url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_DOCUMENTOS_INGRESADOS',
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_SOLICITUD_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos_Ingresados").show();
                for (var i = 0; i < opvp.length; i++) {

                    $('#tabla_Documentos_Ingresados').dataTable().fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].TipoDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].FechaEmision,
                        opvp[i].doc_ImporteTotal,
                        opvp[i].Tienda,
                        opvp[i].Cliente,
                        opvp[i].doc_Total,
                        opvp[i].doc_FechaRegistro,
                        opvp[i].Observacion,
                        "<img src='../img/" + opvp[i].Semaforo + "'  class='VerTonos'  data-toggle='tooltip' title='" + opvp[i].EstadoDoc + "' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >"

                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }


    $(document).on('click', '.VerImagen', function (e) {

        $("#imagenNuevo").attr('src','');

        var FechaImagen="", rutaimg="";

        //alert(str.substr(0, str.length - 3));

        FechaImagen = $(this).parents("tr").find("td").eq(10).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(0).html() + parseInt(dia) + parseInt(mes) + yyy +'.jpg'




        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#imagenNuevo").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });




        $('#modal_Imagen').modal();

    });



});