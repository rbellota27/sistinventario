﻿$(document).ready(function () {
    var iddocum, ID_DOCUMENTO;

    

    Muestra_Datos_Ingresado();

 

    function Muestra_Datos_Ingresado() {

        var Tipo = "1";
        var Tienda = "0";

        var ajax_data = {
            "Tipo": Tipo,
            "Tienda": Tienda,
            "IdUsuario": $.session.get('id')
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/LISTAR_SOLICITUD_DOCUMENTOS',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,


            success: function (respuesta) {
                var Filas = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_Documentos_AprobarFinanzas").show();
                for (var i = 0; i < Filas.length; i++) {
                    //$('#tabla_Documentos_AprobarFinanzas').dataTable().fnDestroy();
                    $('#tabla_Documentos_AprobarFinanzas').dataTable(
                        //{
                        //    columnDefs: [
                        //        {
                        //            "targets": [11],
                        //            "className": "hide_column"

                        //        },
                        //    ]
                        //}

                    ).fnAddData([
                        Filas[i].IdDocumento,
                        Filas[i].TipoDocumento,
                        Filas[i].NroDocumento,
                        Filas[i].FechaEmision,
                        Filas[i].doc_ImporteTotal,
                        Filas[i].Tienda,
                        Filas[i].Cliente,
                        Filas[i].doc_Total,
                        Filas[i].doc_FechaRegistro,
                        Filas[i].Observacion,
                        "<img src='../img/ver_doc.png'  class='VerImagen' style='height: 20px;width: 20px;text-align: center' >",
                        Filas[i].EstadoDoc,
                        "<img src='../img/check_2.png'  class='Aprobar' style='height: 20px;width: 20px;text-align: center' >"
                    ]);
                }

                $('body').removeClass('loading'); //Removemos la clase loading

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });


    }


    $(document).on('click', '.VerImagen', function (e) {


        $("#ImagenMostrar").attr('src', '');
        var FechaImagen="", rutaimg="";

        //alert(str.substr(0, str.length - 3));

        FechaImagen = $(this).parents("tr").find("td").eq(8).html()
        var dia = FechaImagen.substr(0, 2);
        var mes = FechaImagen.substr(3, 2);
        var yyy = FechaImagen.substr(6, 4);

        rutaimg = $(this).parents("tr").find("td").eq(0).html() + parseInt(dia) + parseInt(mes) + yyy + '.jpg'




        var ajax_data = {
            "Ruta_Archivo": rutaimg
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Contabilidad.asmx/MuestraImagen',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                //$('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (resultado) {
                var num = resultado.d;

                $("#ImagenMostrar").attr('src', 'data:image/jpg;base64,' + num);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });

        $('#modal_Imagen').modal();

    });


    $(document).on('click', '.Aprobar', function (e) {


        var TipoDocum, NumNC;
        var Tipo = "F", IDUSUARIO;
        var Ruta = "vacio";


        IDUSUARIO = $.session.get('id');


        TipoDocum = $(this).parents("tr").find("td").eq(1).html();
        NumNC = $(this).parents("tr").find("td").eq(2).html();
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();

        swal({
            title: "¿Esta seguro de validar la NC" + " N° " + NumNC + "  ?",
            //text: "No podrás deshacer este paso...",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "No",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: false
        },

            function (isConfirm) {
                if (isConfirm == true) {
                    var ajax_data = {
                        "Tipo": Tipo,
                        "iddocumento": ID_DOCUMENTO,
                        "IdUsuario": IDUSUARIO,
                        "Ruta": Ruta
                    };

                    $.ajax({
                        type: "POST",
                        url: '../ServicioWeb/WS_Contabilidad.asmx/APROBAR_REGISTROS_SOLICITUDDOCUMENTOS',
                        data: JSON.stringify(ajax_data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                        },
                        success: function (resultado) {
                            var num = resultado.d;

                            if (num == "EXITO") {

                             
                                var tables = $('#tabla_Documentos_AprobarFinanzas').DataTable();
                                tables.clear().draw();

                                Muestra_Datos_Ingresado()
                                swal("Exito!", "Los datos se registraron ", "success");
                            } else {
                                //Indicador=1
                                swal("Error!", num, "error");
                                return false
                            }


                            $('body').removeClass('loading'); //Removemos la clase loading

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var error = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(error.Message);
                        }
                    });
                }
                ;
            }


        );


    });




});