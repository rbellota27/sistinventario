﻿$(document).ready(function () {
    OBTENER_SERIE();
    listarOP();
    $('[data-toggle="tooltip"]').tooltip();
    function OBTENER_SERIE() {
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Obtener_Serie',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var serie = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < serie.length; i++) {
                    $('#ddlserie').append('<option value="' + serie[i].IdSerie + '">' + serie[i].doc_Serie + '</option>');
                    $('#ddlserie2').append('<option value="' + serie[i].IdSerie + '">' + serie[i].doc_Serie + '</option>');

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }
    $("#ddlserie").change(function () {
        $('#txtnrodoc').val('');


        var codSerie = $('select[id=ddlserie]').val();
        var ajax_data = {

            "COD_SERIE": codSerie
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Obtener_Consecutivo',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var nrodoc = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < nrodoc.length; i++) {
                    $('#txtnrodoc').val(nrodoc[0].doc_Codigo);
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });
    $("#ddlserie2").change(function () {
        $('#txtnrodoc2').val('');


        var codSerie = $('select[id=ddlserie2]').val();
        var ajax_data = {

            "COD_SERIE": codSerie
        };
        $.ajax({

            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/Obtener_Consecutivo',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,

            success: function (respuesta) {
                var nrodoc = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < nrodoc.length; i++) {
                    $('#txtnrodoc2').val(nrodoc[0].doc_Codigo);
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    });
    $("#btnbuscar").click(function (e) {

        listarOP();

    });
    function listarOP() {

        var table = $('#tabla_pedidos').DataTable();
        table.clear().draw();
        var IdEmpresa, IdAlmacen, FecIni,
            FecFin, doc_Serie, doc_Codigo, IdPersona, IdDocumento,
            IdTipoOperacion, IdTipoDocumento;


        FecIni = $('#txtfechaini').val();
        FecFin = $('#txtfechafin').val();

        var ajax_data = {

            "IdEmpresa": 1,
            "IdAlmacen": 7,
            "FecIni": FecIni,
            "FecFin": FecFin,
            "doc_Serie": 0,
            "doc_Codigo": 0,
            "IdPersona": 0,
            "IdDocumento": 6,
            "IdTipoOperacion": 0,
            "IdTipoDocumento": 15

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Orden_Pedido_VP',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;
                $("#tabla_pedidos").show();
                for (var i = 0; i < opvp.length; i++) {

                    $('#tabla_pedidos').dataTable().fnDestroy();
                    $('#tabla_pedidos').css("width", "400px");
                    $('#tabla_pedidos').dataTable({
                        columnDefs: [
                            {
                                "targets": [7],
                                "className": "hide_column"
                            }
                        ]
                    }).fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].TipoDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].FechaEmision,
                        opvp[i].Almacen,
                        opvp[i].TipoOperacion,
                        opvp[i].EstadoEnt,
                        opvp[i].IdTipoOperacion,

                        "<img src='../img/check1.png'  class='picking'  data-toggle='tooltip' title='Generar..!' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/ver_doc.png'  class='ver'  data-toggle='tooltip' title='Detalle..!' style='height: 20px;width: 20px;text-align: center' >",
                        "<img src='../img/report.png'  class='observacion' data-toggle='tooltip' title='Observacion..!'  style='height:20px;width:20px;text-align: center' >"


                    ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }


    $(function fecha_actual() {
        var fecha = new Date();
        $.datepicker.setDefaults($.datepicker.regional["es"]);

        $("#txtfechaini").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date(fecha.getTime() - 24 * 60 * 60 * 1000));

        $("#txtfechafin").datepicker({
            dateFormat: 'dd/mm/yy',
            firstDay: 1
        }).datepicker("setDate", new Date());



    });



    $(document).on('click', '.observacion', function (e) {

        $('#txtobservacion').val('');

        var ID_PEDIDO;
        ID_PEDIDO = $(this).parents("tr").find("td").eq(0).html();

        var ajax_data = {
            "ID_PEDIDO": ID_PEDIDO
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/ObtenerObservacion_Orden_Pedido_VP',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var observacion = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < observacion.length; i++) {
                    $('#txtobservacion').val(observacion[0].Observacion);
                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_observacion').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });



    });
    $(document).on('click', '.picking', function (e) {

        $('#modal_picking').modal();
        $('#txtnrodoc').val('');
        $('#ddlserie').val(0);

        var ID_PEDIDO;
        ID_PEDIDO = $(this).parents("tr").find("td").eq(0).html();
        $('#txtidnropedido').val(ID_PEDIDO);

    });

    $("#btngenerarpicking").click(function (e) {

        $("#modal_picking .close").click();


        var ID_PEDIDO, ID_SERIE, NRO_SERIE, NRO_DOC,USUARIO;
        ID_PEDIDO = $('#txtidnropedido').val();
        ID_SERIE = $('#ddlserie').val();
        NRO_SERIE = $("#ddlserie option:selected").html();
        NRO_DOC = $('#txtnrodoc').val();
        USUARIO = $.session.get('id');
       
        if (ID_SERIE === "0") {
            swal("Seleccione Serie");
        }
        else {


            var ajax_data = {
                "ID_PEDIDO": ID_PEDIDO,
                "ID_SERIE": ID_SERIE,
                "NRO_SERIE": NRO_SERIE,
                "NRO_DOC": NRO_DOC,
                "USUARIO": USUARIO
            };


            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Picking.asmx/Generar_Picking',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading'); //Agregamos la clase loading al body
                },
                success: function (resultado) {
                    var num = resultado.d;
                    if (num > 0) {

                        swal("Exito!", " El Picking se Grabo Satisfactoriamente ", "success");
                        listarOP();
                    }
                    else {

                        swal("Error!", num, "error");

                    }
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }





    });
    $(document).on('click', '.ver', function (e) {

        var table = $('#tabla_pedidos_detalle').DataTable();
        table.clear().draw();
        var ID_PEDIDO, ID_TIPO_OPERACION;
        ID_PEDIDO = $(this).parents("tr").find("td").eq(0).html();
        ID_TIPO_OPERACION = $(this).parents("tr").find("td").eq(7).html();
        var ajax_data = {
            "ID_PEDIDO": ID_PEDIDO,
            "ID_TIPO_OPERACION": ID_TIPO_OPERACION

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Detalle_Orden_Pedido_VP',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < detalle.length; i++) {

                    $('#tabla_pedidos_detalle').dataTable().fnDestroy();
                    $('#tabla_pedidos_detalle').css("width", "400px");
                    $('#tabla_pedidos_detalle').dataTable({

                        "searching": false,
                        "paging": false,
                        "info": false,

                        columnDefs: [
                            {
                                "targets": [5, 6, 7, 9],
                                "className": "hide_column",

                            }
                        ]
                    }).fnAddData(
                        [detalle[i].CodigoProducto,
                        detalle[i].NombreProducto,
                        detalle[i].UM,
                        detalle[i].Cantidad,
                            "<img src='../img/ver_doc.png'  class='VerTonos'  data-toggle='tooltip' title='Tonos..!' style='height: 20px;width: 20px;text-align: center' >",
                        detalle[i].IdDocumento,
                        detalle[i].IdProducto,
                        detalle[i].IdAlmacen,
                        "<input type='text' class='form-control'  id='" + detalle[i].IdProducto + '_' + detalle[i].UM + '_' + detalle[i].IdDetalleDocumento + "'>",
                        detalle[i].IdDetalleDocumento
                        ]);

                }
                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_ver').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });



    });
    $(document).on('click', '.VerTonos', function (e) {


        var table = $('#tabla_tono_detalle').DataTable();
        table.clear().draw();
        var ID_PRODUCTO, ID_ALMACEN, ID_DOCUMENTO, CANT_SOLICITADA, UM, ID_DETALLE;
        ID_PRODUCTO = $(this).parents("tr").find("td").eq(6).html();
        ID_ALMACEN = $(this).parents("tr").find("td").eq(7).html();
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(5).html();
        CANT_SOLICITADA = $(this).parents("tr").find("td").eq(3).html();
        ID_DETALLE = $(this).parents("tr").find("td").eq(9).html();
        UM = $(this).parents("tr").find("td").eq(2).html();
        $('#' + ID_PRODUCTO + '_' + UM + '_' + ID_DETALLE).val('');



        var ajax_data = {

            "ID_PRODUCTO": ID_PRODUCTO,
            "ID_ALMACEN": ID_ALMACEN,
            "ID_DOCUMENTO": ID_DOCUMENTO

        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Detalle_Tonos_Orden_Pedido_VP',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < detalle.length; i++) {

                    $('#tabla_tono_detalle').dataTable().fnDestroy();
                    $('#tabla_tono_detalle').css("width", "600px");
                    $('#tabla_tono_detalle').dataTable({
                        columnDefs: [
                            {
                                "targets": [5],
                                "className": "hide_column"
                            }
                        ],

                        "paging": false
                    }).fnAddData(
                        [

                            detalle[i].idtono,
                            detalle[i].nomtono,
                            detalle[i].cantidadtono,
                            "<input type='text' class='form-control'  value='" + CANT_SOLICITADA + "'>",
                            "<input type='checkbox' class='minimal' >",
                            ID_PRODUCTO + '_' + UM + '_' + ID_DETALLE

                        ]);

                }

                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_ver_tono').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });




    });
    $("#btnguardar_tono_seleccionado").click(function (e) {

        var id_producto_seleccionado, idtono_seleccionado, cant_tono_seleccionado;
        var lista_tonos_id = '';
        $('#tabla_tono_detalle input[type=checkbox]').each(function () {


            if (this.checked) {

                id_producto_seleccionado = $(this).parents("tr").find("td").eq(5).html();
                idtono_seleccionado = $(this).parents("tr").find("td").eq(0).html();
                cant_tono_seleccionado = $(this).parents("tr").find("td").eq(3).find(":input").val();


                var a = ('id-producto :' + id_producto_seleccionado + '-id_tono :' + idtono_seleccionado + '- cantidad :' + cant_tono_seleccionado);

                lista_tonos_id = lista_tonos_id + "/" + idtono_seleccionado + ":" + cant_tono_seleccionado;

                $('#' + id_producto_seleccionado).val(lista_tonos_id);
            }
        });


    });

    $(document).on('click', '#btnguardar_picking_manual', function (e) {

        $('#txtidnropedido_manual').val('');
        $('#txtnrodoc2').val('');
        $('#ddlserie2').val(0);
        var id_pedido = $('#tabla_pedidos_detalle').find("tr").find("td").eq(5).html();
        $('#txtidnropedido_manual').val(id_pedido);
        $('#modal_picking_manual_Serie_codigo').modal();

    });



    $("#btngenerarpicking_manual").click(function (e) {                

        var ID_PEDIDO, ID_SERIE, NRO_SERIE, NRO_DOC, USUARIO;
        ID_PEDIDO = $('#txtidnropedido_manual').val();
        ID_SERIE = $('#ddlserie2').val();
        NRO_SERIE = $("#ddlserie2 option:selected").html();
        NRO_DOC = $('#txtnrodoc2').val();
        USUARIO = $.session.get('id');

        if (NRO_DOC === "") {
            swal("Seleccione Serie");
        }
        else {
            valores_tonos = new Array();
            $('#tabla_pedidos_detalle tbody tr').each(function () {
                var ID_DOC_REFERENCIA = $(this).find("td").eq(5).html().trim();
                var ID_PRODUCTO = $(this).find("td").eq(6).html().trim();
                var TONO_SELECCIONADO = $(this).find("td").eq(8).find(":input").val();
                var ID_DETALLE_DOCUMENTO = $(this).find("td").eq(9).html().trim();
                valor = new Array(
                    ID_DOC_REFERENCIA, ID_PRODUCTO, TONO_SELECCIONADO, ID_DETALLE_DOCUMENTO
                );
                valores_tonos.push(valor);
            });



            var ajax_data = {
                "ID_PEDIDO": ID_PEDIDO,
                "ID_SERIE": ID_SERIE,
                "NRO_SERIE": NRO_SERIE,
                "NRO_DOC": NRO_DOC,
                "USUARIO": USUARIO,
                "VALORES": valores_tonos
            };

            $('#modal_picking_manual_Serie_codigo').modal('hide');
            $('#modal_ver').modal('hide');

            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Picking.asmx/Generar_Picking_Manual',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading'); //Agregamos la clase loading al body
                },
                success: function (resultado) {
                    var num = resultado.d;
                    if (num > 0) {
                        swal("Exito!", " El Picking se Grabo Satisfactoriamente ", "success");
                        listarOP();
                    }
                    else {
                        swal("Error!", num, "error");
                    }                    
                    $('body').removeClass('loading'); //Removemos la clase loading
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }
    });

});

