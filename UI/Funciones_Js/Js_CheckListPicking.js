﻿$(document).ready(function () {

    listarPicking();


    function listarPicking() {

        var table = $('#tabla_picking').DataTable();
        table.clear().draw();

       

        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_CheckList_Picking',
            data: JSON.stringify(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var opvp = (typeof respuesta.d) === 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                $("#tabla_picking").show();

                for (var i = 0; i < opvp.length; i++) {

                    $('#tabla_picking').dataTable().fnDestroy();
                    $('#tabla_picking').css("width", "800px");

                    var esta;
                    if (opvp[i].estado == 'COMPLETO') {
                        esta = "<img src='../img/check1.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >";
                    }
                    else {
                        esta = "<img src='../img/eliminarnew.png'  class='ver' style='height: 20px;width: 20px;text-align: center' >";
                    }
                    $('#tabla_picking').dataTable({

                        columnDefs: [
                            {
                                targets: [0,1],
                                className: "hide_column"
                            }
                        ]

                    }).fnAddData([
                        opvp[i].IdDocumento,
                        opvp[i].IdDetalleDocumento,
                        opvp[i].NroDocumento,
                        opvp[i].CodigoProducto,
                        opvp[i].prod_Nombre,
                        opvp[i].Cantidad,
                        opvp[i].UM,                     
                        opvp[i].cantidad_transito,
                        opvp[i].estado,
                        opvp[i].Tie_Ref,
                        opvp[i].obsRef,
                        opvp[i].UsuPick,
                        esta,
                        "<input type='text' class='form-control'  value='" + opvp[i].cantidad_transito + "'>",                        
                        "<button type='button'  class='btnguardar btn btn-warning'><i class='fa fa-check-circle'></i> Procesar</button>"
                         ]);


                }
                $('body').removeClass('loading'); //Removemos la clase loading
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });
    }


    $(document).on('click', '.ver', function (e) {

        var table = $('#tabla_picking_detalle').DataTable();
        table.clear().draw();
        var ID_DOCUMENTO;
        ID_DOCUMENTO = $(this).parents("tr").find("td").eq(0).html();

        console.log(ID_DOCUMENTO);

        var ajax_data = {
            "ID_DOCUMENTO": ID_DOCUMENTO
        };


        $.ajax({
            type: "POST",
            url: '../ServicioWeb/WS_Picking.asmx/listar_Detalle_Picking_EnProceso',
            data: JSON.stringify(ajax_data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function (respuesta) {
                var detalle = (typeof respuesta.d) == 'string' ?
                    eval('(' + respuesta.d + ')') :
                    respuesta.d;

                for (var i = 0; i < detalle.length; i++) {
                    var esta;
                    if (detalle[i].estado == 'LISTO') {
                        esta = "<img src='../img/check1.png'  style='height: 20px;width: 20px;text-align: center' >";
                    }
                    else {
                        esta = "<img src='../img/eliminarnew.png'  style='height: 20px;width: 20px;text-align: center' >";
                    }
                    $('#tabla_picking_detalle').dataTable().fnAddData(
                        [   detalle[i].CodigoProducto,
                            detalle[i].NombreProducto,
                            detalle[i].Cantidad,
                            detalle[i].UM,
                            detalle[i].cantidad_x_atender,
                            detalle[i].cantidad_transito,
                            esta
                                
                        ]);

                }

                $('body').removeClass('loading'); //Removemos la clase loading
                $('#modal_ver').modal();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var error = eval("(" + XMLHttpRequest.responseText + ")");
                alert(error.Message);
            }
        });



    });

    $(document).on('click', '.btnguardar', function (e) {
       

        var ID_DOCUMENTO_DETALLE, CANT_PICKEADA, CANT_VERIFICADOR;


        ID_DOCUMENTO_DETALLE = $(this).parents("tr").find("td").eq(1).html();
        CANT_PICKEADA = $(this).parents("tr").find("td").eq(7).html().trim();
        CANT_VERIFICADOR = $(this).parents("tr").find("td").eq(13).find(":input").val().trim();

    
        if (CANT_PICKEADA != CANT_VERIFICADOR) {

            swal({
                title: "¿Estás seguro?",
                text: "Se modificara la cantidad pickeada de: " +CANT_PICKEADA + " a " + CANT_VERIFICADOR,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Si, modificar!",
                cancelButtonText: "No, cancelar!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                       
                        var ajax_data = {
                            "ID_DOCUMENTO_DETALLE": ID_DOCUMENTO_DETALLE,
                            "CANT_PICKEADA": CANT_PICKEADA,
                            "CANT_VERIFICADOR": CANT_VERIFICADOR

                        };
                        $.ajax({
                            type: "POST",
                            url: '../ServicioWeb/WS_Picking.asmx/Guardar_Verificacion_Picking',
                            data: JSON.stringify(ajax_data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            beforeSend: function () {
                                $('body').addClass('loading'); //Agregamos la clase loading al body
                            },
                            success: function (resultado) {
                                var num = resultado.d;
                                if (num > 0) {

                                    swal("Exito!", " El Registro se Grabo Satisfactoriamente ", "success");
                                    listarPicking();
                                }
                                else {

                                    swal("Error!", " El Registro no se Grabo  ", "error");

                                }
                                $('body').removeClass('loading'); //Removemos la clase loading
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                var error = eval("(" + XMLHttpRequest.responseText + ")");
                                alert(error.Message);
                            }
                        });


                       
                    } else {
                        swal("Cancelado", "El Registro no se Modifico.", "error");
                    }
                });



        }
        else
        {
            var ajax_data = {
                "ID_DOCUMENTO_DETALLE": ID_DOCUMENTO_DETALLE,
                "CANT_PICKEADA": CANT_PICKEADA,
                "CANT_VERIFICADOR": CANT_VERIFICADOR

            };
            $.ajax({
                type: "POST",
                url: '../ServicioWeb/WS_Picking.asmx/Guardar_Verificacion_Picking',
                data: JSON.stringify(ajax_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('body').addClass('loading'); //Agregamos la clase loading al body
                },
                success: function (resultado) {
                    var num = resultado.d;
                    if (num > 0) {

                        swal("Exito!", " El Registro se Grabo Satisfactoriamente ", "success");
                        listarPicking();
                    }
                    else {

                        swal("Error!", " El Registro no se Grabo  ", "error");

                    }
                    $('body').removeClass('loading'); //Removemos la clase loading
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var error = eval("(" + XMLHttpRequest.responseText + ")");
                    alert(error.Message);
                }
            });
        }
    

 


    });

});