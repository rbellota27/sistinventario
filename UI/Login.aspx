﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="UI.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <%--<script src="jquery-ui-1.11.2.custom/jquery-1.11.2.min.js"></script>
    <link href="jquery-ui-1.11.2.custom/jquery-ui.min.css" rel="stylesheet" />
    <script src="jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="Js/bootbox.min.js"></script>
    <link href="alert_swit/sweetalert.css" rel="stylesheet" />
    <script src="alert_swit/sweetalert.min.js"></script>
    <script src="Js/jquerysession.js"></script>--%>
    <link rel="stylesheet" href="~/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/jquery-ui/jquery-ui.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/jquerysession.js"></script>
    <link href="/bootstrap-sweetalert-master/dist/sweetalert.css" rel="stylesheet" />
    <script src="/bootstrap-sweetalert-master/dist/sweetalert.js"></script>

    <title></title>
    <style>
        .container {
            position: relative;
            max-width: 800px; /* Maximum width */
            margin: 0 auto; /* Center it */
        }

            .container .content {
                position: absolute; /* Position the background text */
                bottom: 50; /* At the bottom. Use top:0 to append it to the top */
                background: rgba(60 58 58 / 44%); /* Fallback color */
                background: rgba(60 58 58 / 44%); /* Black background with 0.5 opacity */
                color: #f1f1f1; /* Grey text */
                width: 100%; /* Full width */
                padding: 20px; /* Some padding */
            }

        .modal-header, h4, .close {
            background-color: #4777A0;
            color: white !important;
            text-align: center;
            font-size: 30px;
        }

        body {
            background-image: url(img/Fondo3.jpg);
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }

        /*También puedes escribir el código en una sola línea y quedaría de la siguiente forma:.*/

        /* background: url(img/fon5.jpg) center center  no-repeat fixed;*/
        /*background-color:#4A235A;
}*/
    </style>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <style>
        .responsive {
            width: 100%;
            height: auto;
        }
    </style>
</head>
<body>

    <div class="container">

        <%--<div class="page-header" >
        <span class="icon-bar"><img id="imgLogo" src="img/logo.png"  /></span> 
    
  </div>--%>
        <!-- Modal -->
        <div id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="content">
                    <div class="modal-header" style="padding: 2px 2px; background-color: #07478800; opacity: 0.8;">
                        <%--  <button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                        <%--<h4 style="background-color:#000000"><span class="glyphicon glyphicon-lock" ></span> Login</h4>--%>
                        <span class="icon-bar">
                            <img id="imgLogo" src="img/logoT1.png"  /></span>
                    </div>
                    <div class="modal-body" style="padding: 20px 40px;">
                        <form id="Form1" role="form" runat="server">
                            <div class="form-group">
                                <label for="usrname"><span class="glyphicon glyphicon-user"></span> Usuario</label>
                                <%-- <input type="text" class="form-control" id="usrname" placeholder="Ingrese Usuario">--%>
                                <asp:TextBox ID="txtusuario" CssClass="form-control" placeholder="Ingrese Usuario" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Clave</label>
                                <%--<input type="text" class="form-control" id="psw" placeholder="Ingrese Clave">--%>
                                <asp:TextBox ID="txtclave" placeholder="Ingrese Clave" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                            </div>
                            <%-- <div class="checkbox">
              <label><input type="checkbox" value="" checked>Remember me</label>
            </div>--%>

                            <%-- <asp:Button ID="btningresar" CssClass="btn btn-success btn-block" runat="server" Text=" Ingresar " OnClick="btningresar_Click"  style="background-color:#FA810F"/>--%>
                            <div style="text-align: center">

                                <button type="button" id="btnIngresar" class="btn btn-warning" style="text-align: center">Ingresar</button>
                                <%--<asp:Button ID="btnIngresar" CssClass="btn btn-warning"  runat="server" Text="Ingresar" OnClick="btnIngresar_Click"  />--%>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <%--<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>--%>
                    </div>
                </div>

            </div>
        </div>

        <%-- <center>
 	 <img id="myBtn"  src="img/login1.png" style="cursor:pointer;" class="img-responsive test" data-toggle="tooltip" data-placement="right" title="Ingrese al sistema" />
	 </center>--%>
    </div>

    <script>
        $(document).ready(function () {

            $("#btnIngresar").click(function () {

                var usuario = $("#txtusuario").val();
                var clave = $("#txtclave").val();

                if (usuario == "" || usuario == null) {
                    swal("Error!", " Debe ingresar el Usuario", "error");
                    $("#txtusuario").focus();
                    return false;
                }

                if (clave == "" || clave == null) {
                    swal("Error!", " Debe ingresar la clave", "error");
                    $("#txtclave").focus();
                    return false;
                }

                var ajax_data = {
                    "USUARIO": usuario,
                    "CLAVE": clave
                };
                $.ajax({
                    type: "POST",
                    url: '../ServicioWeb/WS_Contabilidad.asmx/Obtener_Datos_Login',
                    //url: '../ServicioWeb/WS_Picking.asmx/Obtener_Datos_Login',

                    data: JSON.stringify(ajax_data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    success: function (respuesta) {
                        var contactos = (typeof respuesta.d) == 'string' ?
                            eval('(' + respuesta.d + ')') :
                            respuesta.d;
                        var id, nombre, usuario, perfil, idtienda, tienda, idempresa, idalmacenprincipal;

                        var q;

                        q = contactos.length;

                        if (q == 1) {
                            for (var i = 0; i < contactos.length; i++) {

                                id = contactos[i].ID;
                                nombre = contactos[i].NOMBRE;
                                usuario = contactos[i].USUARIO;
                                perfil = contactos[i].PERFIL;
                                idtienda = contactos[i].IDTIENDA;
                                tienda = contactos[i].TIENDA;
                                idempresa = contactos[i].IDEMPRESA;
                                idalmacenprincipal = contactos[i].IDALMACENPRINCIPAL;
                            }
                            $.session.set('id', id);
                            $.session.set('nombre', nombre);
                            $.session.set('usuario', usuario);
                            $.session.set('perfil', perfil);
                            $.session.set('idtienda', idtienda);
                            $.session.set('tienda', tienda);
                            $.session.set('idempresa', idempresa);
                            // $.session.set('idalmacenprincipal', idalmacenprincipal);

                            document.location.href = "Principal.aspx";
                        }

                        else {



                            swal("Error!", " Usuario o Contraseña incorrecta  ", "error");
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var error = eval("(" + XMLHttpRequest.responseText + ")");
                        alert(error.Message);
                    }
                });
            });


            $('#txtclave').keypress(function (e) {
                var key = e.which;
                if (key == 13) { $("#btnIngresar").click(); }
            });


        });
    </script>

</body>

</html>
