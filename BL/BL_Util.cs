﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.UI;

namespace BL
{
    public class BL_Util
    {

        public void Mensaje(string msj, Page pagina)
        {
            pagina.Response.Write("<script>alert('" + msj + "');</script>");
        }

        public void Salir(Page pagina)
        {
            pagina.Response.Write("<script>window.opener=top;window.close();</script>");
        }


    }
}