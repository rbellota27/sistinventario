﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using DA;

namespace BL
{
    public class BL_Despacho
    {

        DA_Despacho obj_DA_Despacho = new DA_Despacho();

        public List<BE_Despacho> Obtener_Combo_Empresa_X_Usuario(int ID_USUARIO)
        {
            return obj_DA_Despacho.OBTENER_COMBO_EMPRESA_X_USUARIO(ID_USUARIO);
        }

        public List<BE_Despacho> Obtener_Combo_Tienda_x_Empresa_x_Usuario(int ID_USUARIO, int ID_EMPRESA)
        {
            return obj_DA_Despacho.OBTENER_COMBO_TIENDA_X_EMPRESA_X_USUARIO(ID_USUARIO, ID_EMPRESA);
        }


        public List<BE_Despacho> Obtener_Combo_Serie_x_Empresa_Tiend_TipDoc(int ID_EMPRESA, int ID_TIENDA, int ID_TIPO_DOC)
        {
            return obj_DA_Despacho.OBTENER_COMBO_SERIE_X_EMPRESA_TIEND_TIPDOC(ID_EMPRESA, ID_TIENDA, ID_TIPO_DOC);
        }

        public List<BE_Despacho> Obtener_Combo_Estado()
        {
            return obj_DA_Despacho.OBTENER_COMBO_ESTADO();
        }

        public List<BE_Despacho> Obtener_Combo_TipoOperacionxIdTpoDocumento(int ID_TIPO_DOCUMENTO)
        {
            return obj_DA_Despacho.OBTENER_COMBO_TIPOOPERACIONXIDTPODOCUMENTO(ID_TIPO_DOCUMENTO);
        }

        public List<BE_Despacho> Obtener_Combo_Motivo_Traslado(int ID_OPERACION)
        {
            return obj_DA_Despacho.OBTENER_COMBO_MOTIVO_TRASLADO(ID_OPERACION);
        }
        public List<BE_Despacho> Obtener_Combo_MotivoTrasladoxIdTipoOperacion(int ID_TIPO_OPERACION)
        {
            return obj_DA_Despacho.OBTENER_COMBO_MOTIVOTRASLADOXIDTIPOOPERACION(ID_TIPO_OPERACION);
        }

        public List<BE_Despacho> Obtener_Combo_AlmacenxIdTienda(int ID_TIENDA)
        {
            return obj_DA_Despacho.OBTENER_COMBO_ALMACENXIDTIENDA(ID_TIENDA);
        }


        public List<BE_Despacho> Obtener_Combo_TipoDocumentoRefxIdTipoDocumento(int ID_TIPO_DOC)
        {
            return obj_DA_Despacho.OBTENER_COMBO_TIPODOCUMENTOREFXIDTIPODOCUMENTO(ID_TIPO_DOC);
        }



        public List<BE_Despacho> listar_DocumentoReferencia(int ID_TIP_DOC_REF, int ID_EMPRESA, int ID_ALMACEN,
        string FEC_INICIO, string FEC_FIN, string SERIE, string CODIGO, int ID_PERSONA
        , int ID_TIPO_DOC, int ID_TIP_OPERACION)
        {
            return obj_DA_Despacho.LISTAR_DOCUMENTOREFERENCIA(ID_TIP_DOC_REF, ID_EMPRESA, ID_ALMACEN,
             FEC_INICIO, FEC_FIN, SERIE, CODIGO, ID_PERSONA
            , ID_TIPO_DOC, ID_TIP_OPERACION);
        }
        public List<BE_Despacho> LLenarCboSeriexIdsEmpTienTipoDoc(int COD_TIENDA, int COD_EMPRESA, int ID_TIPO_DOC)
        {
            return obj_DA_Despacho.LLENAR_CBOSERIEXIDSEMPTIENTIPODOC(COD_TIENDA, COD_EMPRESA, ID_TIPO_DOC);
        }

        public List<BE_Despacho> llenarCboAlmacenxIdTienda(int COD_TIENDA)
        {
            return obj_DA_Despacho.LLENAR_CBOALMACENXIDTIENDA(COD_TIENDA);
        }


        public List<BE_Despacho> listar_Destinatario(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            return obj_DA_Despacho.LISTAR_DESTINATARIO(TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL);
        }

        public List<BE_Despacho> obtener_datos_remitente(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            return obj_DA_Despacho.OBTENER_DATOS_REMITENTE(TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL);
        }
        public List<BE_Despacho> obtener_datos_agentes(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            return obj_DA_Despacho.OBTENER_DATOS_AGENTE(TIPO_PERSONA, DNI, RUC, ROL, RAZON_SOCIAL);
        }
        public List<BE_Despacho> Obtener_datos_transportista_seleccionado(int ID_TRANSPORTISTA)
        {
            return obj_DA_Despacho.OBTER_DATOS_TRANSPORTISTA_SELECCIONADO(ID_TRANSPORTISTA);
        }

        public List<BE_Despacho> Obtener_datos_remitente_seleccionado(int ID_REMITENTE)
        {
            return obj_DA_Despacho.OBTER_DATOS_REMITENTE_SELECCIONADO(ID_REMITENTE);
        }

        public List<BE_Despacho> Obtener_datos_destinatario_seleccionado(int ID_DESTINATARIO)
        {
            return obj_DA_Despacho.OBTER_DATOS_DESTINATARIO_SELECCIONADO(ID_DESTINATARIO);
        }


        public List<BE_Despacho> Obtener_datos_agente_seleccionado(int ID_AGENTE)
        {
            return obj_DA_Despacho.OBTER_DATOS_AGENTE_SELECCIONADO(ID_AGENTE);
        }

        public List<BE_Despacho> Obtener_datos_chofer_seleccionado(int ID_CHOFER)
        {
            return obj_DA_Despacho.OBTER_DATOS_CHOFER_SELECCIONADO(ID_CHOFER);
        }

        public List<BE_Despacho> obtener_vehiculos(string PLACA)
        {
            return obj_DA_Despacho.OBTENER_VEHICULOS(PLACA);
        }
        public List<BE_Despacho> Obtener_Combo_Rol()
        {
            return obj_DA_Despacho.OBTENER_COMBO_ROL();
        }

        public List<BE_Despacho> Obtener_Combo_TipoAlmacen()
        {
            return obj_DA_Despacho.OBTENER_COMBO_TIPOALMACEN();
        }

        public List<BE_Despacho> Obtener_Combo_AlmacenDestinatario_x_Empresa(string COD_ALM_DESTINATARIO, string COD_EMPRESA)
        {
            return obj_DA_Despacho.OBTENER_COMBO_ALMACENDESTINATARIO_X_EMPRESA(COD_ALM_DESTINATARIO, COD_EMPRESA);
        }

        public List<BE_Despacho> Obtener_Combo_Departamento()
        {
            return obj_DA_Despacho.OBTENER_COMBO_DEPARTAMENTO();
        }


        public List<BE_Documento> Obtener_Combo_Tipo_Existencia()
        {
            return obj_DA_Despacho.OBTENER_COMBO_TIPO_EXISTENCIA();
        }
        public List<BE_Documento> Obtener_Combo_Linea_x_idexistencia(int ID_TIPO_EXISTENCIA)
        {
            return obj_DA_Despacho.OBTENER_COMBO_LINEA_X_IDEXISTENCIA(ID_TIPO_EXISTENCIA);
        }

        public List<BE_Despacho> Obtener_Combo_sublinea_x_Linea_idexistencia(int ID_TIPO_EXISTENCIA, int ID_LINEA)
        {
            return obj_DA_Despacho.OBTENER_COMBO_SUBLINEA_X_IDLINEA_X_IDEXISTENCIA(ID_TIPO_EXISTENCIA, ID_LINEA);
        }

        public List<BE_Despacho> buscar_Productos(int ID_EXISTENCIA, int ID_LINEA, int ID_SUBLINEA, string CODIGO_SUBLINEA_BUSCAR_PRODUCTO,
                         string COD_PRODUCTO, string DESCRIPCION_PRODUCTO, int ID_ALMACEN, int ID_EMPRESA, int PAGE_NUMBER, int PAGE_SIZE)
        {
            return obj_DA_Despacho.BUSCAR_PRODUCTOS(ID_EXISTENCIA, ID_LINEA, ID_SUBLINEA, CODIGO_SUBLINEA_BUSCAR_PRODUCTO,
             COD_PRODUCTO, DESCRIPCION_PRODUCTO, ID_ALMACEN, ID_EMPRESA, PAGE_NUMBER, PAGE_SIZE);
        }
        public List<BE_Despacho> Obtener_Datos_Alm_General(string ID_ALMACEN_GENERAL)
        {
            return obj_DA_Despacho.OBTENER_DATOS_ALM_GENERAL(ID_ALMACEN_GENERAL);
        }
        public List<BE_Despacho> Obtener_Combo_Magnitud_Peso(string ID_MAG_PESO)
        {
            return obj_DA_Despacho.OBTENER_COMBO_MAGNITUD_PESO(ID_MAG_PESO);
        }


        public List<BE_Despacho> Obtener_Combo_Provincia_x_iddepartamento(string ID_DEPARTAMENTO)
        {
            return obj_DA_Despacho.OBTENER_COMBO_PROVINCIA_X_IDDEPARTAMENTO(ID_DEPARTAMENTO);
        }


        public List<BE_Despacho> Obtener_Combo_Distrito_x_idprovincia(string ID_DEPARTAMENTO, string ID_PROVINCIA)
        {
            return obj_DA_Despacho.OBTENER_COMBO_DISTRITO_X_IDPROVINCIA(ID_DEPARTAMENTO, ID_PROVINCIA);
        }

        public List<BE_Despacho> cargarPersona(int ID_PERSONA)
        {
            return obj_DA_Despacho.CARGAR_PERSONA(ID_PERSONA);
        }


        public List<BE_Despacho> Obtener_Datos_Doc_Ref(int ID_DOCUMENTO_REF)
        {
            return obj_DA_Despacho.OBTENER_DATOS_DOC_REF(ID_DOCUMENTO_REF);
        }

        public decimal Obtener_Equivalencia(int ID_PRODUCTO, int ID_UM, int VARIABLE, int ID_MAG_PESO, int ID_UM_PESO)
        {
            return obj_DA_Despacho.OBTENER_EQUIVALENCIA(ID_PRODUCTO, ID_UM, VARIABLE, ID_MAG_PESO, ID_UM_PESO);
        }


        public decimal Obtener_Equivalencia1(int ID_PRODUCTO, int ID_UM, int ID_UM_ST, int VARIABLE)
        {
            return obj_DA_Despacho.OBTENER_EQUIVALENCIA1(ID_PRODUCTO, ID_UM, ID_UM_ST, VARIABLE);
        }

        public decimal Obtener_Stock_Real(int ID_EMPRESA, int ID_ALMACEN, int ID_PRODUCTO)
        {
            return obj_DA_Despacho.OBTENER_STOCK_REAL(ID_EMPRESA, ID_ALMACEN, ID_PRODUCTO);
        }

        public decimal Obtener_Stock_Comprometido(int ID_EMPRESA, int ID_ALMACEN, int ID_PRODUCTO)
        {
            return obj_DA_Despacho.OBTENER_STOCK_COMPROMETIDO(ID_EMPRESA, ID_ALMACEN, ID_PRODUCTO);
        }

        public List<BE_Despacho> Obtener_Productos_Kit(int ID_PRODUCTO)
        {
            return obj_DA_Despacho.OBTENER_PRODUCTOS_KIT(ID_PRODUCTO);
        }


        public List<BE_Despacho> Obtener_datos_almacen_seleccionado(int ID_ALMACEN)
        {
            return obj_DA_Despacho.OBTENER_DATOS_ALMACEN_SELECCIONADO(ID_ALMACEN);
        }



        public List<BE_Despacho> Obtener_Combo_Almacen_destinatario(int TIPO_ALMACEN, int ID_DESTINATARIO)
        {
            return obj_DA_Despacho.OBTENER_COMBO_ALMACEN_DESTINATARIO(TIPO_ALMACEN, ID_DESTINATARIO);
        }

        public List<BE_Despacho> listar_Guia_Remision(string FEC_INICIO, string FEC_FIN, string IDTIENDA, string SERIE, string CODIGO)
        {
            return obj_DA_Despacho.LISTAR_GUIA_REMISION(FEC_INICIO, FEC_FIN, IDTIENDA, SERIE, CODIGO);
        }


        public List<BE_Documento_Referencia> listar_Documento_Referencia(int idDocumento)
        {
            return obj_DA_Despacho.LISTAR_DOCUMENTO_REFERENCIA(idDocumento);
        }

        public List<BE_Producto_Final> ListarProductoFinal(int idDocumento, int idUnidadPeso)
        {
            return obj_DA_Despacho.ListarProductoFinal(idDocumento, idUnidadPeso);
        }

        public string INSERTAR_DOCUMENTO(string codigo, string serie, string FechaEmision, string FechaIniTraslado, string FechaAentregar,
            string FechaEntrega, string FechaVenc, decimal ImporteTotal, decimal Descuento, decimal SubTotal, decimal Igv, decimal Total, string TotalLetras,
            decimal TotalAPagar, decimal ValorReferencial, decimal Utilidad, int IdPersona, int IdUsuario, int IdTransportista, int IdRemitente, int IdDestinatario,
            int IdEstadoDoc, int IdCondicionPago, int IdMoneda, int Lugarentrega, int IdTipoOperacion, int IdTienda, int IdSerie, string ExportadoConta, string NroVoucherConta,
            int IdEmpresa, int IdChofer, int IdMotivoT, int IdTipoDoc, int IdVehiculo, int IdEstadoCan, int IdEstadoEnt, int IdAlmacen, int IdTipoPv, int IdCaja, int IdMedioCredito,
            int doc_CompPercepcion, string doc_FechaCancelacion, int IdUsuarioComision, string[][] VALORES, Boolean moverStock, Boolean ValidarDes, string ubigeoPP, string DireccionPP,
          string ubigepPL, string DireccionPL, string observaciones, string[][] DocRef, int almacenDest)
        {
            return obj_DA_Despacho.INSERTAR_DOCUMENTO(codigo, serie, FechaEmision, FechaIniTraslado, FechaAentregar,
             FechaEntrega, FechaVenc, ImporteTotal, Descuento, SubTotal, Igv, Total, TotalLetras,
             TotalAPagar, ValorReferencial, Utilidad, IdPersona, IdUsuario, IdTransportista, IdRemitente, IdDestinatario,
             IdEstadoDoc, IdCondicionPago, IdMoneda, Lugarentrega, IdTipoOperacion, IdTienda, IdSerie, ExportadoConta, NroVoucherConta,
             IdEmpresa, IdChofer, IdMotivoT, IdTipoDoc, IdVehiculo, IdEstadoCan, IdEstadoEnt, IdAlmacen, IdTipoPv, IdCaja, IdMedioCredito,
             doc_CompPercepcion, doc_FechaCancelacion, IdUsuarioComision, VALORES, moverStock, ValidarDes, ubigeoPP, DireccionPP, ubigepPL, DireccionPL,
             observaciones, DocRef, almacenDest);
        }

        public List<BE.BE_Documento> SelectxIdSeriexCodigo(int idSerie, string codigo){
            return obj_DA_Despacho.SelectguiaXSerie(idSerie, codigo);
}

        public List<BE.BE_Documento_Referencia> GuiaDocRef(int IdDocumento)
        {
            return obj_DA_Despacho.GuiaDocRef(IdDocumento);
        }
        public List<BE.BE_Persona> SelectxIdPersona(int IdDocumento)
        {
            return obj_DA_Despacho.SelectxIdPersona(IdDocumento);
        }

        public List<BE.BE_Despacho> SelectPuntoLlegada(int IdDocumento)
        {
            return obj_DA_Despacho.SelectPuntoLlegada(IdDocumento);
        }

        public List<BE.BE_Documento> Detalle_Guia_Remision(int IdDocumento)
        {
            return obj_DA_Despacho.Detalle_Guia_Remision(IdDocumento);
        }

        public List<BE.BE_Producto_Final> DETALLE_PRODUCTO_GUIA(int IdDocumento)
        {
            return obj_DA_Despacho.DETALLE_PRODUCTO_GUIA(IdDocumento);
        }

        public List<BE.BE_Despacho> OBTENER_VEHICULOSXIDVEHICULO(int IdVehiculo)
        {
            return obj_DA_Despacho.OBTENER_VEHICULOSXIDVEHICULO(IdVehiculo);
        }

        public List<BE.BE_Documento> OBTENER_OBSERVACIONES(int IdDocumento)
        {
            return obj_DA_Despacho.OBTENER_OBSERVACIONES(IdDocumento);
        }

    }
}