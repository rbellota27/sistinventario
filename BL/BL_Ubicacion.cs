﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using DA;
namespace BL
{
    public class BL_Ubicacion
    {
        DA_Ubicacion obj_DA_Ubicacion = new DA_Ubicacion();


        public List<BE_Ubicacion> Obtener_Zona()
        {
            return obj_DA_Ubicacion.OBTENER_ZONA();
        }
        public List<BE_Ubicacion> Obtener_Usuarios()
        {
            return obj_DA_Ubicacion.OBTENER_USUARIOS();
        }
        
  public List<BE_Ubicacion> Obtener_Almacen()
        {
            return obj_DA_Ubicacion.OBTENER_ALMACEN();
        }
        

            public List<BE_Ubicacion> Obtener_Linea()
        {
            return obj_DA_Ubicacion.OBTENER_LINEA();
        }
        
    public List<BE_Ubicacion> Obtener_SubLinea(int IDLINEA)
        {
            return obj_DA_Ubicacion.OBTENER_SUBLINEA(IDLINEA);
        }
        
              public List<BE_Ubicacion> Listar_Ubicacion(int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA, int ESTADO)
        {
            return obj_DA_Ubicacion.LISTAR_UBICACION(IDALMACEN, NOMBREUBICACION, IDLINEA, IDSUBLINEA, IDZONA, ESTADO);
        }

        public List<BE_Ubicacion> Listar_Zona_Usuario(int IDUSUARIO, string IDZONA, int ESTADO)
        {
            return obj_DA_Ubicacion.LISTAR_ZONA_USUARIO(IDUSUARIO, IDZONA,ESTADO);
        }
        


        public List<BE_Ubicacion> Obtener_Ubicacion(int ID_UBICACION)
        {
            return obj_DA_Ubicacion.OBTENER_UBICACION(ID_UBICACION);
        }

        
             public List<BE_Ubicacion> Obtener_Zona_Usuario(int ID_ZONA_USUARIO)
        {
            return obj_DA_Ubicacion.OBTENER_ZONA_USUARIO(ID_ZONA_USUARIO);
        }


        
                  public int Eliminar_Zona_Usuario(int ID_ZONA_USUARIO)
        {
            return obj_DA_Ubicacion.ELIMINAR_ZONA_USUARIO(ID_ZONA_USUARIO);
        }


        

                  public int Eliminar_Ubicacion(int ID_UBICACION)
        {
            return obj_DA_Ubicacion.ELIMINAR_UBICACION(ID_UBICACION);
        }
        public int Guardar_Ubicacion(int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA)
        {
            return obj_DA_Ubicacion.GUARDAR_UBICACION( IDALMACEN,  NOMBREUBICACION,  IDLINEA,  IDSUBLINEA,  IDZONA);
        }
        
             public int Grabar_Zona_Usuario(int IDUSUARIO, int IDZONA)
        {
            return obj_DA_Ubicacion.GUARDAR_ZONA_USUARIO(IDUSUARIO,IDZONA);
        }
        public int Actualizar_Zona_Usuario(int ID_ZONA_USUARIO, int IDUSUARIO, int IDZONA, int IDESTADO)
        {
            return obj_DA_Ubicacion.ACTUALIZAR_ZONA_USUARIO(ID_ZONA_USUARIO, IDUSUARIO, IDZONA, IDESTADO);
        }

        

        public int Actualizar_Ubicacion(int IDUBICACION,int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA,int ESTADO)
        {
            return obj_DA_Ubicacion.ACTUALIZAR_UBICACION(IDUBICACION,IDALMACEN, NOMBREUBICACION, IDLINEA, IDSUBLINEA, IDZONA, ESTADO);
        }


    }
}