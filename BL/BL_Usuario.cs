﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BL
{
    public class BL_Usuario
    {
        private DA.DA_Usuario usuario = new DA.DA_Usuario();
        public int ValidarIdentidad(String Login, String Clave)
        {
            return usuario.ValidarIdentidad(Login, Clave);
        }

        public int ValidarIdentidadPerfil(String Login, String clave)
        {
            return usuario.ValidarIdentidadPerfil(Login, clave);
        }

        public string ValidarTiendaxUsuario(int idUsuario)
        {
            return usuario.ValidarTiendaxUsuario(idUsuario);
        }

        public int SelectIdCajaxIdPersona(int IdPersona)
        {
            return usuario.SelectIdCajaxIdPersona(IdPersona);
        }

        public decimal[] getValorParametroGeneral(int[] IdParametroGeneral)
        {
            decimal[] lista = new decimal[IdParametroGeneral.Length - 1 + 1];

            try
            {

                for (int i = 0; i <= IdParametroGeneral.Length - 1; i++)
                    lista[i] = usuario.ParametroGeneralSelectActivoValorxIdParametro(IdParametroGeneral[i]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return lista;
            //hola
        }
    }
}