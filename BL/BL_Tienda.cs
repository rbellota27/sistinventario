﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using DA;
namespace BL
{
    public class BL_Tienda
    {
        DA_Tienda obj_DA_Tienda = new DA_Tienda();

        public List<BE_Tienda> Obtener_Tienda()
        {
            return obj_DA_Tienda.OBTENER_TIENDA();
        }

        public List<BE_Tienda> OBTENER_TIENDA_USUARIO(int IdUsuario)
        {
            return obj_DA_Tienda.OBTENER_TIENDA_USUARIO(IdUsuario);
        }

        

        public List<BE_Tienda> Obtener_TipoDocumento(int ID_TIENDA)
        {
            return obj_DA_Tienda.OBTENER_TIPO_DOCUMENTO(ID_TIENDA);
        }
        public List<BE_Tienda> Obtener_Serie(int ID_TIPODOCUMENTO, int ID_TIENDA)
        {
            return obj_DA_Tienda.OBTENER_SERIE_DOCUMENTO(ID_TIPODOCUMENTO, ID_TIENDA);
        }

        
    }
}