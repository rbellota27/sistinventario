﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DA;

namespace BL
{
    public class BL_Impresion
    {
        private DA.DA_Impresion docMercantiles = new DA.DA_Impresion();

        public DataSet rptDocGuias(int iddocumento)
        {
            return docMercantiles.rptDocGuias(iddocumento);
        }

        public DataSet rptGuiaxTono(int iddocumento)
        {
            return docMercantiles.rptGuiasxTonos(iddocumento);
        }
    }
}