﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using DA;

namespace BL
{
    public class BL_Documento
    {

        DA_Documento obj_DA_Documento = new DA_Documento();
        DA_Reporteria Obj_Reporteria = new DA_Reporteria();

        public List<BE_Documento> listar_Orden_Pedido_VP(string IdEmpresa, string IdAlmacen, string FecIni,
            string FecFin, string doc_Serie, string doc_Codigo, string IdPersona, string IdDocumento,
            string IdTipoOperacion, string IdTipoDocumento)
        {
            return obj_DA_Documento.LISTAR_ORDEN_PEDIDO_VP(IdEmpresa, IdAlmacen, FecIni,
             FecFin, doc_Serie, doc_Codigo, IdPersona, IdDocumento,
             IdTipoOperacion, IdTipoDocumento);
        }




        public List<BE_Documento> listar_Picking(string FecIni, string FecFin)
        {
            return obj_DA_Documento.LISTAR_PICKING(FecIni, FecFin);
        }



        public List<BE_Documento> listar_CheckList_Picking()
        {
            return obj_DA_Documento.LISTAR_CHECKLIST_PICKING();
        }


        public List<BE_Documento> Listar_Productos_Asignados(int IDUSUARIO)
        {
            return obj_DA_Documento.LISTAR_PRODUCTOS_ASIGNADOS(IDUSUARIO);
        }

        public List<BE_Documento> listar_Detalle_Orden_Pedido_VP(string ID_PEDIDO, string ID_TIPO_OPERACION)
        {
            return obj_DA_Documento.LISTAR_DETALLE_ORDEN_PEDIDO_VP(ID_PEDIDO, ID_TIPO_OPERACION);
        }


        public List<BE_Documento> listar_Detalle_Tonos_Orden_Pedido_VP(string ID_PRODUCTO, string ID_ALMACEN, string ID_DOCUMENTO)
        {
            return obj_DA_Documento.LISTAR_DETALLE_TONOS_ORDEN_PEDIDO_VP(ID_PRODUCTO, ID_ALMACEN, ID_DOCUMENTO);
        }


        public List<BE_Documento> listar_Detalle_Picking(int ID_DOCUMENTO)
        {
            return obj_DA_Documento.LISTAR_DETALLE_PICKING(ID_DOCUMENTO);
        }


        public List<BE_Documento> listar_Detalle_Picking_Tonos(int ID_DOCUMENTO, int ID_DETALLE_DOCUMENTO)
        {
            return obj_DA_Documento.LISTAR_DETALLE_PICKING_TONOS(ID_DOCUMENTO, ID_DETALLE_DOCUMENTO);
        }
        public List<BE_Documento> listar_Detalle_Picking_EnProceso(int ID_DOCUMENTO)
        {
            return obj_DA_Documento.LISTAR_DETALLE_PICKING_ENPROCESO(ID_DOCUMENTO);
        }

        public List<BE_Documento> ObtenerObservacion_Orden_Pedido_VP(string ID_PEDIDO)
        {
            return obj_DA_Documento.OBTENER_OBSERVACION_PV(ID_PEDIDO);
        }


        public string Generar_Picking(string ID_PEDIDO, string ID_SERIE, string NRO_SERIE, string NRO_DOC, int USUARIO)
        {
            return obj_DA_Documento.GENERAR_PICKING(ID_PEDIDO, ID_SERIE, NRO_SERIE, NRO_DOC, USUARIO);
        }

        public string Generar_Picking_Manual(string ID_PEDIDO, string ID_SERIE, string NRO_SERIE, string NRO_DOC, int USUARIO, string[][] VALORES)
        {
            return obj_DA_Documento.GENERAR_PICKING_MANUAL(ID_PEDIDO, ID_SERIE, NRO_SERIE, NRO_DOC, USUARIO, VALORES);
        }




        public int Guardar_Verificacion_Picking(int ID_DOCUMENTO_DETALLE, decimal CANT_PICKEADA, decimal CANT_VERIFICADOR)
        {
            return obj_DA_Documento.GUARDAR_VERIFICACION_PICKING(ID_DOCUMENTO_DETALLE, CANT_PICKEADA, CANT_VERIFICADOR);
        }
        public int Actualizar_Picking_Productos_Asignados(int ID_DETALLEDOCUMENTO, decimal CANTIDAD_ENCONTRADA, int IDUSUARIO)
        {
            return obj_DA_Documento.ACTUALIZAR_PICKING_PRODUCTOS_ASIGNADOS(ID_DETALLEDOCUMENTO, CANTIDAD_ENCONTRADA, IDUSUARIO);
        }
        public List<BE_Documento> Obtener_Serie()
        {
            return obj_DA_Documento.OBTENER_SERIE();
        }


        public List<BE_Documento> Obtener_Consecutivo(string COD_SERIE)
        {
            return obj_DA_Documento.OBTENER_CONSECUTIVO(COD_SERIE);
        }



        public List<BE_Documento> Obtener_Datos_Login(string USUARIO, string CLAVE)
        {
            return obj_DA_Documento.OBTENER_DATOS_LOGIN(USUARIO, CLAVE);
        }

        public List<BE_Opcion> Obtener_Datos_Menu(int idusuario, string perfil)
        {
            return obj_DA_Documento.Obtener_Datos_Menu(idusuario, perfil);
        }
        //Nuevo Apartado
        public List<BE_Documento> LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA(string Doc_Serie, string Doc_Codigo, int TipoDocumento)
        {
            return obj_DA_Documento.LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA(Doc_Serie, Doc_Codigo, TipoDocumento);
        }

        //GUARDAR_REGISTROS_DOCUMENTOS(int Tipo, int IdDocumento, decimal Monto, string Ruta, int IdUsuario
        public string GUARDAR_REGISTROS_DOCUMENTOS(string Tipo, int IdDocumento, decimal Monto, string Ruta, int IdUsuario)
        {
            return obj_DA_Documento.GUARDAR_REGISTROS_DOCUMENTOS(Tipo, IdDocumento, Monto, Ruta, IdUsuario);
        }

        public string GUARDAR_REGISTROS_SOLICITUDDOCUMENTOS(string Tipo, string doc_serie, string doc_codigo, decimal Monto, string Ruta, int IdUsuario, string Observacion)
        {
            return obj_DA_Documento.GUARDAR_REGISTROS_SOLICITUDDOCUMENTOS(Tipo, doc_serie, doc_codigo, Monto, Ruta, IdUsuario, Observacion);
        }

        public string APROBAR_REGISTROS_SOLICITUDDOCUMENTOS(string Tipo, int iddocumento, int IdUsuario, string Ruta)
        {
            return obj_DA_Documento.APROBAR_REGISTROS_SOLICITUDDOCUMENTOS(Tipo, iddocumento, IdUsuario, Ruta);
        }

        public List<BE_Documento> LISTAR_DOCUMENTOS_INGRESADOS(string Tipo)
        {
            return obj_DA_Documento.LISTAR_DOCUMENTOS_INGRESADOS(Tipo);
        }



        public List<BE_Documento> LISTAR_SOLICITUD_DOCUMENTOS(string Tipo, int Tienda, int IdUsuario)
        {
            return obj_DA_Documento.LISTAR_SOLICITUD_DOCUMENTOS(Tipo, Tienda, IdUsuario);
        }

        public List<BE_Documento> LISTAR_DOCUMENTOS_INGRESADOS_FECHAS(int Tienda, string fechainicial, string fechafinal)
        {
            return obj_DA_Documento.LISTAR_DOCUMENTOS_INGRESADOS_FECHAS(Tienda, fechainicial, fechafinal);
        }




        public string GUARDAR_REGISTROS_DOCUMENTOS_EXTORNOS_IZIPAY(string Tipo, int IdDocumento, int IdDoc, string CODIGO, string TIPO_DE_MOVIMIENTO, string SERVICIO, string TRANSACCION, DateTime FECHA_DE_TRANSACCION,
          string HORA_DE_TRANSACCION, DateTime FECHA_DE_CIERRE_DE_LOTE, DateTime FECHA_DE_PROCESO, DateTime FECHA_DE_ABONO, string ESTADO, decimal IMPORTE, decimal COMISION, decimal IGV, decimal IMPORTE_NETO,
          decimal ABONO_DEL_LOTE, string NUM_DE_LOTE, string TERMINAL, string NUM_DE_REF_VOUCHER, string MARCA_DE_TARJETA, string NUM_DE_TARJETA, string CODIGO_DE_AUTORIZACION, int CUOTAS, string OBSERVACIONES,
          string MONEDA, string SERIE_TERMINAL, int USUARIO)
        {
            return obj_DA_Documento.GUARDAR_REGISTROS_DOCUMENTOS_IZIPAY(Tipo, IdDocumento, IdDoc, CODIGO, TIPO_DE_MOVIMIENTO, SERVICIO, TRANSACCION, FECHA_DE_TRANSACCION,
            HORA_DE_TRANSACCION, FECHA_DE_CIERRE_DE_LOTE, FECHA_DE_PROCESO, FECHA_DE_ABONO, ESTADO, IMPORTE, COMISION, IGV, IMPORTE_NETO,
            ABONO_DEL_LOTE, NUM_DE_LOTE, TERMINAL, NUM_DE_REF_VOUCHER, MARCA_DE_TARJETA, NUM_DE_TARJETA, CODIGO_DE_AUTORIZACION, CUOTAS, OBSERVACIONES,
            MONEDA, SERIE_TERMINAL, USUARIO);
        }



        public string GUARDAR_REGISTROS_DOCUMENTOS_NIUBIZ(
        string Tipo, int IdDocumento, int IdDoc, string RUC, string RAZON_SOCIAL,
         string COD_COMERCIO,
         string NOMBRE_COMERCIAL,
         string FECHA_HORA_OPERACION,
         string FECHA_DEPOSITO,
         string PRODUCTO,
         string TIPO_OPERACION,
         string TARJETA,
         string ORIGEN_TARJETA,
         string TIPO_TARJETA,
         string MARCA_TARJETA,
         string MONEDA,
         decimal IMPORTE_OPERACION,
         string ES_DCC,
         decimal MONTO_DCC,
         decimal COMISION_TOTAL,
         decimal COMISION_NIUBIZ,
         decimal IGV,
         decimal SUMA_DEPOSITADA,
         string ESTADO,
         string ID_OPERACION,
         string CUENTA_BANCO_PAGADOR,
         string BANCO_PAGADOR,
        int USUARIO)
        {
            return obj_DA_Documento.GUARDAR_REGISTROS_DOCUMENTOS_NIUBIZ(Tipo, IdDocumento, IdDoc, RUC, RAZON_SOCIAL, COD_COMERCIO, NOMBRE_COMERCIAL, FECHA_HORA_OPERACION, FECHA_DEPOSITO,
                PRODUCTO, TIPO_OPERACION, TARJETA, ORIGEN_TARJETA, TIPO_TARJETA, MARCA_TARJETA, MONEDA, IMPORTE_OPERACION, ES_DCC, MONTO_DCC, COMISION_TOTAL, COMISION_NIUBIZ, IGV, SUMA_DEPOSITADA,
                ESTADO, ID_OPERACION, CUENTA_BANCO_PAGADOR, BANCO_PAGADOR, USUARIO);
        }


        public string GUARDAR_REGISTROS_DOCUMENTOS_AMEX(string Tipo, int IdDocumento, int IdDoc, string Establecimiento, string Fecha, string Tarjeta, string CODIGO, string Moneda, Decimal Monto_Venta,
        Decimal Comision_Emisior, Decimal Comision_Mercant, Decimal IGV_Comision_Mercant, Decimal Monto_Depositar, string Fecha_Deposito, string Estado, int USUARIO)
        {
            return obj_DA_Documento.GUARDAR_REGISTROS_DOCUMENTOS_AMEX(Tipo, IdDocumento, IdDoc, Establecimiento, Fecha, Tarjeta, CODIGO, Moneda, Monto_Venta,
         Comision_Emisior, Comision_Mercant, IGV_Comision_Mercant, Monto_Depositar, Fecha_Deposito, Estado, USUARIO);
        }


        public string GUARDAR_REGISTROS_DOCUMENTOS_DINERS(string Tipo, int IdDocumento, int IdDoc, string Red, string Codigo, string Nombre, DateTime Fecha, string Moneda, string Recap, string Ticket, string Tarjeta, DateTime Fecha_Consumo, decimal Consumo, decimal Consumo_Comision, decimal Manejo, decimal Comision_Manejo, decimal IGV, decimal Pago_Consumo, string Estado, string Documento_Autorizado
            , DateTime Fecha_Pago, string Banco, string Cuenta, string Numero_Autorizacion, int USUARIO)
        {
            return obj_DA_Documento.GUARDAR_REGISTROS_DOCUMENTOS_DINERS(Tipo, IdDocumento, IdDoc, Red, Codigo, Nombre, Fecha, Moneda, Recap, Ticket, Tarjeta, Fecha_Consumo, Consumo, Consumo_Comision, Manejo, Comision_Manejo, IGV, Pago_Consumo, Estado, Documento_Autorizado
            , Fecha_Pago, Banco, Cuenta, Numero_Autorizacion, USUARIO);
        }

        public List<BE_Documento> OBTENER_TIPO_BANCO()
        {
            return obj_DA_Documento.OBTENER_TIPO_BANCO();
        }

        public List<BE_Reporte> LISTAR_DOCUMENTOS_AMEX(int Tipo_Banco, int Tipo_Documento, string fechainicial, string fechafinal, string serie, string Numero)
        {
            return obj_DA_Documento.LISTAR_DOCUMENTOS_AMEX(Tipo_Banco, Tipo_Documento, fechainicial, fechafinal, serie, Numero);
        }


        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS(string Tipo, string Estado)
        {
            return Obj_Reporteria.LISTAR_DOCUMENTOS_EXTORNOS(Tipo, Estado);
        }

        public List<BE_Reporte> OBTENER_ESTADO()
        {
            return Obj_Reporteria.OBTENER_ESTADO();
        }
        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS_DETALLE(string Tipo, int Iddocumento)
        {
            return Obj_Reporteria.LISTAR_DOCUMENTOS_EXTORNOS_DETALLE(Tipo, Iddocumento);
        }

        public List<BE_Reporte> ListarComboArea()
        {
            return Obj_Reporteria.ListarComboArea();
        }

        public List<BE_Reporte> ListarComboSede()
        {
            return Obj_Reporteria.ListarComboSede();
        }

        public List<BE_Reporte> ListarTipoCpu()
        {
            return Obj_Reporteria.ListarTipoCpu();
        }

        public List<BE_Reporte> ListarCapacidad()
        {
            return Obj_Reporteria.ListarCapacidad();
        }

        public List<BE_Reporte> ListarTipoDisco()
        {
            return Obj_Reporteria.ListarTipoDisco();
        }
        public List<BE_Reporte> ListarMarcaDisco()
        {
            return Obj_Reporteria.ListarMarcaDisco();
        }

        public List<BE_Reporte> ListarTipoMemoria()
        {
            return Obj_Reporteria.ListarTipoMemoria();
        }




        public Boolean GuardarRegistro(string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
         string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
         string licenciaoffice, string obs, string cboarea, string cbosede, string cbotipomemoria, string cbotipodisco, string cbocapacidad, string marcadisco, string nrocompra, string idusuarioregistro
)
        {
            return Obj_Reporteria.GuardarRegistro(cod, responsable, cel, dominio, cargo, usuario, correo, hostname, ip, ipwir, mac, macwireles, tipo,
             seriecpu, marcacpu, mainboardcpu, procesador, memoria, discoduro, covint, marcamonit, modelomonit, nseriemonit, teclado, mouse, licencia,
             licenciaoffice, obs, cboarea, cbosede, cbotipomemoria, cbotipodisco, cbocapacidad, marcadisco, nrocompra, idusuarioregistro);
        }

        public List<BE_Reporte> ListarInventario()
        {
            return Obj_Reporteria.ListarInventario();
        }

        public BE_Reporte ListarInventarioxid(string iddocumento)
        {
            return Obj_Reporteria.ListarInventarioxid(iddocumento);
        }

        public Boolean ActualizarRegistro(string id, string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
    string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
    string licenciaoffice, string obs, string cboarea, string cbosede, string cbotipomemoria, string cbotipodisco, string cbocapacidad, string marcadisco, string estadoasoignacion, string nrocompra, string idusuarioregistro)
        {
            return Obj_Reporteria.ActualizarRegistro(id, cod, responsable, cel, dominio, cargo, usuario, correo, hostname, ip, ipwir, mac, macwireles, tipo,
             seriecpu, marcacpu, mainboardcpu, procesador, memoria, discoduro, covint, marcamonit, modelomonit, nseriemonit, teclado, mouse, licencia,
             licenciaoffice, obs, cboarea, cbosede, cbotipomemoria, cbotipodisco, cbocapacidad, marcadisco, estadoasoignacion, nrocompra, idusuarioregistro);
        }


        public List<int> CountCantEstaciones()
        {
            return Obj_Reporteria.CountCantEstaciones();
        }
        public List<BE_Reporte> ListarAuditoriaxid(string id)
        {
            return Obj_Reporteria.ListarAuditoriaxid(id);
        }

        public Boolean Eliminarxid(string id)
        {
            return Obj_Reporteria.Eliminarxid(id);
        }

        public Boolean ValidarMacE(string mace)
        {
            return Obj_Reporteria.ValidarMacE(mace);
        }
        public Boolean ValidarMacW(string macw)
        {
            return Obj_Reporteria.ValidarMacW(macw);
        }

        
    }
}