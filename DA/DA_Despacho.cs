﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using System.Data;
using System.Configuration;
using System.Management;
using System.Data.SqlClient;
using System.Diagnostics;
namespace DA
{
    public class DA_Despacho
    {
        string conexion = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        public List<BE_Despacho> OBTENER_COMBO_EMPRESA_X_USUARIO(int ID_USUARIO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_EmpresaTiendaUsuarioSelectEmpresaxIdUsuario", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdUsuario", ID_USUARIO);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idempresa = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomempresa = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_TIENDA_X_EMPRESA_X_USUARIO(int ID_USUARIO, int ID_EMPRESA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            { SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_EmpresaTiendaUsuarioSelectTiendaxIdEmpresaxIdUsuario", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdUsuario", ID_USUARIO);
                    cmd.Parameters.AddWithValue("@IdEmpresa", ID_EMPRESA);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idtienda = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtienda = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_SERIE_X_EMPRESA_TIEND_TIPDOC(int ID_EMPRESA, int ID_TIENDA, int ID_TIPO_DOC)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_SerieSelectxIdsEmpTienTDoc", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdEmpresa", ID_EMPRESA);
                    cmd.Parameters.AddWithValue("@IdTienda", ID_TIENDA);
                    cmd.Parameters.AddWithValue("@IdTipoDocumento", ID_TIPO_DOC);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idserie = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.numserie = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_ESTADO()
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_EstadoDocumentoSelectCbo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idestado = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomestado = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_TIPOOPERACIONXIDTPODOCUMENTO(int ID_TIPO_DOCUMENTO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_TipoOperacionSelectCboxIdTipoDocumento", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoDocumento", ID_TIPO_DOCUMENTO);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idtipooperacion = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtipooperacion = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Despacho> OBTENER_COMBO_MOTIVO_TRASLADO(int ID_OPERACION)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_MotivoTrasladoSelectCboxIdTipoOperacion", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoOperacion", ID_OPERACION);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.IdMotivoT = lector[0].ToString();
                        obj_BE_Documento.motivo_traslado = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_MOTIVOTRASLADOXIDTIPOOPERACION(int ID_TIPO_OPERACION)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_MotivoTrasladoSelectCboxIdTipoOperacion", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoOperacion", ID_TIPO_OPERACION);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idtipooperacion = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtipooperacion = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_ALMACENXIDTIENDA(int ID_TIENDA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_AlmacenSelectCboxIdTienda", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTienda", ID_TIENDA);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idalmacen = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.nomalmacen = lector[0].ToString();
                        obj_BE_Documento.flag_almacen_principal = lector[3].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_TIPODOCUMENTOREFXIDTIPODOCUMENTO(int ID_TIPO_DOC)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_SelectxIdtipoDocRef", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoDocumento", ID_TIPO_DOC);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idtipodocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtipodocumento = lector[2].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> LISTAR_DOCUMENTOREFERENCIA(int ID_TIP_DOC_REF, int ID_EMPRESA, int ID_ALMACEN, string FEC_INICIO, string FEC_FIN, string SERIE, string CODIGO, int ID_PERSONA, int ID_TIPO_DOC, int ID_TIP_OPERACION)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_DocumentoSelectDocxDespachar_V2_Detalle_porborrar2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", ID_TIP_DOC_REF);
                    cmd.Parameters.AddWithValue("@IdEmpresa", ID_EMPRESA);
                    cmd.Parameters.AddWithValue("@IdAlmacen", ID_ALMACEN);
                    cmd.Parameters.AddWithValue("@FechaFin", Convert.ToDateTime(FEC_FIN));
                    cmd.Parameters.AddWithValue("@FechaInicio", Convert.ToDateTime(FEC_INICIO));
                    cmd.Parameters.AddWithValue("@Serie", SERIE);
                    cmd.Parameters.AddWithValue("@Codigo", CODIGO);
                    cmd.Parameters.AddWithValue("@IdPersona", ID_PERSONA);
                    cmd.Parameters.AddWithValue("@IdTipoDocumento", ID_TIPO_DOC);
                    cmd.Parameters.AddWithValue("@IdTipoOperacion", ID_TIP_OPERACION);



                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.iddocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtipodocumento = lector[11].ToString();
                        obj_BE_Documento.numdoc = lector[1].ToString() + '-' + lector[2].ToString();
                        obj_BE_Documento.fechaemision = lector[3].ToString();
                        obj_BE_Documento.empresa = lector[14].ToString();
                        obj_BE_Documento.ruc = lector[18].ToString();
                        obj_BE_Documento.dni = lector[19].ToString();
                        obj_BE_Documento.almacen = lector[15].ToString();
                        obj_BE_Documento.motivo_traslado = lector[21].ToString();
                        obj_BE_Documento.tipo_operacion = lector[22].ToString();
                        obj_BE_Documento.fecha_registro = lector[23].ToString();
                        obj_BE_Documento.idalmacen = Convert.ToInt32(lector[10].ToString());
                        obj_BE_Documento.idtipooperacion = Convert.ToInt32(lector[24].ToString());
                        obj_BE_Documento.idpersona = !lector.IsDBNull(25) ? Convert.ToInt32(lector[24].ToString()) : 0;
                        obj_BE_Documento.serieOP = lector[26].ToString();
                        obj_BE_Documento.codigoOP = lector[27].ToString();
                       


                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> LLENAR_CBOSERIEXIDSEMPTIENTIPODOC(int COD_TIENDA, int COD_EMPRESA, int ID_TIPO_DOC)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_SerieSelectxIdsEmpTienTDoc", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdEmpresa", COD_EMPRESA);
                    cmd.Parameters.AddWithValue("@IdTienda", COD_TIENDA);
                    cmd.Parameters.AddWithValue("@IdTipoDocumento", ID_TIPO_DOC);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idserie = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.numserie = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> LLENAR_CBOALMACENXIDTIENDA(int COD_TIENDA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_AlmacenSelectCboxIdTienda", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdTienda", COD_TIENDA);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idalmacen = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.nomalmacen = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> LISTAR_DESTINATARIO(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_listarxPersonaNaturalxPersonaJuridica", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Dni", DNI);
                    cmd.Parameters.AddWithValue("@Ruc", RUC);
                    cmd.Parameters.AddWithValue("@razonApe", RAZON_SOCIAL);
                    cmd.Parameters.AddWithValue("@tipo", TIPO_PERSONA);
                    cmd.Parameters.AddWithValue("@pageindex", 0);
                    cmd.Parameters.AddWithValue("@pageSize", 10);
                    cmd.Parameters.AddWithValue("@idRol", ROL);
                    cmd.Parameters.AddWithValue("@estado", 1);
                    cmd.Parameters.AddWithValue("@IdNacionalidad", 0);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nombre_persona = lector[1].ToString();
                        obj_BE_Documento.ruc = lector[3].ToString();
                        obj_BE_Documento.dni = lector[2].ToString();
                        obj_BE_Documento.nacionalidad = lector[7].ToString();
                        obj_BE_Documento.estado = lector[4].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Despacho> OBTENER_DATOS_REMITENTE(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_listarxPersonaNaturalxPersonaJuridica", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Dni", DNI);
                    cmd.Parameters.AddWithValue("@Ruc", RUC);
                    cmd.Parameters.AddWithValue("@razonApe", RAZON_SOCIAL);
                    cmd.Parameters.AddWithValue("@tipo", TIPO_PERSONA);
                    cmd.Parameters.AddWithValue("@pageindex", 0);
                    cmd.Parameters.AddWithValue("@pageSize", 10);
                    cmd.Parameters.AddWithValue("@idRol", ROL);
                    cmd.Parameters.AddWithValue("@estado", 1);
                    cmd.Parameters.AddWithValue("@IdNacionalidad", 0);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nombre_persona = lector[1].ToString();
                        obj_BE_Documento.ruc = lector[3].ToString();
                        obj_BE_Documento.dni = lector[2].ToString();
                        obj_BE_Documento.nacionalidad = lector[7].ToString();
                        obj_BE_Documento.estado = lector[4].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_DATOS_AGENTE(string TIPO_PERSONA, string DNI, string RUC, string ROL, string RAZON_SOCIAL)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_listarxPersonaNaturalxPersonaJuridica", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Dni", DNI);
                    cmd.Parameters.AddWithValue("@Ruc", RUC);
                    cmd.Parameters.AddWithValue("@razonApe", RAZON_SOCIAL);
                    cmd.Parameters.AddWithValue("@tipo", TIPO_PERSONA);
                    cmd.Parameters.AddWithValue("@pageindex", 0);
                    cmd.Parameters.AddWithValue("@pageSize", 10);
                    cmd.Parameters.AddWithValue("@idRol", ROL);
                    cmd.Parameters.AddWithValue("@estado", 1);
                    cmd.Parameters.AddWithValue("@IdNacionalidad", 0);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nombre_persona = lector[1].ToString();
                        obj_BE_Documento.ruc = lector[3].ToString();
                        obj_BE_Documento.dni = lector[2].ToString();
                        obj_BE_Documento.nacionalidad = lector[7].ToString();
                        obj_BE_Documento.estado = lector[4].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTER_DATOS_TRANSPORTISTA_SELECCIONADO(int ID_TRANSPORTISTA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_PersonaViewSelectxIdPersona", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdPersona", ID_TRANSPORTISTA);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomcomercial = lector[1].ToString();
                        obj_BE_Documento.nombre_persona = lector[2].ToString();
                        obj_BE_Documento.razonsocial = lector[3].ToString();
                        obj_BE_Documento.dni = lector[4].ToString();
                        obj_BE_Documento.ruc = lector[5].ToString();
                        obj_BE_Documento.direccion = lector[6].ToString();
                        obj_BE_Documento.ubigeo = lector[7].ToString();
                        obj_BE_Documento.propietario = lector[8].ToString();
                        obj_BE_Documento.descripcion = lector[9].ToString();
                        obj_BE_Documento.nrolicencia = lector[10].ToString();
                        obj_BE_Documento.tipo_pv = lector[11].ToString();
                        obj_BE_Documento.idtipo_pv = lector[12].ToString();
                        obj_BE_Documento.telefono = lector[13].ToString();
                        obj_BE_Documento.correo = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Despacho> OBTER_DATOS_REMITENTE_SELECCIONADO(int ID_REMITENTE)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_PersonaViewSelectxIdPersona", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdPersona", ID_REMITENTE);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomcomercial = lector[1].ToString();
                        obj_BE_Documento.nombre_persona = lector[2].ToString();
                        obj_BE_Documento.razonsocial = lector[3].ToString();
                        obj_BE_Documento.dni = lector[4].ToString();
                        obj_BE_Documento.ruc = lector[5].ToString();
                        obj_BE_Documento.direccion = lector[6].ToString();
                        obj_BE_Documento.ubigeo = lector[7].ToString();
                        obj_BE_Documento.propietario = lector[8].ToString();
                        obj_BE_Documento.descripcion = lector[9].ToString();
                        obj_BE_Documento.nrolicencia = lector[10].ToString();
                        obj_BE_Documento.tipo_pv = lector[11].ToString();
                        obj_BE_Documento.idtipo_pv = lector[12].ToString();
                        obj_BE_Documento.telefono = lector[13].ToString();
                        obj_BE_Documento.correo = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Despacho> OBTER_DATOS_DESTINATARIO_SELECCIONADO(int ID_DESTINATARIO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_PersonaViewSelectxIdPersona", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdPersona", ID_DESTINATARIO);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomcomercial = lector[1].ToString();
                        obj_BE_Documento.nombre_persona = lector[2].ToString();
                        obj_BE_Documento.razonsocial = lector[3].ToString();
                        obj_BE_Documento.dni = lector[4].ToString();
                        obj_BE_Documento.ruc = lector[5].ToString();
                        obj_BE_Documento.direccion = lector[6].ToString();
                        obj_BE_Documento.ubigeo = lector[7].ToString();
                        obj_BE_Documento.propietario = lector[8].ToString();
                        obj_BE_Documento.descripcion = lector[9].ToString();
                        obj_BE_Documento.nrolicencia = lector[10].ToString();
                        obj_BE_Documento.tipo_pv = lector[11].ToString();
                        obj_BE_Documento.idtipo_pv = lector[12].ToString();
                        obj_BE_Documento.telefono = lector[13].ToString();
                        obj_BE_Documento.correo = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTER_DATOS_AGENTE_SELECCIONADO(int ID_AGENTE)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_PersonaViewSelectxIdPersona", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdPersona", ID_AGENTE);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomcomercial = lector[1].ToString();
                        obj_BE_Documento.nombre_persona = lector[2].ToString();
                        obj_BE_Documento.razonsocial = lector[3].ToString();
                        obj_BE_Documento.dni = lector[4].ToString();
                        obj_BE_Documento.ruc = lector[5].ToString();
                        obj_BE_Documento.direccion = lector[6].ToString();
                        obj_BE_Documento.ubigeo = lector[7].ToString();
                        obj_BE_Documento.propietario = lector[8].ToString();
                        obj_BE_Documento.descripcion = lector[9].ToString();
                        obj_BE_Documento.nrolicencia = lector[10].ToString();
                        obj_BE_Documento.tipo_pv = lector[11].ToString();
                        obj_BE_Documento.idtipo_pv = lector[12].ToString();
                        obj_BE_Documento.telefono = lector[13].ToString();
                        obj_BE_Documento.correo = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTER_DATOS_CHOFER_SELECCIONADO(int ID_CHOFER)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_PersonaViewSelectxIdPersona", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdPersona", ID_CHOFER);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomcomercial = lector[1].ToString();
                        obj_BE_Documento.nombre_persona = lector[2].ToString();
                        obj_BE_Documento.razonsocial = lector[3].ToString();
                        obj_BE_Documento.dni = lector[4].ToString();
                        obj_BE_Documento.ruc = lector[5].ToString();
                        obj_BE_Documento.direccion = lector[6].ToString();
                        obj_BE_Documento.ubigeo = lector[7].ToString();
                        obj_BE_Documento.propietario = lector[8].ToString();
                        obj_BE_Documento.descripcion = lector[9].ToString();
                        obj_BE_Documento.nrolicencia = lector[10].ToString();
                        obj_BE_Documento.tipo_pv = lector[11].ToString();
                        obj_BE_Documento.idtipo_pv = lector[12].ToString();
                        obj_BE_Documento.telefono = lector[13].ToString();
                        obj_BE_Documento.correo = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_VEHICULOS(string PLACA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_VehiculoSelectActivoxNroPlaca", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Placa", PLACA);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.veh_Placa = lector[0].ToString();
                        obj_BE_Documento.id_producto = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.veh_Modelo = lector[2].ToString();
                        obj_BE_Documento.veh_NConstanciaIns = lector[3].ToString();
                        obj_BE_Documento.veh_ConfVeh = lector[4].ToString();
                        obj_BE_Documento.veh_Tara = lector[5].ToString();
                        obj_BE_Documento.capacidad = lector[6].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_ROL()
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_RolSelectCbo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idrol = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nombre_rol = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_TIPOALMACEN()
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_SelectTipoAlmacen", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idtipoalmacen = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtipoalmacen = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_ALMACENDESTINATARIO_X_EMPRESA(string COD_ALM_DESTINATARIO, string COD_EMPRESA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_AlmacenSelectAllActivoxIdTipoAlmacenxIdEmpresa", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoAlmacen", COD_ALM_DESTINATARIO);
                    cmd.Parameters.AddWithValue("@IdEmpresa", COD_EMPRESA);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idalmacen = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomalmacen = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_DEPARTAMENTO()
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_UbigeoSelectAllDepartamentos", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.iddepartamento = lector[0].ToString();
                        obj_BE_Documento.nomdepartamento = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Documento> OBTENER_COMBO_TIPO_EXISTENCIA()
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_TipoExistenciaSelectAllActivo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.idtipo_existencia = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.existencia_nombre = lector[2].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Documento> OBTENER_COMBO_LINEA_X_IDEXISTENCIA(int ID_TIPO_EXISTENCIA)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_LineaSelectCboxIdtipoexistencia", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", ID_TIPO_EXISTENCIA);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.idlinea = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.Linea = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Despacho> OBTENER_COMBO_SUBLINEA_X_IDLINEA_X_IDEXISTENCIA(int ID_TIPO_EXISTENCIA, int ID_LINEA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_SubLineaSelectActivoxLineaxTipoEx", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoEx", ID_TIPO_EXISTENCIA);
                    cmd.Parameters.AddWithValue("@IdLinea", ID_LINEA);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idsublinea = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.sublinea = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Despacho> BUSCAR_PRODUCTOS(int ID_EXISTENCIA, int ID_LINEA, int ID_SUBLINEA, string CODIGO_SUBLINEA_BUSCAR_PRODUCTO,
                      string COD_PRODUCTO, string DESCRIPCION_PRODUCTO, int ID_ALMACEN, int ID_EMPRESA, int PAGE_NUMBER, int PAGE_SIZE)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_PRODUCTOS_X_PARAMETROS", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdTipoExistencia", ID_EXISTENCIA);
                    cmd.Parameters.AddWithValue("@IdLinea", ID_LINEA);
                    cmd.Parameters.AddWithValue("@IdSubLinea", ID_SUBLINEA);
                    cmd.Parameters.AddWithValue("@CodSubLinea", CODIGO_SUBLINEA_BUSCAR_PRODUCTO);
                    cmd.Parameters.AddWithValue("@CodigoProducto", COD_PRODUCTO);
                    cmd.Parameters.AddWithValue("@Descripcion", DESCRIPCION_PRODUCTO);
                    cmd.Parameters.AddWithValue("@IdAlmacen", ID_ALMACEN);
                    cmd.Parameters.AddWithValue("@IdEmpresa", ID_EMPRESA);
                    cmd.Parameters.AddWithValue("@PageNumber", PAGE_NUMBER);
                    cmd.Parameters.AddWithValue("@PageSize", PAGE_SIZE);



                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Despacho = new BE_Despacho();

                        obj_BE_Despacho.codigo_producto = lector[15].ToString();
                        obj_BE_Despacho.descripcion_producto = lector[4].ToString();
                        obj_BE_Despacho.um_producto = lector[2].ToString();
                        obj_BE_Despacho.stock_disponible = lector[18].ToString();

                        obj_BE_Despacho.id_producto = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Despacho.id_um = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Despacho.kit = lector[16].ToString();

                        lista.Add(obj_BE_Despacho);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_DATOS_ALM_GENERAL(string ID_ALMACEN)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_AlmacenSelectxId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdAlmacen", ID_ALMACEN);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.ubigeo = lector[2].ToString();
                        obj_BE_Documento.direccion = lector[3].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_MAGNITUD_PESO(string ID_MAG_PESO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_MagnitudUnidadSelectCboUM", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdMagnitud", ID_MAG_PESO);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.id_um = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.um_producto = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_PROVINCIA_X_IDDEPARTAMENTO(string ID_DEPARTAMENTO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_UbigeoSelectAllProvinciasxCodDpto", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ub_CodDpto", ID_DEPARTAMENTO);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idprovincia = lector[0].ToString();
                        obj_BE_Documento.nomprovincia = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_COMBO_DISTRITO_X_IDPROVINCIA(string ID_DEPARTAMENTO, string ID_PROVINCIA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_UbigeoSelectAllDistritosxCodDptoxCodProv", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ub_CodDpto", ID_DEPARTAMENTO);
                    cmd.Parameters.AddWithValue("@ub_CodProv", ID_PROVINCIA);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.iddistrito = lector[0].ToString();
                        obj_BE_Documento.nomdistrito = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> CARGAR_PERSONA(int ID_PERSONA)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_PersonaViewSelectxIdPersona", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdPersona", ID_PERSONA);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idpersona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomcomercial = lector[1].ToString();
                        obj_BE_Documento.nombre_persona = lector[2].ToString();
                        obj_BE_Documento.razonsocial = lector[3].ToString();
                        obj_BE_Documento.dni = lector[4].ToString();
                        obj_BE_Documento.ruc = lector[5].ToString();
                        obj_BE_Documento.direccion = lector[6].ToString();
                        obj_BE_Documento.ubigeo = lector[7].ToString();
                        obj_BE_Documento.propietario = lector[8].ToString();
                        obj_BE_Documento.descripcion = lector[9].ToString();
                        obj_BE_Documento.nrolicencia = lector[10].ToString();
                        obj_BE_Documento.tipo_pv = lector[11].ToString();
                        obj_BE_Documento.idtipo_pv = lector[12].ToString();
                        obj_BE_Documento.telefono = lector[13].ToString();
                        obj_BE_Documento.correo = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Despacho> OBTENER_DATOS_DOC_REF(int ID_DOCUMENTO_REF)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_DocumentoSelectxIdDocumento", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdDocumento", ID_DOCUMENTO_REF);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.iddocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.fechaemision = lector[3].ToString();
                        obj_BE_Documento.FechaIniTraslado = lector[4].ToString();
                        obj_BE_Documento.FechaRegistro = lector[5].ToString();
                        obj_BE_Documento.FechaAEntregar = lector[6].ToString();
                        obj_BE_Documento.FechaEntrega = lector[8].ToString();
                        obj_BE_Documento.FechaVenc = lector[7].ToString();
                        obj_BE_Documento.Descuento = lector[11].ToString();
                        obj_BE_Documento.SubTotal = lector[12].ToString();
                        obj_BE_Documento.IGV = lector[13].ToString();
                        obj_BE_Documento.Total = lector[14].ToString();
                        obj_BE_Documento.TotalAPagar = lector[16].ToString();
                        obj_BE_Documento.ValorReferencial = lector[17].ToString();
                        obj_BE_Documento.IdPersona = lector[19].ToString();
                        obj_BE_Documento.IdUsuario = lector[20].ToString();
                        obj_BE_Documento.IdTransportista = lector[21].ToString();
                        obj_BE_Documento.IdRemitente = lector[22].ToString();
                        obj_BE_Documento.IdDestinatario = lector[23].ToString();
                        obj_BE_Documento.IdEstadoDoc = lector[24].ToString();
                        obj_BE_Documento.IdCondicionPago = lector[25].ToString();
                        obj_BE_Documento.IdMoneda = lector[26].ToString();
                        obj_BE_Documento.LugarEntrega = lector[27].ToString();
                        obj_BE_Documento.IdTipoOperacion = lector[28].ToString();
                        obj_BE_Documento.IdTienda = lector[29].ToString();
                        obj_BE_Documento.IdEmpresa = lector[32].ToString();
                        obj_BE_Documento.IdChofer = lector[33].ToString();
                        obj_BE_Documento.IdMotivoT = lector[35].ToString();
                        obj_BE_Documento.IdTipoDocumento = lector[36].ToString();
                        obj_BE_Documento.IdVehiculo = lector[37].ToString();
                        obj_BE_Documento.IdEstadoCancelacion = lector[38].ToString();
                        obj_BE_Documento.IdEstadoEntrega = lector[39].ToString();
                        obj_BE_Documento.FechaCancelacion = lector[9].ToString();
                        obj_BE_Documento.IdAlmacen = lector[42].ToString();
                        obj_BE_Documento.NomAlmacen = lector[43].ToString();
                        obj_BE_Documento.Codigo = lector[2].ToString();
                        obj_BE_Documento.Serie = lector[1].ToString();
                        obj_BE_Documento.Percepcion = lector[44].ToString();
                        obj_BE_Documento.Detraccion = lector[45].ToString();
                        obj_BE_Documento.Retencion = lector[46].ToString();
                        obj_BE_Documento.PoseeOrdenDespacho = lector[48].ToString();
                        obj_BE_Documento.PoseeAmortizaciones = lector[49].ToString();
                        obj_BE_Documento.PoseeGRecepcion = lector[50].ToString();
                        obj_BE_Documento.NomMoneda = lector[47].ToString();
                        obj_BE_Documento.NomTipoDocumento = lector[51].ToString();
                        obj_BE_Documento.Empresa = lector[52].ToString();
                        obj_BE_Documento.Tienda = lector[53].ToString();
                        obj_BE_Documento.NomEstadoDocumento = lector[54].ToString();
                        obj_BE_Documento.NroDocumento = lector[1].ToString() + " - " + lector[2].ToString();
                        obj_BE_Documento.DescripcionPersona = lector[55].ToString();
                        obj_BE_Documento.IdUsuarioComision = lector[56].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public decimal OBTENER_EQUIVALENCIA(int ID_PRODUCTO, int ID_UM, int VARIABLE, int ID_MAG_PESO, int ID_UM_PESO)
        {
            decimal val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                string cadena = " select dbo.fx_getMagnitudProducto(" + Convert.ToString(ID_PRODUCTO) + "," + Convert.ToString(ID_UM) + "," + Convert.ToString(VARIABLE) + "," + Convert.ToString(ID_MAG_PESO) + "," + Convert.ToString(ID_UM_PESO) + " ) ";
                SqlCommand cmd = new SqlCommand(cadena, con);

                val = (decimal)cmd.ExecuteScalar();
                cmd.Connection.Close();
                return val;
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public decimal OBTENER_EQUIVALENCIA1(int ID_PRODUCTO, int ID_UM, int ID_UM_ST, int VARIABLE)
        {
            decimal val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                string cadena = " select dbo.fx_getValorEquivalenteProducto(" + Convert.ToString(ID_PRODUCTO) + "," + Convert.ToString(ID_UM) + "," + Convert.ToString(ID_UM_ST) + "," + Convert.ToString(VARIABLE) + " ) ";
                SqlCommand cmd = new SqlCommand(cadena, con);

                val = (decimal)cmd.ExecuteScalar();
                cmd.Connection.Close();
                return val;
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public decimal OBTENER_STOCK_REAL(int ID_EMPRESA, int ID_ALMACEN, int ID_PRODUCTO)
        {
            decimal val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                string cadena = "select dbo.fx_StockReal(" + Convert.ToString(ID_EMPRESA) + "," + Convert.ToString(ID_ALMACEN) + "," + Convert.ToString(ID_PRODUCTO) + ")";
                SqlCommand cmd = new SqlCommand(cadena, con);

                val = (decimal)cmd.ExecuteScalar();
                cmd.Connection.Close();
                return val;
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public decimal OBTENER_STOCK_COMPROMETIDO(int ID_EMPRESA, int ID_ALMACEN, int ID_PRODUCTO)
        {
            decimal val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                string cadena = "select dbo.fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(" + Convert.ToString(ID_EMPRESA) + "," + Convert.ToString(ID_ALMACEN) + "," + Convert.ToString(ID_PRODUCTO) + ")";
                SqlCommand cmd = new SqlCommand(cadena, con);

                val = (decimal)cmd.ExecuteScalar();
                cmd.Connection.Close();
                return val;
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }

        public List<BE_Despacho> OBTENER_PRODUCTOS_KIT(int ID_PRODUCTO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_Kit_SelectComponentexIdKit", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdKit", ID_PRODUCTO);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idkit = lector[0].ToString();
                        obj_BE_Documento.idcomponente = lector[1].ToString();
                        obj_BE_Documento.cantidad_componente = lector[2].ToString();
                        obj_BE_Documento.idumcomponente = lector[3].ToString();
                        obj_BE_Documento.componente = lector[4].ToString();
                        obj_BE_Documento.codprodcomponente = lector[5].ToString();
                        obj_BE_Documento.umcomponente = lector[6].ToString();
                        obj_BE_Documento.idmonedacomponente = lector[8].ToString();
                        obj_BE_Documento.preciocomponente = lector[7].ToString();
                        obj_BE_Documento.monedacomponente = lector[9].ToString();
                        obj_BE_Documento.porcentajekit = lector[10].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Despacho> OBTENER_DATOS_ALMACEN_SELECCIONADO(int ID_ALMACEN)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_AlmacenSelectxId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdAlmacen", ID_ALMACEN);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.ubigeo = lector[2].ToString();
                        obj_BE_Documento.direccion = lector[3].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Despacho> OBTENER_COMBO_ALMACEN_DESTINATARIO(int TIPO_ALMACEN, int ID_DESTINATARIO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_AlmacenSelectAllActivoxIdTipoAlmacenxIdEmpresa", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdEmpresa", ID_DESTINATARIO);
                    cmd.Parameters.AddWithValue("@IdTipoAlmacen", TIPO_ALMACEN);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.idalmacen = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomalmacen = lector[1].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Despacho> LISTAR_GUIA_REMISION(string FEC_INICIO, string FEC_FIN, string IDTIENDA, string SERIE, string CODIGO)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("USP_LISTAR_GUIA_REMISION", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@fechaIni", FEC_INICIO);
                    cmd.Parameters.AddWithValue("@fechafin", FEC_FIN);
                    if (IDTIENDA == "0") { cmd.Parameters.AddWithValue("@idtienda", ""); }
                    else { cmd.Parameters.AddWithValue("@idtienda", IDTIENDA); }

                    if (SERIE == null | SERIE=="0") { cmd.Parameters.AddWithValue("@serie", ""); }
                    else { cmd.Parameters.AddWithValue("@serie", SERIE); }

                    if (CODIGO == null) { cmd.Parameters.AddWithValue("@codigo", ""); }
                    else { cmd.Parameters.AddWithValue("@codigo", CODIGO); }
                    //cmd.Parameters.AddWithValue("@idtienda",IDTIENDA);
                    //cmd.Parameters.AddWithValue("@serie", SERIE);
                    //cmd.Parameters.AddWithValue("@codigo", CODIGO);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.iddocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.nomtipodocumento = lector[1].ToString();
                        obj_BE_Documento.numdoc = lector[2].ToString();
                        obj_BE_Documento.fechaemision = lector[3].ToString();
                        obj_BE_Documento.almacen = lector[4].ToString();
                        obj_BE_Documento.tipo_operacion = lector[5].ToString();
                        obj_BE_Documento.estado = lector[6].ToString();
                        obj_BE_Documento.idserie = Convert.ToInt32(lector[7].ToString());
                        obj_BE_Documento.Codigo = lector[8].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Documento_Referencia> LISTAR_DOCUMENTO_REFERENCIA(int idDocumento)
        {
            List<BE_Documento_Referencia> lista = new List<BE_Documento_Referencia>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_BuscarDatosDocReferencia", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdDocumento", idDocumento);




                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento_Referencia obj_BE_Documento_Referencia = new BE_Documento_Referencia();
                        obj_BE_Documento_Referencia.FechaEmision = lector[0].ToString();
                        obj_BE_Documento_Referencia.FechaInit = lector[1].ToString();
                        obj_BE_Documento_Referencia.documento = lector[2].ToString();
                        obj_BE_Documento_Referencia.IdDocumento = Convert.ToInt32(lector[3].ToString());
                        obj_BE_Documento_Referencia.doc_Serie = lector[4].ToString();
                        obj_BE_Documento_Referencia.doc_Codigo = lector[5].ToString();
                        obj_BE_Documento_Referencia.empresa = Convert.ToInt32(lector[6].ToString());
                        obj_BE_Documento_Referencia.tipoOperacion = Convert.ToInt32(lector[7].ToString());
                        obj_BE_Documento_Referencia.tipoop = lector[8].ToString();
                        obj_BE_Documento_Referencia.motivoTrans = Convert.ToInt32(lector[9].ToString());
                        obj_BE_Documento_Referencia.motivo = lector[10].ToString();
                        obj_BE_Documento_Referencia.idpersona = Convert.ToInt32(lector[11].ToString());
                        obj_BE_Documento_Referencia.idTienda = Convert.ToInt32(lector[12].ToString());
                        obj_BE_Documento_Referencia.tienda = lector[13].ToString();
                        obj_BE_Documento_Referencia.Idalmacen = Convert.ToInt32(lector[14].ToString());
                        obj_BE_Documento_Referencia.almacen = lector[15].ToString();
                        obj_BE_Documento_Referencia.IdEstado = Convert.ToInt32(lector[16].ToString());
                        obj_BE_Documento_Referencia.estado = lector[17].ToString();
                        obj_BE_Documento_Referencia.rsocial = lector[18].ToString();
                        obj_BE_Documento_Referencia.IdCliente = Convert.ToInt32(lector[19].ToString());
                        obj_BE_Documento_Referencia.dni = lector[20].ToString();
                        obj_BE_Documento_Referencia.ruc = lector[21].ToString();
                        obj_BE_Documento_Referencia.pdireccion = lector[22].ToString();
                        obj_BE_Documento_Referencia.idDepartamentoP = Convert.ToInt32(lector[23].ToString());
                        obj_BE_Documento_Referencia.pdepartamento = lector[24].ToString();
                        obj_BE_Documento_Referencia.idProvinciaP = Convert.ToInt32(lector[25].ToString());
                        obj_BE_Documento_Referencia.pprovincia = lector[26].ToString();
                        obj_BE_Documento_Referencia.idDistritoP = Convert.ToInt32(lector[27].ToString());
                        obj_BE_Documento_Referencia.pdistrito = lector[28].ToString();
                        obj_BE_Documento_Referencia.dnombres = lector[29].ToString();
                        obj_BE_Documento_Referencia.dcodigo = Convert.ToInt32(lector[30].ToString());
                        obj_BE_Documento_Referencia.ddni = lector[31].ToString();
                        obj_BE_Documento_Referencia.druc = lector[32].ToString();
                        obj_BE_Documento_Referencia.lldireccion = lector[33].ToString();
                        obj_BE_Documento_Referencia.idDepartamentoL = lector[34].ToString();
                        obj_BE_Documento_Referencia.idProvinciaL = lector[36].ToString();
                        obj_BE_Documento_Referencia.idDistritoL = lector[38].ToString();
                        obj_BE_Documento_Referencia.idAlm = Convert.ToInt32(lector[40].ToString());
                        obj_BE_Documento_Referencia.idTipoAlmacen = Convert.ToInt32(lector[41].ToString());


                        lista.Add(obj_BE_Documento_Referencia);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Producto_Final> ListarProductoFinal(int idDocumento, int idUnidadPeso)
        {
            List<BE_Producto_Final> lista = new List<BE_Producto_Final>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("USP_LISTAR_PRODUCTOS_DOCREFERENCIA", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@documento", idDocumento);
                    cmd.Parameters.AddWithValue("@idUnidadPeso", idUnidadPeso);




                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Producto_Final obj_Producto_Final = new BE_Producto_Final();
                        obj_Producto_Final.codigo = lector[0].ToString();
                        obj_Producto_Final.nroDocumento = lector[1].ToString();
                        obj_Producto_Final.descripcion = lector[2].ToString();
                        obj_Producto_Final.UM = lector[3].ToString();
                        obj_Producto_Final.cantidad = Convert.ToDecimal(lector[4].ToString());
                        obj_Producto_Final.pendiente = Convert.ToDecimal(lector[5].ToString());
                        obj_Producto_Final.peso = Convert.ToDecimal(lector[6].ToString());
                        obj_Producto_Final.stockReal = Convert.ToDecimal(lector[7].ToString());
                        obj_Producto_Final.stockComprometido = Convert.ToDecimal(lector[8].ToString());
                        obj_Producto_Final.stockdisponible = Convert.ToDecimal(lector[9].ToString());
                        obj_Producto_Final.equivalencia = Convert.ToDecimal(lector[10].ToString());
                        obj_Producto_Final.equivalencia1 = Convert.ToDecimal(lector[11].ToString());
                        obj_Producto_Final.idProducto = Convert.ToInt32(lector[12].ToString());
                        obj_Producto_Final.idDocumento = Convert.ToInt32(lector[13].ToString());
                        obj_Producto_Final.idDetalleDoc = Convert.ToInt32(lector[14].ToString());
                        obj_Producto_Final.idAlmacen = Convert.ToInt32(lector[15].ToString());


                        lista.Add(obj_Producto_Final);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public string INSERTAR_DOCUMENTO(string codigo, string serie, string FechaEmision, string FechaIniTraslado, string FechaAentregar,
        string FechaEntrega, string FechaVenc, decimal ImporteTotal, decimal Descuento, decimal SubTotal, decimal Igv, decimal Total, string TotalLetras,
        decimal TotalAPagar, decimal ValorReferencial, decimal Utilidad, int IdPersona, int IdUsuario, int IdTransportista, int IdRemitente, int IdDestinatario,
        int IdEstadoDoc, int IdCondicionPago, int IdMoneda, int Lugarentrega, int IdTipoOperacion, int IdTienda, int IdSerie, string ExportadoConta, string NroVoucherConta,
        int IdEmpresa, int IdChofer, int IdMotivoT, int IdTipoDoc, int IdVehiculo, int IdEstadoCan, int IdEstadoEnt, int IdAlmacen, int IdTipoPv, int IdCaja, int IdMedioCredito,
        int doc_CompPercepcion, string doc_FechaCancelacion, int IdUsuarioComision, string[][] VALORES, Boolean moverStock, Boolean ValidarDes, string ubigeoPP, string DireccionPP,
        string ubigepPL, string DireccionPL, string observaciones, string[][] DocRef, int almacenDest)
        {
            int val = 0; int IdDetalleDocumento = 0;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            using (SqlTransaction transaccion = con.BeginTransaction())
            {
                try
                {
                    #region CABECERA
                    using (SqlCommand cmd = transaccion.Connection.CreateCommand())
                    {
                        val = 0;
                        cmd.CommandText = "_DocumentoInsert";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@doc_Codigo", codigo);
                        cmd.Parameters.AddWithValue("@doc_Serie", serie);
                        cmd.Parameters.AddWithValue("@doc_FechaEmision", Convert.ToDateTime(FechaEmision));

                        if (FechaIniTraslado == "") { cmd.Parameters.AddWithValue("@doc_FechaIniTraslado", Convert.ToDateTime(FechaIniTraslado)); }
                        else { cmd.Parameters.AddWithValue("@doc_FechaIniTraslado", Convert.ToDateTime(FechaIniTraslado)); }

                        if (FechaAentregar == "") { cmd.Parameters.AddWithValue("@doc_FechaAentregar", null); }
                        else { cmd.Parameters.AddWithValue("@doc_FechaAentregar", FechaAentregar); }

                        if (FechaEntrega == "") { cmd.Parameters.AddWithValue("@doc_FechaEntrega", null); }
                        else { cmd.Parameters.AddWithValue("@doc_FechaEntrega", FechaEntrega); }

                        if (FechaVenc == "") { cmd.Parameters.AddWithValue("@doc_FechaVenc", null); }
                        else { cmd.Parameters.AddWithValue("@doc_FechaVenc", FechaVenc); }

                        if (ImporteTotal == 0) { cmd.Parameters.AddWithValue("@doc_ImporteTotal", null); }
                        else { cmd.Parameters.AddWithValue("@doc_ImporteTotal", ImporteTotal); }

                        if (Descuento == 0) { cmd.Parameters.AddWithValue("@doc_Descuento", null); }
                        else { cmd.Parameters.AddWithValue("@doc_Descuento", Descuento); }

                        if (SubTotal == 0) { cmd.Parameters.AddWithValue("@doc_SubTotal", null); }
                        else { cmd.Parameters.AddWithValue("@doc_SubTotal", SubTotal); }

                        if (Igv == 0) { cmd.Parameters.AddWithValue("@doc_Igv", null); }
                        else { cmd.Parameters.AddWithValue("@doc_Igv", Igv); }

                        if (Total == 0) { cmd.Parameters.AddWithValue("@doc_Total", null); }
                        else { cmd.Parameters.AddWithValue("@doc_Total", Total); }

                        if (TotalLetras == "") { cmd.Parameters.AddWithValue("@doc_TotalLetras", null); }
                        else { cmd.Parameters.AddWithValue("@doc_TotalLetras", TotalLetras); }

                        if (TotalAPagar == 0) { cmd.Parameters.AddWithValue("@doc_TotalAPagar", null); }
                        else { cmd.Parameters.AddWithValue("@doc_TotalAPagar", TotalAPagar); }

                        if (ValorReferencial == 0) { cmd.Parameters.AddWithValue("@doc_ValorReferencial", null); }
                        else { cmd.Parameters.AddWithValue("@doc_ValorReferencial", ValorReferencial); }

                        if (Utilidad == 0) { cmd.Parameters.AddWithValue("@doc_Utilidad", null); }
                        else { cmd.Parameters.AddWithValue("@doc_Utilidad", Utilidad); }

                        cmd.Parameters.AddWithValue("@IdPersona", IdPersona);
                        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);

                        if (IdTransportista == 0) { cmd.Parameters.AddWithValue("@IdTransportista", null); }
                        else { cmd.Parameters.AddWithValue("@IdTransportista", IdTransportista); }

                        cmd.Parameters.AddWithValue("@IdRemitente", IdRemitente);
                        cmd.Parameters.AddWithValue("@IdDestinatario", IdDestinatario);
                        cmd.Parameters.AddWithValue("@IdEstadoDoc", IdEstadoDoc);

                        if (IdCondicionPago == 0) { cmd.Parameters.AddWithValue("@IdCondicionPago", null); }
                        else { cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago); }

                        if (IdMoneda == 0) { cmd.Parameters.AddWithValue("@IdMoneda", null); }
                        else { cmd.Parameters.AddWithValue("@IdMoneda", IdTransportista); }

                        if (Lugarentrega == 0) { cmd.Parameters.AddWithValue("@LugarEntrega", null); }
                        else { cmd.Parameters.AddWithValue("@LugarEntrega", null); }

                        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion);
                        cmd.Parameters.AddWithValue("@IdTienda", IdTienda);
                        cmd.Parameters.AddWithValue("@IdSerie", IdSerie);

                        if (ExportadoConta == "") { cmd.Parameters.AddWithValue("@doc_ExportadoConta", null); }
                        else { cmd.Parameters.AddWithValue("@doc_ExportadoConta", ExportadoConta); }

                        if (NroVoucherConta == "") { cmd.Parameters.AddWithValue("@doc_NroVoucherConta", null); }
                        else { cmd.Parameters.AddWithValue("@doc_NroVoucherConta", NroVoucherConta); }

                        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa);

                        if (IdChofer == 0) { cmd.Parameters.AddWithValue("@IdChofer", null); }
                        else { cmd.Parameters.AddWithValue("@IdChofer", IdChofer); }

                        cmd.Parameters.AddWithValue("@IdMotivoT", IdMotivoT);
                        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDoc);

                        if (IdVehiculo == 0) { cmd.Parameters.AddWithValue("@IdVehiculo", null); }
                        else { cmd.Parameters.AddWithValue("@IdVehiculo", IdVehiculo); }

                        if (IdEstadoCan == 0) { cmd.Parameters.AddWithValue("@IdEstadoCan", null); }
                        else { cmd.Parameters.AddWithValue("@IdEstadoCan", IdEstadoCan); }

                        cmd.Parameters.AddWithValue("@IdEstadoEnt", IdEstadoEnt);
                        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen);

                        if (IdTipoPv == 0) { cmd.Parameters.AddWithValue("@IdTipoPV", null); }
                        else { cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPv); }

                        if (IdCaja == 0) { cmd.Parameters.AddWithValue("@IdCaja", null); }
                        else { cmd.Parameters.AddWithValue("@IdCaja", IdCaja); }

                        if (IdMedioCredito == 0) { cmd.Parameters.AddWithValue("@IdMedioPagoCredito", null); }
                        else { cmd.Parameters.AddWithValue("@IdMedioPagoCredito", IdMedioCredito); }

                        if (doc_CompPercepcion == 0) { cmd.Parameters.AddWithValue("@doc_CompPercepcion", null); }
                        else { cmd.Parameters.AddWithValue("@doc_CompPercepcion", doc_CompPercepcion); }

                        if (doc_FechaCancelacion == "") { cmd.Parameters.AddWithValue("@doc_FechaCancelacion", null); }
                        else { cmd.Parameters.AddWithValue("@doc_FechaCancelacion", doc_FechaCancelacion); }

                        if (IdUsuarioComision == 0) { cmd.Parameters.AddWithValue("@IdUsuarioComision", null); }
                        else { cmd.Parameters.AddWithValue("@IdUsuarioComision", IdUsuarioComision); }


                        SqlParameter ValorRetorno = new SqlParameter("@IdDocumento", SqlDbType.Int);
                        ValorRetorno.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(ValorRetorno);
                        val = cmd.ExecuteNonQuery();
                        int IdDocumento = Convert.ToInt32(ValorRetorno.Value);
                        cmd.Parameters.Clear();
                        #endregion

                        #region DETALLE
                        for (int i = 0; i < VALORES.Length; i++)
                        {
                            if (VALORES[i][0].Trim() == null)
                            {
                            }
                            else
                            {

                                val = 0;
                                int IDDOCUMENTO = IdDocumento;
                                cmd.CommandText = "_DocumentoGuiaRemision_DetalleInsert_Nuevo";
                                cmd.Transaction = transaccion;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@IdDocumento", IDDOCUMENTO);
                                if (VALORES[i][0].ToString().Trim() == "") { cmd.Parameters.AddWithValue("@IdDocumentoref", null); }
                                else { cmd.Parameters.AddWithValue("@IdDocumentoref", VALORES[i][0].ToString().Trim()); }

                                cmd.Parameters.AddWithValue("@MoverStockFisico", moverStock);
                                cmd.Parameters.AddWithValue("@ValidarDespacho", ValidarDes);
                                cmd.Parameters.AddWithValue("@IdProducto", VALORES[i][1].ToString().Trim());
                                cmd.Parameters.AddWithValue("@UM", VALORES[i][2].ToString().Trim());

                                if (VALORES[i][3].ToString().Trim() == "") { cmd.Parameters.AddWithValue("@Tono", null); }
                                else { cmd.Parameters.AddWithValue("@Tono", VALORES[i][3].ToString().Trim()); }

                                if (VALORES[i][4].ToString().Trim() == "") { cmd.Parameters.AddWithValue("@Id_detalle_documento_ref", null); }
                                else { cmd.Parameters.AddWithValue("@Id_detalle_documento_ref", VALORES[i][4].ToString().Trim()); }

                                cmd.Parameters.AddWithValue("@cantidad", VALORES[i][5].ToString().Trim());

                                SqlParameter ValorRetorno2 = new SqlParameter("@IdDetalleDocumento", SqlDbType.Int);
                                ValorRetorno2.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(ValorRetorno2);
                                val = cmd.ExecuteNonQuery();
                                IdDetalleDocumento = Convert.ToInt32(ValorRetorno2.Value);
                                cmd.Parameters.Clear();
                            }
                        }
                        #endregion

                        #region PUNTO PARTIDA

                        val = 0;
                        cmd.CommandText = "_PuntoPartidaInsert";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen);
                        cmd.Parameters.AddWithValue("@IdTienda", null);
                        cmd.Parameters.AddWithValue("@pp_Ubigeo", ubigeoPP);
                        cmd.Parameters.AddWithValue("@pp_Direccion", DireccionPP);
                        val = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        #endregion


                        #region PUNTO LLEGADA

                        val = 0;
                        cmd.CommandText = "_PuntoLlegadaInsert";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (almacenDest == 0) { cmd.Parameters.AddWithValue("@IdAlmacen", null); }
                        else { cmd.Parameters.AddWithValue("@IdAlmacen", almacenDest); }
                        cmd.Parameters.AddWithValue("@pll_Ubigeo", ubigepPL);
                        cmd.Parameters.AddWithValue("@pll_Direccion", DireccionPL);
                        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                        cmd.Parameters.AddWithValue("@IdTienda", null);
                        val = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        #endregion

                        #region Observaciones
                        if (observaciones != "")
                        {
                            val = 0;
                            cmd.CommandText = "_ObservacionInsert";
                            cmd.Transaction = transaccion;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                            cmd.Parameters.AddWithValue("@ob_Observacion", observaciones);
                            val = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                        #endregion

                        #region RELACION DOCUMENTO
                        for (int i = 0; i < DocRef.Length; i++)
                        {
                            if (DocRef[i][0].Trim() == null)
                            {
                            }
                            else
                            {



                                val = 0;
                                cmd.CommandText = "_RelacionDocumentoInsert";
                                cmd.Transaction = transaccion;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@IdDocumento1", VALORES[i][0].ToString().Trim());
                                cmd.Parameters.AddWithValue("@IdDocumento2", IdDocumento);
                                val = cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                #endregion
                            }
                        }
                    }
                    transaccion.Commit();

                }
                catch (Exception ex)
                {

                    StackTrace st = new StackTrace(ex, true);
                    StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                         && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                         && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                         && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                    string MachineName = System.Environment.MachineName;
                    string UserName = System.Environment.UserName.ToUpper();
                    string Mensaje = ex.Message;
                    int LineaError = frame.GetFileLineNumber();
                    string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                    string Clase = frame.GetMethod().DeclaringType.Name;
                    string metodo = frame.GetMethod().Name;
                    string codigoError = Convert.ToString(frame.GetHashCode());
                    transaccion.Rollback();

                    return Mensaje;
                }



            }

            return Convert.ToString(IdDetalleDocumento);
        }


        public List<BE_Documento> SelectguiaXSerie(int idSerie, string Codigo)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            using (SqlCommand cmd = new SqlCommand("_DocumentoSelectxIdSeriexCodigo", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdSerie", idSerie);
                cmd.Parameters.AddWithValue("@doc_Codigo", Codigo);

                SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                BE_Documento obj_Documento = new BE_Documento();
                while (lector.Read())
                {

                    obj_Documento.IdDocumento = Convert.ToInt32(lector[0].ToString());
                    obj_Documento.doc_Codigo = lector[1].ToString();
                    obj_Documento.doc_Serie = lector[2].ToString();
                    obj_Documento.doc_FechaEmision = lector[3].ToString();
                    obj_Documento.doc_FechaIniTraslado = lector[4].ToString();
                    obj_Documento.doc_FechaRegistro = lector[5].ToString();
                    obj_Documento.doc_FechaAentregar = !lector.IsDBNull(6) ? lector[6].ToString() : "";
                    obj_Documento.doc_FechaVenc = !lector.IsDBNull(7) ? lector[7].ToString() : "";
                    obj_Documento.doc_FechaCancelacion = !lector.IsDBNull(8) ? lector[8].ToString() : "";
                    obj_Documento.doc_FechaEntrega = !lector.IsDBNull(9) ? lector[9].ToString() : "";
                    obj_Documento.doc_ImporteTotal = !lector.IsDBNull(10) ? Convert.ToDecimal(lector[10].ToString()) : 0;
                    obj_Documento.doc_Descuento = !lector.IsDBNull(11) ? Convert.ToDecimal(lector[11].ToString()) : 0;
                    obj_Documento.doc_SubTotal = !lector.IsDBNull(12) ? Convert.ToDecimal(lector[12].ToString()) : 0;
                    obj_Documento.doc_Igv = !lector.IsDBNull(13) ? Convert.ToDecimal(lector[13].ToString()) : 0;
                    obj_Documento.doc_Total = !lector.IsDBNull(14) ? Convert.ToDecimal(lector[14].ToString()) : 0;
                    obj_Documento.doc_ValorReferencial = !lector.IsDBNull(17) ? Convert.ToDecimal(lector[17].ToString()) : 0;
                    obj_Documento.doc_Utilidad = !lector.IsDBNull(18) ? Convert.ToDecimal(lector[18].ToString()) : 0;
                    obj_Documento.IdPersona = !lector.IsDBNull(20) ? Convert.ToInt32(lector[20].ToString()) : 0;
                    obj_Documento.IdTransportista = !lector.IsDBNull(22) ? Convert.ToInt32(lector[22].ToString()) : 0;
                    obj_Documento.IdRemitente = !lector.IsDBNull(23) ? Convert.ToInt32(lector[23].ToString()) : 0;
                    obj_Documento.IdDestinatario = !lector.IsDBNull(24) ? Convert.ToInt32(lector[24].ToString()) : 0;
                    obj_Documento.IdEstadoDoc = !lector.IsDBNull(25) ? Convert.ToInt32(lector[25].ToString()) : 0;
                    obj_Documento.IdCondicionPago = !lector.IsDBNull(26) ? Convert.ToInt32(lector[26].ToString()) : 0;
                    obj_Documento.IdMoneda = !lector.IsDBNull(27) ? Convert.ToInt32(lector[27].ToString()) : 0;
                    obj_Documento.LugarEntrega = !lector.IsDBNull(28) ? Convert.ToInt32(lector[28].ToString()) : 0;
                    obj_Documento.IdTipoOperacion = !lector.IsDBNull(29) ? Convert.ToInt32(lector[29].ToString()) : 0;
                    obj_Documento.IdTienda = !lector.IsDBNull(30) ? Convert.ToInt32(lector[30].ToString()) : 0;
                    obj_Documento.IdSerie = !lector.IsDBNull(31) ? Convert.ToInt32(lector[31].ToString()) : 0;
                    obj_Documento.IdEmpresa = !lector.IsDBNull(33) ? Convert.ToInt32(lector[33].ToString()) : 0;
                    obj_Documento.IdChofer = !lector.IsDBNull(34) ? Convert.ToInt32(lector[34].ToString()) : 0;
                    obj_Documento.IdMotivoT = !lector.IsDBNull(36) ? Convert.ToInt32(lector[36].ToString()) : 0;
                    obj_Documento.IdTipoDocumento = !lector.IsDBNull(37) ? Convert.ToInt32(lector[37].ToString()) : 0;
                    obj_Documento.IdVehiculo = !lector.IsDBNull(38) ? Convert.ToInt32(lector[38].ToString()) : 0;
                    obj_Documento.IdEstadoCan = !lector.IsDBNull(39) ? Convert.ToInt32(lector[39].ToString()) : 0;
                    obj_Documento.IdEstadoEnt = !lector.IsDBNull(40) ? Convert.ToInt32(lector[40].ToString()) : 0;
                    obj_Documento.IdTipoPV = !lector.IsDBNull(41) ? Convert.ToInt32(lector[41].ToString()) : 0;
                    obj_Documento.IdCaja = !lector.IsDBNull(42) ? Convert.ToInt32(lector[42].ToString()) : 0;
                    obj_Documento.IdAlmacen = !lector.IsDBNull(43) ? Convert.ToInt32(lector[43].ToString()) : 0;
                    obj_Documento.PoseeOrdenDespacho = !lector.IsDBNull(49) ? Convert.ToInt32(lector[49].ToString()) : 0;
                    obj_Documento.PoseeAmortizaciones = !lector.IsDBNull(50) ? Convert.ToInt32(lector[50].ToString()) : 0;
                    obj_Documento.PoseecomPersepcion = !lector.IsDBNull(51) ? Convert.ToInt32(lector[51].ToString()) : 0;
                    obj_Documento.PoseeGRecepcion = !lector.IsDBNull(52) ? Convert.ToInt32(lector[52].ToString()) : 0;
                    obj_Documento.IdUsuarioComision = !lector.IsDBNull(53) ? Convert.ToInt32(lector[53].ToString()) : 0;

                    lista.Add(obj_Documento);
                }

                return lista;
            }


        }

        public List<BE_Documento_Referencia> GuiaDocRef(int IdDocumento)
        {
            List<BE_Documento_Referencia> lista = new List<BE_Documento_Referencia>();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            using (SqlCommand cmd = new SqlCommand("USP_GUIA_REMISION_REF", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);


                SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while (lector.Read())
                {
                    BE_Documento_Referencia obj_Documento = new BE_Documento_Referencia();
                    obj_Documento.documento = lector[0].ToString();
                    obj_Documento.doc_Serie = lector[1].ToString();
                    obj_Documento.doc_Codigo = lector[2].ToString();
                    obj_Documento.empresa = Convert.ToInt16(lector[3].ToString());
                    obj_Documento.tienda = lector[4].ToString();
                    obj_Documento.almacen = lector[5].ToString();
                    obj_Documento.estado = !lector.IsDBNull(6) ? lector[6].ToString() : "";

                    lista.Add(obj_Documento);
                }

                return lista;

            }


        }

        public List<BE_Persona> SelectxIdPersona(int IdDocumento)
        {
            List<BE_Persona> lista = new List<BE_Persona>();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            using (SqlCommand cmd = new SqlCommand("USP_GUIAREMISION_DESTINATARIO", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);

                SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                BE_Persona obj_Persona = new BE_Persona();
                while (lector.Read())
                {
                    obj_Persona.IdPersona = Convert.ToInt32(lector[0].ToString());
                    obj_Persona.per_NComercial = lector[1].ToString();
                    obj_Persona.Nombre = lector[2].ToString();
                    obj_Persona.jur_Rsocial = lector[3].ToString();
                    obj_Persona.Dni = lector[4].ToString();
                    obj_Persona.Ruc = lector[5].ToString();
                    obj_Persona.dir_Direccion = lector[6].ToString();
                    obj_Persona.IdAlmacen = Convert.ToInt32(lector[7].ToString());
                    obj_Persona.IdTipoAlmacen = Convert.ToInt16(lector[8].ToString());

                    lista.Add(obj_Persona);
                }

                return lista;
            }
        }


        public List<BE_Despacho> SelectPuntoLlegada(int IdDocumento)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            using (SqlCommand cmd = new SqlCommand("USP_GREMISION_PLLEGADA", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);

                SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                BE_Despacho obj_Punto = new BE_Despacho();
                while (lector.Read())
                {
                    obj_Punto.direccion = lector[0].ToString();
                    obj_Punto.iddepartamento = lector[1].ToString();
                    obj_Punto.idprovincia = lector[2].ToString();
                    obj_Punto.iddistrito = lector[3].ToString();

                    lista.Add(obj_Punto);
                }

                return lista;
            }
        }


        public List<BE_Documento> Detalle_Guia_Remision(int IdDocumento)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            using (SqlCommand cmd = new SqlCommand("_DocumentoGuiaRemisionSelectDetalle", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);

                SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                BE_Documento obj_Detalle = new BE_Documento();
                while (lector.Read())
                {
                    obj_Detalle.Cantidad = lector[0].ToString();
                    obj_Detalle.IdDocumento = Convert.ToInt16(lector[1].ToString());
                    obj_Detalle.IdProducto = Convert.ToInt16(lector[2].ToString());
                    obj_Detalle.UM = lector[3].ToString();
                    obj_Detalle.NombreProducto = lector[3].ToString();
                    obj_Detalle.IdUnidadMedida = Convert.ToInt16(lector[3].ToString());
                    obj_Detalle.IdDetalleDocumento = Convert.ToInt16(lector[3].ToString());
                    obj_Detalle.IdDetalleAfecto = Convert.ToInt16(lector[3].ToString());
                    obj_Detalle.IdDocumentoRef = Convert.ToInt16(lector[3].ToString());
                    obj_Detalle.CantidadxAtenderText = lector[3].ToString();
                    obj_Detalle.CodigoProducto = lector[3].ToString();
                    obj_Detalle.kit = Convert.ToBoolean(lector[3].ToString());
                    obj_Detalle.nroDocumento = lector[3].ToString();

                    lista.Add(obj_Detalle);
                }
                return lista;
            }
        }


        public List<BE_Producto_Final> DETALLE_PRODUCTO_GUIA(int IdDocumento)
        {
            List<BE_Producto_Final> lista = new List<BE_Producto_Final>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("USP_DETALLE_PRODUCTO_GREMISION", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Producto_Final obj_Producto = new BE_Producto_Final();

                        obj_Producto.codigo = lector[0].ToString();
                        obj_Producto.nroDocumento = lector[1].ToString();
                        obj_Producto.descripcion = lector[2].ToString();
                        obj_Producto.UM = lector[3].ToString();
                        obj_Producto.cantidad = Convert.ToDecimal(lector[4].ToString());
                        obj_Producto.Pend = lector[5].ToString();
                        obj_Producto.peso = Convert.ToDecimal(lector[6].ToString());
                        obj_Producto.stockReal = Convert.ToDecimal(lector[7].ToString());
                        obj_Producto.stockComprometido = Convert.ToDecimal(lector[8].ToString());
                        obj_Producto.stockdisponible = Convert.ToDecimal(lector[9].ToString());
                        obj_Producto.Tonos = lector[10].ToString();
                        obj_Producto.idDetalleDoc = Convert.ToInt32(lector[11].ToString());
                        lista.Add(obj_Producto);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Despacho> OBTENER_VEHICULOSXIDVEHICULO(int IdVehiculo)
        {
            List<BE_Despacho> lista = new List<BE_Despacho>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_VehiculoSelectxIdVehiculo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdVehiculo", IdVehiculo);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Despacho obj_BE_Documento = new BE_Despacho();
                        obj_BE_Documento.veh_Placa = lector[5].ToString();
                        obj_BE_Documento.veh_Modelo = lector[4].ToString();
                        obj_BE_Documento.veh_NConstanciaIns = lector[3].ToString();


                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Documento> OBTENER_OBSERVACIONES(int IdDocumento)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                using (SqlCommand cmd = new SqlCommand("_ObservacionesSelectxIdDocumento", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.ID =Convert.ToInt32( lector[0].ToString());
                        obj_BE_Documento.Observacion = lector[2].ToString();
                        


                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


    }


          
}

  