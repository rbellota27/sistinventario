﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using System.Data;
using System.Configuration;
using System.Management;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data.Common;

namespace DA
{
    public class DA_Reporteria
    {
        string conexion = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;


        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS(string Tipo, string Estado)
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_Documento_Solicitud_Extornos_Banco", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@Estado", Estado);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {

                        BE_Reporte obj_BE_Documento = new BE_Reporte();

                        obj_BE_Documento.Campo1 = lector[0].ToString();
                        obj_BE_Documento.Campo2 = lector[1].ToString();
                        obj_BE_Documento.Campo3 = lector[2].ToString();
                        obj_BE_Documento.Campo4 = lector[3].ToString();
                        obj_BE_Documento.Campo5 = lector[4].ToString();
                        obj_BE_Documento.Campo6 = lector[5].ToString();
                        obj_BE_Documento.Campo7 = lector[6].ToString();
                        obj_BE_Documento.Campo8 = lector[7].ToString();
                        obj_BE_Documento.Campo9 = lector[8].ToString();
                        obj_BE_Documento.Campo10 = lector[9].ToString();
                        obj_BE_Documento.Campo11 = lector[10].ToString();
                        obj_BE_Documento.Campo12 = lector[11].ToString();
                        obj_BE_Documento.Campo13 = lector[12].ToString();
                        obj_BE_Documento.Campo14 = lector[13].ToString();
                        obj_BE_Documento.Campo15 = lector[14].ToString();
                        obj_BE_Documento.Campo16 = lector[15].ToString();
                        obj_BE_Documento.Campo17 = lector[16].ToString();
                        obj_BE_Documento.Campo18 = lector[17].ToString();
                        obj_BE_Documento.Campo19 = lector[18].ToString();
                        obj_BE_Documento.Campo20 = lector[19].ToString();
                        obj_BE_Documento.Campo21 = lector[20].ToString();
                        obj_BE_Documento.Campo22 = lector[21].ToString();

                        lista.Add(obj_BE_Documento);

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Reporte> OBTENER_ESTADO()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_ESTADO_SOLICITUD", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.Campo1 = lector[0].ToString();
                        obj_BE_Documento.Campo2 = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }




        public List<BE_Reporte> LISTAR_DOCUMENTOS_EXTORNOS_DETALLE(string Tipo, int IdDocumento)
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_DETALLE_DOCUMENTOS_EXTORNOS", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {

                        BE_Reporte obj_BE_Documento = new BE_Reporte();

                        obj_BE_Documento.Campo1 = lector[0].ToString();
                        obj_BE_Documento.Campo2 = lector[1].ToString();
                        obj_BE_Documento.Campo3 = lector[2].ToString();
                        obj_BE_Documento.Campo4 = lector[3].ToString();
                        obj_BE_Documento.Campo5 = lector[4].ToString();
                        obj_BE_Documento.Campo6 = lector[5].ToString();
                        obj_BE_Documento.Campo7 = lector[6].ToString();
                        obj_BE_Documento.Campo8 = lector[7].ToString();
                        obj_BE_Documento.Campo9 = lector[8].ToString();
                        obj_BE_Documento.Campo10 = lector[9].ToString();
                        obj_BE_Documento.Campo11 = lector[10].ToString();
                        obj_BE_Documento.Campo12 = lector[11].ToString();
                        obj_BE_Documento.Campo13 = lector[12].ToString();
                        obj_BE_Documento.Campo14 = lector[13].ToString();
                        obj_BE_Documento.Campo15 = lector[14].ToString();
                        obj_BE_Documento.Campo16 = lector[15].ToString();
                        obj_BE_Documento.Campo17 = lector[16].ToString();
                        obj_BE_Documento.Campo18 = lector[17].ToString();
                        if (Tipo == "2")
                        {
                            obj_BE_Documento.Campo19 = lector[18].ToString();
                            obj_BE_Documento.Campo20 = lector[19].ToString();
                            obj_BE_Documento.Campo21 = lector[20].ToString();
                            obj_BE_Documento.Campo22 = lector[21].ToString();
                            obj_BE_Documento.Campo23 = lector[22].ToString();
                            obj_BE_Documento.Campo24 = lector[23].ToString();
                            obj_BE_Documento.Campo25 = lector[24].ToString();
                            obj_BE_Documento.Campo26 = lector[25].ToString();
                            obj_BE_Documento.Campo27 = lector[26].ToString();
                        }
                        if (Tipo == "3" || Tipo == "4")
                        {
                            obj_BE_Documento.Campo19 = lector[18].ToString();
                            obj_BE_Documento.Campo20 = lector[19].ToString();
                            obj_BE_Documento.Campo21 = lector[20].ToString();
                            obj_BE_Documento.Campo22 = lector[21].ToString();
                            obj_BE_Documento.Campo23 = lector[22].ToString();
                            obj_BE_Documento.Campo24 = lector[23].ToString();
                            obj_BE_Documento.Campo25 = lector[24].ToString();
                            obj_BE_Documento.Campo26 = lector[25].ToString();
                            obj_BE_Documento.Campo27 = lector[26].ToString();
                            obj_BE_Documento.Campo28 = lector[27].ToString();
                            obj_BE_Documento.Campo29 = lector[28].ToString();
                            obj_BE_Documento.Campo30 = lector[29].ToString();

                        }

                        //obj_BE_Documento.Campo19 = lector[18].ToString();


                        lista.Add(obj_BE_Documento);

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Reporte> ListarComboSede()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[_TiendaSelectAllActivo_inventario]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.idTienda = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.tie_nombre = lector[1].ToString();
                        obj_BE_Documento.tie_estado = Convert.ToInt32(lector[2].ToString());
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }




        public List<BE_Reporte> ListarTipoCpu()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_LISTARINVENTARIO_CBO_TIPO_CPU]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.vc_tipocpu = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Reporte> ListarCapacidad()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_LISTARINVENTARIO_CBO_CAPACIDAD_DISCO]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.vc_capacidad_disco = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Reporte> ListarTipoDisco()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_LISTARINVENTARIO_CBO_TIPO_DISCO]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.vc_tipo_disco = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Reporte> ListarMarcaDisco()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_LISTARINVENTARIO_CBO_MARCA_DISCO]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.vc_marcadisco = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }




        public List<BE_Reporte> ListarTipoMemoria()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_LISTARINVENTARIO_CBO_TIPO_MEMORIA]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.vc_tipomemoria = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }




        public List<BE_Reporte> ListarComboArea()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[_AreaSelectAllActivo]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.idarea = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.ar_Nombrelargo = lector[1].ToString();
                        obj_BE_Documento.ar_NombreCorto = lector[2].ToString();
                        obj_BE_Documento.ar_estado = Convert.ToInt32(lector[3].ToString());
                        obj_BE_Documento.idCentroCosto = lector[4].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        //       public Boolean GuardarRegistro(string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
        //string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
        //string licenciaoffice, string obs, string cboarea, string cbosede)
        //       {
        //           return Obj_Reporteria.GuardarRegistro(cod, responsable, cel, dominio, cargo, usuario, correo, hostname, ip, ipwir, mac, macwireles, tipo,
        //            seriecpu, marcacpu, mainboardcpu, procesador, memoria, discoduro, covint, marcamonit, modelomonit, nseriemonit, teclado, mouse, licencia,
        //            licenciaoffice, obs, cboarea, cbosede);
        //       }
        public Boolean GuardarRegistro(string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
     string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
     string licenciaoffice, string obs, string cboarea, string cbosede, string cbotipomemoria, string cbotipodisco, string cbocapacidad, string cbomarcadisco, string nrocompra, string idusuarioregistro)

        {
            Boolean resultado = false;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {


                    cmd.Parameters.AddWithValue("@cod", cod);
                    cmd.Parameters.AddWithValue("@idtienda", Convert.ToInt32(cbosede));
                    cmd.Parameters.AddWithValue("@IDAREA", Convert.ToInt32(cboarea));
                    cmd.Parameters.AddWithValue("@resp", responsable);
                    cmd.Parameters.AddWithValue("@cel", cel);
                    cmd.Parameters.AddWithValue("@dominio", dominio);
                    cmd.Parameters.AddWithValue("@cargo", cargo);
                    cmd.Parameters.AddWithValue("@usuario", usuario);
                    cmd.Parameters.AddWithValue("@correo", correo);
                    cmd.Parameters.AddWithValue("@hostname", hostname);
                    cmd.Parameters.AddWithValue("@ip", ip);
                    cmd.Parameters.AddWithValue("@ipwir", ipwir);
                    cmd.Parameters.AddWithValue("@mac", mac);
                    cmd.Parameters.AddWithValue("@macwireles", macwireles);
                    cmd.Parameters.AddWithValue("@tipo", tipo);
                    cmd.Parameters.AddWithValue("@seriecpu", seriecpu);
                    cmd.Parameters.AddWithValue("@marcacpu", marcacpu);
                    cmd.Parameters.AddWithValue("@mainboardcpu", mainboardcpu);
                    cmd.Parameters.AddWithValue("@procesador", procesador);
                    cmd.Parameters.AddWithValue("@memoria", memoria);
                    cmd.Parameters.AddWithValue("@discoduro", discoduro);
                    cmd.Parameters.AddWithValue("@covint", covint);
                    cmd.Parameters.AddWithValue("@marcamonit", marcamonit);
                    cmd.Parameters.AddWithValue("@modelomonit", modelomonit);
                    cmd.Parameters.AddWithValue("@nseriemonit", nseriemonit);
                    cmd.Parameters.AddWithValue("@teclado", teclado);
                    cmd.Parameters.AddWithValue("@mouse", mouse);
                    cmd.Parameters.AddWithValue("@licencia", licencia);
                    cmd.Parameters.AddWithValue("@licenciaoffice", licenciaoffice);
                    cmd.Parameters.AddWithValue("@obs", obs);
                    cmd.Parameters.AddWithValue("@tipomemoria", cbotipomemoria);
                    cmd.Parameters.AddWithValue("@tipodisco", cbotipodisco);
                    cmd.Parameters.AddWithValue("@capacidad", cbocapacidad);
                    cmd.Parameters.AddWithValue("@marcadisco", cbomarcadisco);
                    cmd.Parameters.AddWithValue("@nrocompra", nrocompra);
                    cmd.Parameters.AddWithValue("@idusuarioregistro", Convert.ToInt32(idusuarioregistro));


                    cmd.Connection = cn;
                    cmd.CommandText = "USP_INSERT_REGISTRO_INVENTARIO_TI_2022";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                //  Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            resultado = true;
            return resultado;
        }

        public List<BE_Reporte> ListarInventario()
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();


                using (SqlCommand cmd = new SqlCommand("USP_LISTA_INVENTARIO_TI_2022", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.Campo1 = lector[0].ToString();
                        obj_BE_Documento.Campo2 = lector[1].ToString();
                        obj_BE_Documento.Campo3 = lector[2].ToString();
                        obj_BE_Documento.Campo4 = lector[3].ToString();
                        obj_BE_Documento.Campo5 = lector[4].ToString();
                        obj_BE_Documento.Campo6 = lector[5].ToString();
                        obj_BE_Documento.Campo7 = lector[6].ToString();
                        obj_BE_Documento.Campo8 = lector[7].ToString();
                        obj_BE_Documento.Campo9 = lector[8].ToString();
                        obj_BE_Documento.Campo10 = lector[9].ToString();
                        obj_BE_Documento.Campo11 = lector[10].ToString();
                        obj_BE_Documento.Campo12 = lector[11].ToString();
                        obj_BE_Documento.Campo13 = lector[12].ToString();
                        obj_BE_Documento.Campo14 = lector[13].ToString();
                        obj_BE_Documento.Campo15 = lector[14].ToString();
                        obj_BE_Documento.Campo16 = lector[15].ToString();
                        obj_BE_Documento.Campo17 = lector[16].ToString();
                        obj_BE_Documento.Campo18 = lector[17].ToString();
                        obj_BE_Documento.Campo19 = lector[18].ToString();
                        obj_BE_Documento.Campo20 = lector[19].ToString();
                        obj_BE_Documento.Campo21 = lector[20].ToString();
                        obj_BE_Documento.Campo22 = lector[21].ToString();
                        obj_BE_Documento.Campo23 = lector[22].ToString();
                        obj_BE_Documento.Campo24 = lector[23].ToString();
                        obj_BE_Documento.Campo25 = lector[24].ToString();
                        obj_BE_Documento.Campo26 = lector[25].ToString();
                        obj_BE_Documento.Campo27 = lector[26].ToString();
                        obj_BE_Documento.Campo28 = lector[27].ToString();
                        obj_BE_Documento.Campo29 = lector[28].ToString();
                        obj_BE_Documento.Campo30 = lector[29].ToString();
                        obj_BE_Documento.Campo31 = lector[30].ToString();
                        obj_BE_Documento.Campo32 = lector[31].ToString();
                        obj_BE_Documento.Campo33 = lector[32].ToString();
                        obj_BE_Documento.Campo34 = lector[33].ToString();
                        obj_BE_Documento.Campo35 = lector[34].ToString();
                        obj_BE_Documento.Campo36 = lector[35].ToString();
                        obj_BE_Documento.Campo37 = lector[36].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public BE_Reporte ListarInventarioxid(string iddocumento)
        {
            BE_Reporte obj_BE_Documento = new BE_Reporte();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("USP_LISTA_INVENTARIOxid_TI_2022", con))
                {
                    cmd.Parameters.AddWithValue("@iddocumento", Convert.ToInt32(iddocumento));
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        obj_BE_Documento.Campo1 = lector[0].ToString();
                        obj_BE_Documento.Campo2 = lector[1].ToString();
                        obj_BE_Documento.Campo3 = lector[2].ToString();
                        obj_BE_Documento.Campo4 = lector[3].ToString();
                        obj_BE_Documento.Campo5 = lector[4].ToString();
                        obj_BE_Documento.Campo6 = lector[5].ToString();
                        obj_BE_Documento.Campo7 = lector[6].ToString();
                        obj_BE_Documento.Campo8 = lector[7].ToString();
                        obj_BE_Documento.Campo9 = lector[8].ToString();
                        obj_BE_Documento.Campo10 = lector[9].ToString();
                        obj_BE_Documento.Campo11 = lector[10].ToString();
                        obj_BE_Documento.Campo12 = lector[11].ToString();
                        obj_BE_Documento.Campo13 = lector[12].ToString();
                        obj_BE_Documento.Campo14 = lector[13].ToString();
                        obj_BE_Documento.Campo15 = lector[14].ToString();
                        obj_BE_Documento.Campo16 = lector[15].ToString();
                        obj_BE_Documento.Campo17 = lector[16].ToString();
                        obj_BE_Documento.Campo18 = lector[17].ToString();
                        obj_BE_Documento.Campo19 = lector[18].ToString();
                        obj_BE_Documento.Campo20 = lector[19].ToString();
                        obj_BE_Documento.Campo21 = lector[20].ToString();
                        obj_BE_Documento.Campo22 = lector[21].ToString();
                        obj_BE_Documento.Campo23 = lector[22].ToString();
                        obj_BE_Documento.Campo24 = lector[23].ToString();
                        obj_BE_Documento.Campo25 = lector[24].ToString();
                        obj_BE_Documento.Campo26 = lector[25].ToString();
                        obj_BE_Documento.Campo27 = lector[26].ToString();
                        obj_BE_Documento.Campo28 = lector[27].ToString();
                        obj_BE_Documento.Campo29 = lector[28].ToString();
                        obj_BE_Documento.Campo30 = lector[29].ToString();
                        obj_BE_Documento.Campo31 = lector[30].ToString();
                        obj_BE_Documento.Campo32 = lector[31].ToString();
                        obj_BE_Documento.Campo33 = lector[32].ToString();
                        obj_BE_Documento.Campo34 = lector[33].ToString();
                        obj_BE_Documento.Campo35 = lector[34].ToString();
                        obj_BE_Documento.Campo36 = lector[35].ToString();
                        obj_BE_Documento.Campo37 = lector[36].ToString();

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return obj_BE_Documento;
        }



        public List<BE_Reporte> ListarAuditoriaxid(string iddocumento)
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();

            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("USP_TBL_LOG_INVENTARIO_IT_2022_LIST", con))
                {
                    cmd.Parameters.AddWithValue("@id", Convert.ToInt32(iddocumento));
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.vc_usuario = lector[0].ToString();
                        obj_BE_Documento.vc_campo_cambio = lector[1].ToString();
                        obj_BE_Documento.vc_textoanterior = lector[2].ToString();
                        obj_BE_Documento.vc_textonuevo = lector[3].ToString();
                        obj_BE_Documento.fecha = lector[4].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }

            return lista;
        }



        public Boolean ActualizarRegistro(string id, string cod, string responsable, string cel, string dominio, string cargo, string usuario, string correo, string hostname, string ip, string ipwir, string mac, string macwireles, string tipo,
string seriecpu, string marcacpu, string mainboardcpu, string procesador, string memoria, string discoduro, string covint, string marcamonit, string modelomonit, string nseriemonit, string teclado, string mouse, string licencia,
string licenciaoffice, string obs, string cboarea, string cbosede, string cbotipomemoria, string cbotipodisco, string cbocapacidad, string marcadisco, string estadoasoignacion, string nrocompra, string idusuarioregistro)
        {


            Boolean resultado = false;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Parameters.AddWithValue("@id", Convert.ToInt32(id));
                    cmd.Parameters.AddWithValue("@cod", cod);
                    cmd.Parameters.AddWithValue("@idtienda", Convert.ToInt32(cbosede));
                    cmd.Parameters.AddWithValue("@IDAREA", Convert.ToInt32(cboarea));
                    cmd.Parameters.AddWithValue("@resp", responsable);
                    cmd.Parameters.AddWithValue("@cel", cel);
                    cmd.Parameters.AddWithValue("@dominio", dominio);
                    cmd.Parameters.AddWithValue("@cargo", cargo);
                    cmd.Parameters.AddWithValue("@usuario", usuario);
                    cmd.Parameters.AddWithValue("@correo", correo);
                    cmd.Parameters.AddWithValue("@hostname", hostname);
                    cmd.Parameters.AddWithValue("@ip", ip);
                    cmd.Parameters.AddWithValue("@ipwir", ipwir);
                    cmd.Parameters.AddWithValue("@mac", mac);
                    cmd.Parameters.AddWithValue("@macwireles", macwireles);
                    cmd.Parameters.AddWithValue("@tipo", tipo);
                    cmd.Parameters.AddWithValue("@seriecpu", seriecpu);
                    cmd.Parameters.AddWithValue("@marcacpu", marcacpu);
                    cmd.Parameters.AddWithValue("@mainboardcpu", mainboardcpu);
                    cmd.Parameters.AddWithValue("@procesador", procesador);
                    cmd.Parameters.AddWithValue("@memoria", memoria);
                    cmd.Parameters.AddWithValue("@discoduro", discoduro);
                    cmd.Parameters.AddWithValue("@covint", covint);
                    cmd.Parameters.AddWithValue("@marcamonit", marcamonit);
                    cmd.Parameters.AddWithValue("@modelomonit", modelomonit);
                    cmd.Parameters.AddWithValue("@nseriemonit", nseriemonit);
                    cmd.Parameters.AddWithValue("@teclado", teclado);
                    cmd.Parameters.AddWithValue("@mouse", mouse);
                    cmd.Parameters.AddWithValue("@licencia", licencia);
                    cmd.Parameters.AddWithValue("@licenciaoffice", licenciaoffice);
                    cmd.Parameters.AddWithValue("@obs", obs);
                    cmd.Parameters.AddWithValue("@idusuariosist", Convert.ToInt32(idusuarioregistro));
                    cmd.Parameters.AddWithValue("@tipomemoria", cbotipomemoria);
                    cmd.Parameters.AddWithValue("@tipodisco", cbotipodisco);
                    cmd.Parameters.AddWithValue("@capacidad", cbocapacidad);
                    cmd.Parameters.AddWithValue("@marcadisco", marcadisco);
                    cmd.Parameters.AddWithValue("@estadoasignacion", Convert.ToInt32(estadoasoignacion));
                    cmd.Parameters.AddWithValue("@nrocompra", nrocompra);



                    cmd.Connection = cn;
                    cmd.CommandText = "USP_UPDATE_REGISTRO_INVENTARIO_TI_2022";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                //  Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            resultado = true;
            return resultado;
        }


        public Boolean Eliminarxid(string id)
        {
            Boolean resultado = false;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@id", id);

                    cmd.Connection = cn;
                    cmd.CommandText = "USP_ELIMINAR_REGISTRO_INVENTARIO_TI_2022";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                //  Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            resultado = true;
            return resultado;
        }



        public List<int> CountCantEstaciones()
        {
            List<int> listadoCount = new List<int>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();


                using (SqlCommand cmd = new SqlCommand("USP_COUNT_ESTACIONES", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        listadoCount.Add(Convert.ToInt32(lector[0].ToString()));
                        listadoCount.Add(Convert.ToInt32(lector[1].ToString()));
                        listadoCount.Add(Convert.ToInt32(lector[2].ToString()));
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return listadoCount;
        }

        public Boolean ValidarMacE(string mace)
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_INVENTARIO_VALIDAR_MACE]", con))
                {
                    cmd.Parameters.AddWithValue("@mace", mace);
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.validarmace = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }

            Boolean resultado = false;

            Int32 validar = Convert.ToInt32(lista[0].validarmace);

            if (validar > 0)
            {
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }


        public Boolean ValidarMacW(string macw)
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("[USP_INVENTARIO_VALIDAR_MACW]", con))
                {
                    cmd.Parameters.AddWithValue("@macw", macw);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.validarmacw = lector[0].ToString();
                        lista.Add(obj_BE_Documento);
                    }
                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }

            Boolean resultado = false;

            Int32 validar = Convert.ToInt32(lista[0].validarmacw);

            if (validar > 0)
            {
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }

        

    }
}