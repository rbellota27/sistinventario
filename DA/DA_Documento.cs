﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BE;
using System.Data;
using System.Configuration;
using System.Management;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data.Common;

namespace DA
{
    public class DA_Documento
    {
        string conexion = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;

        #region ORDEN PEDIDO
        public List<BE_Documento> LISTAR_ORDEN_PEDIDO_VP(string IdEmpresa, string IdAlmacen, string FecIni,
            string FecFin, string doc_Serie, string doc_Codigo, string IdPersona, string IdDocumento,
            string IdTipoOperacion, string IdTipoDocumento)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_ORDEN_PEDIDO_VP", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa);
                    cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen);
                    cmd.Parameters.AddWithValue("@FechaInicio", Convert.ToDateTime(FecIni));
                    cmd.Parameters.AddWithValue("@FechaFin", Convert.ToDateTime(FecFin));
                    cmd.Parameters.AddWithValue("@Serie", doc_Serie);
                    cmd.Parameters.AddWithValue("@Codigo", doc_Codigo);
                    cmd.Parameters.AddWithValue("@IdPersona", IdPersona);
                    //cmd.Parameters.AddWithValue("@IdTipoDocumento", IdDocumento);
                    cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion);
                    //cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", IdTipoDocumento);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.TipoDocumento = lector[11].ToString();
                        obj_BE_Documento.NroDocumento = lector[24].ToString();
                        obj_BE_Documento.Almacen = lector[15].ToString();
                        obj_BE_Documento.TipoOperacion = lector[22].ToString();
                        obj_BE_Documento.EstadoEnt = lector[16].ToString();
                        obj_BE_Documento.FechaEmision = lector[3].ToString();
                        obj_BE_Documento.IdTipoOperacion = Convert.ToInt32(lector[25].ToString());
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Documento> LISTAR_DETALLE_ORDEN_PEDIDO_VP(string ID_PEDIDO, string ID_TIPO_OPERACION)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_DetalleDocumentoSelectDetallexAtenderxIdDocumento_Detalle", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdDocumento", ID_PEDIDO);
                    cmd.Parameters.AddWithValue("@IdTipoOperacion", ID_TIPO_OPERACION);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        int cant = Convert.ToInt32(Convert.ToDouble(lector[6].ToString()));

                        if (cant == 0)
                        {

                        }
                        else
                        {
                            BE_Documento obj_BE_Documento = new BE_Documento();
                            obj_BE_Documento.CodigoProducto = lector[18].ToString();
                            obj_BE_Documento.NombreProducto = lector[4].ToString();
                            obj_BE_Documento.UM = lector[5].ToString();
                            obj_BE_Documento.Cantidad = lector[6].ToString();

                            obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[1].ToString());
                            obj_BE_Documento.IdProducto = Convert.ToInt32(lector[3].ToString());
                            obj_BE_Documento.IdAlmacen = Convert.ToInt32(lector[16].ToString());
                            obj_BE_Documento.IdDetalleDocumento = Convert.ToInt32(lector[0].ToString());
                            lista.Add(obj_BE_Documento);
                        }


                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Documento> LISTAR_DETALLE_TONOS_ORDEN_PEDIDO_VP(string ID_PRODUCTO, string ID_ALMACEN, string ID_DOCUMENTO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_SELECT_TONO_X_PRODUCTO_NUEVO_V2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@ID_PRODUCTO", ID_PRODUCTO);
                    cmd.Parameters.AddWithValue("@ID_ALMACEN", ID_ALMACEN);
                    cmd.Parameters.AddWithValue("@idDOcumento", ID_DOCUMENTO);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.idtono = lector[3].ToString();
                        obj_BE_Documento.nomtono = lector[4].ToString();
                        obj_BE_Documento.cantidadtono = lector[6].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Documento> OBTENER_OBSERVACION_PV(string ID_PEDIDO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("_ObservacionesSelectxIdDocumento", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdDocumento", ID_PEDIDO);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.Observacion = lector[2].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        #endregion


        #region PICKING
        public string GENERAR_PICKING(string ID_PEDIDO, string ID_SERIE, string NRO_SERIE, string NRO_DOC,int USUARIO)
        {
            int val = 0; int IdDetalleDocumento = 0;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            using (SqlTransaction transaccion = con.BeginTransaction())
            {
                try
                {
                    #region CABECERA
                    using (SqlCommand cmd = transaccion.Connection.CreateCommand())
                    {
                        val = 0;
                        cmd.CommandText = "SP_INSERTAR_CAB_PICKING";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@doc_Codigo", NRO_DOC);
                        cmd.Parameters.AddWithValue("@doc_Serie", NRO_SERIE);
                        cmd.Parameters.AddWithValue("@IdSerie", ID_SERIE);
                        cmd.Parameters.AddWithValue("@Usuario", USUARIO);
                        SqlParameter ValorRetorno = new SqlParameter("@IdDocumento", SqlDbType.Int);
                        ValorRetorno.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(ValorRetorno);
                        val = cmd.ExecuteNonQuery();
                        int IdDocumento = Convert.ToInt32(ValorRetorno.Value);
                        cmd.Parameters.Clear();
                        #endregion

                        #region DETALLE
                        val = 0;
                        int IDDOCUMENTO = IdDocumento;
                        cmd.CommandText = "SP_INSERTAR_DET_PICKING";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdDocumento", IDDOCUMENTO);
                        cmd.Parameters.AddWithValue("@IdDocumentoref", ID_PEDIDO);
                        SqlParameter ValorRetorno2 = new SqlParameter("@IdDetalleDocumento", SqlDbType.Int);
                        ValorRetorno2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(ValorRetorno2);
                        val = cmd.ExecuteNonQuery();
                        IdDetalleDocumento = Convert.ToInt32(ValorRetorno2.Value);
                        cmd.Parameters.Clear();
                        #endregion                     
                    }
                    transaccion.Commit();

                }
                catch (Exception ex)
                {

                    StackTrace st = new StackTrace(ex, true);
                    StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                         && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                         && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                         && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                    string MachineName = System.Environment.MachineName;
                    string UserName = System.Environment.UserName.ToUpper();
                    string Mensaje = ex.Message;
                    int LineaError = frame.GetFileLineNumber();
                    string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                    string Clase = frame.GetMethod().DeclaringType.Name;
                    string metodo = frame.GetMethod().Name;
                    string codigoError = Convert.ToString(frame.GetHashCode());
                    transaccion.Rollback();

                    return Mensaje;
                }



            }

            return Convert.ToString(IdDetalleDocumento);
        }


        public string GENERAR_PICKING_MANUAL(string ID_PEDIDO, string ID_SERIE, string NRO_SERIE, string NRO_DOC, int USUARIO,string[][] VALORES)
        {
            int val = 0; int IdDetalleDocumento = 0;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            using (SqlTransaction transaccion = con.BeginTransaction())
            {
                try
                {
                    #region CABECERA
                    using (SqlCommand cmd = transaccion.Connection.CreateCommand())
                    {
                        val = 0;
                        cmd.CommandText = "SP_INSERTAR_CAB_PICKING";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@doc_Codigo", NRO_DOC);
                        cmd.Parameters.AddWithValue("@doc_Serie", NRO_SERIE);
                        cmd.Parameters.AddWithValue("@IdSerie", ID_SERIE);
                        cmd.Parameters.AddWithValue("@Usuario", USUARIO);
                        SqlParameter ValorRetorno = new SqlParameter("@IdDocumento", SqlDbType.Int);
                        ValorRetorno.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(ValorRetorno);
                        val = cmd.ExecuteNonQuery();
                        int IdDocumento = Convert.ToInt32(ValorRetorno.Value);
                        cmd.Parameters.Clear();
                        #endregion

                        #region DETALLE


                        for (int i = 0; i < VALORES.Length; i++)
                        {
                            if (VALORES[i][0].Trim() == null)
                            {
                            }
                            else
                            {

                                val = 0;
                                int IDDOCUMENTO = IdDocumento;
                                cmd.CommandText = "SP_INSERTAR_DET_PICKING_MANUAL";
                                cmd.Transaction = transaccion;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@IdDocumento", IDDOCUMENTO);
                                cmd.Parameters.AddWithValue("@IdDocumentoref", ID_PEDIDO);
                                cmd.Parameters.AddWithValue("@IdProducto", VALORES[i][1].ToString().Trim());
                                cmd.Parameters.AddWithValue("@Tono", VALORES[i][2].ToString().Trim());
                                cmd.Parameters.AddWithValue("@Id_detalle_documento_ref", VALORES[i][3].ToString().Trim());
                                SqlParameter ValorRetorno2 = new SqlParameter("@IdDetalleDocumento", SqlDbType.Int);
                                ValorRetorno2.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(ValorRetorno2);
                                val = cmd.ExecuteNonQuery();
                                IdDetalleDocumento = Convert.ToInt32(ValorRetorno2.Value);
                                cmd.Parameters.Clear();
                            }
                        }
                        #endregion


                        #region PUNTO PARTIDA

                        val = 0;
                        cmd.CommandText = "SP_INSERTAR_PUNTO_PARTIDA";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                        cmd.Parameters.AddWithValue("@IdDocumentoref", ID_PEDIDO);
                        val = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        #endregion


                        #region PUNTO LLEGADA

                        val = 0;
                        cmd.CommandText = "SP_INSERTAR_PUNTO_LLEGADA";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                        cmd.Parameters.AddWithValue("@IdDocumentoref", ID_PEDIDO);
                        val = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        #endregion

                        #region RELACION DOCUMENTO

                        val = 0;
                        cmd.CommandText = "_RelacionDocumentoInsert";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdDocumento1", ID_PEDIDO);
                        cmd.Parameters.AddWithValue("@IdDocumento2", IdDocumento);
                        val = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        #endregion
                    }
                    transaccion.Commit();

                }
                catch (Exception ex)
                {

                    StackTrace st = new StackTrace(ex, true);
                    StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                         && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                         && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                         && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                    string MachineName = System.Environment.MachineName;
                    string UserName = System.Environment.UserName.ToUpper();
                    string Mensaje = ex.Message;
                    int LineaError = frame.GetFileLineNumber();
                    string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                    string Clase = frame.GetMethod().DeclaringType.Name;
                    string metodo = frame.GetMethod().Name;
                    string codigoError = Convert.ToString(frame.GetHashCode());
                    transaccion.Rollback();

                    return Mensaje;
                }



            }




            return Convert.ToString(IdDetalleDocumento);
        }





        public int GUARDAR_VERIFICACION_PICKING(int ID_DOCUMENTO_DETALLE, decimal CANT_PICKEADA, decimal CANT_VERIFICADOR)
        {
            int val = 0;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            using (SqlTransaction transaccion = con.BeginTransaction())
            {
                try
                {

                    using (SqlCommand cmd = transaccion.Connection.CreateCommand())
                    {
                        val = 0;
                        cmd.CommandText = "SP_VERIFICACION_CANT_PICKING";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID_DOCUMENTO_DETALLE", ID_DOCUMENTO_DETALLE);
                        cmd.Parameters.AddWithValue("@CANT_PICKEADA", CANT_PICKEADA);
                        cmd.Parameters.AddWithValue("@CANT_VERIFICADOR", CANT_VERIFICADOR);
                        val = cmd.ExecuteNonQuery();

                    }
                    transaccion.Commit();

                }
                catch (Exception ex)
                {

                    StackTrace st = new StackTrace(ex, true);
                    StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                         && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                         && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                         && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                    string MachineName = System.Environment.MachineName;
                    string UserName = System.Environment.UserName.ToUpper();
                    string Mensaje = ex.Message;
                    int LineaError = frame.GetFileLineNumber();
                    string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                    string Clase = frame.GetMethod().DeclaringType.Name;
                    string metodo = frame.GetMethod().Name;
                    string codigoError = Convert.ToString(frame.GetHashCode());
                    transaccion.Rollback();


                }
            }

            return val;
        }
        public int ACTUALIZAR_PICKING_PRODUCTOS_ASIGNADOS(int ID_DETALLEDOCUMENTO, decimal CANTIDAD_ENCONTRADA, int IDUSUARIO)
        {
            int val = 0;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            using (SqlTransaction transaccion = con.BeginTransaction())
            {
                try
                {
                    using (SqlCommand cmd = transaccion.Connection.CreateCommand())
                    {
                        val = 0;
                        cmd.CommandText = "SP_ACTUALIZAR_PK_PROD_ASIG";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID_DETALLEDOCUMENTO", ID_DETALLEDOCUMENTO);
                        cmd.Parameters.AddWithValue("@CANTIDAD_ENCONTRADA", CANTIDAD_ENCONTRADA);
                        cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);

                        val = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    transaccion.Commit();

                }
                catch (Exception ex)
                {

                    StackTrace st = new StackTrace(ex, true);
                    StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                         && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                         && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                         && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();
                    string MachineName = System.Environment.MachineName;
                    string UserName = System.Environment.UserName.ToUpper();
                    string Mensaje = ex.Message;
                    int LineaError = frame.GetFileLineNumber();
                    string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                    string Clase = frame.GetMethod().DeclaringType.Name;
                    string metodo = frame.GetMethod().Name;
                    string codigoError = Convert.ToString(frame.GetHashCode());
                    transaccion.Rollback();
                }
            }
            return val;
        }
        public List<BE_Documento> LISTAR_PICKING(string FecIni, string FecFin)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_PICKING", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.AddWithValue("@FechaInicio", Convert.ToDateTime(FecIni));
                    cmd.Parameters.AddWithValue("@FechaFin", Convert.ToDateTime(FecFin));


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.NroDocumento = lector[1].ToString();
                        obj_BE_Documento.Tienda = lector[6].ToString();
                        obj_BE_Documento.Almacen = lector[12].ToString();
                        obj_BE_Documento.TipoOperacion = lector[8].ToString();
                        obj_BE_Documento.EstadoEnt = lector[10].ToString();
                        obj_BE_Documento.EstadoDoc = lector[4].ToString();
                        obj_BE_Documento.FechaEmision = lector[2].ToString();
                        obj_BE_Documento.Tienda_Pedido = lector[13].ToString();
                  
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        



        

        public List<BE_Documento> LISTAR_CHECKLIST_PICKING()
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_CHECKLIST_PICKING", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.IdDocumento= Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.IdDetalleDocumento = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.NroDocumento = lector[2].ToString();
                        obj_BE_Documento.CodigoProducto = lector[4].ToString();
                        obj_BE_Documento.prod_Nombre = lector[5].ToString();
                        obj_BE_Documento.Cantidad = lector[6].ToString();
                        obj_BE_Documento.UM = lector[7].ToString();
                        obj_BE_Documento.cantidad_transito = lector[9].ToString();
                        obj_BE_Documento.cantidad_x_atender = lector[8].ToString();
                        obj_BE_Documento.estado = lector[10].ToString();
                        obj_BE_Documento.doc_referencia = lector[11].ToString();
                        obj_BE_Documento.Tie_Ref = lector[12].ToString();
                        obj_BE_Documento.obsRef = lector[13].ToString();
                        obj_BE_Documento.UsuPick = lector[14].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Documento> LISTAR_PRODUCTOS_ASIGNADOS(int IDUSUARIO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_PROD_ASIG_USU_PICKING", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.NroDocumento = lector[0].ToString();
                        obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.IdDetalleDocumento = Convert.ToInt32(lector[2].ToString());
                        obj_BE_Documento.IdProducto = Convert.ToInt32(lector[3].ToString());
                        obj_BE_Documento.prod_Codigo = lector[5].ToString();
                        obj_BE_Documento.prod_Nombre = lector[6].ToString();
                        obj_BE_Documento.Cantidad = lector[7].ToString();
                        obj_BE_Documento.UM = lector[8].ToString();
                        obj_BE_Documento.Zona = lector[9].ToString();
                        obj_BE_Documento.Linea = lector[10].ToString();
                        obj_BE_Documento.SubLinea = lector[11].ToString();
                        obj_BE_Documento.cantidad_final = lector[12].ToString();
                        obj_BE_Documento.doc_referencia = lector[14].ToString();
                        obj_BE_Documento.Tie_Ref = lector[15].ToString();
                        obj_BE_Documento.obsRef = lector[16].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Documento> LISTAR_DETALLE_PICKING(int ID_DOCUMENTO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_DETALLE_PICKING", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdDocumento", ID_DOCUMENTO);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {


                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.IdDetalleDocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.CodigoProducto = lector[8].ToString();
                        obj_BE_Documento.NroDocumento = lector[9].ToString();
                        obj_BE_Documento.NombreProducto = lector[7].ToString();
                        obj_BE_Documento.UM = lector[5].ToString();
                        obj_BE_Documento.Cantidad = lector[4].ToString();

                        obj_BE_Documento.StockReal = lector[13].ToString();
                        obj_BE_Documento.StockComprometido = lector[14].ToString();
                        obj_BE_Documento.StockDisponible = lector[15].ToString();
                        obj_BE_Documento.peso_producto = Convert.ToDecimal(lector[16].ToString());

                        lista.Add(obj_BE_Documento);



                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }




        public List<BE_Documento> LISTAR_DETALLE_PICKING_TONOS(int ID_DOCUMENTO, int ID_DETALLE_DOCUMENTO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_SELECT_TONO_X_PRODUCTO_EDITAR", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@idDocumento", ID_DOCUMENTO);
                    cmd.Parameters.AddWithValue("@idDetalleDocumento", ID_DETALLE_DOCUMENTO);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {


                        BE_Documento obj_BE_Documento = new BE_Documento();

                        obj_BE_Documento.idtono = lector[1].ToString();
                        obj_BE_Documento.Tono = lector[2].ToString();
                        obj_BE_Documento.cantidadtono = lector[3].ToString();

                        lista.Add(obj_BE_Documento);

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Documento> LISTAR_DETALLE_PICKING_ENPROCESO(int ID_DOCUMENTO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_DETALLE_PICKING_ENPROCESO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdDocumento", ID_DOCUMENTO);


                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {


                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.CodigoProducto = lector[2].ToString();
                        obj_BE_Documento.NombreProducto = lector[3].ToString();
                        obj_BE_Documento.Cantidad = lector[4].ToString();
                        obj_BE_Documento.UM = lector[5].ToString();
                        obj_BE_Documento.cantidad_x_atender = lector[6].ToString();
                        obj_BE_Documento.cantidad_transito = lector[7].ToString();
                        obj_BE_Documento.estado = lector[8].ToString();
                        lista.Add(obj_BE_Documento);



                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        #endregion


        public List<BE_Documento> OBTENER_SERIE()
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_SERIE", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.IdSerie = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.doc_Serie = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }
        public List<BE_Documento> OBTENER_CONSECUTIVO(string COD_SERIE)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_CONSECUTIVO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdSerie", COD_SERIE);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.doc_Codigo = lector[0].ToString();

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Documento> OBTENER_DATOS_LOGIN(string USUARIO, string CLAVE)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_DATOS_LOGIN", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USUARIO", USUARIO);
                    cmd.Parameters.AddWithValue("@CLAVE", CLAVE);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.ID = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.NOMBRE = lector[1].ToString();
                        obj_BE_Documento.USUARIO = lector[2].ToString();
                        obj_BE_Documento.PERFIL = lector[5].ToString();
                        obj_BE_Documento.IDTIENDA = lector[6].ToString();
                        obj_BE_Documento.TIENDA = lector[7].ToString();
                        obj_BE_Documento.IDEMPRESA = Convert.ToInt32(lector[8].ToString());
                        obj_BE_Documento.IDALMACENPRINCIPAL = Convert.ToInt32(lector[9].ToString());

                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Opcion> Obtener_Datos_Menu(int idusuario,string perfil)
        {
            List<BE_Opcion> listadoopciones = new List<BE_Opcion>();
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();


                //using (SqlCommand cmd = new SqlCommand("SP_OBTENER_DATOS_MENU", con))
                using (SqlCommand cmd = new SqlCommand("UP_OBTENER_DATOS_MENUS1", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idusuario", idusuario);
                    cmd.Parameters.AddWithValue("@perfil", perfil);
                    //cmd.Parameters.AddWithValue("@flagmodulo", 3);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Opcion obj_Opcion = new BE_Opcion();
                        obj_Opcion.IdUsuario = Convert.ToInt32(lector[0].ToString());
                        obj_Opcion.IdOpcion = lector[1].ToString();
                        obj_Opcion.IdMenu = lector[2].ToString();
                        obj_Opcion.op_Orden = lector[3].ToString();
                        obj_Opcion.op_Nombre = lector[4].ToString();
                        obj_Opcion.op_Formulario = lector[5].ToString();

                        listadoopciones.Add(obj_Opcion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return listadoopciones;
        }

        public List<BE_Documento> LISTAR_DETALLE_DOCUMENTOS_BUSQUEDA(string DOC_SERIE, string DOC_CODIGO,int IDTIPODOCUMENTO)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTIENE_DOCUMENTO_NUEVO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;



                    cmd.Parameters.AddWithValue("@DOC_SERIE", DOC_SERIE);
                    cmd.Parameters.AddWithValue("@DOC_CODIGO", DOC_CODIGO);
                    cmd.Parameters.AddWithValue("@IDTIPODOCUMENTO", IDTIPODOCUMENTO);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {

                            BE_Documento obj_BE_Documento = new BE_Documento();

                            obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[0].ToString());
                            obj_BE_Documento.TipoDocumento= lector[1].ToString();
                            obj_BE_Documento.doc_Codigo= lector[2].ToString();
                            obj_BE_Documento.FechaEmision = lector[3].ToString();
                            obj_BE_Documento.doc_ImporteTotal = Convert.ToDecimal(lector[4].ToString());
                            obj_BE_Documento.Tienda = lector[5].ToString();
                            obj_BE_Documento.Cliente = lector[6].ToString();
                            obj_BE_Documento.TipoOperacion = lector[7].ToString();
                            obj_BE_Documento.EstadoDoc = lector[8].ToString();
                            obj_BE_Documento.Ruta = lector[9].ToString();

                        lista.Add(obj_BE_Documento);
                        
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public string GUARDAR_REGISTROS_DOCUMENTOS_NEW(string Tipo, int IdDocumento,decimal Monto, string Ruta,int IdUsuario)
        {


            string val = "";
            SqlDataReader dr;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            using (SqlTransaction transaccion = con.BeginTransaction())
            {
                try
                {

                    using (SqlCommand cmd = transaccion.Connection.CreateCommand())
                    {
                        //val = 0;
                        cmd.CommandText = "UP_Documento_Extornos";
                        cmd.Transaction = transaccion;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Tipo", Tipo);
                        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                        cmd.Parameters.AddWithValue("@Monto", Monto);
                        cmd.Parameters.AddWithValue("@Ruta", Ruta);
                        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
                        dr = cmd.ExecuteReader();


                        //using (SqlDataReader response = cmd.ExecuteReader())
                        //{
                        //    if (response.HasRows)
                        //    {

                        //        while (response.Read())
                        //        {

                        //            val = response["ERROR"].ToString();

                        //        }
                        //    }
                        //}





                        if (dr != null)
                        {
                            if (dr.HasRows)
                            {
                                dr.Read();
                                val = dr.GetString(0);
                            }
                            dr.Close();
                        }

                    }
                    //transaccion.Commit();

                }
                catch (Exception ex)
                {

                    StackTrace st = new StackTrace(ex, true);
                    StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                         && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                         && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                         && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                    string MachineName = System.Environment.MachineName;
                    string UserName = System.Environment.UserName.ToUpper();
                    string Mensaje = ex.Message;
                    int LineaError = frame.GetFileLineNumber();
                    string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                    string Clase = frame.GetMethod().DeclaringType.Name;
                    string metodo = frame.GetMethod().Name;
                    string codigoError = Convert.ToString(frame.GetHashCode());
                    //transaccion.Rollback();


                }
            }

            return val;
        }


        public string GUARDAR_REGISTROS_DOCUMENTOS(string Tipo, int IdDocumento, decimal Monto, string Ruta, int IdUsuario)
        {
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                    cmd.Parameters.AddWithValue("@Monto", Monto);
                    cmd.Parameters.AddWithValue("@Ruta", Ruta);
                    cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);

                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Extornos";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }


        public List<BE_Documento> LISTAR_DOCUMENTOS_INGRESADOS(string Tipo)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_Muestra_Documentos_Ingresados", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {

                        BE_Documento obj_BE_Documento = new BE_Documento();

                        obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.IdDoc = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Documento.TipoDocumento = lector[2].ToString();
                        obj_BE_Documento.doc_Codigo = lector[3].ToString();
                        obj_BE_Documento.FechaEmision = lector[4].ToString();
                        obj_BE_Documento.doc_ImporteTotal = Convert.ToDecimal(lector[5].ToString());
                        obj_BE_Documento.Tienda = lector[6].ToString();
                        obj_BE_Documento.Cliente = lector[7].ToString();
                        obj_BE_Documento.TipoOperacion = lector[8].ToString();
                        obj_BE_Documento.doc_Total =  Convert.ToDecimal(lector[9].ToString());
                        obj_BE_Documento.doc_FechaRegistro = lector[10].ToString();
                        obj_BE_Documento.Semaforo = lector[11].ToString();
                        obj_BE_Documento.EstadoDoc = lector[12].ToString();
                        


                        lista.Add(obj_BE_Documento);

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public string GUARDAR_REGISTROS_DOCUMENTOS_IZIPAY(string Tipo, int IdDocumento,int IdDoc,string CODIGO, string TIPO_DE_MOVIMIENTO,string SERVICIO,string TRANSACCION,DateTime FECHA_DE_TRANSACCION,
            string HORA_DE_TRANSACCION,DateTime FECHA_DE_CIERRE_DE_LOTE,DateTime FECHA_DE_PROCESO,DateTime FECHA_DE_ABONO,string ESTADO,decimal IMPORTE,decimal COMISION,decimal IGV,decimal IMPORTE_NETO,
            decimal ABONO_DEL_LOTE,string NUM_DE_LOTE,string TERMINAL,string NUM_DE_REF_VOUCHER,string MARCA_DE_TARJETA,string NUM_DE_TARJETA, string CODIGO_DE_AUTORIZACION,int CUOTAS,string OBSERVACIONES,
            string MONEDA,string SERIE_TERMINAL, int USUARIO)
        {
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                    cmd.Parameters.AddWithValue("@IdDoc", IdDoc);
                    cmd.Parameters.AddWithValue("@CODIGO", CODIGO);
                    cmd.Parameters.AddWithValue("@TIPO_DE_MOVIMIENTO", TIPO_DE_MOVIMIENTO);
                    cmd.Parameters.AddWithValue("@SERVICIO", SERVICIO);
                    cmd.Parameters.AddWithValue("@TRANSACCION", TRANSACCION);
                    cmd.Parameters.AddWithValue("@FECHA_DE_TRANSACCION", FECHA_DE_TRANSACCION);
                    cmd.Parameters.AddWithValue("@HORA_DE_TRANSACCION", HORA_DE_TRANSACCION);
                    cmd.Parameters.AddWithValue("@FECHA_DE_CIERRE_DE_LOTE", FECHA_DE_CIERRE_DE_LOTE);
                    cmd.Parameters.AddWithValue("@FECHA_DE_PROCESO", FECHA_DE_PROCESO);
                    cmd.Parameters.AddWithValue("@FECHA_DE_ABONO", FECHA_DE_ABONO);
                    cmd.Parameters.AddWithValue("@ESTADO", ESTADO);
                    cmd.Parameters.AddWithValue("@IMPORTE", IMPORTE);
                    cmd.Parameters.AddWithValue("@COMISION", COMISION);
                    cmd.Parameters.AddWithValue("@IGV", IGV);
                    cmd.Parameters.AddWithValue("@IMPORTE_NETO", IMPORTE_NETO);
                    cmd.Parameters.AddWithValue("@ABONO_DEL_LOTE", ABONO_DEL_LOTE);
                    cmd.Parameters.AddWithValue("@NUM_DE_LOTE", NUM_DE_LOTE);
                    cmd.Parameters.AddWithValue("@TERMINAL", TERMINAL);
                    cmd.Parameters.AddWithValue("@NUM_DE_REF_VOUCHER", NUM_DE_REF_VOUCHER);
                    cmd.Parameters.AddWithValue("@MARCA_DE_TARJETA", MARCA_DE_TARJETA);
                    cmd.Parameters.AddWithValue("@NUM_DE_TARJETA", NUM_DE_TARJETA);
                    cmd.Parameters.AddWithValue("@CODIGO_DE_AUTORIZACION", CODIGO_DE_AUTORIZACION);
                    cmd.Parameters.AddWithValue("@CUOTAS", CUOTAS);
                    cmd.Parameters.AddWithValue("@OBSERVACIONES", OBSERVACIONES);
                    cmd.Parameters.AddWithValue("@MONEDA", MONEDA);
                    cmd.Parameters.AddWithValue("@SERIE_TERMINAL", SERIE_TERMINAL);
                    cmd.Parameters.AddWithValue("@USUARIO", USUARIO);
   



                

                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Extornos_Izipay";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }



        public string GUARDAR_REGISTROS_DOCUMENTOS_NIUBIZ(
            string Tipo,int IdDocumento,int IdDoc,string RUC,string RAZON_SOCIAL,
             string COD_COMERCIO,
             string NOMBRE_COMERCIAL,
             string FECHA_HORA_OPERACION,
             string FECHA_DEPOSITO,
             string PRODUCTO,
             string TIPO_OPERACION,
             string TARJETA,
             string ORIGEN_TARJETA,
             string TIPO_TARJETA,
             string MARCA_TARJETA,
             string MONEDA,
             decimal IMPORTE_OPERACION,
             string ES_DCC,
             decimal MONTO_DCC,
             decimal COMISION_TOTAL,
             decimal COMISION_NIUBIZ,
             decimal IGV,
             decimal SUMA_DEPOSITADA,
             string ESTADO,
             string ID_OPERACION,
             string CUENTA_BANCO_PAGADOR,
             string BANCO_PAGADOR,
            int USUARIO)
        {
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                    cmd.Parameters.AddWithValue("@IdDoc", IdDoc);
                    cmd.Parameters.AddWithValue("@RUC", RUC);
                    cmd.Parameters.AddWithValue("@RAZON_SOCIAL", RAZON_SOCIAL);
                    cmd.Parameters.AddWithValue("@COD_COMERCIO", COD_COMERCIO);
                    cmd.Parameters.AddWithValue("@NOMBRE_COMERCIAL", NOMBRE_COMERCIAL);
                    cmd.Parameters.AddWithValue("@FECHA_HORA_OPERACION", FECHA_HORA_OPERACION);
                    cmd.Parameters.AddWithValue("@FECHA_DEPOSITO", FECHA_DEPOSITO);
                    cmd.Parameters.AddWithValue("@PRODUCTO", PRODUCTO);
                    cmd.Parameters.AddWithValue("@TIPO_OPERACION", TIPO_OPERACION);
                    cmd.Parameters.AddWithValue("@TARJETA", TARJETA);
                    cmd.Parameters.AddWithValue("@ORIGEN_TARJETA", ORIGEN_TARJETA);
                    cmd.Parameters.AddWithValue("@TIPO_TARJETA", TIPO_TARJETA);
                    cmd.Parameters.AddWithValue("@MARCA_TARJETA", MARCA_TARJETA);
                    cmd.Parameters.AddWithValue("@MONEDA", MONEDA);
                    cmd.Parameters.AddWithValue("@IMPORTE_OPERACION", IMPORTE_OPERACION);
                    cmd.Parameters.AddWithValue("@ES_DCC", ES_DCC);
                    cmd.Parameters.AddWithValue("@MONTO_DCC", MONTO_DCC);
                    cmd.Parameters.AddWithValue("@COMISION_TOTAL", COMISION_TOTAL);
                    cmd.Parameters.AddWithValue("@COMISION_NIUBIZ", COMISION_NIUBIZ);
                    cmd.Parameters.AddWithValue("@IGV", IGV);
                    cmd.Parameters.AddWithValue("@SUMA_DEPOSITADA", SUMA_DEPOSITADA);
                    cmd.Parameters.AddWithValue("@ESTADO", ESTADO);
                    cmd.Parameters.AddWithValue("@ID_OPERACION", ID_OPERACION);
                    cmd.Parameters.AddWithValue("@CUENTA_BANCO_PAGADOR", CUENTA_BANCO_PAGADOR);
                    cmd.Parameters.AddWithValue("@BANCO_PAGADOR", BANCO_PAGADOR);
                    cmd.Parameters.AddWithValue("@USUARIO", USUARIO);







                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Extornos_NIUBIZ";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }



        public string GUARDAR_REGISTROS_DOCUMENTOS_AMEX(string Tipo, int IdDocumento, int IdDoc, string Establecimiento, string Fecha, string Tarjeta, string CODIGO, string Moneda, Decimal Monto_Venta,
        Decimal Comision_Emisior, Decimal Comision_Mercant, Decimal IGV_Comision_Mercant, Decimal Monto_Depositar, string Fecha_Deposito, string Estado, int USUARIO)
        {
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                    cmd.Parameters.AddWithValue("@IdDoc", IdDoc);
                    cmd.Parameters.AddWithValue("@Codigo_Establecimiento", Establecimiento);
                    cmd.Parameters.AddWithValue("@Fecha_Hora_Venta", Fecha);
                    cmd.Parameters.AddWithValue("@Tarjeta", Tarjeta);
                    cmd.Parameters.AddWithValue("@Codigo_Autorizacion", CODIGO);
                    cmd.Parameters.AddWithValue("@Moneda", Moneda);
                    cmd.Parameters.AddWithValue("@Monto_Venta", Monto_Venta);
                    cmd.Parameters.AddWithValue("@Comision_Emisor", Comision_Emisior);
                    cmd.Parameters.AddWithValue("@Comision_Merchant", Comision_Mercant);
                    cmd.Parameters.AddWithValue("@IGV_Comision_Merchant", IGV_Comision_Mercant);
                    cmd.Parameters.AddWithValue("@Monto_Depositar", Monto_Depositar);
                    cmd.Parameters.AddWithValue("@Fecha_Deposito", Fecha_Deposito);
                    cmd.Parameters.AddWithValue("@Estado", Estado);
                    cmd.Parameters.AddWithValue("@Usuario", USUARIO);

                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Extornos_Amex";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }


        public string GUARDAR_REGISTROS_DOCUMENTOS_DINERS(string Tipo, int IdDocumento, int IdDoc, string Red, string Codigo, string Nombre, DateTime Fecha, string Moneda, string Recap, string Ticket, string Tarjeta, DateTime Fecha_Consumo, decimal Consumo, decimal Consumo_Comision, decimal Manejo, decimal Comision_Manejo, decimal IGV, decimal Pago_Consumo, string Estado, string Documento_Autorizado
       , DateTime Fecha_Pago, string Banco, string Cuenta, string Numero_Autorizacion, int USUARIO)
        {

       
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento);
                    cmd.Parameters.AddWithValue("@IdDoc", IdDoc);
                    cmd.Parameters.AddWithValue("@Red", Red);
                    cmd.Parameters.AddWithValue("@Codigo", Codigo);
                    cmd.Parameters.AddWithValue("@Nombre", Nombre);
                    cmd.Parameters.AddWithValue("@Fecha", Fecha);
                    cmd.Parameters.AddWithValue("@Moneda", Moneda);
                    cmd.Parameters.AddWithValue("@Recap", Recap);
                    cmd.Parameters.AddWithValue("@Ticket", Ticket);
                    cmd.Parameters.AddWithValue("@Tarjeta", Tarjeta);
                    cmd.Parameters.AddWithValue("@Fecha_Consumo", Fecha_Consumo);
                    cmd.Parameters.AddWithValue("@Consumo", Consumo);
                    cmd.Parameters.AddWithValue("@Consumo_Comision", Consumo_Comision);
                    cmd.Parameters.AddWithValue("@Manejo", Manejo);
                    cmd.Parameters.AddWithValue("@Comision_Manejo", Comision_Manejo);
                    cmd.Parameters.AddWithValue("@IGV", IGV);
                    cmd.Parameters.AddWithValue("@Pago_Consumo", Pago_Consumo);
                    cmd.Parameters.AddWithValue("@Estado", Estado);
                    cmd.Parameters.AddWithValue("@Documento_Autorizado", Documento_Autorizado);
                    cmd.Parameters.AddWithValue("@Fecha_Pago", Fecha_Pago);
                    cmd.Parameters.AddWithValue("@Banco", Banco);
                    cmd.Parameters.AddWithValue("@Cuenta", Cuenta);
                    cmd.Parameters.AddWithValue("@Numero_Autorizacion", Numero_Autorizacion);
                    cmd.Parameters.AddWithValue("@Usuario", USUARIO);

                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Extornos_Diners";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }



        public List<BE_Documento> LISTAR_DOCUMENTOS_INGRESADOS_FECHAS(int Tienda,string fechainicial,string fechafinal)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_REPORTE_DOCUMENTOS_INGRESADOS", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Tienda", Tienda);
                    cmd.Parameters.AddWithValue("@Fecha_Inicial", fechainicial);
                    cmd.Parameters.AddWithValue("@Fecha_Final", fechafinal);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {

                        BE_Documento obj_BE_Documento = new BE_Documento();

                        obj_BE_Documento.TipoDocumento = lector[0].ToString();
                        obj_BE_Documento.NroDocumento = lector[1].ToString();
                        obj_BE_Documento.doc_ImporteTotal = Convert.ToDecimal(lector[2].ToString());
                        obj_BE_Documento.FechaEmision = lector[3].ToString();
                        obj_BE_Documento.Tienda = lector[4].ToString();
                        obj_BE_Documento.doc_Total = Convert.ToDecimal(lector[5].ToString());
                        obj_BE_Documento.doc_FechaRegistro = lector[6].ToString();
                        obj_BE_Documento.NOMBRE = lector[7].ToString();
                        obj_BE_Documento.EstadoDoc = lector[8].ToString();
                        obj_BE_Documento.Semaforo = lector[9].ToString();
                        obj_BE_Documento.Almacen = lector[10].ToString();

                        lista.Add(obj_BE_Documento);

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Documento> OBTENER_TIPO_BANCO()
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_Muestra_Bancos", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Documento obj_BE_Documento = new BE_Documento();
                        obj_BE_Documento.IdSerie = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.doc_Serie = lector[1].ToString();
                        lista.Add(obj_BE_Documento); 
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        
        public List<BE_Reporte> LISTAR_DOCUMENTOS_AMEX(int Tipo_Banco, int Tipo_Documento, string fechainicial, string fechafinal, string serie, string Numero)
        {
            List<BE_Reporte> lista = new List<BE_Reporte>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_Muestra_Bancos", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Reporte obj_BE_Documento = new BE_Reporte();
                        obj_BE_Documento.Campo1 = lector[0].ToString();
                        obj_BE_Documento.Campo2 = lector[1].ToString();
                        lista.Add(obj_BE_Documento);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public string GUARDAR_REGISTROS_SOLICITUDDOCUMENTOS(string Tipo, string doc_serie, string doc_codigo, decimal Monto, string Ruta, int IdUsuario,string Observacion)
        {
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {


                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@doc_serie", doc_serie);
                    cmd.Parameters.AddWithValue("@Doc_codigo", doc_codigo);
                    cmd.Parameters.AddWithValue("@Monto", Monto);
                    cmd.Parameters.AddWithValue("@Ruta", Ruta);
                    cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
                    cmd.Parameters.AddWithValue("@Observacion", Observacion);

                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Solicitud_Extornos";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }

        public List<BE_Documento> LISTAR_SOLICITUD_DOCUMENTOS(string Tipo,int Tienda,int IdUsuario)
        {
            List<BE_Documento> lista = new List<BE_Documento>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("UP_Muestra_Solicitud_Documentos_Ingresados", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@Tienda", Tienda);
                    cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {

                        BE_Documento obj_BE_Documento = new BE_Documento();

                        obj_BE_Documento.IdDocumento = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Documento.TipoDocumento = lector[1].ToString();
                        obj_BE_Documento.NroDocumento = lector[2].ToString();
                        obj_BE_Documento.FechaEmision = lector[3].ToString();
                        obj_BE_Documento.doc_ImporteTotal = Convert.ToDecimal(lector[4].ToString());
                        obj_BE_Documento.Tienda = lector[5].ToString();
                        obj_BE_Documento.Cliente = lector[6].ToString();
                        obj_BE_Documento.doc_Total = Convert.ToDecimal(lector[7].ToString());
                        obj_BE_Documento.doc_FechaRegistro = lector[8].ToString();
                        obj_BE_Documento.Observacion = lector[9].ToString();
                        obj_BE_Documento.Semaforo = lector[10].ToString();
                        obj_BE_Documento.EstadoDoc = lector[11].ToString();
                        obj_BE_Documento.NOMBRE = lector[12].ToString();
                        obj_BE_Documento.TipoOperacion = lector[13].ToString();

                        lista.Add(obj_BE_Documento);

                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public string APROBAR_REGISTROS_SOLICITUDDOCUMENTOS(string Tipo, int iddocumento, int IdUsuario,string Ruta)
        {
            string Mensaje = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                using (SqlCommand cmd = new SqlCommand())
                {


                    cmd.Parameters.AddWithValue("@Tipo", Tipo);
                    cmd.Parameters.AddWithValue("@iddocumento", iddocumento);
                    cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
                    cmd.Parameters.AddWithValue("@Ruta", Ruta); 

                    cmd.Connection = cn;
                    cmd.CommandText = "UP_Documento_Solicitud_Extornos_Autorizar";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cn.Open();
                    using (SqlDataReader response = cmd.ExecuteReader())
                    {
                        if (response.HasRows)
                        {
                            while (response.Read())
                            {

                                Mensaje = response["Error"].ToString();
                            }
                        }
                    }
                }
            }
            return Mensaje;
        }


    }
}