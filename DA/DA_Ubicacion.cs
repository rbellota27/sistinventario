﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using BE;

namespace DA
{
    public class DA_Ubicacion
    {

        string conexion = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;

        public List<BE_Ubicacion> OBTENER_ZONA()
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_ZONA", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdZona = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionZona = lector[1].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Ubicacion> OBTENER_USUARIOS()
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_USUARIOS", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdUsuario = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionUsuario = lector[1].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Ubicacion> OBTENER_ALMACEN()
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_ALMACEN", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdAlmacen = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionAlmacen = lector[1].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Ubicacion> OBTENER_LINEA()
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_LINEA", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdLinea = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionLinea = lector[1].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Ubicacion> OBTENER_SUBLINEA(int IDLINEA)
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_SUBLINEA", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDLINEA", IDLINEA);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdSublinea = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionSublinea = lector[2].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Ubicacion> LISTAR_UBICACION(int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA, int ESTADO)
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_UBICACION", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDALMACEN", IDALMACEN);
                    cmd.Parameters.AddWithValue("@DESCRIPCION", NOMBREUBICACION);
                    cmd.Parameters.AddWithValue("@IDLINEA", IDLINEA);
                    cmd.Parameters.AddWithValue("@IDSUBLINEA", IDSUBLINEA);
                    cmd.Parameters.AddWithValue("@IDZONA", IDZONA);
                    cmd.Parameters.AddWithValue("@ESTADO", ESTADO);
                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdUbicacion = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionUbicacion = lector[1].ToString();
                        obj_BE_Ubicacion.DescripcionAlmacen = lector[2].ToString();
                        obj_BE_Ubicacion.DescripcionLinea = lector[3].ToString();
                        obj_BE_Ubicacion.DescripcionSublinea = lector[4].ToString();
                        obj_BE_Ubicacion.DescripcionZona = lector[5].ToString();
                        obj_BE_Ubicacion.DescripcionEstado = lector[6].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }



        public List<BE_Ubicacion> LISTAR_ZONA_USUARIO(int IDUSUARIO, string IDZONA, int ESTADO)
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_LISTAR_ZONA_USUARIO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
                    cmd.Parameters.AddWithValue("@IDZONA", IDZONA);
                    cmd.Parameters.AddWithValue("@ESTADO", ESTADO);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdZona_Usuario = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.DescripcionZona = lector[1].ToString();
                        obj_BE_Ubicacion.DescripcionUsuario = lector[2].ToString();
                        obj_BE_Ubicacion.DescripcionEstado = lector[3].ToString();
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }


        public List<BE_Ubicacion> OBTENER_UBICACION(int ID_UBICACION)
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_UBICACION", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID_UBICACION", ID_UBICACION);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdUbicacion = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.IdAlmacen = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Ubicacion.DescripcionUbicacion = lector[5].ToString();
                        obj_BE_Ubicacion.IdLinea = Convert.ToInt32(lector[6].ToString());
                        obj_BE_Ubicacion.IdSublinea = Convert.ToInt32(lector[7].ToString());
                        obj_BE_Ubicacion.IdZona = Convert.ToInt32(lector[8].ToString());
                        obj_BE_Ubicacion.Estado = Convert.ToInt32(lector[9].ToString());
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public List<BE_Ubicacion> OBTENER_ZONA_USUARIO(int ID_ZONA_USUARIO)
        {
            List<BE_Ubicacion> lista = new List<BE_Ubicacion>();
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SP_OBTENER_ZONA_USUARIO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID_ZONA_USUARIO", ID_ZONA_USUARIO);

                    SqlDataReader lector = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (lector.Read())
                    {
                        BE_Ubicacion obj_BE_Ubicacion = new BE_Ubicacion();
                        obj_BE_Ubicacion.IdZona_Usuario = Convert.ToInt32(lector[0].ToString());
                        obj_BE_Ubicacion.IdZona = Convert.ToInt32(lector[1].ToString());
                        obj_BE_Ubicacion.IdUsuario = Convert.ToInt32(lector[2].ToString());
                        obj_BE_Ubicacion.Estado = Convert.ToInt32(lector[3].ToString());
                        lista.Add(obj_BE_Ubicacion);
                    }

                    lector.Close();
                }
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return lista;
        }

        public int ELIMINAR_ZONA_USUARIO(int ID_ZONA_USUARIO)
        {
            int val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_ELIMINAR_ZONA_USUARIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_ZONA_USUARIO", ID_ZONA_USUARIO);


                val = cmd.ExecuteNonQuery();
                cmd.Connection.Close();


            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }

        public int ELIMINAR_UBICACION(int ID_UBICACION)
        {
            int val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_ELIMINAR_UBICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_UBICACION", ID_UBICACION);


                val = cmd.ExecuteNonQuery();
                cmd.Connection.Close();


            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public int GUARDAR_UBICACION(int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA)
        {
            int val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_INSERTAR_UBICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDALMACEN", IDALMACEN);
                cmd.Parameters.AddWithValue("@NOMBRE_UBICACION", NOMBREUBICACION);
                cmd.Parameters.AddWithValue("@IDLINEA", IDLINEA);
                cmd.Parameters.AddWithValue("@IDSUBLINEA", IDSUBLINEA);
                cmd.Parameters.AddWithValue("@IDZONA", IDZONA);

                val = cmd.ExecuteNonQuery();
                cmd.Connection.Close();


            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public int ACTUALIZAR_UBICACION(int IDUBICACION, int IDALMACEN, string NOMBREUBICACION, int IDLINEA, int IDSUBLINEA, int IDZONA, int ESTADO)
        {
            int val = 0;
            try
            {

                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_ACTUALIZAR_UBICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDUBICACION", IDUBICACION);
                cmd.Parameters.AddWithValue("@IDALMACEN", IDALMACEN);
                cmd.Parameters.AddWithValue("@NOMBRE_UBICACION", NOMBREUBICACION);
                cmd.Parameters.AddWithValue("@IDLINEA", IDLINEA);
                cmd.Parameters.AddWithValue("@IDSUBLINEA", IDSUBLINEA);
                cmd.Parameters.AddWithValue("@IDZONA", IDZONA);
                cmd.Parameters.AddWithValue("@ESTADO", ESTADO);
                val = cmd.ExecuteNonQuery();
                cmd.Connection.Close();


            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public int GUARDAR_ZONA_USUARIO(int IDUSUARIO, int IDZONA)
        {
            int val = 0;
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_INSERTAR_ZONA_USUARIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
                cmd.Parameters.AddWithValue("@IDZONA", IDZONA);
                val = cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }
        public int ACTUALIZAR_ZONA_USUARIO(int ID_ZONA_USUARIO, int IDUSUARIO, int IDZONA, int IDESTADO)
        {
            int val = 0;
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_ACTUALIZAR_ZONA_USUARIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_ZONA_USUARIO", ID_ZONA_USUARIO);
                cmd.Parameters.AddWithValue("@IDUSUARIO", IDUSUARIO);
                cmd.Parameters.AddWithValue("@IDZONA", IDZONA);
                cmd.Parameters.AddWithValue("@IDESTADO", IDESTADO);
                val = cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                StackTrace st = new StackTrace(ex, true);
                StackFrame frame = st.GetFrames().Where(f => !String.IsNullOrEmpty(f.GetFileName())
                     && f.GetILOffset() != StackFrame.OFFSET_UNKNOWN
                     && f.GetNativeOffset() != StackFrame.OFFSET_UNKNOWN
                     && !f.GetMethod().Module.Assembly.GetName().Name.Contains("mscorlib")).First();

                string MachineName = System.Environment.MachineName;
                string UserName = System.Environment.UserName.ToUpper();
                string Mensaje = ex.Message;
                int LineaError = frame.GetFileLineNumber();
                string Proyecto = frame.GetMethod().Module.Assembly.GetName().Name;
                string Clase = frame.GetMethod().DeclaringType.Name;
                string metodo = frame.GetMethod().Name;
                string codigoError = Convert.ToString(frame.GetHashCode());
            }
            return val;
        }

        


    }
}
