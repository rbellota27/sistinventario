﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DA
{
    public class DA_Impresion
    {
        string conexion = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        public DataSet rptDocGuias(int iddocumento)
        {
       
            SqlDataAdapter da, da2;
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("_CR_DocGuiaRemisionRemitenteCab", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDocumento", iddocumento);

                // *********** lleno cabecera
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, "DT_DocCaBecera");

                // detalle
                cmd.CommandText = "_CR_DocGuiaRemisionRemitenteDet";
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, "DT_DocDetalle");

                SqlCommand cmd2 = new SqlCommand("List_ImagenReporte", cn);
                cmd2.CommandType = CommandType.StoredProcedure;
                da2 = new SqlDataAdapter(cmd2);
                da2.Fill(ds, "DtRptImagen");
            }
            catch (Exception ex)
            {
                ds = null;
            }
           
            return ds;
        }


        public DataSet rptGuiasxTonos(int iddocumento)
        {
            SqlDataAdapter da, da2;
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("_CR_DocGuiaRemisionRemitenteCab", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDocumento", iddocumento);

                // *********** lleno cabecera
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, "DT_DocCaBecera");

                // detalle
                cmd.CommandText = "_CR_DocGuiaRemisionRemitenteDet_v2";
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, "DT_DocDetalle");

                SqlCommand cmd2 = new SqlCommand("List_ImagenReporte", cn);
                cmd2.CommandType = CommandType.StoredProcedure;
                da2 = new SqlDataAdapter(cmd2);
                da2.Fill(ds, "DtRptImagen");
            }
            catch (Exception ex)
            {
                ds = null;
            }
          
            return ds;
        }

    }
}