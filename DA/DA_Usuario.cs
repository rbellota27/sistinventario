﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DA
{
    public class DA_Usuario
    {
        string conexion = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        public int ValidarIdentidad(string Login, string Clave)
        {
            int retorno;
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();
            SqlCommand cmd = new SqlCommand("_UsuarioValidarIdentidad", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@us_Login", Login);
            cmd.Parameters.AddWithValue("@us_Clave", Clave);
            cmd.Parameters.AddWithValue("@retorno", 0).Direction = ParameterDirection.Output;
          
            using (cn)
            {              
                cmd.ExecuteNonQuery();
                retorno = System.Convert.ToInt32(cmd.Parameters["@retorno"].Value);
                cn.Close();
                return retorno;

            }
        }

        public int ValidarIdentidadPerfil(string Login, string Clave)
        {
            int retorno;
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();
            SqlCommand cmd = new SqlCommand("_UsuarioValidarIdentidadPerfil", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@us_Login", Login);
            cmd.Parameters.AddWithValue("@us_Clave", Clave);
            cmd.Parameters.AddWithValue("@retorno", 0).Direction = ParameterDirection.Output;
            
         

            using (cn)
            {              
                cmd.ExecuteNonQuery();
                retorno = System.Convert.ToInt32(cmd.Parameters["@retorno"].Value);
                cn.Close();
                return retorno;
            }
        }

        public string ValidarTiendaxUsuario( int idUsuario)
        {
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();

            string cadenaIds = "";
            using (SqlCommand cmd = new SqlCommand("SP_SELECT_TIENDA_X_USUARIO", cn))
            {
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("IdUsuario", DbType.Int32)).Value = idUsuario;
                }

                try
                {
                    cadenaIds = System.Convert.ToString(cmd.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return cadenaIds;
            }
        }


        public int SelectIdCajaxIdPersona(int IdPersona)
        {
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();
            SqlCommand cmd = new SqlCommand("_CajaSelectIdCajaxIdPersona", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdPersona", IdPersona);
            SqlDataReader lector;
            try
            {
                using (cn)
                {                  
                    lector = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                    lector.Read();
                    if (lector.HasRows == false)
                        return System.Convert.ToInt32(0);
                    int IdCaja = !lector.IsDBNull(0) ? Convert.ToInt32(lector[0].ToString()) : 0;
                 
                    return IdCaja;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public decimal ParametroGeneralSelectActivoValorxIdParametro(int Idparametro)
        {
            SqlConnection cn = new SqlConnection(conexion);
            cn.Open();
            SqlCommand cmd = new SqlCommand("_ParametroGeneralSelectActivoValorxIdParametro", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdParametro", Idparametro);

            return System.Convert.ToDecimal(cmd.ExecuteScalar());
        }


    }
}