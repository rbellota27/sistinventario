﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Documento_Referencia
    {
        public string FechaEmision { get; set; }
        public string FechaInit { get; set; }
        public string documento { get; set; }
        public int IdDocumento { get; set; }
        public string doc_Codigo { get; set; }
        public string doc_Serie { get; set; }
        public int empresa { get; set; }
        public int tipoOperacion { get; set; }
        public string tipoop { get; set; }
        public int motivoTrans { get; set; }
        public string motivo { get; set; }
        public int idpersona { get; set; }
        public int idTienda { get; set; }
        public string tienda { get; set; }
        public int Idalmacen { get; set; }
        public string almacen { get; set; }
        public int IdEstado { get; set; }
        public string estado { get; set; }
        public string rsocial { get; set; }
        public int IdCliente { get; set; }
        public string ruc { get; set; }
        public string dni { get; set; }
        public string pdireccion { get; set; }
        public int idDepartamentoP { get; set; }
        public string pdepartamento {get;set;}
        public int idProvinciaP { get; set; }
        public string pprovincia { get; set; }
        public int idDistritoP { get; set; }
        public string pdistrito { get; set; }
        public int idTipoAlmacen { get;set; }
        public string TipoAlmacen { get; set; }
        public string dnombres { get; set; }
        public int dcodigo { get; set; }       
        public string ddni { get; set; }
        public string druc { get; set; }
        public string lldireccion { get; set; }
        public string idDepartamentoL { get; set; }       
        public string idProvinciaL { get; set; }      
        public string idDistritoL { get; set; }       
        public int idAlm { get; set; }

               

    }
}