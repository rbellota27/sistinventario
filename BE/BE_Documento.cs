﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Documento
    {
        public int IdDocumento { get; set; }
        public int IdDetalleDocumento { get; set; }
        public string doc_Codigo { get; set; }
        public string doc_Serie { get; set; }
        public string doc_FechaEmision { get; set; }
        public string doc_FechaIniTraslado { get; set; }
        public string doc_FechaRegistro { get; set; }
        public string doc_FechaAentregar { get; set; }
        public string doc_FechaEntrega { get; set; }
        public string doc_FechaVenc { get; set; }
        public Nullable<decimal> doc_ImporteTotal { get; set; }
        public Nullable<decimal> doc_Descuento { get; set; }
        public Nullable<decimal> doc_SubTotal { get; set; }
        public Nullable<decimal> doc_Igv { get; set; }
        public Nullable<decimal> doc_Total { get; set; }
        public string doc_TotalLetras { get; set; }
        public Nullable<decimal> doc_TotalAPagar { get; set; }
        public Nullable<decimal> doc_ValorReferencial { get; set; }
        public Nullable<decimal> doc_Utilidad { get; set; }
        public Nullable<int> IdPersona { get; set; }
        public Nullable<int> IdUsuario { get; set; }
        public Nullable<int> IdTransportista { get; set; }
        public Nullable<int> IdRemitente { get; set; }
        public Nullable<int> IdDestinatario { get; set; }
        public Nullable<int> IdEstadoDoc { get; set; }
        public Nullable<int> IdCondicionPago { get; set; }
        public Nullable<int> IdMoneda { get; set; }
        public Nullable<int> LugarEntrega { get; set; }
        public Nullable<int> IdTipoOperacion { get; set; }
        public Nullable<int> IdTienda { get; set; }
        public Nullable<int> IdSerie { get; set; }
        public string doc_ExportadoConta { get; set; }
        public string CantidadxAtenderText { get; set; }
        public string doc_NroVoucherConta { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public Nullable<int> IdChofer { get; set; }
        public Nullable<int> IdMotivoT { get; set; }
        public Nullable<int> IdTipoDocumento { get; set; }
        public Nullable<int> IdVehiculo { get; set; }
        public Nullable<int> IdEstadoCan { get; set; }
        public Nullable<int> IdEstadoEnt { get; set; }
        public Nullable<int> IdTipoPV { get; set; }
        public Nullable<int> IdCaja { get; set; }
        public Nullable<int> IdAlmacen { get; set; }
        public Nullable<int> IdMedioPagoCredito { get; set; }
        public Nullable<bool> doc_CompPercepcion { get; set; }
        public string doc_FechaCancelacion { get; set; }
        public Nullable<int> IdUsuarioComision { get; set; }
        public string DB_NAME { get; set; }
        public Nullable<int> flagSustento { get; set; }
        public string CodigoPase { get; set; }
        public Nullable<int> IdPase { get; set; }
        public Nullable<System.DateTime> fecInicioDespacho { get; set; }
        public Nullable<System.DateTime> fecfinDespacho { get; set; }
        public Nullable<int> IdProgramado { get; set; }
        public Nullable<decimal> pesoItemsDocumento { get; set; }
        public System.Guid rowguid { get; set; }
        public string pathFacElectronica { get; set; }
        public string pathFacElectronicaPdf { get; set; }

        public string NroDocumento { get; set; }
      
        public string DocumentoRelacion { get; set; }

        public string FecIni { get; set; }
        public string FecFin { get; set; }
        public string Almacen { get; set; }
        public string TipoOperacion { get; set; }
        public string TipoDocumento { get; set; }
        public string EstadoEnt { get; set; }
        public string EstadoDoc { get; set; }

        public string FechaEmision { get; set; }

        public string CodigoProducto { get; set; }
        public string NombreProducto { get; set; }
        public string UM { get; set; }
        public int IdUnidadMedida { get; set; }
        public int IdDetalleAfecto { get; set; }
        public int IdDocumentoRef { get; set; }
        public string Cantidad { get; set; }
        public string Tienda { get; set; }
        public string Tienda_Pedido { get; set; }
        public string Observacion { get; set; }
        public string Tono { get; set; }

        public string Pendiente { get; set; }


        public string StockReal { get; set; }
        public string StockDisponible { get; set; }
        public string StockComprometido { get; set; }

        //usuario
        public int ID { get; set; }
        public string NOMBRE { get; set; }
        public string USUARIO { get; set; }
        public string PERFIL { get; set; }
        public string IDTIENDA { get; set; }
        public string TIENDA { get; set; }
        public int IDEMPRESA { get; set; }
        public int IDALMACENPRINCIPAL { get; set; }


        public int IdProducto { get; set; }
        public string prod_Codigo { get; set; }
        public string prod_Nombre { get; set; }

        public string Zona { get; set; }
        public string Linea { get; set; }
        public string SubLinea { get; set; }

        public string cantidad_final { get; set; }

        public string cantidad_x_atender { get; set; }
        public decimal peso_producto { get; set; }
        public string cantidad_transito { get; set; }
        public string estado { get; set; }
        public string idtono { get; set; }
        public string nomtono { get; set; }
        public string cantidadtono { get; set; }


        public int idtipo_existencia { get; set; }
        public string existencia_nombre { get; set; }

        public int idlinea { get; set; }
        public int PoseeOrdenDespacho { get; set; }

        public int PoseeAmortizaciones { get; set; }
        public int PoseecomPersepcion { get; set; }
        public int PoseeGRecepcion { get; set; }
        public bool kit { get; set; }
        public string nroDocumento { get; set; }
        public string doc_referencia { get; set; }
        public string Tie_Ref { get; set; }
        public string obsRef { get; set; }
        public string UsuPick { get; set; }

        public string Cliente { get; set; }
        public string Ruta { get; set; }
        public string Semaforo { get; set; }
        public int IdDoc { get; set; }
    }
}