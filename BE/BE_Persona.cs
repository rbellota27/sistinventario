﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Persona
    {

        public int IdPersona { get; set; }
        public string per_NComercial { get; set; }
        public string jur_Rsocial  { get; set; }
        public string Nombre  { get; set; }
        public string Dni { get; set; }
        public string Ruc { get; set; }
        public string dir_Direccion { get; set; }
        public string dir_Ubigeo { get; set; }
        public string g_dir_Referencia { get; set; }
        public string  tel_Numero { get; set; }
        public string tel_Numero_Movil { get; set; }
        public string per_estado { get; set; }
        public int IdTipoPV { get; set; }
        public bool Propietario { get; set; }
        public decimal PorcentPercepcion { get; set; }
        public decimal PorcentRetencion { get; set; }
        public string departamento { get; set; }
        public string provincia { get; set; }
        public string distrito { get; set; }

        public string ApPaterno { get; set; }
        public string ApMaterno { get; set; }
        public string Nombres { get; set; }
        public int IdTipoAgente { get; set; }
        public int TipoPersona { get; set; }
        public string Descripcion { get; set; }
        public string Rol { get; set; }
        public int IdRol { get; set; }

        public string login { get; set; }

        public string Correo { get; set; }
        public decimal PorcentTipoAgente { get; set; }
        public string NroLicencia { get; set; }
        public string TipoPV { get; set; }

        public bool SujetoAPercepcion { get; set; }
        public bool SujetoARetencion { get; set; }
        public string FechaNac { get; set; }
        public string EMail { get; set; }
        public string Nacionalidad { get; set; }

        ///almacen
        public int IdAlmacen { get; set; }
        public int IdTipoAlmacen { get; set; }
    }
}