﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Opcion
    {
        public int IdUsuario { get; set; }
        public string IdOpcion { get; set; }
        public string IdMenu { get; set; }
        public string op_Nombre { get; set; }
        public string op_Formulario { get; set; }
        public int op_Estado { get; set; }
        public string op_Orden { get; set; }
        public string DB_NAME { get; set; }
        public string dir_img_menu { get; set; }
        public int flagmodulo { get; set; }        

    }
}