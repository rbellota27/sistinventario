﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Producto_Final
    {
        public string codigo { get; set; }
        public string nroDocumento { get; set; }
        public string descripcion { get; set; }
        public string UM { get; set; }
        public decimal cantidad { get; set; }
        public decimal pendiente { get; set; }
        public decimal peso { get; set; }
        public decimal stockReal { get; set; }
        public decimal stockComprometido { get; set; }
        public decimal stockdisponible { get; set; }
        public decimal equivalencia { get; set; }
        public decimal equivalencia1 { get; set; }
        public int idProducto { get; set; }
        public int idDocumento { get; set; }
        public int idDetalleDoc { get; set; }
        public int idAlmacen { get; set; }
        public int idUnidadMedida { get; set; }
        public string Pend { get; set; }
        public string Tonos { get; set; }    

    }
}