﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Reporte
    {
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Campo3 { get; set; }
        public string Campo4 { get; set; }
        public string Campo5 { get; set; }
        public string Campo6 { get; set; }
        public string Campo7 { get; set; }
        public string Campo8 { get; set; }
        public string Campo9 { get; set; }
        public string Campo10 { get; set; }
        public string Campo11 { get; set; }
        public string Campo12 { get; set; }
        public string Campo13 { get; set; }
        public string Campo14 { get; set; }
        public string Campo15 { get; set; }
        public string Campo16 { get; set; }
        public string Campo17 { get; set; }
        public string Campo18 { get; set; }
        public string Campo19 { get; set; }
        public string Campo20 { get; set; }
        public string Campo21 { get; set; }
        public string Campo22 { get; set; }
        public string Campo23 { get; set; }
        public string Campo24 { get; set; }
        public string Campo25 { get; set; }
        public string Campo26 { get; set; }
        public string Campo27 { get; set; }
        public string Campo28 { get; set; }
        public string Campo29 { get; set; }
        public string Campo30 { get; set; }

        public string Campo31 { get; set; }

        public string Campo32 { get; set; }
        public string Campo33 { get; set; }

        public string Campo34 { get; set; }
        public string Campo35 { get; set; }
        public string Campo36 { get; set; }
        public string Campo37 { get; set; }

        public string Campo38 { get; set; }


        public int idTienda { get; set; }
        public string tie_nombre { get; set; }
        public int tie_estado { get; set; }


        public int idarea { get; set; }
        public string ar_Nombrelargo { get; set; }
        public string ar_NombreCorto { get; set; }
        public int ar_estado { get; set; }
        public string idCentroCosto { get; set; }
        public string vc_usuario { get; set; }
        public string vc_campo_cambio { get; set; }
        public string vc_textoanterior { get; set; }
        public string vc_textonuevo { get; set; }
        public string fecha { get; set; }
        public string vc_tipocpu { get; set; }
        public string vc_tipomemoria { get; set; }
        public string vc_marcadisco { get; set; }
        public string vc_tipo_disco { get; set; }
        public string vc_capacidad_disco { get; set; }

        public string validarmace { get; set; }

        public string validarmacw { get; set; }
    }
}