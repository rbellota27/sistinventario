﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Tienda
    {
        //Tienda 
        public int IdTienda { get; set; }
        public string Descripcion { get; set; }
        //Tipo Documento
        public int IdTipoDocumento { get; set; }
        public string NombreTipoDocumento { get; set; }
        //Serie por Tienda
        public int IdSerie { get; set; }
        public string NombreSerie { get; set; }


    }
}