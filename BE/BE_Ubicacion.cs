﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Ubicacion
    {
        public int IdUbicacion { get; set; }
        public int IdZona { get; set; }
        public string DescripcionZona { get; set; }
        public int IdAlmacen { get; set; }
        public string DescripcionAlmacen { get; set; }
        public int IdLinea { get; set; }
        public string DescripcionLinea { get; set; }
        public int IdSublinea { get; set; }
        public string DescripcionSublinea { get; set; }
        public string DescripcionUbicacion { get; set; }
        public int Estado { get; set; }

        public string DescripcionEstado { get; set; }
        public int IdUsuario { get; set; }
        public string DescripcionUsuario { get; set; }

        public int IdZona_Usuario{ get; set; }
    }
}