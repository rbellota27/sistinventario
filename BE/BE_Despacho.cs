﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BE
{
    public class BE_Despacho
    {
        public int idempresa { get; set; }
        public string nomempresa { get; set; }
        public int idtienda { get; set; }
        public string nomtienda { get; set; }
        public int idserie { get; set; }
        public string numserie { get; set; }

        public int idestado { get; set; }
        public string nomestado { get; set; }

        public int idtipooperacion { get; set; }
        public string nomtipooperacion { get; set; }

        public int idalmacen { get; set; }
        public string nomalmacen { get; set; }
        public int idtipoalmacen { get; set; }
        public string nomtipoalmacen { get; set; }

        public int idtipodocumento { get; set; }
        public string nomtipodocumento { get; set; }

        public int iddocumento { get; set; }
        public string numdoc { get; set; }
        public string fechaemision { get; set; }
        public string fechaentrega { get; set; }
        public string empresa { get; set; }
        public string ruc { get; set; }
        public string dni { get; set; }
        public string almacen { get; set; }
        public string doc_referencia { get; set; }
        public string motivo_traslado { get; set; }
        public string fecha_registro { get; set; }
        public string tipo_operacion { get; set; }
        public Nullable<int> idpersona { get; set; }
        public string nombre_persona { get; set; }
        public string nacionalidad { get; set; }
        public string estado { get; set; }
        public int idrol { get; set; }
        public string nombre_rol { get; set; }
        public string iddepartamento { get; set; }
        public string nomdepartamento { get; set; }
        public string idprovincia { get; set; }
        public string nomprovincia { get; set; }
        public string ubigeo { get; set; }
        public string direccion { get; set; }
        public string iddistrito { get; set; }
        public string nomdistrito { get; set; }
        public string flag_almacen_principal { get; set; }
        public string nomcomercial { get; set; }

        public string razonsocial { get; set; }
        public string propietario { get; set; }

        public string descripcion { get; set; }
        public string nrolicencia { get; set; }
        public string tipo_pv { get; set; }
        public string idtipo_pv { get; set; }
       public string telefono { get; set; }
        public string creo { get; set; }




     public string   FechaIniTraslado    { get; set; }
     public string FechaRegistro         { get; set; }
     public string   FechaAEntregar      { get; set; }
     public string   FechaEntrega        { get; set; }
     public string   FechaVenc           { get; set; }
     public string   Descuento           { get; set; }
     public string   SubTotal         { get; set; }
     public string   IGV          { get; set; }
     public string   Total        { get; set; }
     public string   TotalAPagar       { get; set; }
     public string ValorReferencial { get; set; }
     public string   IdPersona           { get; set; }
     public string   IdUsuario           { get; set; }
     public string IdTransportista { get; set; }
     public string   IdRemitente        { get; set; }
     public string   IdDestinatario      { get; set; }
     public string   IdEstadoDoc        { get; set; }
     public string   IdCondicionPago     { get; set; }
     public string   IdMoneda            { get; set; }
     public string   LugarEntrega        { get; set; }
     public string   IdTipoOperacion     { get; set; }
     public string   IdTienda            { get; set; }
     public string   IdEmpresa           { get; set; }
     public string   IdChofer            { get; set; }
     public string   IdMotivoT           { get; set; }
     public string   IdTipoDocumento     { get; set; }
     public string   IdVehiculo          { get; set; }
     public string   IdEstadoCancelacion { get; set; }
     public string   IdEstadoEntrega     { get; set; }
     public string   FechaCancelacion    { get; set; }
     public string   IdAlmacen           { get; set; }
     public string   NomAlmacen          { get; set; }
     public string   Codigo              { get; set; }
     public string   Serie             { get; set; }
     public string   Percepcion          { get; set; }
     public string   Detraccion          { get; set; }
     public string   Retencion           { get; set; }
     public string   PoseeOrdenDespacho  { get; set; }
     public string PoseeAmortizaciones { get; set; }
                     
     public string   PoseeGRecepcion     { get; set; }
     public string   NomMoneda           { get; set; }
     public string   NomTipoDocumento    { get; set; }
     public string   Empresa             { get; set; }
     public string   Tienda              { get; set; }
     public string   NomEstadoDocumento  { get; set; }
     public string   NroDocumento        { get; set; }
     public string   DescripcionPersona  { get; set; }
     public string IdUsuarioComision { get; set; }
        public int idDocRef { get; set; }

        public string correo { get; set; }
        public int idsublinea { get; set; }
        public string sublinea { get; set; }

        public int idlinea { get; set; }
        public string linea { get; set; }

        public string codigo_producto { get; set; }
        public string descripcion_producto { get; set; }
        public string um_producto { get; set; }
        public string stock_disponible { get; set; }

        public int id_producto { get; set; }
        public int id_um { get; set; }
        public string kit { get; set; }

        public string idkit { get; set; }
        public string idcomponente { get; set; }
        public string cantidad_componente { get; set; }
        public string idumcomponente { get; set; }
        public string componente { get; set; }
        public string codprodcomponente { get; set; }
        public string umcomponente { get; set; }
        public string idmonedacomponente { get; set; }
        public string preciocomponente { get; set; }
        public string monedacomponente { get; set; }
        public string porcentajekit { get; set; }

        public string veh_Placa { get; set; }
        public string IdProducto { get; set; }
        public string veh_Modelo { get; set; }
        public string veh_NConstanciaIns { get; set; }
        public string veh_ConfVeh { get; set; }
        public string veh_Tara { get; set; }
        public string capacidad { get; set; }

        public string serieOP { get; set; }
        public string codigoOP { get; set; }

    }
}